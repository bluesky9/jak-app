import Format from '../helpers/Format';

it('Should format a date', () => {
  const testDate = new Date(Date.UTC(2011, 5, 10));
  const date = Format.date(testDate);

  expect(typeof date).toBe('string');
  expect(date.length).toBe(10);
  expect(date).toBe('09/06/2011');
});

it('Should format time', () => {
  const testTime = new Date();
  testTime.setHours(16, 10, 10);

  const time = Format.time(testTime);
  expect(typeof time).toBe('string');
  expect(time.length).toBe(7);
  expect(time).toBe('4:10 PM');
});
