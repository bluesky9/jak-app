import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  modalStyle: {
    overflow: 'hidden',
  },
  modalContent: {
    flex: 1,
    backgroundColor: '#FFFFFF',
    borderRadius: 5,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  headerView: {
    backgroundColor: '#ED174B',
    alignItems: 'center',
    padding: 20,
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
  },
  headerTitle: {
    fontSize: 16,
    color: 'white',
    fontWeight: '600',
  },
  headerCost: {
    fontSize: 24,
    color: 'white',
    fontWeight: '600',
    marginTop: 20,
    marginBottom: 10,
  },
  headerSubTitle: {
    fontSize: 12,
    color: 'white',
    fontWeight: '600',
  },
  contentContainer: {
    padding: 20,
  },
  timeDateView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingBottom: 20,
    borderBottomWidth: 1,
    borderBottomColor: '#ccc',
  },
  rowStyle: {
    flexDirection: 'row',
  },
  timeDateText: {
    color: '#ED174B',
    marginLeft: 10,
  },
  locationView: {
    flexDirection: 'row',
    marginTop: 10,
    marginBottom: 10,
  },
  mapWrap: {
    width: '90%',
    marginLeft: 10,
  },
  textStyle: {
    fontWeight: '600',
  },
  mapView: {
    width: '100%',
    height: 100,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: '#ccc',
    marginTop: 15,
    marginBottom: 15,
  },
  textStyle1: {
    fontSize: 13,
    color: '#888',
  },
  divider: {
    borderBottomColor: '#ccc',
    borderBottomWidth: 1,
    marginTop: 15,
  },
  buttonView: {
    borderTopColor: '#ccc',
    borderTopWidth: 1,
    paddingTop: 20,
    paddingBottom: 20,
  },
  acceptBtn: {
    backgroundColor: '#ED174B',
    borderColor: '#ED174B',
    borderWidth: 1,
    padding: 10,
    alignItems: 'center',
    borderRadius: 20,
    width: '100%',
    marginBottom: 20,
  },
  acceptBtnText: {
    color: 'white',
  },
  rejectBtn: {
    backgroundColor: 'white',
    borderColor: 'black',
    borderWidth: 1,
    padding: 10,
    alignItems: 'center',
    borderRadius: 20,
    width: '100%',
  },
});
