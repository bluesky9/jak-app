import React, { Component } from 'react';
import {
  FlatList, View, Text, TouchableOpacity,
} from 'react-native';
import { MapView } from 'expo';
import PropTypes from 'prop-types';
import Modal from 'react-native-modal';
import Icon from 'react-native-vector-icons/FontAwesome';
import moment from 'moment';
import styles from './NewRouteAssignmentModal.style';

class NewRouteAssignmentModal extends Component {
  constructor(props) {
    super(props);

    this.renderItem = ({ item: shipment }) => {
      const senderLatitude = shipment.senderGeoPoint && shipment.senderGeoPoint.lat
        ? shipment.senderGeoPoint.lat
        : 37.78825;
      const senderLongitude = shipment.senderGeoPoint && shipment.senderGeoPoint.lgn
        ? shipment.senderGeoPoint.lgn
        : -122.4324;
      const recipientLatitude = shipment.recipientGeoPoint && shipment.recipientGeoPoint.lat
        ? shipment.recipientGeoPoint.lat
        : 37.78825;
      const recipientLongitude = shipment.recipientGeoPoint && shipment.recipientGeoPoint.lgn
        ? shipment.recipientGeoPoint.lgn
        : -122.4324;

      return (
        <View key={shipment.id}>
          <View style={styles.locationView}>
            <Icon name="map-marker" size={15} />
            <View style={styles.mapWrap}>
              <Text style={styles.textStyle}>Pick-up location</Text>
              <MapView
                style={styles.mapView}
                initialRegion={{
                  latitude: senderLatitude,
                  longitude: senderLongitude,
                  latitudeDelta: 0.0122,
                  longitudeDelta: 0.0021,
                }}
              >
                <MapView.Marker
                  coordinate={{
                    latitude: senderLatitude,
                    longitude: senderLongitude,
                  }}
                  title="marker.title"
                  description="desss"
                >
                  <Icon name="map-marker" size={25} color="green" />
                </MapView.Marker>
              </MapView>
              <Text style={styles.textStyle1}>
                {shipment.senderAddress
                  ? shipment.senderAddress
                  : 'No address information available'}
              </Text>
              <View style={styles.divider} />
            </View>
          </View>
          <View style={styles.locationView}>
            <Icon name="map-marker" size={15} />
            <View style={styles.mapWrap}>
              <Text style={styles.textStyle}>Delivery location</Text>
              <MapView
                style={styles.mapView}
                initialRegion={{
                  latitude: recipientLatitude,
                  longitude: recipientLongitude,
                  latitudeDelta: 0.0122,
                  longitudeDelta: 0.0021,
                }}
              >
                <MapView.Marker
                  coordinate={{
                    latitude: recipientLatitude,
                    longitude: recipientLongitude,
                  }}
                  title="marker.title"
                  description="desss"
                >
                  <Icon name="map-marker" size={25} color="red" />
                </MapView.Marker>
              </MapView>
              <Text style={styles.textStyle1}>
                {shipment.recipientAddress
                  ? shipment.recipientAddress
                  : 'No address information available'}
              </Text>
            </View>
          </View>
        </View>
      );
    };

    this.keyExtractor = item => String(item.id);
  }

  render() {
    const { visible, onDismissModal, route } = this.props;

    return (
      <View style={styles.container}>
        <Modal
          isVisible={visible}
          style={styles.modalStyle}
          onBackdropPress={onDismissModal}
        >
          <View style={styles.modalContent}>
            <View style={styles.headerView}>
              <Text style={styles.headerTitle}>NEW ROUTE ASSIGNMENT</Text>
              <Text style={styles.headerCost}>130.50 SAR</Text>
              <Text style={styles.headerSubTitle}>(Cash - Pay at pick-up)</Text>
            </View>
            {route
              && route.shipments && (
                <FlatList
                  style={styles.scrollView}
                  contentContainerStyle={styles.contentContainer}
                  showsVerticalScrollIndicator={false}
                  data={route.shipments}
                  keyExtractor={this.keyExtractor}
                  ListHeaderComponent={() => (
                    <View style={styles.timeDateView}>
                      <View style={styles.rowStyle}>
                        <Icon name="calendar" size={13} color="#ED174B" />
                        <Text style={styles.timeDateText}>
                          {route && route.startDatetime
                            ? moment(route.startDatetime).format('DD/MM/YYYY')
                            : '01/01/2000'}
                        </Text>
                      </View>
                      <View style={styles.rowStyle}>
                        <Icon name="clock-o" size={15} color="#ED174B" />
                        <Text style={styles.timeDateText}>
                          {route && route.startDatetime
                            ? moment(route.startDatetime).format('HH:mm A')
                            : '12:00 AM'}
                        </Text>
                      </View>
                    </View>
                  )}
                  ListFooterComponent={() => (
                    <View style={styles.buttonView}>
                      <TouchableOpacity style={styles.acceptBtn}>
                        <Text style={styles.acceptBtnText}>Accept Route!</Text>
                      </TouchableOpacity>
                      <TouchableOpacity style={styles.rejectBtn}>
                        <Text>Reject</Text>
                      </TouchableOpacity>
                    </View>
                  )}
                  renderItem={this.renderItem}
                />
            )}
          </View>
        </Modal>
      </View>
    );
  }
}

NewRouteAssignmentModal.propTypes = {
  visible: PropTypes.bool,
  onDismissModal: PropTypes.func.isRequired,
  route: PropTypes.shape({}).isRequired,
};

NewRouteAssignmentModal.defaultProps = {
  visible: false,
};

export default NewRouteAssignmentModal;
