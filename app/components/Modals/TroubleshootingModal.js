import React from 'react';
import {
  View, Text, ScrollView, TouchableOpacity, Modal,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import PropTypes from 'prop-types';

import API from '../API/API';
import Picker from '../Picker';
import Loader from '../Loader';
import styles from './TroubleshootingModal.style';

const issueTypes = [
  { label: "Customer didn't answer", value: 'customerDidNotAnswer' },
  { label: 'Car trouble', value: 'CarTrouble' },
];

class TroubleshootingModal extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      selectedIssue: issueTypes[0],
      isLoading: true,
    };

    this.submit = async () => {
      /* TODO: contact operations team */
      const { onSuccess } = this.props;
      onSuccess();
    };
  }

  async componentDidMount() {
    this.API = await API.instantiate();
  }

  render() {
    const { isLoading, selectedIssue } = this.state;
    const { shipment, onClose } = this.props;
    const { trackingId } = shipment;

    return (
      <View style={styles.modalContainer}>
        <Modal onRequestClose={onClose} animationType="slide">
          <View style={styles.modalHeaderContainer}>
            <TouchableOpacity
              style={styles.modalCloseIconContainer}
              onPress={onClose}
            >
              <Icon name="arrow-back" size={25} color="white" />
            </TouchableOpacity>
            <Text style={styles.modalHeaderText}>Troubleshooting</Text>
          </View>
          <ScrollView>
            <View style={styles.container}>
              <Text style={styles.titleText}>Contact Operations Team</Text>
              <Text style={styles.subTitleText}>
                {`Shipment #${trackingId}`}
              </Text>

              <View style={styles.pickerBox}>
                <Text style={styles.issueText}>Select an Issue:</Text>
                <Picker
                  selectedItem={selectedIssue}
                  items={issueTypes}
                  onValueChange={value => this.setState({ selectedIssue: value })
                  }
                />
              </View>

              <TouchableOpacity
                style={styles.confirmButton}
                onPress={this.submit}
              >
                <Text style={styles.buttonText}>Confirm</Text>
              </TouchableOpacity>
            </View>
          </ScrollView>
        </Modal>
        <Loader visible={isLoading} />
      </View>
    );
  }
}

TroubleshootingModal.propTypes = {
  route: PropTypes.shape({}).isRequired,
  shipment: PropTypes.shape({}).isRequired,
  onClose: PropTypes.func.isRequired,
  onSuccess: PropTypes.func.isRequired,
};

export default TroubleshootingModal;
