import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  modalStyle: {
    width: '90%',
  },
  modalContent: {
    backgroundColor: 'white',
    justifyContent: 'center',
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  section: {
    flexDirection: 'row',
    padding: 20,
    paddingBottom: 0,
  },
  section1: {
    flexDirection: 'row',
    borderBottomColor: '#ccc',
    borderBottomWidth: 1,
    paddingLeft: 20,
  },
  headerTitle: {
    backgroundColor: '#250060',
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 20,
    borderTopLeftRadius: 4,
    borderTopRightRadius: 4,
  },
  headerTitleText: {
    fontSize: 20,
    color: 'white',
    fontWeight: '600',
  },
  detailView: {
    width: '100%',
    borderBottomWidth: 1,
    borderBottomColor: '#ccc',
    marginLeft: 10,
    paddingBottom: 10,
  },
  textTitle: {
    fontSize: 14,
    fontWeight: '400',
    color: '#250060',
    marginBottom: 10,
  },
  text: {
    fontSize: 12,
    color: '#333',
    paddingBottom: 10,
    marginRight: 10,
  },
  textStyle1: {
    textAlign: 'center',
    color: '#444',
    fontWeight: '400',
  },
  textStyle2: {
    color: '#250060',
    textAlign: 'center',
    fontSize: 24,
    fontWeight: '600',
    marginTop: 10,
    marginBottom: 10,
  },
  textStyle3: {
    textAlign: 'center',
    color: '#333',
    fontSize: 10,
  },
  textStyle4: {
    color: '#250060',
    marginLeft: 5,
  },
  dateTimeView: {
    flexDirection: 'row',
    marginTop: 20,
    marginBottom: 20,
    width: '70%',
  },
  resultView: {
    alignContent: 'center',
    padding: 20,
  },
});
