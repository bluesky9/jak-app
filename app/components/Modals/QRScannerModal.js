import React from 'react';
import {
  Text, View, Alert, Modal, TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { BarCodeScanner, Permissions } from 'expo';
import PropTypes from 'prop-types';
import T from 'i18n-react';
import API from '../API/API';
import styles from './QRScannerModal.style';
import Loader from '../Loader';

class QRScanner extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      hasCameraPermission: null,
      isLoading: false,
      scanDisabled: false,
    };

    this.handleBarCodeRead = (barCode) => {
      const { onSuccess, shipmentId } = this.props;

      this.setState({
        isLoading: true,
        scanDisabled: true,
      });

      this.API.get('/shipments', {
        params: { filter: { where: { trackingId: barCode.data } } },
      })
        .then((response) => {
          if (
            response.data
            && response.data[0]
            && ((shipmentId && response.data[0].id === shipmentId)
              || shipmentId === null)
          ) {
            this.setState({ isLoading: false });
            onSuccess(response.data[0]);
          } else {
            throw new Error();
          }
        })
        .catch(() => {
          // Actually we should move this to Pickup/Delivery confirmations
          // and redirect to the Dashboard.
          Alert.alert(
            'Shipment & QR code do not match!',
            'Please re-scan the package',
            [
              {
                text: T.translate('defaults.ok'),
                onPress: () => {
                  this.setState({ scanDisabled: false });
                },
              },
            ],
            { cancelable: false },
          );

          this.setState({ isLoading: false });
        });
    };
  }

  async componentDidMount() {
    this.API = await API.instantiate();
    const { status } = await Permissions.askAsync(Permissions.CAMERA);
    this.setState({ hasCameraPermission: status === 'granted' });
  }

  render() {
    const { hasCameraPermission, isLoading, scanDisabled } = this.state;
    const { onClose } = this.props;

    if (hasCameraPermission === null) {
      return (
        <View>
          <Modal
            style={styles.modalStyle}
            onRequestClose={onClose}
            animationType="slide"
          >
            <View style={styles.container}>
              <Text style={styles.text}>Opening the scanner...</Text>
            </View>
          </Modal>
        </View>
      );
    }

    if (hasCameraPermission === false) {
      return (
        <View>
          <Modal
            style={styles.modalStyle}
            onRequestClose={onClose}
            animationType="slide"
          >
            <View style={styles.container}>
              <Text style={styles.text}>No access to camera!</Text>
            </View>
          </Modal>
        </View>
      );
    }

    return (
      <View style={styles.modalContainer}>
        <Modal
          style={styles.modalStyle}
          onRequestClose={onClose}
          animationType="slide"
        >
          <View style={styles.modalHeaderContainer}>
            <TouchableOpacity
              style={styles.modalCloseIconContainer}
              onPress={onClose}
            >
              <Icon name="arrow-back" size={25} color="white" />
            </TouchableOpacity>
            <Text style={styles.modalHeaderText}>Scan Shipment Code</Text>
          </View>
          <View style={styles.barCodeContainer}>
            <View>
              {!scanDisabled && (
                <BarCodeScanner
                  onBarCodeRead={this.handleBarCodeRead}
                  style={styles.barCodeScanner}
                />
              )}
            </View>
          </View>
        </Modal>
        <Loader visible={isLoading} />
      </View>
    );
  }
}

QRScanner.propTypes = PropTypes.shape({
  navigation: PropTypes.object.isRequired,
  onRequestClose: PropTypes.func.isRequired,
  onClose: PropTypes.func.isRequired,
  shipmentId: PropTypes.number,
}).isRequired;

QRScanner.defaultProps = {
  shipmentId: null,
};

export default QRScanner;
