import React from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  ImageBackground,
  Modal,
} from 'react-native';
import PropTypes from 'prop-types'; //
import styles from './ShipmentOrderCompleteModal.style';
import icDoneImg from '../../assets/img/ic_done.png';
import icShapesImg from '../../assets/img/ic_shapes_bg.png';

const ShipmentOrderComplete = (props) => {
  const { onClose, navigate } = props;

  return (
    <View style={styles.modalContainer}>
      <Modal onRequestClose={onClose} animationType="slide">
        <ImageBackground
          source={icShapesImg}
          style={styles.imgBackground}
          resizeMode="center"
        >
          <View style={styles.container}>
            <Image style={styles.icDone} source={icDoneImg} />
            <Text style={styles.wellDoneTxt}>Well Done!</Text>
            <Text style={styles.routeCompleteTxt}>Route is complete</Text>
            <TouchableOpacity style={styles.goToDashboardBtn} onPress={onClose}>
              <Text style={styles.goToDashboardTxt}>Go to Dashboard</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.viewPendingOrdersBtn}
              onPress={() => navigate('RouteOffers')}
            >
              <Text style={styles.viewPendingOrdersTxt}>
                View Available Route Offers
              </Text>
            </TouchableOpacity>
          </View>
        </ImageBackground>
      </Modal>
    </View>
  );
};

ShipmentOrderComplete.propTypes = {
  navigate: PropTypes.func.isRequired,
  onClose: PropTypes.func.isRequired,
};

export default ShipmentOrderComplete;
