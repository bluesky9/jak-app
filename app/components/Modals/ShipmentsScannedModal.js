import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  FlatList,
  Alert,
  Modal,
} from 'react-native';
import T from 'i18n-react';
import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/MaterialIcons';
import moment from 'moment';
import API from '../API/API';
import Loader from '../Loader';
import styles from './ShipmentsScannedModal.style';

class ShipmentsScannedModal extends Component {
  constructor(props) {
    super(props);

    const { route } = this.props;

    this.state = {
      currentShipments: route.shipments,
      currentSelectedTab: -1,
      isLoading: false,
    };

    this.removeItem = (index) => {
      const { currentShipments } = this.state;
      const item = currentShipments[index];

      Alert.alert(
        T.translate('shipmentsScannedModal.confirmAlert.title', {
          shipment: item.trackingId,
        }),
        T.translate('shipmentsScannedModal.confirmAlert.message', {
          shipment: item.trackingId,
        }),
        [
          {
            text: T.translate('defaults.yes'),
            onPress: async () => {
              try {
                this.setState({ isLoading: true });
                await this.API.get(
                  `/shipments/${item.id}/remove-from-directed-bulk-route/${
                    route.id
                  }`,
                );
                currentShipments.splice(index, 1);
                this.setState({
                  currentSelectedTab: -1,
                  currentShipments,
                  isLoading: false,
                });
                if (currentShipments.length === 0) {
                  const { onClose } = this.props;
                  onClose();
                }
              } catch (err) {
                Alert.alert(
                  T.translate('shipmentsScannedModal.errorAlert.title'),
                  T.translate('shipmentsScannedModal.errorAlert.message'),
                  [{ text: T.translate('defaults.ok') }],
                  { cancelable: false },
                );
              }
            },
          },
          { text: T.translate('defaults.no') },
        ],
        { cancelable: true },
      );
    };

    this.renderItem = ({ item, index }) => {
      const { currentSelectedTab } = this.state;

      if (index === currentSelectedTab) {
        return (
          <View
            key={item.trackingId}
            style={styles.activePackageDetailContainer}
          >
            <TouchableOpacity
              style={styles.activePackageDetailTab}
              onPress={() => {
                this.setState({
                  currentSelectedTab: currentSelectedTab === index ? -1 : index,
                });
              }}
            >
              <Text style={styles.inactivePackageDetailTabText}>
                {`#${item.trackingId}`}
              </Text>
              <View style={styles.flexStyle} />
              <View style={styles.packageUpDownIcon}>
                <Icon name="keyboard-arrow-up" size={20} color="black" />
              </View>
            </TouchableOpacity>
            <View style={styles.activePackageDetailSubContainer}>
              <View style={styles.activePackageDetailSubContainerHeader}>
                <View style={styles.activePackageDetailSubContainerIcon}>
                  <Icon name="description" size={15} color="black" />
                </View>
                <Text style={styles.activePackageDetailSubContainerHeaderText}>
                  Description
                </Text>
              </View>
              <View style={styles.marginLeftStyle}>
                <Text style={styles.activePackageDetailSubContainerText}>
                  {item.description}
                </Text>
              </View>
            </View>
            <View style={styles.activePackageDetailSubContainer}>
              <View style={styles.activePackageDetailSubContainerHeader}>
                <View style={styles.activePackageDetailSubContainerIcon}>
                  <Icon name="location-on" size={15} color="black" />
                </View>
                <Text style={styles.activePackageDetailSubContainerHeaderText}>
                  Pick-up location
                </Text>
              </View>
              <View style={styles.marginLeftStyle}>
                <Text style={styles.activePackageDetailSubContainerTextMargin}>
                  {item.senderAddress}
                </Text>
                <Text style={styles.activePackageDetailSubContainerTextMargin}>
                  {item.senderName}
                </Text>
                <Text style={styles.activePackageDetailSubContainerText}>
                  {item.senderPhone}
                </Text>
              </View>
            </View>
            <View style={styles.activePackageDetailSubContainer}>
              <View style={styles.activePackageDetailSubContainerHeader}>
                <View style={styles.activePackageDetailSubContainerIcon}>
                  <Icon name="location-on" size={15} color="black" />
                </View>
                <Text style={styles.activePackageDetailSubContainerHeaderText}>
                  Delivery location
                </Text>
              </View>
              <View style={styles.marginLeftStyle}>
                <Text style={styles.activePackageDetailSubContainerTextMargin}>
                  {item.recipientAddress}
                </Text>
                <Text style={styles.activePackageDetailSubContainerTextMargin}>
                  {item.recipientName}
                </Text>
                <Text style={styles.activePackageDetailSubContainerText}>
                  {item.recipientPhone}
                </Text>
              </View>
            </View>
            <View style={styles.activePackageDetailTimeContainer}>
              <View style={styles.activePackageDetailTimeSubContainer}>
                <View style={styles.activePackageDetailSubContainerIcon}>
                  <Icon name="date-range" size={15} color="black" />
                </View>
                <Text style={styles.activePackageDetailSubContainerHeaderText}>
                  {moment(item.deliveryDatetime).format('DD/MM/YYYY')}
                </Text>
                <View style={styles.flexStyle} />
                <View style={styles.activePackageDetailSubContainerIcon}>
                  <Icon name="access-time" size={15} color="black" />
                </View>
                <Text style={styles.activePackageDetailSubContainerHeaderText}>
                  {moment(item.deliveryDatetime).format('h:m A')}
                </Text>
              </View>
            </View>
            <TouchableOpacity
              onPress={() => this.removeItem(index)}
              style={styles.removeBtnContainer}
            >
              <View style={styles.removeBtn}>
                <Text style={styles.removeBtnText}>
                  {T.translate('shipmentsScannedModal.removeButton')}
                </Text>
              </View>
            </TouchableOpacity>
          </View>
        );
      }
      return (
        <TouchableOpacity
          key={item.trackingId}
          style={styles.inactivePackageDetailTab}
          onPress={() => {
            this.setState({
              currentSelectedTab: currentSelectedTab === index ? -1 : index,
            });
          }}
        >
          <Text style={styles.inactivePackageDetailTabText}>
            {`#${item.trackingId}`}
          </Text>
          <View style={styles.flexStyle} />
          <View style={styles.packageUpDownIcon}>
            <Icon name="keyboard-arrow-down" size={20} color="black" />
          </View>
        </TouchableOpacity>
      );
    };

    this.keyExtractor = item => String(item.id);
  }

  async componentDidMount() {
    this.API = await API.instantiate();
  }

  render() {
    const { onClose } = this.props;
    const { currentShipments, currentSelectedTab, isLoading } = this.state;

    return (
      <View style={styles.modalContainer}>
        <Modal onRequestClose={onClose} animationType="slide">
          <View style={styles.modalHeaderContainer}>
            <TouchableOpacity
              style={styles.modalCloseIconContainer}
              onPress={onClose}
            >
              <Icon name="arrow-back" size={25} color="white" />
            </TouchableOpacity>
            <Text style={styles.modalHeaderText}>
              {T.translate('shipmentsScannedModal.title')}
            </Text>
          </View>
          <View style={styles.container}>
            <FlatList
              style={styles.list}
              contentContainerStyle={styles.contentContainer}
              data={currentShipments}
              extraData={currentSelectedTab}
              renderItem={this.renderItem}
              keyExtractor={this.keyExtractor}
            />
            {currentShipments.length === 0 && (
              <View style={styles.emptyMessageContainer}>
                <Text style={styles.emptyMessageText}>
                  {T.translate('shipmentsScannedModal.emptyMessage')}
                </Text>
              </View>
            )}
          </View>
          <Loader visible={isLoading} />
        </Modal>
      </View>
    );
  }
}

ShipmentsScannedModal.propTypes = {
  route: PropTypes.shape({}).isRequired,
  onClose: PropTypes.func.isRequired,
};

export default ShipmentsScannedModal;
