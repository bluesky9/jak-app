import React, { Component } from 'react';
import {
  View, Text, TouchableOpacity, ScrollView,
} from 'react-native';
import PropTypes from 'prop-types';
import Modal from 'react-native-modal';
import Icon from 'react-native-vector-icons/FontAwesome';
import moment from 'moment';
import API from '../API/API';
import Loader from '../Loader';
import styles from './ShipmentDetailModal.style';

class ShipmentDetailModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentShipment: null,
      isLoading: true,
    };
  }

  async componentDidMount() {
    this.API = await API.instantiate();
    const { shipmentId } = this.props;
    this.API.get(`/shipments/${shipmentId}`)
      .then((response) => {
        this.setState({
          currentShipment: response.data,
          isLoading: false,
        });
      })
      .catch(() => {
        this.setState({ isLoading: false });
      });
  }

  render() {
    const { visible, onDismissModal } = this.props;
    const { currentShipment, isLoading } = this.state;
    return (
      <View style={styles.container}>
        <Modal
          isVisible={visible}
          style={styles.modalStyle}
          onBackdropPress={onDismissModal}
        >
          <View style={styles.modalContent}>
            <View style={styles.headerTitle}>
              <Text style={styles.headerTitleText}>Shipment Details</Text>
              <TouchableOpacity onPress={onDismissModal}>
                <Icon name="close" size={18} color="white" />
              </TouchableOpacity>
            </View>
            <ScrollView showsVerticalScrollIndicator={false}>
              <View style={styles.section}>
                <Icon name="file" size={13} color="#250060" />
                <View style={styles.detailView}>
                  <Text style={styles.textTitle}>Tracking ID</Text>
                  <Text style={styles.text}>
                    {currentShipment ? currentShipment.trackingId : ''}
                  </Text>
                </View>
              </View>
              <View style={styles.section}>
                <Icon name="file" size={13} color="#250060" />
                <View style={styles.detailView}>
                  <Text style={styles.textTitle}>Description</Text>
                  <Text style={styles.text}>
                    {currentShipment ? currentShipment.description : ''}
                  </Text>
                </View>
              </View>
              <View style={styles.section}>
                <Icon name="map-marker" size={15} color="#250060" />
                <View style={styles.detailView}>
                  <Text style={styles.textTitle}>Pick-up location</Text>
                  <Text style={styles.text}>
                    {currentShipment ? currentShipment.senderAddress : ''}
                  </Text>
                  <Text style={styles.text}>
                    {currentShipment ? currentShipment.senderName : ''}
                  </Text>
                  <Text style={styles.text}>
                    {currentShipment ? currentShipment.senderPhone : ''}
                  </Text>
                </View>
              </View>
              <View style={styles.section}>
                <Icon name="map-marker" size={15} color="#250060" />
                <View style={styles.detailView}>
                  <Text style={styles.textTitle}>Delivery location</Text>
                  <Text style={styles.text}>
                    {currentShipment ? currentShipment.recipientAddress : ''}
                  </Text>
                  <Text style={styles.text}>
                    {currentShipment ? currentShipment.recipientName : ''}
                  </Text>
                  <Text style={styles.text}>
                    {currentShipment ? currentShipment.recipientPhone : ''}
                  </Text>
                </View>
              </View>
              <View style={styles.section1}>
                <View style={styles.dateTimeView}>
                  <Icon name="calendar" size={13} color="#250060" />
                  <Text style={styles.textStyle4}>
                    {moment(
                      currentShipment ? currentShipment.deliveryDatetime : '',
                    ).format('DD/MM/YYYY')}
                  </Text>
                </View>
                <View style={styles.dateTimeView}>
                  <Icon name="clock-o" size={15} color="#250060" />
                  <Text style={styles.textStyle4}>
                    {moment(
                      currentShipment ? currentShipment.deliveryDatetime : '',
                    ).format('h:m A')}
                  </Text>
                </View>
              </View>
              <View style={styles.resultView}>
                <Text style={styles.textStyle1}>ORDER TOTAL</Text>
                <Text style={styles.textStyle2}>
                  {`${
                    currentShipment && currentShipment.cashToCollectOnDelivery
                      ? currentShipment.cashToCollectOnDelivery.toFixed(2)
                      : 0
                  } SAR`}
                </Text>
                <Text style={styles.textStyle3}>(Cash - Paid at Pick-up)</Text>
              </View>
            </ScrollView>
          </View>
          <Loader visible={isLoading} />
        </Modal>
      </View>
    );
  }
}

ShipmentDetailModal.propTypes = {
  visible: PropTypes.bool,
  onDismissModal: PropTypes.func.isRequired,
  shipmentId: PropTypes.number.isRequired,
};

ShipmentDetailModal.defaultProps = {
  visible: false,
};

export default ShipmentDetailModal;
