import { StyleSheet } from 'react-native';
import variables from '../../assets/styles/variables';

export default StyleSheet.create({
  modalContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-start',
    flexDirection: 'column',
    backgroundColor: 'white',
    width: '100%',
    height: '100%',
    margin: 0,
  },
  container: {
    position: 'absolute',
  },
  modalStyle: {
    overflow: 'hidden',
    padding: 20,
    backgroundColor: 'black',
  },
  textContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'white',
  },
  text: {
    color: variables.colors.gray,
    fontFamily: 'dubai-medium',
    fontSize: 16,
  },
  barCodeContainer: {
    flex: 4,
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'center',
    backgroundColor: 'black',
  },
  barCodeScanner: {
    flex: 1,
    aspectRatio: 1,
  },
  modalHeaderContainer: {
    height: 72,
    elevation: 0,
    borderBottomWidth: 1,
    borderBottomColor: variables.colors.magenta,
    backgroundColor: variables.colors.magenta,
    flexDirection: 'row',
    alignItems: 'center',
  },
  flexStyle: {
    flex: 1,
  },
  modalCloseIconContainer: {
    width: 25,
    height: 25,
    marginLeft: 20,
    marginRight: 14,
  },
  modalHeaderText: {
    color: 'white',
    fontSize: 24,
    fontFamily: 'dubai-bold',
    fontWeight: 'normal',
    marginLeft: 4,
    flex: 1,
    textAlignVertical: 'center',
  },
});
