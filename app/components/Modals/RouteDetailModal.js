import React, { Component } from 'react';
import {
  View, Text, TouchableOpacity, FlatList,
} from 'react-native';
import PropTypes from 'prop-types';
import Modal from 'react-native-modal';
import Icon from 'react-native-vector-icons/MaterialIcons';
import moment from 'moment';
import API from '../API/API';
import Loader from '../Loader';
import styles from './RouteDetailModal.style';

class RouteDetailModal extends Component {
  constructor(props) {
    super(props);

    this.state = {
      currentShipments: [],
      isLoading: true,
    };

    this.renderItem = ({ item, index }) => {
      const { currentSelectedTab, selectionChange } = this.props;

      if (index === currentSelectedTab) {
        return (
          <View
            key={item.trackingId}
            style={styles.activePackageDetailContainer}
          >
            <TouchableOpacity
              style={styles.activePackageDetailTab}
              onPress={() => {
                selectionChange(currentSelectedTab === index ? -1 : index);
              }}
            >
              <Text style={styles.inactivePackageDetailTabText}>
                {`#${item.trackingId}`}
              </Text>
              <View style={styles.flexStyle} />
              <View style={styles.packageUpDownIcon}>
                <Icon name="keyboard-arrow-up" size={20} color="black" />
              </View>
            </TouchableOpacity>
            <View style={styles.activePackageDetailSubContainer}>
              <View style={styles.activePackageDetailSubContainerHeader}>
                <View style={styles.activePackageDetailSubContainerIcon}>
                  <Icon name="description" size={15} color="black" />
                </View>
                <Text style={styles.activePackageDetailSubContainerHeaderText}>
                  Description
                </Text>
              </View>
              <View style={styles.marginLeftStyle}>
                <Text style={styles.activePackageDetailSubContainerText}>
                  {item.description}
                </Text>
              </View>
            </View>
            <View style={styles.activePackageDetailSubContainer}>
              <View style={styles.activePackageDetailSubContainerHeader}>
                <View style={styles.activePackageDetailSubContainerIcon}>
                  <Icon name="location-on" size={15} color="black" />
                </View>
                <Text style={styles.activePackageDetailSubContainerHeaderText}>
                  Pick-up location
                </Text>
              </View>
              <View style={styles.marginLeftStyle}>
                <Text style={styles.activePackageDetailSubContainerTextMargin}>
                  {item.senderAddress}
                </Text>
                <Text style={styles.activePackageDetailSubContainerTextMargin}>
                  {item.senderName}
                </Text>
                <Text style={styles.activePackageDetailSubContainerText}>
                  {item.senderPhone}
                </Text>
              </View>
            </View>
            <View style={styles.activePackageDetailSubContainer}>
              <View style={styles.activePackageDetailSubContainerHeader}>
                <View style={styles.activePackageDetailSubContainerIcon}>
                  <Icon name="location-on" size={15} color="black" />
                </View>
                <Text style={styles.activePackageDetailSubContainerHeaderText}>
                  Delivery location
                </Text>
              </View>
              <View style={styles.marginLeftStyle}>
                <Text style={styles.activePackageDetailSubContainerTextMargin}>
                  {item.recipientAddress}
                </Text>
                <Text style={styles.activePackageDetailSubContainerTextMargin}>
                  {item.recipientName}
                </Text>
                <Text style={styles.activePackageDetailSubContainerText}>
                  {item.recipientPhone}
                </Text>
              </View>
            </View>
            <View style={styles.activePackageDetailTimeContainer}>
              <View style={styles.activePackageDetailTimeSubContainer}>
                <View style={styles.activePackageDetailSubContainerIcon}>
                  <Icon name="date-range" size={15} color="black" />
                </View>
                <Text style={styles.activePackageDetailSubContainerHeaderText}>
                  {moment(item.deliveryDatetime).format('DD/MM/YYYY')}
                </Text>
                <View style={styles.flexStyle} />
                <View style={styles.activePackageDetailSubContainerIcon}>
                  <Icon name="access-time" size={15} color="black" />
                </View>
                <Text style={styles.activePackageDetailSubContainerHeaderText}>
                  {moment(item.deliveryDatetime).format('h:m A')}
                </Text>
              </View>
            </View>
          </View>
        );
      }
      return (
        <TouchableOpacity
          key={item.trackingId}
          style={styles.inactivePackageDetailTab}
          onPress={() => {
            selectionChange(currentSelectedTab === index ? -1 : index);
          }}
        >
          <Text style={styles.inactivePackageDetailTabText}>
            {`#${item.trackingId}`}
          </Text>
          <View style={styles.flexStyle} />
          <View style={styles.packageUpDownIcon}>
            <Icon name="keyboard-arrow-down" size={20} color="black" />
          </View>
        </TouchableOpacity>
      );
    };

    this.keyExtractor = item => String(item.id);
  }

  async componentDidMount() {
    this.API = await API.instantiate();
    const { currentPackage } = this.props;
    this.API.get(`/routes/${currentPackage}/shipments`)
      .then((response) => {
        this.setState({
          currentShipments: response.data,
          isLoading: false,
        });
      })
      .catch(() => {
        this.setState({ isLoading: false });
      });
  }

  render() {
    const {
      visible,
      currentSelectedTab,
      onDismissModal,
      currentPackage,
      onPressOverview,
    } = this.props;
    const { currentShipments, isLoading } = this.state;

    return (
      <View style={styles.container}>
        <Modal style={styles.container} isVisible={visible}>
          <View style={styles.modalContainer}>
            <View style={styles.modalHeaderContainer}>
              <Text style={styles.modalHeaderText}>Route Details</Text>
              <View style={styles.flexStyle} />
              <TouchableOpacity
                style={styles.modalCloseIconContainer}
                onPress={() => {
                  onDismissModal();
                }}
              >
                <Icon name="close" size={25} color="white" />
              </TouchableOpacity>
            </View>
            <FlatList
              style={styles.list}
              contentContainerStyle={styles.contentContainer}
              data={currentShipments}
              extraData={currentSelectedTab}
              renderItem={this.renderItem}
              keyExtractor={this.keyExtractor}
            />
            <View style={styles.horizontalDivider} />
            <View style={styles.orderTotalContainer}>
              <Text style={styles.orderTotalTitle}>ORDER TOTAL</Text>
              <Text style={styles.orderTotalValue}>173.00 SAR</Text>
              <Text style={styles.orderSmallText}>(Paid Online)</Text>
            </View>

            <TouchableOpacity
              onPress={() => onPressOverview && onPressOverview(currentPackage)}
              style={styles.overviewBtnContainer}
            >
              <View style={styles.overviewBtn}>
                <Text style={styles.overviewBtnText}>Open Overview Map</Text>
              </View>
            </TouchableOpacity>
          </View>

          <Loader visible={isLoading} />
        </Modal>
      </View>
    );
  }
}

RouteDetailModal.propTypes = {
  visible: PropTypes.bool,
  onDismissModal: PropTypes.func.isRequired,
  selectionChange: PropTypes.func.isRequired,
  onPressOverview: PropTypes.func.isRequired,
  currentPackage: PropTypes.number.isRequired,
  currentSelectedTab: PropTypes.number.isRequired,
};

RouteDetailModal.defaultProps = {
  visible: false,
};

export default RouteDetailModal;
