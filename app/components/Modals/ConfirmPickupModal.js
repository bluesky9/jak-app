import React from 'react';
import {
  ScrollView,
  Text,
  TouchableOpacity,
  View,
  Alert,
  Modal,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import QRCode from 'react-native-qrcode';
import PropTypes from 'prop-types';

import { ROUTE_BUNDLE } from 'common/constants/models/route-bundle';

import API from '../API/API';
import Loader from '../Loader';
import styles from './ConfirmPickupModal.style';

class ConfirmPickupModal extends React.Component {
  constructor(props) {
    super(props);

    const { shipment } = props;

    const {
      trackingId,
      recipientName: customerName,
      recipientPhone: customerPhone,
      recipientAddress: customerLocation,
      description,
      cashToCollectOnDelivery: orderAmount,
    } = shipment;

    this.state = {
      trackingId,
      customerName,
      customerPhone,
      customerLocation,
      description,
      orderAmount,
      paymentType: '(Cash - Pay at pick-up)',
      isLoading: false,
    };

    this.submit = () => {
      const { onSuccess } = this.props;
      this.setState({ isLoading: true });
      this.API.get(`/shipments/${shipment.id}/mark-as-scanned-before-pickup`)
        .then(() => {
          onSuccess();
        })
        .catch((err) => {
          Alert.alert(
            err.message,
            'Please re-scan the package',
            [{ text: 'OK', onPress: () => {} }],
            { cancelable: false },
          );
          this.setState({ isLoading: false });
        });
    };
  }

  async componentDidMount() {
    this.API = await API.instantiate();
  }

  render() {
    const { shipment, onClose, bundle } = this.props;
    const {
      trackingId,
      customerName,
      customerPhone,
      customerLocation,
      description,
      paymentType,
      orderAmount,
      isLoading,
    } = this.state;

    const information = [
      {
        key: 0,
        label: 'Tracking ID',
        value: <Text style={styles.valueText}>{`#${trackingId}`}</Text>,
      },
      {
        key: 1,
        label: 'Customer name',
        value: <Text style={styles.valueText}>{customerName}</Text>,
      },
      {
        key: 2,
        label: 'Customer phone',
        value: <Text style={styles.valueText}>{customerPhone}</Text>,
      },
      {
        key: 3,
        label: 'Customer location',
        value: <Text style={styles.valueText}>{customerLocation}</Text>,
      },
      {
        key: 4,
        label: 'Description',
        value: <Text style={styles.valueText}>{description}</Text>,
      },
      {
        key: 5,
        label: 'Order Amount',
        value: (
          <View>
            <Text style={styles.moneyText}>
              {`${orderAmount.toFixed(2)} SAR`}
            </Text>
            <Text style={styles.valueText}>{paymentType}</Text>
          </View>
        ),
      },
    ];

    return (
      <View style={styles.modalContainer}>
        <Modal onRequestClose={onClose} animationType="slide">
          <View style={styles.modalHeaderContainer}>
            <TouchableOpacity
              style={styles.modalCloseIconContainer}
              onPress={onClose}
            >
              <Icon name="arrow-back" size={25} color="white" />
            </TouchableOpacity>
            <Text style={styles.modalHeaderText}>Confirm Delivery</Text>
          </View>
          <ScrollView>
            <View style={styles.container}>
              <View style={styles.scanStatusView}>
                <Icon name="check" size={16} color="#ED174B" />
                <Text style={styles.scanStatus}>SCAN COMPLETE</Text>
              </View>
              <View style={styles.rectangleBox}>
                <View style={styles.qrBox}>
                  <QRCode
                    value={shipment.id}
                    size={140}
                    bgColor="black"
                    fgColor="white"
                  />
                </View>
                <View>
                  {information.map((info, index) => (
                    <View
                      key={info.key}
                      style={[
                        styles.entry,
                        index !== information.length - 1 && styles.entryBorder,
                      ]}
                    >
                      <View style={styles.propertyView}>
                        <Text style={styles.propertyText}>{info.label}</Text>
                      </View>
                      <View style={styles.valueView}>{info.value}</View>
                    </View>
                  ))}
                </View>
              </View>
              <TouchableOpacity
                style={styles.confirmButton}
                onPress={this.submit}
              >
                <Text style={styles.buttonText}>
                  {bundle === ROUTE_BUNDLE.MIX.id
                    ? 'Confirm & Start Delivery Route'
                    : 'Confirm'}
                </Text>
              </TouchableOpacity>
            </View>
          </ScrollView>
        </Modal>
        <Loader visible={isLoading} />
      </View>
    );
  }
}

ConfirmPickupModal.propTypes = {
  bundle: PropTypes.number.isRequired,
  shipment: PropTypes.shape({}).isRequired,
  onClose: PropTypes.func.isRequired,
  onSuccess: PropTypes.func.isRequired,
};

export default ConfirmPickupModal;
