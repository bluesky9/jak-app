import React from 'react';
import {
  View,
  Text,
  ScrollView,
  TouchableOpacity,
  TextInput,
  Alert,
  Modal,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import QRCode from 'react-native-qrcode';
import PropTypes from 'prop-types';

import { ROUTE_BUNDLE } from 'common/constants/models/route-bundle';
import { JOURNEY_GURU } from 'common/constants/journey-guru';

import API from '../API/API';
import Loader from '../Loader';
import styles from './ConfirmDeliveryModal.style';

class ConfirmDeliveryModal extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      trackingId: '',
      customerName: '',
      customerPhone: '',
      customerLocation: '',
      description: '',
      orderAmount: 0,
      paymentType: '(Cash - Pay at pick-up)',
      otpNumber: '',
      isLoading: true,
    };

    this.submit = async () => {
      const { shipment, onSuccess } = this.props;
      this.setState({ isLoading: true });
      try {
        await this.API.get(
          `/shipments/${
            shipment.id
          }/mark-as-scanned-after-contacting-recipient`,
        );
        this.setState({ isLoading: false });
        onSuccess();
      } catch (err) {
        Alert.alert(
          err.message,
          'Please re-scan the package',
          [{ text: 'OK', onPress: () => {} }],
          { cancelable: false },
        );
        this.setState({ isLoading: false });
      }
    };

    this.submitOTP = async () => {
      const { otpNumber } = this.state;
      const { shipment, onSuccess } = this.props;
      this.setState({ isLoading: true }, async () => {
        try {
          await this.API.post(`/shipments/${shipment.id}/confirm-otp-number`, {
            otpNumber: parseInt(otpNumber, 10),
          });
          onSuccess();
        } catch (err) {
          Alert.alert(
            err.message,
            'Please re-enter the OTP number',
            [{ text: 'OK', onPress: () => {} }],
            { cancelable: false },
          );
        }
        this.setState({ isLoading: false });
      });
    };
  }

  async componentDidMount() {
    this.API = await API.instantiate();

    const { shipment, bundle, journey } = this.props;

    const {
      trackingId,
      recipientName: customerName,
      recipientPhone: customerPhone,
      recipientAddress: customerLocation,
      description,
      cashToCollectOnDelivery: orderAmount,
    } = shipment;

    this.setState({
      trackingId,
      customerName,
      customerPhone,
      customerLocation,
      description,
      orderAmount,
      isLoading: false,
      scanShipment:
        // Mix
        (bundle === ROUTE_BUNDLE.MIX.id
          && journey
            === JOURNEY_GURU.MIX_ROUTE.SCAN_SHIPMENT_TO_CONFIRM_DELIVERY)
        // Bulk
        || (bundle === ROUTE_BUNDLE.BULK.id
          && journey
            === JOURNEY_GURU.BULK_ROUTE.SCAN_SHIPMENT_TO_CONFIRM_DELIVERY)
        // Directed Bulk
        || (bundle === ROUTE_BUNDLE.DIRECTED_BULK.id
          && journey
            === JOURNEY_GURU.DIRECTED_BULK_ROUTE.SCAN_SHIPMENT_TO_CONFIRM_DELIVERY),
    });
  }

  render() {
    const { bundle, shipment, onClose } = this.props;
    const {
      trackingId,
      customerName,
      customerPhone,
      customerLocation,
      description,
      paymentType,
      orderAmount,
      otpNumber,
      isLoading,
      scanShipment,
    } = this.state;

    const information = [
      {
        key: 0,
        label: 'Tracking ID',
        value: <Text style={styles.valueText}>{`#${trackingId}`}</Text>,
      },
      {
        key: 1,
        label: 'Customer name',
        value: <Text style={styles.valueText}>{customerName}</Text>,
      },
      {
        key: 2,
        label: 'Customer phone',
        value: <Text style={styles.valueText}>{customerPhone}</Text>,
      },
      {
        key: 3,
        label: 'Customer location',
        value: <Text style={styles.valueText}>{customerLocation}</Text>,
      },
      {
        key: 4,
        label: 'Description',
        value: <Text style={styles.valueText}>{description}</Text>,
      },
      {
        key: 5,
        label: 'Order Amount',
        value: (
          <View>
            <Text style={styles.moneyText}>
              {`${orderAmount.toFixed(2)} SAR`}
            </Text>
            <Text style={styles.valueText}>{paymentType}</Text>
          </View>
        ),
      },
    ];

    return (
      <View style={styles.modalContainer}>
        <Modal onRequestClose={onClose} animationType="slide">
          <View style={styles.modalHeaderContainer}>
            <TouchableOpacity
              style={styles.modalCloseIconContainer}
              onPress={onClose}
            >
              <Icon name="arrow-back" size={25} color="white" />
            </TouchableOpacity>
            <Text style={styles.modalHeaderText}>Confirm Delivery</Text>
          </View>
          <ScrollView>
            <View style={styles.container}>
              {bundle !== ROUTE_BUNDLE.GENERIC.id && (
                <View style={styles.scanStatusView}>
                  <Icon name="check" size={16} color="#76BC21" />
                  <Text style={styles.scanStatus}>SCAN COMPLETE</Text>
                </View>
              )}
              <View style={styles.rectangleBox}>
                {bundle !== ROUTE_BUNDLE.GENERIC.id && (
                  <View style={styles.qrBox}>
                    <QRCode
                      value={shipment.id}
                      size={140}
                      bgColor="black"
                      fgColor="white"
                    />
                  </View>
                )}
                <View>
                  {information.map((info, index) => (
                    <View
                      key={info.key}
                      style={[
                        styles.entry,
                        index !== information.length - 1 && styles.entryBorder,
                      ]}
                    >
                      <View style={styles.propertyView}>
                        <Text style={styles.propertyText}>{info.label}</Text>
                      </View>
                      <View style={styles.valueView}>{info.value}</View>
                    </View>
                  ))}
                </View>
              </View>
              {// Show confirm button when journey instruction is to scan package
              !!scanShipment && (
                <TouchableOpacity
                  style={styles.confirmButton}
                  onPress={this.submit}
                >
                  <Text style={styles.buttonText}>Confirm Package</Text>
                </TouchableOpacity>
              )}
              {// Only shows input if OTP number is needed
              !scanShipment && (
                <View style={styles.box}>
                  <TextInput
                    autoCapitalize="none"
                    placeholder="Enter OTP Code"
                    style={styles.inputComponent}
                    underlineColorAndroid="transparent"
                    keyboardType="numeric"
                    value={otpNumber}
                    autoCorrect={false}
                    onChangeText={(num) => {
                      this.setState({ otpNumber: num.replace(/\D+/g, '') });
                    }}
                  />
                </View>
              )}
              {// Only shows confirmation button if OTP number is entered
              !scanShipment
                && !!otpNumber && (
                  <TouchableOpacity
                    style={styles.confirmButton}
                    onPress={this.submitOTP}
                  >
                    <Text style={styles.buttonText}>Confirm Delivery</Text>
                  </TouchableOpacity>
              )}
              {// Display "Skip" button if OTP number is not mandatory
              !scanShipment
                && shipment.otpNumberIsNotMandatory === true && (
                  <TouchableOpacity
                    style={styles.confirmButton}
                    onPress={() => {
                      this.setState({ otpNumber: null }, this.submitOTP);
                    }}
                  >
                    <Text style={styles.buttonText}>Skip</Text>
                  </TouchableOpacity>
              )}
            </View>
          </ScrollView>
        </Modal>
        <Loader visible={isLoading} />
      </View>
    );
  }
}

ConfirmDeliveryModal.propTypes = {
  bundle: PropTypes.number.isRequired,
  journey: PropTypes.number.isRequired,
  shipment: PropTypes.shape({}).isRequired,
  onClose: PropTypes.func.isRequired,
  onSuccess: PropTypes.func.isRequired,
};

export default ConfirmDeliveryModal;
