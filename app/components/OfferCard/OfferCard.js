import React, { Component } from 'react';
import { View, Text, TouchableWithoutFeedback } from 'react-native';
import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/FontAwesome';
import styles from './OfferCard.style';
import OfferCardDetail from '../OfferCardDetail/OfferCardDetail';
import Format from '../../helpers/Format';

class OfferCard extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isCollapsed: true,
    };

    this.onPress = () => {
      this.setState(prevState => ({ isCollapsed: !prevState.isCollapsed }));
    };
  }

  render() {
    const {
      offer: {
        bundle: { name: title },
        createdAt,
        value,
      },
    } = this.props;

    const { isCollapsed } = this.state;

    const time = Format.time(createdAt);
    const date = Format.date(createdAt);

    let cardContent;

    if (isCollapsed) {
      cardContent = (
        <View style={styles.container}>
          <View style={styles.rowStyle1}>
            <Text style={styles.title}>{title}</Text>
            <View style={styles.rowStyle}>
              <Text style={[styles.costText, styles.costColor]}>
                {Format.money(value)}
              </Text>
              <Text style={[styles.unitText, styles.costColor]}>SAR</Text>
            </View>
          </View>
          <View style={styles.rowStyle1}>
            <View style={styles.rowStyle}>
              <Icon name="clock-o" size={15} color="#888" />
              <Text style={styles.dateTimeText}>{time}</Text>
            </View>
            <View style={styles.rowStyle}>
              <Icon name="calendar" size={13} color="#888" />
              <Text style={styles.dateTimeText}>{date}</Text>
            </View>
          </View>
        </View>
      );
    } else {
      cardContent = <OfferCardDetail {...this.props} />;
    }

    return (
      <View>
        <TouchableWithoutFeedback onPress={this.onPress}>
          <View>{cardContent}</View>
        </TouchableWithoutFeedback>
      </View>
    );
  }
}

OfferCard.propTypes = {
  offer: PropTypes.shape({
    bundle: PropTypes.shape({
      name: PropTypes.string,
      createdAt: PropTypes.instanceOf(new Date()),
    }),
  }).isRequired,
  onPressAccept: PropTypes.func.isRequired,
  onPressIgnore: PropTypes.func.isRequired,
  onPressOverview: PropTypes.func.isRequired,
};

export default OfferCard;
