import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    backgroundColor: 'white',
    width: '100%',
    borderRadius: 3,
    shadowOffset: { width: 1, height: 1 },
    shadowColor: 'rgba(0, 0, 0, 0.1)',
    shadowOpacity: 1.0,
    marginBottom: 10,
    padding: 15,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  rowStyle: {
    flexDirection: 'row',
  },
  rowStyle1: {
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
  title: {
    color: 'black',
    fontWeight: '600',
  },
  costText: {
    fontSize: 20,
    fontWeight: '600',
  },
  unitText: {
    alignSelf: 'flex-end',
    fontSize: 12,
    fontWeight: '600',
    marginLeft: 5,
    paddingBottom: 2,
  },
  costColor: {
    color: '#ED174B',
  },
  dateTimeText: {
    color: '#646464',
    marginLeft: 15,
  },
});
