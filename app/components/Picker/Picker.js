import React, { Component } from 'react';
import {
  Modal,
  Picker as RCPicker,
  Platform,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import PropTypes from 'prop-types';
import styles from './Picker.style';

class Picker extends Component {
  constructor(props) {
    super(props);

    this.state = {
      pickerVisible: false,
    };

    this.showPicker = () => {
      this.setState({ pickerVisible: true });
    };

    this.hidePicker = () => {
      this.setState({ pickerVisible: false });
    };

    this.confirm = () => {
      const { items, onValueChange } = this.props;
      const { selectedValue } = this.state;
      const selectedItem = items.find(item => item.value === selectedValue) || items[0];

      this.setState(
        { pickerVisible: false },
        () => onValueChange && onValueChange(selectedItem),
      );
    };

    this.renderItems = () => {
      const { items } = this.props;

      return items.map(item => (
        <RCPicker.Item key={item.value} label={item.label} value={item.value} />
      ));
    };
  }

  render() {
    const { placeholder, selectedItem } = this.props;
    const { pickerVisible, selectedValue } = this.state;
    const items = this.renderItems();

    if (Platform.OS === 'android') {
      return (
        <View style={styles.pickerField}>
          <RCPicker
            selectedValue={selectedItem.value}
            onValueChange={(selectedVal) => {
              this.setState({ selectedValue: selectedVal }, this.confirm);
            }}
          >
            {items}
          </RCPicker>
        </View>
      );
    }

    return (
      <View style={styles.container}>
        <TouchableOpacity activeOpacity={0.8} onPress={this.showPicker}>
          <View style={styles.pickerField}>
            {selectedItem ? (
              <Text style={styles.selectedValue}>{selectedItem.label}</Text>
            ) : (
              <Text style={styles.placeholder}>{placeholder || 'Select'}</Text>
            )}
          </View>
        </TouchableOpacity>
        <Modal
          animationType="none"
          transparent
          visible={pickerVisible}
          onRequestClose={this.hidePicker}
        >
          <View style={styles.background}>
            <View style={styles.pickerContainer}>
              <View style={styles.actionBar}>
                <TouchableOpacity onPress={this.hidePicker}>
                  <View style={styles.actionButton}>
                    <Text style={styles.actionButtonText}>Cancel</Text>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={this.confirm}>
                  <View style={styles.actionButton}>
                    <Text style={styles.actionButtonText}>Done</Text>
                  </View>
                </TouchableOpacity>
              </View>
              <RCPicker
                selectedValue={selectedValue}
                onValueChange={selectedVal => this.setState({ selectedValue: selectedVal })
                }
              >
                {items}
              </RCPicker>
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}

Picker.propTypes = PropTypes.shape({
  placeholder: PropTypes.string,
  selectedItem: PropTypes.object.isRequired,
  items: PropTypes.array.isRequired,
  onValueChange: PropTypes.func.isRequired,
}).isRequired;

export default Picker;
