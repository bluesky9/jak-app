import { StyleSheet } from 'react-native';
import variables from '../../assets/styles/variables';

export default StyleSheet.create({
  container: {},
  background: {
    flex: 1,
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
    justifyContent: 'flex-end',
  },
  pickerField: {
    borderWidth: 1,
    borderColor: variables.colorContext.secondary,
    borderStyle: 'solid',
    borderRadius: 22.5,
    height: 45,
    width: '100%',
    paddingLeft: 16,
    paddingRight: 16,
    justifyContent: 'center',
  },
  placeholder: {
    fontSize: 14,
    fontFamily: 'dubai-regular',
    color: '#808080',
  },
  selectedValue: {
    fontSize: 14,
    fontFamily: 'dubai-regular',
    color: variables.colorContext.secondary,
  },
  pickerContainer: {
    backgroundColor: variables.colors.white,
  },
  actionBar: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: 40,
    borderBottomWidth: 1,
    borderColor: 'rgba(0, 0, 0, 0.8)',
  },
  actionButton: {
    flex: 1,
    justifyContent: 'center',
    paddingHorizontal: 10,
  },
  actionButtonText: {
    color: '#0076FF',
    fontSize: 16,
    fontWeight: '700',
  },
});
