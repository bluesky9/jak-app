import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    borderRadius: 4,
    borderWidth: 1,
    borderColor: '#ccc',
    backgroundColor: 'white',
    padding: 15,
    marginBottom: 10,
  },
  title: {
    fontSize: 20,
    fontWeight: '600',
    color: '#2F1944',
  },
  newNotification: {
    color: 'red',
  },
  dateTime: {
    color: '#888',
    fontSize: 12,
    marginTop: 10,
    marginBottom: 10,
  },
  content: {
    color: '#333',
    fontSize: 12,
  },
  newNotificationContent: {
    color: '#2F1944',
  },
  blueText: {
    color: 'blue',
  },
});
