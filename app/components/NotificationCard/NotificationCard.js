import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import styles from './NotificationCard.style';

class NotificationCard extends Component {
  constructor(props) {
    super(props);

    const { data } = props;

    this.state = {
      read: data.hasBeenRead,
    };

    this.markAsRead = () => {
      const { onPressNotification } = this.props;
      this.setState(
        { read: true },
        () => onPressNotification && onPressNotification(data),
      );
    };
  }

  render() {
    const { data } = this.props;
    const { read } = this.state;

    return (
      <TouchableOpacity
        activeOpacity={1}
        style={styles.container}
        onPress={this.markAsRead}
      >
        <Text style={[styles.title, !read ? styles.newNotification : null]}>
          {data.title}
        </Text>
        <View style={styles.timeDateView}>
          <Text style={styles.dateTime}>{`${data.time} - ${data.date}`}</Text>
        </View>
        <Text
          numberOfLines={read ? undefined : 1}
          style={[
            styles.content,
            !data.hasBeenRead ? styles.newNotificationContent : null,
          ]}
        >
          {read ? data.message : `${data.message.substring(0, 40)}...`}
        </Text>
        {!read && (
          <Text style={[styles.content, styles.blueText]}>(read more)</Text>
        )}
      </TouchableOpacity>
    );
  }
}

NotificationCard.propTypes = {
  data: PropTypes.shape({
    title: PropTypes.string.isRequired,
    time: PropTypes.string.isRequired,
    date: PropTypes.string.isRequired,
    hasBeenRead: PropTypes.bool.isRequired,
  }).isRequired,
  onPressNotification: PropTypes.func.isRequired,
};

export default NotificationCard;
