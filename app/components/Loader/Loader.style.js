import { StyleSheet } from 'react-native';

const blackOverlay = 'rgba(0, 0, 0, 0.5)';

const styles = StyleSheet.create({
  loader: {
    alignItems: 'center',
    justifyContent: 'center',
    padding: 8,
    backgroundColor: blackOverlay,
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
  },
});

export default styles;
