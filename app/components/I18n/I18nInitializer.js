import { SecureStore } from 'expo';
import T from 'i18n-react';
import i18nAr from '../../i18n/ar.json';
import i18nEn from '../../i18n/en.json';

const Initializer = async () => {
  const language = await SecureStore.getItemAsync('language');

  switch (language) {
    case 'ar':
      T.setTexts(i18nAr);
      break;
    case 'en':
    default:
      T.setTexts(i18nEn);
      break;
  }
};

export default Initializer;
