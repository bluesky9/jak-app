import axios from 'axios';
import { Alert } from 'react-native';
import { API_PROTOCOL, API_HOSTNAME, API_PORT } from 'react-native-dotenv';
import { AuthProvider } from '../Auth/AuthProvider';
import NavigationService from '../Navigation/NavigationService';

const API = {
  instantiate: async () => {
    const auth = new AuthProvider();
    const accessToken = await auth.getAccessToken();

    const errorResponseHandler = async (error) => {
      const shouldRedirectToLoginScreen = error.response
        && (typeof error.config.errorHandle === 'undefined'
          || error.config.errorHandle === true);

      if (shouldRedirectToLoginScreen) {
        switch (error.response.status) {
          case 401:
            Alert.alert(
              'Access Denied',
              'Please, confirm your credentials.',
              [
                {
                  text: 'OK',
                  onPress: () => {
                    NavigationService.navigate('Logout', {});
                  },
                },
              ],
              {
                cancelable: false,
              },
            );
            break;
          default:
        }
      }

      return Promise.reject(error);
    };

    const axiosConf = {
      baseURL: `${API_PROTOCOL}://${API_HOSTNAME}:${API_PORT}/api`,
    };

    if (accessToken) {
      axiosConf.headers = {
        Authorization: accessToken,
      };
    }

    const axiosInstance = axios.create(axiosConf);

    axiosInstance.interceptors.response.use(
      response => response,
      errorResponseHandler,
    );

    return axiosInstance;
  },
};

export default API;
