import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    backgroundColor: 'white',
    width: '100%',
    borderRadius: 3,
    shadowOffset: { width: 1, height: 1 },
    shadowColor: 'rgba(0, 0, 0, 0.1)',
    shadowOpacity: 1.0,
    marginBottom: 10,
    padding: 15,
  },
  section: {
    paddingTop: 10,
    paddingBottom: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#ccc',
  },
  rowStyle: {
    flexDirection: 'row',
  },
  rowStyle1: {
    paddingTop: 0,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  title: {
    color: 'black',
    fontWeight: '600',
  },
  costText: {
    fontSize: 20,
    fontWeight: '600',
  },
  unitText: {
    alignSelf: 'flex-end',
    fontSize: 12,
    fontWeight: '600',
    marginLeft: 5,
    paddingBottom: 2,
  },
  costColor: {
    color: '#ED174B',
  },
  dateTimeText: {
    color: '#888',
    marginLeft: 15,
  },
  paddingTopBottom15: {
    paddingTop: 15,
    paddingBottom: 15,
  },
  paddingTopBottom10: {
    paddingTop: 10,
    paddingBottom: 10,
  },
  acceptBtn: {
    backgroundColor: '#ED174B',
    borderColor: '#ED174B',
    borderWidth: 1,
    padding: 10,
    alignItems: 'center',
    borderRadius: 20,
    width: '40%',
  },
  overviewBtn: {
    backgroundColor: '#ED174B',
    borderRadius: 20,
    padding: 10,
    alignItems: 'center',
  },
  acceptBtnText: {
    color: 'white',
  },
  ignoreBtn: {
    backgroundColor: 'white',
    borderColor: 'black',
    borderWidth: 1,
    padding: 10,
    alignItems: 'center',
    borderRadius: 20,
    width: '40%',
  },
  textStyle1: {
    fontSize: 12,
    lineHeight: 14,
    color: '#646464',
    paddingRight: 10,
  },
  underLineStyle: {
    borderBottomColor: '#ccc',
    borderBottomWidth: 1,
  },
  timelineCircle: {
    marginVertical: 8,
  },
  timelineVerticalLine: {
    width: 1,
    backgroundColor: '#E6E6E6',
    flex: 1,
  },
  timelineVerticalLineHidden: {
    width: 0,
    flex: 1,
  },
  alignItemsCenter: {
    alignItems: 'center',
  },
  textContainer: {
    flex: 1,
    marginLeft: 10,
  },
});
