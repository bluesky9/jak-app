import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/FontAwesome';
import styles from './OfferCardDetail.style';
import Format from '../../helpers/Format';

const OfferCardDetail = (props) => {
  const {
    offer,
    offer: {
      bundle: { name: title },
      createdAt,
      value,
      shipments,
    },
    onPressAccept,
    onPressIgnore,
    onPressOverview,
  } = props;

  const time = Format.time(createdAt);
  const date = Format.date(createdAt);

  return (
    <View style={styles.container}>
      <View style={[styles.section, styles.rowStyle1]}>
        <Text style={styles.title}>{title}</Text>
        <View style={styles.rowStyle}>
          <Text style={[styles.costText, styles.costColor]}>
            {Format.money(value)}
          </Text>
          <Text style={[styles.unitText, styles.costColor]}>SAR</Text>
        </View>
      </View>
      <View style={styles.section}>
        {shipments.map((shipment, index, list) => (
          <View key={shipment.trackingId}>
            {(title === 'Mix' || (title === 'Bulk' && index === 0)) && (
              <View style={[styles.rowStyle]}>
                <View style={styles.alignItemsCenter}>
                  <View
                    style={
                      index === 0
                        ? styles.timelineVerticalLineHidden
                        : styles.timelineVerticalLine
                    }
                  />
                  <Icon
                    style={styles.timelineCircle}
                    name="circle"
                    size={12}
                    color="#FFCF01"
                  />
                  <View style={styles.timelineVerticalLine} />
                </View>
                <View style={styles.textContainer}>
                  <View
                    style={[styles.paddingTopBottom10, styles.underLineStyle]}
                  >
                    <Text style={styles.textStyle1}>
                      {shipment.senderAddress}
                    </Text>
                  </View>
                </View>
              </View>
            )}
            <View style={[styles.rowStyle]}>
              <View style={styles.alignItemsCenter}>
                <View
                  style={
                    index === 0 && title !== 'Mix' && title !== 'Bulk'
                      ? styles.timelineVerticalLineHidden
                      : styles.timelineVerticalLine
                  }
                />
                <Icon
                  style={styles.timelineCircle}
                  name="circle"
                  size={12}
                  color="#76BC21"
                />
                <View
                  style={
                    index < list.length - 1
                      ? styles.timelineVerticalLine
                      : styles.timelineVerticalLineHidden
                  }
                />
              </View>
              <View style={styles.textContainer}>
                <View
                  style={[
                    styles.paddingTopBottom10,
                    index < list.length - 1 && styles.underLineStyle,
                  ]}
                >
                  <Text style={styles.textStyle1}>
                    {shipment.recipientAddress}
                  </Text>
                </View>
              </View>
            </View>
          </View>
        ))}
      </View>
      <View style={[styles.rowStyle1, styles.paddingTopBottom15]}>
        <View style={styles.rowStyle}>
          <Icon name="calendar" size={13} color="#888" />
          <Text style={styles.dateTimeText}>{date}</Text>
        </View>
        <View style={styles.rowStyle}>
          <Icon name="clock-o" size={15} color="#888" />
          <Text style={styles.dateTimeText}>{time}</Text>
        </View>
      </View>
      <View style={[styles.rowStyle1, styles.paddingTopBottom15]}>
        <TouchableOpacity
          style={styles.acceptBtn}
          onPress={() => onPressAccept && onPressAccept(offer)}
        >
          <Text style={styles.acceptBtnText}>Accept</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.ignoreBtn}
          onPress={() => onPressIgnore && onPressIgnore(offer)}
        >
          <Text>Ignore</Text>
        </TouchableOpacity>
      </View>
      <TouchableOpacity
        onPress={() => onPressOverview && onPressOverview(offer)}
      >
        <View style={styles.overviewBtn}>
          <Text style={styles.acceptBtnText}>Open Overview Map</Text>
        </View>
      </TouchableOpacity>
    </View>
  );
};

OfferCardDetail.propTypes = {
  offer: PropTypes.shape({
    bundle: PropTypes.shape({
      name: PropTypes.string,
      createdAt: PropTypes.instanceOf(new Date()),
      shipments: PropTypes.array,
    }),
  }).isRequired,
  onPressAccept: PropTypes.func.isRequired,
  onPressIgnore: PropTypes.func.isRequired,
  onPressOverview: PropTypes.func.isRequired,
};

export default OfferCardDetail;
