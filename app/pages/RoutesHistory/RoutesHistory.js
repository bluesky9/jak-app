import React, { Component } from 'react';
import {
  FlatList, View, Text, TouchableOpacity,
} from 'react-native';
import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/FontAwesome';
import moment from 'moment';
import styles from './RoutesHistory.style';
import { AuthProvider } from '../../components/Auth/AuthProvider';
import API from '../../components/API/API';
import Picker from '../../components/Picker';
import Loader from '../../components/Loader';
import RouteDetailModal from '../../components/Modals/RouteDetailModal';

const filterTypes = [
  { label: 'View All', value: 'all' },
  { label: 'Assigned', value: 'Assigned' },
  { label: 'Started', value: 'Started' },
  { label: 'Completed', value: 'Completed' },
];

class RoutesHistory extends Component {
  constructor(props) {
    super(props);

    this.state = {
      listItems: null,
      selectedFilter: filterTypes[0],
      showRouteDetailModal: false,
      currentPackage: -1,
      currentSelectedTab: -1,
      isLoading: true,
    };

    this.openOverviewPage = (routeId) => {
      const {
        navigation: { navigate },
      } = this.props;
      this.setState({ showRouteDetailModal: false });
      navigate('RouteOverview', { routeId });
    };

    this.keyExtractor = item => String(item.id);

    this.renderItem = ({ item: route }) => {
      let cardBg;

      switch (route.status.name) {
        default:
        case 'Assigned':
        case 'Started':
          cardBg = styles.routeAssignedBg;
          break;
        case 'Completed':
          cardBg = styles.routeCompletedBg;
          break;
      }

      return (
        <View key={route.id} style={[styles.routeCard, cardBg]}>
          <View style={styles.routeCardLeftCont}>
            <Text style={styles.cashCurrency}>SAR</Text>
            <Text style={styles.cashAmount}>{route.routeValue}</Text>
            <Text style={styles.startDate}>
              {route.startDatetime
                ? moment(route.startDatetime).format('DD/MM/YYYY')
                : '01/01/2000'}
            </Text>
          </View>
          <View style={styles.routeCardRightCont}>
            <View style={styles.directionsCont}>
              <View style={[styles.rowStyle, styles.paddingBottom10]}>
                <Icon name="circle" size={13} color="yellow" />
                <Text style={styles.textStyle1}>
                  {route.shipments
                  && route.shipments.length > 0
                  && route.shipments[0].senderAddress
                    ? route.shipments[0].senderAddress
                    : 'Starting point not available'}
                </Text>
              </View>
              <View style={styles.underLineStyle} />
              <View style={[styles.rowStyle, styles.paddingTop10]}>
                <Icon name="circle" size={13} color="green" />
                <Text style={styles.textStyle1}>
                  {route.shipments
                  && route.shipments.length > 0
                  && route.shipments[route.shipments.length - 1].recipientAddress
                    ? route.shipments[route.shipments.length - 1]
                      .recipientAddress
                    : 'Ending point not available'}
                </Text>
              </View>
            </View>
            <View style={[styles.rowStyle, styles.infoCont]}>
              <View
                style={
                  route.shipments.length === 1 ? styles.width50 : styles.width35
                }
              >
                <Text style={[styles.statusLabel, styles.textStyle2]}>
                  Status
                </Text>
                <Text style={[styles.routeStatus, styles.textStyle2]}>
                  {route.status.name}
                </Text>
              </View>
              <TouchableOpacity
                onPress={() => {
                  this.setState({
                    currentPackage: route.id,
                    showRouteDetailModal: true,
                  });
                }}
                style={[
                  styles.routeDetailsBtn,
                  styles.width50,
                  styles.absoluteBottomRight0,
                ]}
              >
                <Text style={styles.multiplePackagesTxt}>Route Details</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      );
    };
  }

  async componentDidMount() {
    this.API = await API.instantiate();
    const user = await new AuthProvider().getUser();
    const agentType = await this.API.get(`/agents/${user.agentId}/type`);
    this.API.get(`/agents/${user.agentId}/routes`, {
      params: {
        filter: {
          include: ['shipments', 'bundle', 'status'],
        },
      },
    })
      .then((response) => {
        const { data: routes } = response;
        const listItems = routes.map((route) => {
          const { totalCoD, value, shipments } = route;
          let routeValue;

          if (agentType.name === 'Freelancer') {
            if (value !== null) {
              routeValue = shipments.length * user.agent.agreedCommissionPerShipment;
            } else {
              routeValue = value;
            }
          } else {
            /* Full-timer */
            routeValue = totalCoD;
          }

          return {
            ...route,
            routeValue,
          };
        });

        this.setState({
          listItems,
          isLoading: false,
        });
      })
      .catch(() => {
        this.setState({ isLoading: false });
      });
  }

  render() {
    const { listItems } = this.state;

    const {
      selectedFilter,
      showRouteDetailModal,
      currentPackage,
      currentSelectedTab,
      isLoading,
    } = this.state;

    const filteredRoutes = listItems
      && listItems.filter((route) => {
        if (selectedFilter.value === 'all') {
          return true;
        }

        return route.status.name === selectedFilter.value;
      });

    return (
      <View style={styles.mainContainer}>
        <FlatList
          style={styles.mainContainer}
          contentContainerStyle={styles.container}
          data={filteredRoutes}
          keyExtractor={this.keyExtractor}
          ListHeaderComponent={() => (
            <View>
              <Text style={styles.filterByTxt}>Filter by</Text>
              <Picker
                selectedItem={selectedFilter}
                items={filterTypes}
                onValueChange={selectedFil => this.setState({ selectedFilter: selectedFil })
                }
              />
              {filteredRoutes
                && !filteredRoutes.length && (
                  <Text style={styles.notFoundMessage}>
                    {`No ${selectedFilter.value} Routes Found`}
                  </Text>
              )}
            </View>
          )}
          renderItem={this.renderItem}
        />
        {showRouteDetailModal && (
          <RouteDetailModal
            visible={showRouteDetailModal}
            currentPackage={currentPackage}
            currentSelectedTab={currentSelectedTab}
            onDismissModal={() => {
              this.setState({ showRouteDetailModal: false });
            }}
            selectionChange={(item) => {
              this.setState({ currentSelectedTab: item });
            }}
            onPressOverview={this.openOverviewPage}
          />
        )}
        <Loader visible={isLoading} />
      </View>
    );
  }
}

RoutesHistory.propTypes = PropTypes.shape({
  navigation: PropTypes.object.isRequired,
}).isRequired;

export default RoutesHistory;
