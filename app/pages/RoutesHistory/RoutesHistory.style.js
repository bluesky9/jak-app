import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  mainContainer: {
    flex: 1,
  },
  container: {
    padding: 20,
  },
  filterByTxt: {
    color: '#969696',
    height: 24,
    lineHeight: 24,
    fontSize: 14,
    fontWeight: '400',
    marginBottom: 5,
    marginLeft: 10,
  },
  filterDropdownHolder: {
    height: 50,
    width: '100%',
    borderWidth: 1,
    borderStyle: 'solid',
    borderColor: '#000',
    borderRadius: 22.5,
    marginBottom: 20,
    backgroundColor: '#fff',
  },
  filterDropdown: {
    height: 50,
    width: '100%',
  },
  routeCard: {
    width: '100%',
    minHeight: 150,
    paddingLeft: 5,
    marginTop: 10,
    marginBottom: 10,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    borderRadius: 5,
    flexDirection: 'row',
  },
  routeCompletedBg: {
    backgroundColor: '#76BC21',
  },
  routeAssignedBg: {
    backgroundColor: '#FFCF01',
  },
  routeCardLeftCont: {
    width: '30%',
    height: '100%',
    borderRightWidth: 1,
    borderRightColor: '#ccc',
    backgroundColor: '#fff',
    paddingLeft: 5,
    paddingRight: 10,
    paddingTop: 20,
    paddingBottom: 20,
  },
  routeCardRightCont: {
    width: '70%',
    height: '100%',
    backgroundColor: '#fff',
    paddingLeft: 10,
    paddingRight: 20,
    paddingTop: 20,
    paddingBottom: 20,
  },
  directionsCont: {
    minHeight: 70,
  },
  textStyle1: {
    fontSize: 13,
    color: '#ccc',
    marginLeft: 10,
    paddingRight: 10,
  },
  underLineStyle: {
    borderBottomColor: '#ccc',
    borderBottomWidth: 1,
    marginLeft: 15,
  },
  paddingTop10: {
    paddingTop: 10,
  },
  paddingBottom10: {
    paddingBottom: 10,
  },
  cashCurrency: {
    fontWeight: 'bold',
    height: 15,
    lineHeight: 15,
    fontSize: 14,
  },
  cashAmount: {
    fontWeight: 'bold',
    height: 25,
    lineHeight: 25,
    fontSize: 20,
  },
  startDate: {
    height: 15,
    lineHeight: 15,
    fontSize: 14,
    position: 'absolute',
    bottom: 20,
    left: 5,
    color: '#2E1A46',
  },
  rowStyle: {
    flexDirection: 'row',
  },
  infoCont: {
    marginTop: 10,
  },
  textStyle2: {
    fontSize: 13,
    height: 16,
    lineHeight: 16,
    textAlign: 'left',
  },
  statusLabel: {
    color: '#999',
  },
  routeStatus: {
    fontWeight: 'bold',
    color: '#646464',
  },
  width35: {
    width: '35%',
  },
  width50: {
    width: '50%',
  },
  routeDetailsBtn: {
    borderRadius: 3,
    borderWidth: 1,
    borderColor: '#646464',
    height: 35,
  },
  multiplePackagesTxt: {
    color: '#646464',
    fontSize: 11,
    lineHeight: 35,
    textAlign: 'center',
    fontWeight: 'bold',
  },
  absoluteBottomRight0: {
    position: 'absolute',
    bottom: 0,
    right: 0,
  },
  notFoundMessage: {
    fontSize: 20,
    color: '#ccc',
    margin: 30,
    textAlign: 'center',
  },
});
