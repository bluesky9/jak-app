import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-start',
    flexDirection: 'column',
    paddingTop: 30,
    paddingLeft: 20,
    paddingRight: 20,
  },
  inputView: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputComponent: {
    borderWidth: 1,
    borderColor: '#2E1A46',
    borderStyle: 'solid',
    borderRadius: 22.5,
    height: 45,
    marginBottom: 20,
    width: '100%',
    paddingLeft: 16,
    paddingRight: 16,
    color: '#2E1A46',
  },
  enabledInput: {
    backgroundColor: 'white',
  },
  disabledInput: {
    backgroundColor: '#ccc',
  },
  changePass: {
    justifyContent: 'space-evenly',
    alignItems: 'center',
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  passwordIcon: {
    marginRight: 5,
  },
  changePasswordText: {
    color: '#2E1A46',
    fontSize: 16,
    lineHeight: 27,
    fontWeight: '500',
    paddingLeft: 10,
  },
  saveButton: {
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    borderRadius: 22.5,
    backgroundColor: '#ED174B',
    height: 45,
    marginTop: 20,
  },
  saveButtonText: {
    color: 'white',
    fontSize: 16,
    fontWeight: 'bold',
    lineHeight: 27,
  },
});
