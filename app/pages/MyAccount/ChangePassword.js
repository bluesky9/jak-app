import React from 'react';
import PropTypes from 'prop-types';
import {
  View, TextInput, Text, TouchableOpacity, Alert,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import API from '../../components/API/API';
import Loader from '../../components/Loader';

import styles from './MyAccount.style';

class ChangePassword extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      oldPassword: '',
      newPassword: '',
      retypePassword: '',
      isLoading: false,
    };

    this.submit = () => {
      const { oldPassword, newPassword } = this.state;
      const data = {
        oldPassword,
        newPassword,
      };
      const validation = this.validate();

      if (validation > 0) {
        this.setState({ isLoading: true });
        this.API.post('/users/change-password', data)
          .then(() => {
            this.setState({ isLoading: false });
            this.loginSuccessAlert('Password changed successfully');
          })
          .catch((err) => {
            this.setState({ isLoading: false });
            this.loginErrorAlert(JSON.stringify(err));
          });
      } else if (validation === -2) {
        this.loginErrorAlert('Passwords do not match!');
      }
    };

    this.loginErrorAlert = err => Alert.alert('Change failed', err, [{ text: 'OK', onPress: () => {} }], {
      cancelable: false,
    });

    this.loginSuccessAlert = (res) => {
      const { navigation } = this.props;

      Alert.alert(
        'Change succeed',
        res,
        [
          {
            text: 'OK',
            onPress: () => {
              navigation.goBack();
            },
          },
        ],
        { cancelable: false },
      );
    };

    this.validate = () => {
      const { oldPassword, newPassword, retypePassword } = this.state;

      if (oldPassword === newPassword) {
        return -1;
      }
      if (newPassword !== retypePassword) {
        return -2;
      }

      return 1;
    };
  }

  async componentDidMount() {
    this.API = await API.instantiate();
  }

  render() {
    const {
      oldPassword, newPassword, retypePassword, isLoading,
    } = this.state;

    return (
      <View style={styles.container}>
        <TextInput
          secureTextEntry
          placeholderTextColor="#2E1A46"
          placeholder="Old password"
          textContentType="password"
          style={styles.inputComponent}
          underlineColorAndroid="transparent"
          value={oldPassword}
          autoCorrect={false}
          onChangeText={(text) => {
            this.setState({ oldPassword: text });
          }}
        />
        <TextInput
          secureTextEntry
          placeholderTextColor="#2E1A46"
          placeholder="New password"
          textContentType="password"
          style={styles.inputComponent}
          underlineColorAndroid="transparent"
          value={newPassword}
          autoCorrect={false}
          onBlur={() => this.validate()}
          onChangeText={(text) => {
            this.setState({ newPassword: text });
          }}
        />
        <TextInput
          secureTextEntry
          placeholderTextColor="#2E1A46"
          placeholder="New password"
          textContentType="password"
          style={styles.inputComponent}
          underlineColorAndroid="transparent"
          value={retypePassword}
          autoCorrect={false}
          onBlur={() => this.validate()}
          onChangeText={(text) => {
            this.setState({ retypePassword: text });
          }}
        />
        <View style={styles.changePass}>
          <Icon name="lock" size={22} color="#000" />
          <Text style={styles.changePasswordText}>Change password</Text>
        </View>
        <TouchableOpacity style={styles.saveButton} onPress={this.submit}>
          <Text style={styles.saveButtonText}>Save</Text>
        </TouchableOpacity>
        <Loader visible={isLoading} />
      </View>
    );
  }
}

ChangePassword.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func.isRequired,
  }).isRequired,
};

ChangePassword.navigationOptions = {
  title: 'Change Password',
  headerBackTitle: null,
};

export default ChangePassword;
