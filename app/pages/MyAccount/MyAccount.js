import React from 'react';
import PropTypes from 'prop-types';
import {
  View, TextInput, Text, TouchableOpacity, Alert,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import API from '../../components/API/API';
import Loader from '../../components/Loader';
import {
  withAuth,
  AuthProps,
  AuthProvider,
} from '../../components/Auth/AuthProvider';
import styles from './MyAccount.style';

class MyAccount extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      name: '',
      phoneNumber: '',
      email: '',
      oldName: '',
      oldPhoneNumber: '',
      oldEmail: '',
      id: 0,
      agentId: 0,
      accessToken: '',
      friendlyId: '',
      isLoading: false,
    };

    this.submit = () => {
      const {
        name,
        oldName,
        phoneNumber,
        oldPhoneNumber,
        email,
        oldEmail,
        id,
        agentId,
        accessToken,
      } = this.state;
      const apiRequests = [];
      if (name !== oldName || phoneNumber !== oldPhoneNumber) {
        this.setState({ isLoading: true });
        const data = {
          name,
          phone: phoneNumber,
        };
        apiRequests.push(this.API.patch(`/agents/${agentId}`, data));
      }
      if (email !== oldEmail) {
        this.setState({ isLoading: true });
        const data = {
          email,
        };
        apiRequests.push(this.API.patch(`/users/${id}`, data));
      }
      if (apiRequests.length > 0) {
        Promise.all(apiRequests)
          .then(async (response) => {
            if (email !== oldEmail) {
              const user = response[response.length - 1].data;
              const { auth } = this.props;
              await auth.login(accessToken, user);
              this.loginSuccessAlert('Saved successfully');
            } else {
              const agent = response[0].data;
              const user = await new AuthProvider().getUser();
              const newUser = { ...user, agent };
              const { auth } = this.props;
              await auth.login(accessToken, newUser);
              this.loginSuccessAlert('Saved successfully');
            }

            this.setState({ isLoading: false });
          })
          .catch((err) => {
            this.setState({ isLoading: false });
            this.loginErrorAlert(err.message);
          });
      }
    };

    this.loginErrorAlert = err => Alert.alert('Save failed', err, [{ text: 'OK', onPress: () => {} }], {
      cancelable: false,
    });

    this.loginSuccessAlert = res => Alert.alert(
      'Save succeed',
      res,
      [
        {
          text: 'OK',
          onPress: async () => {
            await this.initialStates();
          },
        },
      ],
      { cancelable: false },
    );

    this.initialStates = async () => {
      const {
        email, agent, id, agentId,
      } = await new AuthProvider().getUser();
      const accessToken = await new AuthProvider().getAccessToken();
      const { phone, name, friendlyId } = agent;
      this.setState({
        email,
        oldEmail: email,
        phoneNumber: phone,
        oldPhoneNumber: phone,
        name,
        friendlyId,
        oldName: name,
        id,
        agentId,
        accessToken,
      });
    };
  }

  async componentDidMount() {
    this.API = await API.instantiate();
    await this.initialStates();
  }

  render() {
    const {
      name, phoneNumber, email, friendlyId, isLoading,
    } = this.state;
    const { navigation } = this.props;
    const { navigate } = navigation;

    let friendlyIdField;

    if (friendlyId.length) {
      friendlyIdField = (
        <TextInput
          placeholderTextColor="#2E1A46"
          textContentType="username"
          style={[styles.inputComponent, styles.enabledInput]}
          underlineColorAndroid="transparent"
          value={friendlyId}
          editable={false}
          selectTextOnFocus={false}
        />
      );
    }

    return (
      <View style={styles.container}>
        <TextInput
          placeholderTextColor="#2E1A46"
          placeholder="Name"
          textContentType="name"
          style={[styles.inputComponent, styles.enabledInput]}
          underlineColorAndroid="transparent"
          keyboardType="name-phone-pad"
          autoCorrect={false}
          value={name}
          onChangeText={(newName) => {
            this.setState({ name: newName });
          }}
        />
        <TextInput
          placeholderTextColor="#2E1A46"
          placeholder="Phone Number"
          textContentType="telephoneNumber"
          style={[styles.inputComponent, styles.enabledInput]}
          underlineColorAndroid="transparent"
          keyboardType="phone-pad"
          value={phoneNumber}
          autoCorrect={false}
          onChangeText={(newNumber) => {
            this.setState({ phoneNumber: newNumber });
          }}
        />
        <TextInput
          placeholder="Email"
          placeholderTextColor="#2E1A46"
          textContentType="emailAddress"
          style={[styles.inputComponent, styles.enabledInput]}
          underlineColorAndroid="transparent"
          keyboardType="email-address"
          autoCorrect={false}
          value={email}
          onChangeText={(newEmail) => {
            this.setState({ email: newEmail });
          }}
        />
        {friendlyIdField}
        <TouchableOpacity
          style={styles.changePass}
          onPress={() => navigate('ChangePassword')}
        >
          <Icon name="lock" size={22} color="#000" />
          <Text style={styles.changePasswordText}>Change password</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.saveButton} onPress={this.submit}>
          <Text style={styles.saveButtonText}>Save</Text>
        </TouchableOpacity>
        <Loader visible={isLoading} />
      </View>
    );
  }
}

MyAccount.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func.isRequired,
  }).isRequired,
  auth: AuthProps.isRequired,
};

MyAccount.navigationOptions = {
  title: 'My Account',
  headerBackTitle: null,
};

export default withAuth(MyAccount);
