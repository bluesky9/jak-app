import React, { Component } from 'react';
import PropTypes from 'prop-types';
import T from 'i18n-react';
import { View, Text, TouchableOpacity } from 'react-native';
import styles from './Dashboard.style';

const DASHBOARD_TAB = {
  CURRENT_TRIP: 1,
  SHIPMENTS_IN_ROUTE: 2,
};

class DashboardTabs extends Component {
  constructor(props) {
    super(props);

    this.buildButtonClasses = (selectedTabId) => {
      const { selectedTab } = this.props;
      return {
        button: [
          styles.buttonTop,
          selectedTab === selectedTabId ? styles.buttonSelected : '',
        ],
        text: [
          styles.buttonText,
          selectedTab === selectedTabId ? styles.buttonSelectedText : '',
        ],
      };
    };
  }

  render() {
    const { onTabChanged } = this.props;
    const buttonStyles = {
      route: this.buildButtonClasses(DASHBOARD_TAB.CURRENT_TRIP),
      shipments: this.buildButtonClasses(DASHBOARD_TAB.SHIPMENTS_IN_ROUTE),
    };
    return (
      <View style={styles.buttonView}>
        <TouchableOpacity
          style={buttonStyles.route.button}
          onPress={() => onTabChanged(DASHBOARD_TAB.CURRENT_TRIP)}
        >
          <Text style={buttonStyles.route.text}>
            {T.translate('dashboard.currentTrip')}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={buttonStyles.shipments.button}
          onPress={() => onTabChanged(DASHBOARD_TAB.SHIPMENTS_IN_ROUTE)}
        >
          <Text style={buttonStyles.shipments.text}>
            {T.translate('dashboard.shipmentsInRoute')}
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}

DashboardTabs.propTypes = {
  selectedTab: PropTypes.number.isRequired,
  onTabChanged: PropTypes.func,
};

DashboardTabs.defaultProps = {
  onTabChanged: () => undefined,
};

export default DashboardTabs;
