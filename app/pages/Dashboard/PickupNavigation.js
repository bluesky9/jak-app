import React from 'react';
import {
  View, Text, TouchableOpacity, FlatList,
} from 'react-native';
import { MapView } from 'expo';
import { SafeAreaView } from 'react-navigation';
import Icon from 'react-native-vector-icons/FontAwesome';
import MIcon from 'react-native-vector-icons/MaterialIcons';
import moment from 'moment';

import T from 'i18n-react';
import { AuthProvider } from '../../components/Auth/AuthProvider';
import API from '../../components/API/API';
import Loader from '../../components/Loader';
import styles from './Dashboard.style';

export default class Dashboard extends React.Component {
  static navigationOptions = {
    headerStyle: {
      shadowColor: 'transparent',
      elevation: 0,
      shadowOpacity: 0,
    },
  };

  constructor(props) {
    super(props);
    this.state = {
      selected: 'route',
      listItems: null,
      isLoading: true,
    };
  }

  async componentDidMount() {
    this.API = await API.instantiate();
    const user = await new AuthProvider().getUser();
    this.API.get(`/agents/${user.agentId}/routes`, {
      params: {
        filter: {
          include: ['shipments', 'bundle', 'status'],
        },
      },
    }).then((response) => {
      let tempData = [];
      response.data.map((item) => {
        const { shipments } = item;
        const tempShipments = [];
        shipments.map((subItem) => {
          tempShipments.push({ ...subItem, bundle: item.bundle });
          return true;
        });
        tempData = [...tempData, ...tempShipments];
        return true;
      });
      this.setState({
        listItems: tempData,
        isLoading: false,
      });
    }).catch(() => {
      this.setState({ isLoading: false });
    });
  }

  renderItem = ({ item, index }) => (
    <View
      key={item.trackingId}
      style={[
        styles.shipmentDetailsCardContainer,
        index === 0 && styles.selectedBorderColor,
      ]}
    >
      <View
        style={[
          styles.flexFullContainer,
          styles.justifyContentCenter,
          styles.pickupLeftAddition]}
      >
        <Text style={styles.pickupSarText}>SAR</Text>
        <Text style={styles.pickupCostText}>{item.cashToCollectOnDelivery}</Text>
        {index === 0 ? (
          <View style={styles.pickupInRouteContainer}>
            <MIcon name="directions-car" size={13} color="white" />
            <Text style={styles.pickupInRouteText}>IN ROUTE</Text>
          </View>
        ) : null}
      </View>
      <View style={styles.pickupRightContainer}>
        <View style={styles.height20}>
          <Text style={styles.pickupRightHeader}>{`${item.bundle.name}`}</Text>
        </View>
        <View
          style={styles.flexFullContainer}
        >
          {item.senderAddress && item.senderAddress !== '' ? (
            <View style={[styles.rowStyle, styles.paddingTopBottom10]}>
              <Icon name="circle" size={12} color="green" />
              <Text style={[
                styles.textStyle1,
                styles.mainTextColor,
                styles.ml5]}
              >
                {item.senderAddress}
              </Text>
            </View>
          ) : null}
          {item.senderAddress && item.senderAddress !== '' ? <View style={styles.underLineStyle} /> : null}
          <View style={[styles.rowStyle, styles.paddingTopBottom10]}>
            <Icon name="circle" size={12} color="yellow" />
            <Text style={[
              styles.textStyle1,
              styles.mainTextColor,
              styles.ml5]}
            >
              {item.recipientAddress}
            </Text>
          </View>
          {item.senderAddress && item.senderAddress !== '' ? (
            <View style={styles.verticalLineContainer}>
              <View style={styles.verticalLine} />
              <View style={styles.flexFullContainer} />
              <View style={styles.height10} />
            </View>
          ) : null}
        </View>
        <View style={[styles.height20, styles.rowStyle]}>
          <View
            style={styles.activePackageDetailTimeSubContainer}
          >
            <View
              style={styles.activePackageDetailSubContainerIcon}
            >
              <MIcon name="date-range" size={15} color="black" />
            </View>
            <Text
              style={styles.activePackageDetailSubContainerHeaderText}
            >
              {moment(item.deliveryDatetime).format('DD/MM/YYYY')}
            </Text>
            <View
              style={styles.flexFullContainer}
            />
            <View
              style={styles.activePackageDetailSubContainerIcon}
            >
              <MIcon name="access-time" size={15} color="black" />
            </View>
            <Text
              style={styles.activePackageDetailSubContainerHeaderText}
            >
              {moment(item.deliveryDatetime).format('h:m A')}
            </Text>
          </View>
        </View>
      </View>
    </View>
  )

  keyExtractor = item => String(item.id)

  render() {
    const {
      selected,
      isLoading,
      listItems,
    } = this.state;
    return (
      <View style={styles.container}>
        <View style={styles.buttonView}>
          <TouchableOpacity
            style={[styles.buttonTop, selected === 'route' ? styles.buttonSelected : '']}
            onPress={() => {
              this.setState({ selected: 'route' });
            }}
          >
            <Text
              style={[styles.buttonText, selected === 'route' ? styles.buttonSelectedText : '']}
            >
              {T.translate('dashboard.currentTrip')}
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={[styles.buttonTop, selected === 'shipment' ? styles.buttonSelected : '']}
            onPress={() => {
              this.setState({ selected: 'shipment' });
            }}
          >
            <Text
              style={[styles.buttonText, selected === 'shipment' ? styles.buttonSelectedText : '']}
            >
              {T.translate('dashboard.shipmentsInRoute')}
            </Text>
          </TouchableOpacity>
        </View>
        <View style={styles.mapContainer}>
          <MapView
            style={styles.map}
            provider={MapView.PROVIDER_GOOGLE}
            showsUserLocation
            followsUserLocation
            showsTraffic
            loadingEnabled
            initialRegion={{
              latitude: -19.8959104,
              longitude: -43.9697408,
              latitudeDelta: 0.0922,
              longitudeDelta: 0.0421,
            }}
          />
          <View style={styles.section}>
            <View style={[styles.rowStyle, styles.paddingTopBottom10, styles.alignItemsCenter]}>
              <Icon name="circle" size={12} color="green" />
              <Text style={styles.textStyle1}>Stree name enim in dolor euismod lorem</Text>
            </View>
            <View style={styles.underLineStyle} />
            <View style={[styles.rowStyle, styles.paddingTopBottom10, styles.alignItemsCenter]}>
              <Icon name="circle" size={12} color="yellow" />
              <Text style={styles.textStyle1}>Meuismod, id commodo mi consectetur urabitur</Text>
            </View>
            <View style={styles.verticalLineContainer1}>
              <View style={styles.flexFullContainer} />
              <View style={styles.verticalLine} />
              <View style={styles.flexFullContainer} />
            </View>
          </View>
        </View>
        {selected === 'shipment' && (
          <SafeAreaView forceInset={{ top: 'always' }} style={styles.shipmentContainer}>
            <FlatList
              style={styles.flexFullContainer}
              contentContainerStyle={styles.scrollViewContentContainerStyle}
              data={listItems}
              keyExtractor={this.keyExtractor}
              renderItem={this.renderItem}
            />

          </SafeAreaView>)}
        <Loader visible={isLoading} />
      </View>
    );
  }
}
