import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { SecureStore, MapView, Location } from 'expo';
import { SafeAreaView } from 'react-navigation';
import Call from 'react-native-phone-call';
import openMap from 'react-native-open-maps';
import Icon from 'react-native-vector-icons/MaterialIcons';
import MIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import FAIcon from 'react-native-vector-icons/FontAwesome';
import Polyline from '@mapbox/polyline';
import moment from 'moment';
import {
  Image,
  View,
  Text,
  TouchableOpacity,
  Platform,
  FlatList,
  Alert,
} from 'react-native';
import T from 'i18n-react';

import { AGENT_TYPE } from 'common/constants/models/agent-type';
import { SHIPMENT_STATUS } from 'common/constants/models/shipment-status';
import { ROUTE_BUNDLE } from 'common/constants/models/route-bundle';
import { JOURNEY_GURU } from 'common/constants/journey-guru';

import API from '../../components/API/API';
import Loader from '../../components/Loader';
import { AuthProvider } from '../../components/Auth/AuthProvider';
import IconDriver from '../../assets/img/ic_driver.png';
import IconPickup from '../../assets/img/ic_pickup.png';
import IconDelivery from '../../assets/img/ic_delivery.png';
import HeaderLogo from '../../assets/img/jak_agent_logo.png';
import styles from './Dashboard.style';

import DashboardTabs from './DashboardTabs';

import TroubleshootingModal from '../../components/Modals/TroubleshootingModal';
import QRScannerModal from '../../components/Modals/QRScannerModal';
import ConfirmPickupModal from '../../components/Modals/ConfirmPickupModal';
import ConfirmDeliveryModal from '../../components/Modals/ConfirmDeliveryModal';
import ShipmentsScannedModal from '../../components/Modals/ShipmentsScannedModal';
import ShipmentOrderCompleteModal from '../../components/Modals/ShipmentOrderCompleteModal';
import ShipmentItem from './ShipmentItem';

const DASHBOARD_TAB = {
  CURRENT_TRIP: 1,
  SHIPMENTS_IN_ROUTE: 2,
};

class Dashboard extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: true,
      assignedRoute: null,
      selectedDashboardTab: DASHBOARD_TAB.CURRENT_TRIP,
      latitude: 24.7249316,
      longitude: 46.5423399,
      expandInfoBlock: false,
      journeyInstructions: {},
      routeCoordinates: null,
      routeShipments: [],
      autoCenter: true,
      updateJourneyGuru: false,
      showScanner: false,
      showDelivery: false,
      showPickup: false,
      showTroubleshooting: false,
      showShipmentsScanned: false,
    };

    this.mark = '';
    this.map = '';
    this.watchLocation = null;
    this.gpsOptions = {
      /**
       * Whether to enable high accuracy mode. For low accuracy the
       * implementation can avoid geolocation providers that consume
       * a significant amount of power (such as GPS). (Boolean)
       * @type {boolean}
       */
      enableHighAccuracy: true,

      /**
       * Minimum time to wait between each update in milliseconds.
       * Larger values will result in fewer activity detections
       * while improving battery life.
       * @type {number}
       */
      timeInterval: 1000,

      /**
       * Receive updates only when the location has changed by
       * at least this distance in meters. Larger values will result
       * in fewer activity detections while improving battery life.
       * @type {number}
       */
      distanceInterval: 5,
    };

    this.getNotifications = async () => {
      const { navigation } = this.props;
      const agent = await new AuthProvider().getAgent();
      const response = await this.API.get(`/agents/${agent.id}/notifications`);
      const { data } = response;
      data.forEach((entry) => {
        const notification = entry;
        notification.time = moment(notification.createdAt).format('hh:mm A');
        notification.date = moment(notification.createdAt).format('DD/MM/YYYY');
      });
      data.sort((first, second) => {
        if (first.createdAt < second.createdAt) {
          return Platform.OS === 'android' ? 1 : true;
        }
        return Platform.OS === 'android' ? -1 : false;
      });
      const count = data.filter(item => !item.hasBeenRead).length;
      navigation.setParams({ notificationCount: count });
    };

    this.getAssignedRoute = async () => {
      const { agent } = await new AuthProvider().getUser();
      const agentType = await this.API.get(`/agents/${agent.id}/type`);
      let assignedRoute;
      let routeShipments;
      try {
        assignedRoute = await this.API.get(
          `/agents/${agent.id}/assigned-route`,
        );
        if (assignedRoute.status === 204) {
          // Agent without route assigned
          assignedRoute = null;
          routeShipments = [];
        } else {
          // Assigned route found for the agent
          assignedRoute = assignedRoute.data;
          routeShipments = assignedRoute.shipments.map((shipment) => {
            let value;
            if (agentType.id === AGENT_TYPE.FREELANCER.id) {
              value = assignedRoute.shipments.length
                * agent.agreedCommissionPerShipment;
            } else {
              /* Full-timer */
              value = shipment.cashToCollectOnDelivery;
            }
            return {
              ...shipment,
              value,
              bundle: assignedRoute.bundle,
            };
          });
        }
      } catch (err) {
        // Couldn't fetch agent
        assignedRoute = null;
        routeShipments = [];
      }
      await SecureStore.setItemAsync(
        'assignedRoute',
        JSON.stringify(assignedRoute),
      );
      this.setState({ routeShipments, assignedRoute });
    };

    this.initiateStartingRoute = async () => {
      const { assignedRoute } = this.state;
      if (!assignedRoute) {
        this.setState({ isLoading: false });
        return;
      }
      const routeId = assignedRoute.id;
      const { navigation } = this.props;
      const propRouteId = navigation.getParam('routeId', 0);
      const id = routeId || propRouteId;
      if (id) await this.getJourneyGuruInstructions(id);
      this.setState(() => ({ updateJourneyGuru: false, isLoading: false }));
    };

    this.getJourneyGuruInstructions = async (routeId) => {
      const { data: journeyInstructions } = await this.API.get(
        `/routes/${routeId}/journey-instructions`,
      );
      const { shipment } = journeyInstructions;
      const { assignedRoute } = this.state;
      const { bundle } = assignedRoute;

      switch (bundle.id) {
        // Generic Route
        case ROUTE_BUNDLE.GENERIC.id:
          switch (journeyInstructions.id) {
            case JOURNEY_GURU.GENERIC_ROUTE.CONTACT_RECIPIENT:
              journeyInstructions.title = T.translate(
                'journeyGuru.generic.contactRecipient.title',
              );
              journeyInstructions.message = T.translate(
                'journeyGuru.generic.contactRecipient.message',
                {
                  name: shipment.recipientName,
                  address: shipment.recipientAddress,
                  phone: shipment.recipientPhone
                    ? ` (${shipment.recipientPhone})`
                    : '',
                },
              );
              break;
            case JOURNEY_GURU.GENERIC_ROUTE.CONFIRM_OTP_NUMBER:
              journeyInstructions.title = T.translate(
                'journeyGuru.generic.confirmOTP.title',
              );
              journeyInstructions.message = T.translate(
                'journeyGuru.generic.confirmOTP.message',
                {
                  trackingId: shipment.trackingId,
                  address: shipment.recipientAddress,
                  name: shipment.recipientName,
                },
              );
              break;
            default:
              journeyInstructions.title = T.translate(
                'journeyGuru.generic.completed.title',
              );
              journeyInstructions.message = T.translate(
                'journeyGuru.generic.completed.message',
              );
              break;
          }
          break;

        // Mix Route
        case ROUTE_BUNDLE.MIX.id:
          switch (journeyInstructions.id) {
            case JOURNEY_GURU.MIX_ROUTE.CONTACT_SENDER:
              journeyInstructions.title = T.translate(
                'journeyGuru.mix.contactSender.title',
              );
              journeyInstructions.message = T.translate(
                'journeyGuru.mix.contactSender.message',
                {
                  name: shipment.senderName,
                  address: shipment.senderAddress,
                  phone: shipment.senderPhone
                    ? ` (${shipment.senderPhone})`
                    : '',
                },
              );
              break;
            case JOURNEY_GURU.MIX_ROUTE.SCAN_SHIPMENT_TO_CONFIRM_PICKUP:
              journeyInstructions.title = T.translate(
                'journeyGuru.mix.scanShipmentPickup.title',
              );
              journeyInstructions.message = T.translate(
                'journeyGuru.mix.scanShipmentPickup.message',
                {
                  trackingId: shipment.trackingId,
                },
              );
              break;
            case JOURNEY_GURU.MIX_ROUTE.CONTACT_RECIPIENT:
              journeyInstructions.title = T.translate(
                'journeyGuru.mix.contactRecipient.title',
              );
              journeyInstructions.message = T.translate(
                'journeyGuru.mix.contactRecipient.message',
                {
                  name: shipment.recipientName,
                  address: shipment.recipientAddress,
                  phone: shipment.recipientPhone
                    ? ` (${shipment.recipientPhone})`
                    : '',
                },
              );
              break;
            case JOURNEY_GURU.MIX_ROUTE.SCAN_SHIPMENT_TO_CONFIRM_DELIVERY:
              journeyInstructions.title = T.translate(
                'journeyGuru.mix.scanShipmentDelivery.title',
              );
              journeyInstructions.message = T.translate(
                'journeyGuru.mix.scanShipmentDelivery.message',
                {
                  trackingId: shipment.trackingId,
                  address: shipment.recipientAddress,
                },
              );
              break;
            case JOURNEY_GURU.MIX_ROUTE.CONFIRM_OTP_NUMBER:
              journeyInstructions.title = T.translate(
                'journeyGuru.mix.confirmOTP.title',
              );
              journeyInstructions.message = T.translate(
                'journeyGuru.mix.confirmOTP.message',
                {
                  trackingId: shipment.trackingId,
                  address: shipment.recipientAddress,
                  name: shipment.recipientName,
                },
              );
              break;
            default:
              journeyInstructions.title = T.translate(
                'journeyGuru.mix.completed.title',
              );
              journeyInstructions.message = T.translate(
                'journeyGuru.mix.completed.message',
              );
              break;
          }
          break;

        // Bulk Route
        case ROUTE_BUNDLE.BULK.id:
          switch (journeyInstructions.id) {
            case JOURNEY_GURU.BULK_ROUTE.SCAN_ALL_SHIPMENTS_TO_CONFIRM_PICKUP:
              journeyInstructions.title = T.translate(
                'journeyGuru.bulk.scanAllShipments.title',
              );
              journeyInstructions.message = T.translate(
                'journeyGuru.bulk.scanAllShipments.message',
              );
              break;
            case JOURNEY_GURU.BULK_ROUTE.CONTACT_RECIPIENT:
              journeyInstructions.title = T.translate(
                'journeyGuru.bulk.contactRecipient.title',
              );
              journeyInstructions.message = T.translate(
                'journeyGuru.bulk.contactRecipient.message',
                {
                  name: shipment.recipientName,
                  address: shipment.recipientAddress,
                  phone: shipment.recipientPhone
                    ? ` (${shipment.recipientPhone})`
                    : '',
                },
              );
              break;
            case JOURNEY_GURU.BULK_ROUTE.SCAN_SHIPMENT_TO_CONFIRM_DELIVERY:
              journeyInstructions.title = T.translate(
                'journeyGuru.bulk.scanShipmentDelivery.title',
              );
              journeyInstructions.message = T.translate(
                'journeyGuru.bulk.scanShipmentDelivery.message',
                {
                  trackingId: shipment.trackingId,
                  address: shipment.recipientAddress,
                },
              );
              break;
            case JOURNEY_GURU.BULK_ROUTE.CONFIRM_OTP_NUMBER:
              journeyInstructions.title = T.translate(
                'journeyGuru.bulk.confirmOTP.title',
              );
              journeyInstructions.message = T.translate(
                'journeyGuru.bulk.confirmOTP.message',
                {
                  trackingId: shipment.trackingId,
                  address: shipment.recipientAddress,
                  name: shipment.recipientName,
                },
              );
              break;
            default:
              journeyInstructions.title = T.translate(
                'journeyGuru.bulk.completed.title',
              );
              journeyInstructions.message = T.translate(
                'journeyGuru.bulk.completed.message',
              );
              break;
          }
          break;

        // Directed Bulk Route
        case ROUTE_BUNDLE.DIRECTED_BULK.id:
          switch (journeyInstructions.id) {
            case JOURNEY_GURU.DIRECTED_BULK_ROUTE.ADD_SHIPMENT_TO_ROUTE:
              journeyInstructions.title = T.translate(
                'journeyGuru.directedBulk.addShipmentToRoute.title',
              );
              journeyInstructions.message = T.translate(
                'journeyGuru.directedBulk.addShipmentToRoute.message',
              );
              break;
            case JOURNEY_GURU.DIRECTED_BULK_ROUTE.CONTACT_RECIPIENT:
              journeyInstructions.title = T.translate(
                'journeyGuru.directedBulk.contactRecipient.title',
              );
              journeyInstructions.message = T.translate(
                'journeyGuru.directedBulk.contactRecipient.message',
                {
                  name: shipment.recipientName,
                  address: shipment.recipientAddress,
                  phone: shipment.recipientPhone
                    ? ` (${shipment.recipientPhone})`
                    : '',
                },
              );
              break;
            case JOURNEY_GURU.DIRECTED_BULK_ROUTE
              .SCAN_SHIPMENT_TO_CONFIRM_DELIVERY:
              journeyInstructions.title = T.translate(
                'journeyGuru.directedBulk.scanShipmentDelivery.title',
              );
              journeyInstructions.message = T.translate(
                'journeyGuru.directedBulk.scanShipmentDelivery.message',
                {
                  trackingId: shipment.trackingId,
                  address: shipment.recipientAddress,
                },
              );
              break;
            case JOURNEY_GURU.DIRECTED_BULK_ROUTE.CONFIRM_OTP_NUMBER:
              journeyInstructions.title = T.translate(
                'journeyGuru.directedBulk.confirmOTP.title',
              );
              journeyInstructions.message = T.translate(
                'journeyGuru.directedBulk.confirmOTP.message',
                {
                  trackingId: shipment.trackingId,
                  address: shipment.recipientAddress,
                  name: shipment.recipientName,
                },
              );
              break;
            default:
              journeyInstructions.title = T.translate(
                'journeyGuru.directedBulk.completed.title',
              );
              journeyInstructions.message = T.translate(
                'journeyGuru.directedBulk.completed.message',
              );
              break;
          }
          break;

        default:
          journeyInstructions.title = null;
          journeyInstructions.message = null;
          break;
      }

      this.setState(
        {
          isLoading: false,
          expandInfoBlock: false,
          journeyInstructions,
        },
        () => {
          this.buildRoute();
        },
      );
    };

    this.scan = () => {
      this.setState({ showScanner: true });
    };

    this.showTroubleshooting = () => {
      this.setState({ showTroubleshooting: true });
    };

    this.senderContacted = () => {
      const { journeyInstructions } = this.state;
      const { shipment } = journeyInstructions;
      this.API.get(`/shipments/${shipment.id}/mark-sender-as-contacted`);
      this.setState(() => ({ updateJourneyGuru: true }));
    };

    this.recipientContacted = () => {
      const { journeyInstructions } = this.state;
      const { shipment } = journeyInstructions;
      this.API.get(`/shipments/${shipment.id}/mark-sender-as-contacted`);
      this.setState(() => ({ updateJourneyGuru: true }));
    };

    this.toggleInfoBlock = () => {
      const { expandInfoBlock } = this.state;
      this.setState({ expandInfoBlock: !expandInfoBlock });
    };

    this.routeOffers = () => {
      const { navigation } = this.props;
      const { navigate } = navigation;
      navigate('RouteOffers');
    };

    this.selectButton = (buttonId) => {
      this.setState({ selectedDashboardTab: buttonId });
    };

    this.getPosition = async () => {
      const location = await Location.getCurrentPositionAsync({});
      this.onLocation(location);
    };

    this.watchPosition = () => {
      this.watchLocation = Location.watchPositionAsync(
        this.gpsOptions,
        this.onLocation,
      );
    };

    this.onLocation = (position) => {
      const { autoCenter } = this.state;
      this.setState({
        latitude: position.coords.latitude,
        longitude: position.coords.longitude,
      });

      if (autoCenter && this.map) {
        this.map.animateToCoordinate(
          {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
          },
          1000,
        );
      }
    };

    this.openGoogleMap = () => {
      const { journeyInstructions, latitude, longitude } = this.state;
      const { shipment } = journeyInstructions;

      if (!shipment) return;

      const { senderGeoPoint, recipientGeoPoint } = shipment;
      const isPickup = shipment.statusId === SHIPMENT_STATUS.CREATED.id && !!senderGeoPoint;

      const origin = `${latitude},${longitude}`;
      const destination = isPickup
        ? `${senderGeoPoint.lat},${senderGeoPoint.lng}`
        : `${recipientGeoPoint.lat},${recipientGeoPoint.lng}`;

      openMap({ start: origin, end: destination });
    };

    this.buildRoute = () => {
      const { journeyInstructions, latitude, longitude } = this.state;
      const { shipment } = journeyInstructions;

      if (!shipment) return;

      const { senderGeoPoint, recipientGeoPoint } = shipment;
      const isPickup = shipment.statusId === SHIPMENT_STATUS.CREATED.id && !!senderGeoPoint;

      const origin = `${latitude},${longitude}`;
      const destination = isPickup
        ? `${senderGeoPoint.lat},${senderGeoPoint.lng}`
        : `${recipientGeoPoint.lat},${recipientGeoPoint.lng}`;

      this.API.get('/routes/directions-for-google-maps', {
        params: {
          origin,
          destination,
        },
      })
        .then((response) => {
          const { data } = response;
          if (!data.json || !data.json.routes || !data.json.routes[0]) return;
          const points = Polyline.decode(
            data.json.routes[0].overview_polyline.points,
          );
          const coords = points.map(point => ({
            latitude: point[0],
            longitude: point[1],
          }));
          this.setState({
            routeCoordinates: coords,
          });
        })
        .catch();
    };

    this.initiateCall = async () => {
      const { journeyInstructions, assignedRoute } = this.state;
      const { shipment } = journeyInstructions;

      let action;
      let number;
      if (
        // Mix
        assignedRoute.bundle.id === ROUTE_BUNDLE.MIX.id
        && journeyInstructions.id === JOURNEY_GURU.MIX_ROUTE.CONTACT_SENDER
      ) {
        action = 'mark-sender-as-contacted';
        number = shipment.senderPhone;
      } else {
        action = 'mark-recipient-as-contacted';
        number = shipment.recipientPhone;
      }

      const args = {
        number,
        prompt: true,
      };
      Call(args);

      await this.API.get(`/shipments/${shipment.id}/${action}`);
      this.setState({ updateJourneyGuru: true });
    };

    this.recenter = () => {
      if (this.map) {
        this.setState({ autoCenter: true });
        const { latitude, longitude } = this.state;
        this.map.animateToCoordinate({ latitude, longitude }, 1000);
      }
    };

    this.openOverviewPage = () => {
      const {
        navigation: { navigate },
      } = this.props;
      const { assignedRoute } = this.state;
      const routeId = assignedRoute.id;

      navigate('RouteOverview', { routeId });
    };

    this.renderShipmentItem = ({ item }) => {
      const { journeyInstructions } = this.state;
      const { shipment } = journeyInstructions;
      return (
        <ShipmentItem item={item} currentShipment={item.id === shipment.id} />
      );
    };

    this.keyExtractor = item => String(item.id);

    this.scannerCallback = async (shipment) => {
      const { assignedRoute, journeyInstructions } = this.state;

      if (
        assignedRoute.bundle.id === ROUTE_BUNDLE.DIRECTED_BULK.id
        && journeyInstructions.id
          === JOURNEY_GURU.DIRECTED_BULK_ROUTE.ADD_SHIPMENT_TO_ROUTE
      ) {
        try {
          await this.API.get(
            `/shipments/${shipment.id}/add-to-directed-bulk-route/${
              assignedRoute.id
            }`,
          );
          Alert.alert(
            T.translate('dashboard.alerts.shipmentAddedToRoute.title'),
            T.translate('dashboard.alerts.shipmentAddedToRoute.message'),
            [{ text: T.translate('defaults.ok') }],
            { cancelable: false },
          );
          this.setState({ showScanner: false });
        } catch (err) {
          this.setState({ showScanner: false });
          Alert.alert(
            T.translate('dashboard.alerts.failedToAddShipmentToRoute.title'),
            T.translate('dashboard.alerts.failedToAddShipmentToRoute.message'),
            [
              {
                text: T.translate('defaults.ok'),
                onPress: () => {
                  this.setState({ showScanner: true });
                },
              },
            ],
            { cancelable: false },
          );
        }
      } else {
        this.setState({
          showScanner: false,
          showPickup:
            // Mix
            (assignedRoute.bundle.id === ROUTE_BUNDLE.MIX.id
              && journeyInstructions.id
                === JOURNEY_GURU.MIX_ROUTE.SCAN_SHIPMENT_TO_CONFIRM_PICKUP)
            // Bulk
            || (assignedRoute.bundle.id === ROUTE_BUNDLE.BULK.id
              && journeyInstructions.id
                === JOURNEY_GURU.BULK_ROUTE.SCAN_ALL_SHIPMENTS_TO_CONFIRM_PICKUP),
          showDelivery:
            // Mix
            (assignedRoute.bundle.id === ROUTE_BUNDLE.MIX.id
              && journeyInstructions.id
                === JOURNEY_GURU.MIX_ROUTE.SCAN_SHIPMENT_TO_CONFIRM_DELIVERY)
            // Bulk
            || (assignedRoute.bundle.id === ROUTE_BUNDLE.BULK.id
              && journeyInstructions.id
                === JOURNEY_GURU.BULK_ROUTE.SCAN_SHIPMENT_TO_CONFIRM_DELIVERY)
            // Directed Bulk
            || (assignedRoute.bundle.id === ROUTE_BUNDLE.DIRECTED_BULK.id
              && journeyInstructions.id
                === JOURNEY_GURU.DIRECTED_BULK_ROUTE
                  .SCAN_SHIPMENT_TO_CONFIRM_DELIVERY),
        });
      }
    };

    this.bulkListShipments = async () => {
      await this.getAssignedRoute();
      this.setState({ showShipmentsScanned: true });
    };

    this.bulkStartRoute = async () => {
      const { assignedRoute } = this.state;
      await this.API.get(`/routes/${assignedRoute.id}/mark-as-started`);
      this.setState({ updateJourneyGuru: true });
    };
  }

  async componentDidMount() {
    this.API = await API.instantiate();
    this.getNotifications();

    this.getPosition();
    this.watchPosition();

    await this.getAssignedRoute();
    await this.initiateStartingRoute();
  }

  componentDidUpdate() {
    const { updateJourneyGuru } = this.state;

    if (updateJourneyGuru) {
      this.initiateStartingRoute();
    }
  }

  componentWillUnmount() {
    this.watchLocation.remove();
  }

  render() {
    const {
      isLoading,
      assignedRoute,
      journeyInstructions,
      routeCoordinates,
      latitude,
      longitude,
      selectedDashboardTab,
      routeShipments,
      expandInfoBlock,
      showScanner,
      showPickup,
      showDelivery,
      showTroubleshooting,
      showShipmentsScanned,
    } = this.state;
    const { shipment } = journeyInstructions;

    const { navigation } = this.props;
    const { navigate } = navigation;

    // Contact sender/recipient partial view
    const ContactView = ({ type }) => (
      <View>
        <TouchableOpacity
          onPress={this.toggleInfoBlock}
          style={styles.actionButton}
        >
          <Text style={styles.actionButtonText}>
            {T.translate(
              `dashboard.actions.contact${
                type === 'sender' ? 'Sender' : 'Recipient'
              }`,
            )}
          </Text>
        </TouchableOpacity>
        <View style={styles.journeyMessage}>
          <TouchableOpacity onPress={this.toggleInfoBlock}>
            <Text style={styles.journeyMessageShipment}>
              {`Shipment ${shipment.trackingId}`}
            </Text>
            <Text style={styles.journeyMessageTitle}>
              {journeyInstructions.title}
            </Text>
            <Text style={styles.journeyMessageDescription}>
              {journeyInstructions.message}
            </Text>
          </TouchableOpacity>
          {expandInfoBlock && (
            <TouchableOpacity
              style={styles.journeyMessagePhoneView}
              onPress={this.initiateCall}
            >
              <View style={styles.circlePhone}>
                <Icon name="phone" color="#2E1A46" size={22} />
              </View>
              <Text style={styles.journeyMessagePhoneNumber}>
                {type === 'sender'
                  ? shipment.senderName
                  : shipment.recipientName}
              </Text>
            </TouchableOpacity>
          )}
        </View>
      </View>
    );

    // Journey Instructions
    const JourneyInstructions = () => shipment && (
    <TouchableOpacity
      style={styles.journeyMessage}
      onPress={this.toggleInfoBlock}
    >
      <Text style={styles.journeyMessageShipment}>
        {`Shipment ${shipment.trackingId}`}
      </Text>
      {// Only show sender address if isn't a Generic route
          assignedRoute.bundle.id !== ROUTE_BUNDLE.GENERIC.id && (
            <View>
              <View style={styles.journeyMessageDescriptionContainer}>
                <View style={styles.journeyMessageDescriptionIcon}>
                  <View style={styles.journeyGreenCircle} />
                </View>
                <View>
                  <Text style={styles.journeyMessageTitle}>
                    {T.translate('dashboard.labels.pickup')}
                  </Text>
                  <Text style={styles.journeyMessageDescription}>
                    {shipment.senderAddress}
                  </Text>
                </View>
              </View>
              <View style={[styles.separator, styles.journeySeparator]} />
            </View>
          )}
      <View style={styles.journeyMessageDescriptionContainer}>
        <View style={styles.journeyMessageDescriptionIcon}>
          <View style={styles.journeyOrangeCircle} />
        </View>
        <View>
          <Text style={styles.journeyMessageTitle}>
            {T.translate('dashboard.labels.delivery')}
          </Text>
          <Text style={styles.journeyMessageDescription}>
            {shipment.recipientAddress}
          </Text>
        </View>
      </View>
      {expandInfoBlock && (
      <View>
        <View style={styles.separator} />
        <Text style={styles.journeyMessageTitle}>
          {journeyInstructions.title}
        </Text>
        <Text style={styles.journeyMessageDescription}>
          {journeyInstructions.message}
        </Text>
      </View>
      )}
      {!expandInfoBlock && (
      <View style={styles.journeyMessageExpandContainer}>
        <Icon name="arrow-drop-down" size={25} />
      </View>
      )}
    </TouchableOpacity>
    );

    // "Current Trip" Tab contents
    let currentTripView;
    if (selectedDashboardTab === DASHBOARD_TAB.CURRENT_TRIP) {
      currentTripView = (
        <View style={styles.mapContainer}>
          <MapView
            ref={(ref) => {
              this.map = ref;
            }}
            style={styles.map}
            provider={MapView.PROVIDER_GOOGLE}
            showsMyLocationButton
            loadingEnabled
            onMoveShouldSetResponder={() => {
              this.setState({
                autoCenter: false,
              });
            }}
            onLayout={() => {
              if (this.mark) this.mark.showCallout();
            }}
            initialRegion={{
              latitude,
              longitude,
              latitudeDelta: 0.0922,
              longitudeDelta: 0.0421,
            }}
          >
            {// Agent marker
            !!latitude
              && !!longitude && (
                <MapView.Marker
                  ref={(ref) => {
                    this.mark = ref;
                  }}
                  coordinate={{ latitude, longitude }}
                  title={T.translate('dashboard.labels.you')}
                  image={IconDriver}
                />
            )}
            {// Shipment marker with 'Created' or 'Picked Up' status
            !!shipment
              && [
                SHIPMENT_STATUS.CREATED.id,
                SHIPMENT_STATUS.PICKED_UP.id,
              ].includes(shipment.statusId)
              && !!shipment.senderGeoPoint && (
                <MapView.Marker
                  ref={(ref) => {
                    this.mark = ref;
                  }}
                  coordinate={{
                    latitude: shipment.senderGeoPoint.lat,
                    longitude: shipment.senderGeoPoint.lng,
                  }}
                  title={T.translate('dashboard.labels.pickup')}
                  image={IconPickup}
                />
            )}
            {// Shipment marker with 'On Route' or 'Delivered' status
            !!shipment
              && [
                SHIPMENT_STATUS.ON_ROUTE.id,
                SHIPMENT_STATUS.DELIVERED.id,
              ].includes(shipment.statusId)
              && !!shipment.recipientGeoPoint && (
                <MapView.Marker
                  ref={(ref) => {
                    this.mark = ref;
                  }}
                  coordinate={{
                    latitude: shipment.recipientGeoPoint.lat,
                    longitude: shipment.recipientGeoPoint.lng,
                  }}
                  title={T.translate('dashboard.labels.delivery')}
                  image={IconDelivery}
                />
            )}
            {// Navigation polyline
            !!shipment
              && !!routeCoordinates && (
                <MapView.Polyline
                  coordinates={routeCoordinates}
                  strokeWidth={2}
                  strokeColor="blue"
                />
            )}
          </MapView>
          <View style={styles.messageContainer}>
            {// Contact Operations Team (Troubleshooting)
            assignedRoute && (
              <View style={styles.rightAligned}>
                <TouchableOpacity
                  style={styles.mapButton}
                  onPress={this.showTroubleshooting}
                >
                  <FAIcon name="exclamation" size={24} color="#2E1A46" />
                </TouchableOpacity>
              </View>
            )}
            {// Build route navigation button
            !!routeCoordinates && (
              <View style={styles.rightAligned}>
                <TouchableOpacity
                  style={styles.mapButton}
                  onPress={this.buildRoute}
                >
                  <Icon name="directions" size={24} color="#2E1A46" />
                </TouchableOpacity>
              </View>
            )}
            {// Open Google Maps button
            !!routeCoordinates && (
              <View style={styles.rightAligned}>
                <TouchableOpacity
                  style={styles.mapButton}
                  onPress={this.openGoogleMap}
                >
                  <MIcon name="google-maps" size={24} color="#2E1A46" />
                </TouchableOpacity>
              </View>
            )}
            {/* Re-center map button */}
            <View style={styles.rightAligned}>
              <TouchableOpacity
                style={styles.mapButton}
                onPress={this.recenter}
              >
                <Icon name="gps-fixed" size={24} color="#2E1A46" />
              </TouchableOpacity>
            </View>
            {// Agent has no route assigned
            !assignedRoute && (
              <TouchableOpacity
                key={12012}
                style={styles.routeOffersButton}
                onPress={this.routeOffers}
              >
                <Icon name="local-offer" size={22} color="#ED174B" />
                <Text style={styles.routeOffersButtonText}>
                  {T.translate('dashboard.availableOffers')}
                </Text>
              </TouchableOpacity>
            )}
            {// Scan shipment to add to route when in directed bulk route
            assignedRoute
              // Directed Bulk
              && (assignedRoute.bundle.id === ROUTE_BUNDLE.DIRECTED_BULK.id
                && journeyInstructions.id
                  === JOURNEY_GURU.DIRECTED_BULK_ROUTE.ADD_SHIPMENT_TO_ROUTE) && (
                  <View>
                    <TouchableOpacity
                      onPress={this.scan}
                      style={styles.actionButton}
                    >
                      <Text style={styles.actionButtonText}>
                        {T.translate('dashboard.actions.addShipmentToRoute')}
                      </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      onPress={this.bulkListShipments}
                      style={styles.actionButton}
                    >
                      <Text style={styles.actionButtonText}>
                        {T.translate('dashboard.actions.listShipmentsAdded')}
                      </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      onPress={this.bulkStartRoute}
                      style={styles.actionButton}
                    >
                      <Text style={styles.actionButtonText}>
                        {T.translate('dashboard.actions.startTheRoute')}
                      </Text>
                    </TouchableOpacity>
                    <View style={styles.journeyMessage}>
                      <Text style={styles.journeyMessageTitle}>
                        {journeyInstructions.title}
                      </Text>
                      <Text style={styles.journeyMessageDescription}>
                        {journeyInstructions.message}
                      </Text>
                    </View>
                  </View>
            )}
            {// Needs to contact sender.
            assignedRoute
              // Mix
              && (assignedRoute.bundle.id === ROUTE_BUNDLE.MIX.id
                && journeyInstructions.id
                  === JOURNEY_GURU.MIX_ROUTE.CONTACT_SENDER) && (
                  <ContactView type="sender" />
            )}
            {// Needs to contact recipient.
            assignedRoute
              // Generic
              && ((assignedRoute.bundle.id === ROUTE_BUNDLE.GENERIC.id
                && journeyInstructions.id
                  === JOURNEY_GURU.GENERIC_ROUTE.CONTACT_RECIPIENT)
                // Mix
                || (assignedRoute.bundle.id === ROUTE_BUNDLE.MIX.id
                  && journeyInstructions.id
                    === JOURNEY_GURU.MIX_ROUTE.CONTACT_RECIPIENT)
                // Bulk
                || (assignedRoute.bundle.id === ROUTE_BUNDLE.BULK.id
                  && journeyInstructions.id
                    === JOURNEY_GURU.BULK_ROUTE.CONTACT_RECIPIENT)
                // Directed Bulk
                || (assignedRoute.bundle.id === ROUTE_BUNDLE.DIRECTED_BULK.id
                  && journeyInstructions.id
                    === JOURNEY_GURU.DIRECTED_BULK_ROUTE.CONTACT_RECIPIENT)) && (
                    <ContactView type="recipient" />
            )}
            {// Scan shipment to confirm pickup when mixed route
            assignedRoute
              // Mix
              && (assignedRoute.bundle.id === ROUTE_BUNDLE.MIX.id
                && journeyInstructions.id
                  === JOURNEY_GURU.MIX_ROUTE.SCAN_SHIPMENT_TO_CONFIRM_PICKUP) && (
                  <View>
                    <TouchableOpacity
                      onPress={this.scan}
                      style={styles.actionButton}
                    >
                      <Text style={styles.actionButtonText}>
                        {T.translate('dashboard.actions.scanShipmentPickup')}
                      </Text>
                    </TouchableOpacity>
                    <JourneyInstructions />
                  </View>
            )}
            {// Scan shipment to confirm pickup when bulk route
            assignedRoute
              // Mix
              && (assignedRoute.bundle.id === ROUTE_BUNDLE.BULK.id
                && journeyInstructions.id
                  === JOURNEY_GURU.BULK_ROUTE
                    .SCAN_ALL_SHIPMENTS_TO_CONFIRM_PICKUP) && (
                    <TouchableOpacity
                      onPress={this.scan}
                      style={styles.actionButton}
                    >
                      <Text style={styles.actionButtonText}>
                        {T.translate('dashboard.actions.scanAllShipments')}
                      </Text>
                    </TouchableOpacity>
            )}
            {// Scan shipment to confirm delivery
            assignedRoute // Mix
              && ((assignedRoute.bundle.id === ROUTE_BUNDLE.MIX.id
                && journeyInstructions.id
                  === JOURNEY_GURU.MIX_ROUTE.SCAN_SHIPMENT_TO_CONFIRM_DELIVERY)
                // Bulk
                || (assignedRoute.bundle.id === ROUTE_BUNDLE.BULK.id
                  && journeyInstructions.id
                    === JOURNEY_GURU.BULK_ROUTE
                      .SCAN_SHIPMENT_TO_CONFIRM_DELIVERY)
                // Directed Bulk
                || (assignedRoute.bundle.id === ROUTE_BUNDLE.DIRECTED_BULK.id
                  && journeyInstructions.id
                    === JOURNEY_GURU.DIRECTED_BULK_ROUTE
                      .SCAN_SHIPMENT_TO_CONFIRM_DELIVERY)) && (
                      <View>
                        <TouchableOpacity
                          onPress={this.scan}
                          style={styles.actionButton}
                        >
                          <Text style={styles.actionButtonText}>
                            {T.translate('dashboard.actions.scanShipmentDelivery')}
                          </Text>
                        </TouchableOpacity>
                        <JourneyInstructions />
                      </View>
            )}
            {// Confirm OTP number in Generic route
            assignedRoute
              // Generic
              && (assignedRoute.bundle.id === ROUTE_BUNDLE.GENERIC.id
                && journeyInstructions.id
                  === JOURNEY_GURU.GENERIC_ROUTE.CONFIRM_OTP_NUMBER) && (
                  <View>
                    <TouchableOpacity
                      onPress={() => this.setState({ showDelivery: true })}
                      style={styles.actionButton}
                    >
                      <Text style={styles.actionButtonText}>
                        {T.translate('dashboard.actions.confirmOTP')}
                      </Text>
                    </TouchableOpacity>
                    <JourneyInstructions />
                  </View>
            )}
          </View>
        </View>
      );
    }

    // "Shipments in Route" Tab contents
    let shipmentsInRouteView;
    if (selectedDashboardTab === DASHBOARD_TAB.SHIPMENTS_IN_ROUTE) {
      shipmentsInRouteView = (
        <SafeAreaView
          forceInset={{ top: 'always' }}
          style={styles.flexFullContainer}
        >
          <TouchableOpacity onPress={this.openOverviewPage}>
            <View style={[styles.buttonSelected, styles.overviewButton]}>
              <Text style={[styles.buttonText, styles.buttonSelectedText]}>
                {T.translate('dashboard.openOverviewMap')}
              </Text>
            </View>
          </TouchableOpacity>
          <FlatList
            style={styles.flexFullContainer}
            contentContainerStyle={styles.scrollViewContentContainerStyle}
            data={routeShipments}
            keyExtractor={this.keyExtractor}
            ItemSeparatorComponent={() => (
              <View style={styles.shipmentDivider} />
            )}
            renderItem={this.renderShipmentItem}
          />
        </SafeAreaView>
      );
    }

    return (
      <View style={styles.container}>
        {!!assignedRoute
          && !(
            assignedRoute.bundle.id === ROUTE_BUNDLE.DIRECTED_BULK.id
            && journeyInstructions.id
              === JOURNEY_GURU.DIRECTED_BULK_ROUTE.ADD_SHIPMENT_TO_ROUTE
          ) && (
            <DashboardTabs
              selectedTab={selectedDashboardTab}
              onTabChanged={this.selectButton}
            />
        )}
        {currentTripView}
        {shipmentsInRouteView}
        {!!showTroubleshooting && (
          <TroubleshootingModal
            route={assignedRoute}
            shipment={shipment}
            onClose={() => this.setState({
              showTroubleshooting: false,
              updateJourneyGuru: true,
            })
            }
            onSuccess={() => this.setState({
              showTroubleshooting: false,
              updateJourneyGuru: true,
            })
            }
          />
        )}
        {!!showShipmentsScanned && (
          <ShipmentsScannedModal
            route={assignedRoute}
            onClose={() => {
              this.setState({ showShipmentsScanned: false });
              this.getAssignedRoute();
            }}
          />
        )}
        {!!showScanner && (
          <QRScannerModal
            shipmentId={
              shipment
              && !(
                assignedRoute.bundle.id === ROUTE_BUNDLE.DIRECTED_BULK.id
                && journeyInstructions.id
                  === JOURNEY_GURU.DIRECTED_BULK_ROUTE.ADD_SHIPMENT_TO_ROUTE
              )
                ? shipment.id
                : null
            }
            onClose={() => this.setState({
              showScanner: false,
              updateJourneyGuru: true,
            })
            }
            onSuccess={result => this.scannerCallback(result)}
          />
        )}
        {!!showPickup && (
          <ConfirmPickupModal
            bundle={assignedRoute.bundle.id}
            shipment={shipment}
            onClose={() => this.setState({ showPickup: false, updateJourneyGuru: true })
            }
            onSuccess={() => this.setState({ showPickup: false, updateJourneyGuru: true })
            }
          />
        )}
        {(!!showDelivery
          || (assignedRoute
            // Mix
            && ((assignedRoute.bundle.id === ROUTE_BUNDLE.MIX.id
              && journeyInstructions.id
                === JOURNEY_GURU.MIX_ROUTE.CONFIRM_OTP_NUMBER)
              // Bulk
              || (assignedRoute.bundle.id === ROUTE_BUNDLE.BULK.id
                && journeyInstructions.id
                  === JOURNEY_GURU.BULK_ROUTE.CONFIRM_OTP_NUMBER)
              // Directed Bulk
              || (assignedRoute.bundle.id === ROUTE_BUNDLE.DIRECTED_BULK.id
                && journeyInstructions.id
                  === JOURNEY_GURU.DIRECTED_BULK_ROUTE.CONFIRM_OTP_NUMBER)))) && (
                  <ConfirmDeliveryModal
                    bundle={assignedRoute.bundle.id}
                    journey={journeyInstructions.id}
                    shipment={shipment}
                    onClose={() => this.setState({ showDelivery: false, updateJourneyGuru: true })
            }
                    onSuccess={() => this.setState({ showDelivery: false, updateJourneyGuru: true })
            }
                  />
        )}
        {// Route completed
        assignedRoute // Generic
          && ((assignedRoute.bundle.id === ROUTE_BUNDLE.GENERIC.id
            && journeyInstructions.id === JOURNEY_GURU.GENERIC_ROUTE.COMPLETED)
            // Mix
            || (assignedRoute.bundle.id === ROUTE_BUNDLE.MIX.id
              && journeyInstructions.id === JOURNEY_GURU.MIX_ROUTE.COMPLETED)
            // Bulk
            || (assignedRoute.bundle.id === ROUTE_BUNDLE.BULK.id
              && journeyInstructions.id === JOURNEY_GURU.BULK_ROUTE.COMPLETED)
            // Directed Bulk
            || (assignedRoute.bundle.id === ROUTE_BUNDLE.DIRECTED_BULK.id
              && journeyInstructions.id
                === JOURNEY_GURU.DIRECTED_BULK_ROUTE.COMPLETED)) && (
                <ShipmentOrderCompleteModal
                  onClose={() => this.setState({
                    assignedRoute: null,
                    routeShipments: [],
                    routeCoordinates: null,
                    journeyInstructions: {},
                  })
              }
                  navigate={navigate}
                />
        )}
        <Loader visible={isLoading} />
      </View>
    );
  }
}

Dashboard.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func.isRequired,
  }).isRequired,
};

Dashboard.navigationOptions = ({
  navigation: {
    state: { params },
    navigate,
  },
}) => ({
  headerTitle: (
    <Image
      style={styles.headerTitle}
      resizeMode="contain"
      source={HeaderLogo}
    />
  ),
  headerRight:
    params && params.notificationCount ? (
      <TouchableOpacity onPress={() => navigate('Notifications')}>
        <View style={styles.headerRightButtonContainer}>
          <Icon name="notifications" size={30} color="#ED174B" />
          <View style={styles.headerRightButton}>
            <Text style={styles.headerRightCountText}>
              {params.notificationCount}
            </Text>
          </View>
        </View>
      </TouchableOpacity>
    ) : (
      <TouchableOpacity onPress={() => navigate('Notifications')}>
        <View style={styles.headerRightButtonContainer}>
          <Icon name="notifications-none" size={30} color="#888" />
        </View>
      </TouchableOpacity>
    ),
});

export default Dashboard;
