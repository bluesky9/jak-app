import React from 'react';
import {
  View, Text, TouchableOpacity,
} from 'react-native';
import { MapView } from 'expo';

import Icon from 'react-native-vector-icons/MaterialIcons';
import T from 'i18n-react';
import IDriver from '../../assets/img/ic_driver.png';
// import IconPickup from '../../assets/img/ic_pickup.png';
// import IconDelivery from '../../assets/img/ic_delivery.png';

import styles from './Dashboard.style';

export default class StartRoute extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      latitude: -19.8959104,
      longitude: -43.9697408,
    };
  }

  componentDidMount() {
    this.getPosition();
  }

  getPosition = () => {
    navigator.geolocation.getCurrentPosition((position) => {
      this.setState({
        latitude: position.coords.latitude,
        longitude: position.coords.longitude,
      });
    });
    // We can add some sort of error handling here, too
  }

  startNavigation = () => {
    // const url = 'https://www.google.com/maps/dir/?api=1&travelmode=driving&dir_action=navigate&destination=Riyadh';
    // return Linking.canOpenURL(url).then((supported) => {
    //   if (supported) {
    //     return Linking.openURL(url);
    //   }
    // });
  };

  render() {
    const { latitude, longitude } = this.state;

    return (
      <View style={styles.container}>
        <View style={styles.mapContainer}>
          <MapView
            style={styles.map}
            provider={MapView.PROVIDER_GOOGLE}
            // showsUserLocation
            showsMyLocationButton
            // followsUserLocation
            showsTraffic
            loadingEnabled
            initialRegion={{
              latitude,
              longitude,
              latitudeDelta: 0.0922,
              longitudeDelta: 0.0421,
            }}
          >
            {
              !!latitude
              && !!longitude
              && <MapView.Marker coordinate={{ latitude, longitude }} title="You" image={IDriver} />
            }
          </MapView>
          <TouchableOpacity style={styles.gps} onPress={this.getPosition}>
            <Icon name="my-location" size={22} color="#000" />
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.startNavigationButton}
            onPress={this.startNavigation}
          >
            <Text style={styles.startNavigationButtonText}>{T.translate('dashboard.startRoute')}</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
