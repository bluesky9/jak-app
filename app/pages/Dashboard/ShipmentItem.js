import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import { View, Text } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import FAIcon from 'react-native-vector-icons/FontAwesome';
import T from 'i18n-react';
import styles from './Dashboard.style';

const ShipmentItem = ({ item, currentShipment }) => (
  <View
    style={[
      styles.shipmentDetailsCardContainer,
      currentShipment && styles.selectedBorderColor,
    ]}
  >
    <View
      style={[
        styles.flexFullContainer,
        styles.justifyContentCenter,
        styles.pickupLeftAddition,
      ]}
    >
      <Text style={styles.pickupSarText}>SAR</Text>
      <Text style={styles.pickupCostText}>{item.value}</Text>
      {currentShipment ? (
        <View style={styles.pickupInRouteContainer}>
          <Icon name="directions-car" size={13} color="white" />
          <Text style={styles.pickupInRouteText}>
            {T.translate('dashboard.inRoute')}
          </Text>
        </View>
      ) : null}
    </View>
    <View style={styles.pickupRightContainer}>
      <View style={styles.height20}>
        <Text style={styles.pickupRightHeader}>{`${item.trackingId}`}</Text>
      </View>
      <View style={styles.flexFullContainer}>
        {item.senderAddress && item.senderAddress !== '' ? (
          <View style={[styles.rowStyle, styles.paddingTopBottom10]}>
            <FAIcon name="circle" size={12} color="yellow" />
            <Text style={[styles.textStyle1, styles.mainTextColor, styles.ml5]}>
              {item.senderAddress}
            </Text>
          </View>
        ) : null}
        {item.senderAddress && item.senderAddress !== '' ? (
          <View style={styles.underLineStyle} />
        ) : null}
        <View style={[styles.rowStyle, styles.paddingTopBottom10]}>
          <FAIcon name="circle" size={12} color="green" />
          <Text style={[styles.textStyle1, styles.mainTextColor, styles.ml5]}>
            {item.recipientAddress}
          </Text>
        </View>
        {item.senderAddress && item.senderAddress !== '' ? (
          <View style={styles.verticalLineContainer}>
            <View style={styles.verticalLine} />
            <View style={styles.flexFullContainer} />
            <View style={styles.height10} />
          </View>
        ) : null}
      </View>
      <View style={[styles.height20, styles.rowStyle]}>
        <View style={styles.activePackageDetailTimeSubContainer}>
          <View style={styles.activePackageDetailSubContainerIcon}>
            <Icon name="date-range" size={15} color="black" />
          </View>
          <Text style={styles.activePackageDetailSubContainerHeaderText}>
            {moment(item.deliveryDatetime).format('DD/MM/YYYY')}
          </Text>
          <View style={styles.flexFullContainer} />
          <View style={styles.activePackageDetailSubContainerIcon}>
            <Icon name="access-time" size={15} color="black" />
          </View>
          <Text style={styles.activePackageDetailSubContainerHeaderText}>
            {moment(item.deliveryDatetime).format('h:m A')}
          </Text>
        </View>
      </View>
    </View>
  </View>
);

ShipmentItem.propTypes = {
  item: PropTypes.shape({}).isRequired,
  currentShipment: PropTypes.bool,
};

ShipmentItem.defaultProps = {
  currentShipment: false,
};

export default ShipmentItem;
