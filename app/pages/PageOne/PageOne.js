import React from 'react';
import { View, Text } from 'react-native';
import T from 'i18n-react';
import styles from './PageOne.style';

const PageOne = () => (
  <View style={styles.container}>
    <Text>{T.translate('pageOne.text')}</Text>
  </View>
);

export default PageOne;
