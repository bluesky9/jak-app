import React, { Component } from 'react';
import { SecureStore } from 'expo';
import { withNavigation } from 'react-navigation';
import I18nInitializer from '../../components/I18n/I18nInitializer';
import LoadingScreen from '../../components/LoadingScreen/LoadingScreen';

class LanguageSelector extends Component {
  async componentDidMount() {
    const { navigation } = this.props;
    const currentLanguage = await SecureStore.getItemAsync('language');
    const language = currentLanguage === 'en' ? 'ar' : 'en';
    await SecureStore.setItemAsync('language', language);
    await I18nInitializer();
    navigation.replace('Drawer');
  }

  render() {
    return <LoadingScreen />;
  }
}

export default withNavigation(LanguageSelector);
