import React from 'react';
import PropTypes from 'prop-types';
import { Permissions, Notifications } from 'expo';
import {
  View,
  Image,
  TextInput,
  Text,
  TouchableOpacity,
  Alert,
  Linking,
} from 'react-native';

import { withAuth, AuthProps } from '../../components/Auth/AuthProvider';
import API from '../../components/API/API';
import Loader from '../../components/Loader';

import Logo from '../../assets/img/login-logo.png';
import LoginFooter from '../../assets/img/login-footer.png';
import styles from './Login.style';
import LoadingScreen from '../../components/LoadingScreen/LoadingScreen';

class Login extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      username: '',
      password: '',
      isLoading: false,
      isAuth: true,
    };

    this.isEmail = (str) => {
      const regExp = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return regExp.test(str);
    };

    this.registerForPushNotificationsAsync = async (userId) => {
      const { status: existingStatus } = await Permissions.getAsync(
        Permissions.NOTIFICATIONS,
      );

      let finalStatus = existingStatus;

      // only ask if permissions have not already been determined, because
      // iOS won't necessarily prompt the user a second time.
      if (existingStatus !== 'granted') {
        // Android remote notification permissions are granted during the app
        // install, so this will only ask on iOS
        const { status } = await Permissions.askAsync(
          Permissions.NOTIFICATIONS,
        );
        finalStatus = status;
      }

      // Stop here if the user did not grant permissions
      if (finalStatus !== 'granted') {
        throw new Error(
          'User did not grant permission for push notifications.',
        );
      }

      // Get the token that uniquely identifies this device
      const token = await Notifications.getExpoPushTokenAsync();

      // Refresh the API, now that we're logged in.
      this.API = await API.instantiate();

      return this.API.post('/userDevices/register', {
        registrationToken: token,
        userId,
      });
    };

    this.submit = () => {
      const { navigation } = this.props;
      const { replace } = navigation;
      const { username, password } = this.state;
      let data = null;

      this.setState({ isLoading: true });

      if (this.isEmail(username)) {
        data = {
          email: username,
          password,
        };
      } else {
        data = {
          username,
          password,
        };
      }

      this.API.post('/users/login', data, {
        params: {
          include: 'user',
        },
      })
        .then((response) => {
          const { id: accessToken, user } = response.data;
          const { auth } = this.props;
          auth
            .login(accessToken, user)
            .then(() => {
              this.registerForPushNotificationsAsync(user.id);
              this.setState({ isLoading: false });
              replace('Drawer'); // Go to Dashboard.
            })
            .catch(() => {
              this.setState({ isLoading: false });
              replace('Drawer'); // Go to Dashboard.
            });
        })
        .catch((err) => {
          this.setState({ isLoading: false });
          // Maybe set state properties and color input forms red
          this.loginErrorAlert(err);
        });
    };

    this.createAccountAlert = () => Linking.canOpenURL('https://jakapp.co/')
      .then((flag) => {
        if (flag === true) {
          Linking.openURL('https://jakapp.co/');
        }
      })
      .catch(() => {
        // Maybe set state properties and color input forms red
        // console.log(err);
      });

    // tcp: I added an alert if things go south, but we should probably use some sort of
    //      style in the input form to indicate to the user that login failed.
    this.loginErrorAlert = err => Alert.alert(
      'Login failed',
      err.message,
      [{ text: 'OK', onPress: () => {} }],
      {
        cancelable: false,
      },
    );
  }

  async componentWillMount() {
    const { auth } = this.props;
    const isAuth = await auth.isAuthAsync();
    this.setState({ isAuth });
    if (isAuth) {
      const { navigation } = this.props;
      const { replace } = navigation;
      // go to dashboard
      replace('Drawer');
    }
  }

  async componentDidMount() {
    this.API = await API.instantiate();
  }

  render() {
    const {
      username, password, isLoading, isAuth,
    } = this.state;

    if (isAuth) {
      return <LoadingScreen />;
    }
    return (
      <View style={styles.container}>
        <View style={styles.logoDiv}>
          <Image source={Logo} />
        </View>
        <View style={styles.firstInputView}>
          <TextInput
            autoCapitalize="none"
            placeholder="Username or Email"
            textContentType="nickname"
            style={styles.inputComponent}
            underlineColorAndroid="transparent"
            keyboardType="default"
            value={username}
            autoCorrect={false}
            onChangeText={(text) => {
              this.setState({ username: text });
            }}
          />
          <TextInput
            secureTextEntry
            placeholder="Password"
            textContentType="password"
            style={styles.inputComponent}
            underlineColorAndroid="transparent"
            value={password}
            onChangeText={(text) => {
              this.setState({ password: text });
            }}
          />
          <TouchableOpacity style={styles.loginButton} onPress={this.submit}>
            <Text style={styles.loginText}>Login</Text>
          </TouchableOpacity>
          <Text style={styles.createAccountText}>
            New here? Please create a new account.
          </Text>
          <TouchableOpacity
            style={styles.createAccountButton}
            onPress={this.createAccountAlert}
          >
            <Text style={styles.createAccountButtonText}>
              Create New Account
            </Text>
          </TouchableOpacity>
        </View>
        <View style={styles.loginFooter}>
          <Image
            style={styles.loginFooterImage}
            resizeMode="cover"
            source={LoginFooter}
          />
        </View>
        <Loader visible={isLoading} />
      </View>
    );
  }
}

Login.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func.isRequired,
  }).isRequired,
  auth: AuthProps.isRequired,
};

export default withAuth(Login);
