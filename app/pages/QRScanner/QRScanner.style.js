import { StyleSheet } from 'react-native';
import variables from '../../assets/styles/variables';

export default StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: variables.lightBackground,
  },
  textContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    color: variables.colors.gray,
    fontFamily: 'dubai-medium',
    fontSize: 16,
  },
  barCodeContainer: {
    flex: 8,
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'center',
  },
  barCodeScanner: {
    marginLeft: 20,
    marginRight: 20,
    flex: 1,
    aspectRatio: 1,
  },
});
