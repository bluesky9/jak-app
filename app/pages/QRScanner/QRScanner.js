import React from 'react';
import { Text, View, Alert } from 'react-native';
import { BarCodeScanner, Permissions } from 'expo';
import PropTypes from 'prop-types';
import API from '../../components/API/API';
import Loader from '../../components/Loader';
import styles from './QRScanner.style';

export default class QRScanner extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      hasCameraPermission: null,
      isLoading: false,
    };

    this.handleBarCodeRead = (barCode) => {
      const { navigation } = this.props;
      const { navigate, state } = navigation;
      const { params } = state;
      const { isPickup, shipmentId } = params;

      const navigateTo = isPickup ? 'ConfirmPickup' : 'ConfirmDelivery';

      this.setState({ isLoading: true });
      this.API.get('/shipments', {
        params: { filter: { where: { trackingId: barCode.data } } },
      })
        .then((response) => {
          if (response.data[0].id === shipmentId) {
            this.setState({ isLoading: false });
            navigate(navigateTo, { shipment: response.data[0], shipmentId });
          } else {
            throw new Error('Shipment & QR code do not match!');
          }
        })
        .catch((err) => {
          Alert.alert(
            err.message,
            'Please re-scan the package',
            [
              { text: 'OK', onPress: () => {} },
            ],
            { cancelable: false },
          );

          this.setState({ isLoading: false });
        });
    };
  }

  async componentWillMount() {
    this.API = await API.instantiate();
    const { status } = await Permissions.askAsync(Permissions.CAMERA);
    this.setState({ hasCameraPermission: status === 'granted' });
  }

  render() {
    const { hasCameraPermission, isLoading } = this.state;

    if (hasCameraPermission === null) {
      return (
        <View style={styles.container}>
          <Text style={styles.text}>Requesting for camera permission</Text>
        </View>
      );
    } if (hasCameraPermission === false) {
      return (
        <View style={styles.container}>
          <Text style={styles.text}>No access to camera</Text>
        </View>
      );
    }
    return (
      <View style={styles.container}>
        <View style={styles.textContainer}>
          <Text style={styles.text}>
            SCAN CODE
          </Text>
        </View>
        <View style={styles.barCodeContainer}>
          <BarCodeScanner
            onBarCodeRead={this.handleBarCodeRead}
            style={styles.barCodeScanner}
          />
        </View>
        <Loader visible={isLoading} />
      </View>
    );
  }
}

QRScanner.propTypes = PropTypes.shape({
  navigation: PropTypes.object.isRequired,
}).isRequired;
