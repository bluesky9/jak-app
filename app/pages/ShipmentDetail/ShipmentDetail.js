import React from 'react';
import { View, Button, Text } from 'react-native';

import styles from './ShipmentDetail.style';
import ShipmentDetailModal from '../../components/Modals/ShipmentDetailModal';

const shipmentId = 4; // TODO: Remove this after this page is properly integrated.

class ShipmentDetail extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      modalVisible: false,
    };

    this.openShipmentDetailModal = () => {
      this.setState({ modalVisible: true });
    };

    this.closeShipmentDetailModal = () => {
      this.setState({ modalVisible: false });
    };
  }

  render() {
    const { modalVisible } = this.state;
    return (
      <View style={styles.container}>
        <Text>Press the button below</Text>
        <Button
          title="Open Shipment Detail Modal"
          onPress={this.openShipmentDetailModal}
        />
        {modalVisible && (
          <ShipmentDetailModal
            visible={modalVisible}
            shipmentId={shipmentId}
            onDismissModal={this.closeShipmentDetailModal}
          />
        )}
      </View>
    );
  }
}

export default ShipmentDetail;
