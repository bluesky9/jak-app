import React from 'react';
import {
  View, Text, ScrollView, TouchableOpacity, TextInput,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import QRCode from 'react-native-qrcode';
import PropTypes from 'prop-types';

import API from '../../components/API/API';
import Loader from '../../components/Loader';
import styles from './Delivery.style';

export default class DeliveryConfirmation extends React.Component {
  static navigationOptions = {
    title: 'Confirm Delivery',
    headerBackTitle: null,
    headerTintColor: 'white',
    headerBackStyle: {
      color: 'white',
    },
    headerTitleStyle: {
      color: 'white',
    },
    headerStyle: {
      backgroundColor: '#ED174B',
      shadowColor: 'transparent',
      elevation: 0,
      shadowOpacity: 0,
    },
  };

  static propTypes = {
    shipmentId: PropTypes.number,
  }

  static defaultProps = {
    shipmentId: 1,
  }

  constructor(props) {
    super(props);

    this.state = {
      customerName: '',
      customerPhone: '',
      customerLocation: '',
      description: '',
      orderAmount: 0,
      paymentType: '(Cash - Pay at pick-up)',
      otpNumber: 0,
      isLoading: true,
    };
  }

  async componentDidMount() {
    this.API = await API.instantiate();

    const { shipmentId } = this.props;

    this.API.get(`shipments/${shipmentId}/mark-as-scanned-after-contacting-recipient`)
      .then((response) => {
        const {
          data: {
            recipientName: customerName,
            recipientPhone: customerPhone,
            recipientAddress: customerLocation,
            description,
            cashToCollectOnDelivery: orderAmount,
          },
        } = response;

        this.setState({
          customerName,
          customerPhone,
          customerLocation,
          description,
          orderAmount,
          isLoading: false,
        });
      })
      .catch(() => {
        this.setState({ isLoading: false });
      });
  }

  submit = () => {
    const { navigation: { state: { params: { shipment } } } } = this.props;
    const shipmentId = shipment.id;
    this.API.get(`/shipments/${shipmentId}/mark-as-scanned-after-contacting-recipient`);
  };

  submitOTP = () => {
    const { otpNumber } = this.state;
    const { navigation: { navigate, state: { params: { shipment } } } } = this.props;
    const shipmentId = shipment.id;
    this.API.get(`/shipments/${shipmentId}/confirm-otp-number`, { otpNumber })
      .then(() => navigate('Dashboard', { updatedJourneyGuru: true }))
      .catch();
  };

  render() {
    const { shipmentId } = this.props;
    const {
      customerName,
      customerPhone,
      customerLocation,
      description,
      paymentType,
      orderAmount,
      otpNumber,
      isLoading,
    } = this.state;

    const information = [
      { key: 0, label: 'Customer name', value: <Text style={styles.valueText}>{customerName}</Text> },
      { key: 1, label: 'Customer phone', value: <Text style={styles.valueText}>{customerPhone}</Text> },
      { key: 2, label: 'Customer location', value: <Text style={styles.valueText}>{customerLocation}</Text> },
      { key: 3, label: 'Description', value: <Text style={styles.valueText}>{description}</Text> },
      {
        key: 4,
        label: 'Order Amount',
        value: (
          <View>
            <Text style={styles.moneyText}>{`${orderAmount.toFixed(2)} SAR`}</Text>
            <Text style={styles.valueText}>{paymentType}</Text>
          </View>
        ),
      },
    ];

    return (
      <ScrollView>
        <View style={styles.container}>
          <View style={styles.scanStatusView}>
            <Icon name="check" size={16} color="#76BC21" />
            <Text style={styles.scanStatus}>SCAN COMPLETE</Text>
          </View>
          <View style={styles.rectangleBox}>
            <View style={styles.qrBox}>
              <QRCode
                value={shipmentId}
                size={140}
                bgColor="black"
                fgColor="white"
              />
            </View>
            <View>
              {
                information.map((info, index) => (
                  <View
                    key={info.key}
                    style={[styles.entry, index !== (information.length - 1) && styles.entryBorder]}
                  >
                    <View style={styles.propertyView}>
                      <Text style={styles.propertyText}>{info.label}</Text>
                    </View>
                    <View style={styles.valueView}>
                      {info.value}
                    </View>
                  </View>
                ))
              }
            </View>
            <TouchableOpacity style={styles.optButton} onPress={this.submitOTP}>
              <TextInput
                autoCapitalize="none"
                placeholder="Enter OPT code"
                style={styles.inputComponent}
                underlineColorAndroid="transparent"
                keyboardType="number-pad"
                value={otpNumber}
                autoCorrect={false}
                onChangeText={(num) => { this.setState({ otpNumber: num }); }}
              />
              <Text style={styles.optButtonText}>Enter OPT Code</Text>
            </TouchableOpacity>
          </View>
          {
            // Only shows confirmation button if OTP number is entered
            otpNumber
            && (
              <TouchableOpacity style={styles.confirmButton} onPress={this.submit}>
                <Text style={styles.buttonText}>Confirm Delivery</Text>
              </TouchableOpacity>
            )
          }
        </View>
        <Loader visible={isLoading} />
      </ScrollView>
    );
  }
}

DeliveryConfirmation.propTypes = {
  navigation: PropTypes.shape({
    shipmentId: PropTypes.number.isRequired,
  }).isRequired,
};
