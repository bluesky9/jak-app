import React from 'react';
import {
  ScrollView, Text, TouchableOpacity, View,
} from 'react-native';
import QRCode from 'react-native-qrcode';
import Icon from 'react-native-vector-icons/MaterialIcons';
import PropTypes from 'prop-types';
import API from '../../components/API/API';
import Loader from '../../components/Loader';
import styles from './Pickup.style';
import { withAuth } from '../../components/Auth/AuthProvider';

class PickupConfirmation extends React.Component {
  static propTypes = {
    shipmentId: PropTypes.number,
  }

  static defaultProps = {
    shipmentId: 1,
  }

  static navigationOptions = {
    title: 'Confirm Pick-up',
    headerBackTitle: null,
    headerTintColor: 'white',
    headerBackStyle: {
      color: 'white',
    },
    headerTitleStyle: {
      color: 'white',
    },
    headerStyle: {
      backgroundColor: '#ED174B',
      shadowColor: 'transparent',
      elevation: 0,
      shadowOpacity: 0,
    },
  };

  constructor(props) {
    super(props);

    this.state = {
      customerName: '',
      customerPhone: '',
      customerLocation: '',
      description: '',
      orderAmount: 0,
      paymentType: '(Cash - Pay at pick-up)',
      isLoading: true,
    };
  }

  async componentDidMount() {
    this.API = await API.instantiate();

    const { navigation: { state: { params: { shipment } } } } = this.props;
    const {
      recipientName: customerName,
      recipientPhone: customerPhone,
      recipientAddress: customerLocation,
      description,
      cashToCollectOnDelivery: orderAmount,
    } = shipment;

    this.setState({
      customerName,
      customerPhone,
      customerLocation,
      description,
      orderAmount,
      isLoading: false,
    });
  }

  submit = () => {
    const { navigation } = this.props;
    const { navigate } = navigation;
    const { state: { params: { shipmentId } } } = navigation;
    this.API.get(`/shipments/${shipmentId}/mark-as-scanned-before-pickup`)
      .then(() => navigate('Dashboard', { updatedJourneyGuru: true }))
      .catch();
  };

  render() {
    const { shipmentId } = this.props;
    const {
      customerName,
      customerPhone,
      customerLocation,
      description,
      paymentType,
      orderAmount,
      isLoading,
    } = this.state;

    const information = [
      { key: 0, label: 'Customer name', value: <Text style={styles.valueText}>{customerName}</Text> },
      { key: 1, label: 'Customer phone', value: <Text style={styles.valueText}>{customerPhone}</Text> },
      { key: 2, label: 'Customer location', value: <Text style={styles.valueText}>{customerLocation}</Text> },
      { key: 3, label: 'Description', value: <Text style={styles.valueText}>{description}</Text> },
      {
        key: 4,
        label: 'Order Amount',
        value: (
          <View>
            <Text style={styles.moneyText}>{`${orderAmount.toFixed(2)} SAR`}</Text>
            <Text style={styles.valueText}>{paymentType}</Text>
          </View>
        ),
      },
    ];

    return (
      <ScrollView>
        <View style={styles.container}>
          <View style={styles.scanStatusView}>
            <Icon name="check" size={16} color="#ED174B" />
            <Text style={styles.scanStatus}>SCAN COMPLETE</Text>
          </View>
          <View style={styles.rectangleBox}>
            <View style={styles.qrBox}>
              <QRCode
                value={shipmentId}
                size={140}
                bgColor="black"
                fgColor="white"
              />
            </View>
            <View>
              {
                information.map((info, index) => (
                  <View
                    key={info.key}
                    style={[styles.entry, index !== (information.length - 1) && styles.entryBorder]}
                  >
                    <View style={styles.propertyView}>
                      <Text style={styles.propertyText}>{info.label}</Text>
                    </View>
                    <View style={styles.valueView}>
                      {info.value}
                    </View>
                  </View>
                ))
              }
            </View>
          </View>
          <TouchableOpacity style={styles.confirmButton} onPress={this.submit}>
            <Text style={styles.buttonText}>Confirm & Start Delivery Route</Text>
          </TouchableOpacity>
        </View>
        <Loader visible={isLoading} />
      </ScrollView>
    );
  }
}

PickupConfirmation.propTypes = {
  navigation: PropTypes.shape({
    state: PropTypes.shape({
      params: PropTypes.shape({
        shipmentId: PropTypes.number.isRequired,
      }),
    }),
  }).isRequired,
};

export default withAuth(PickupConfirmation);
