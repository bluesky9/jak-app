import React, { Component } from 'react';
import { Alert, FlatList, View } from 'react-native';
import { SecureStore } from 'expo';
import OfferCard from '../../components/OfferCard/OfferCard';
import styles from './RouteOffers.style';
import API from '../../components/API/API';
import Loader from '../../components/Loader';
import { withAuth, AuthProvider } from '../../components/Auth/AuthProvider';

class RouteOffers extends Component {
  constructor(props) {
    super(props);

    this.state = {
      routeOffers: [],
      isLoading: true,
    };

    this.toggleOfferCard = index => index;

    this.onOfferAccept = async (offer) => {
      this.setState({ isLoading: true });

      const { id: routeId } = offer;
      const user = await new AuthProvider().getUser();
      const { agentId } = user;

      this.API.get(`/agents/${agentId}/accept-route-offer/${routeId}`)
        .then(() => {
          this.setState({ isLoading: false });
          const { navigation } = this.props;
          const { navigate } = navigation;
          navigate('Dashboard', { routeId });
        })
        .catch((errorResponse) => {
          this.setState({ isLoading: false });
          Alert.alert(
            errorResponse.response.data.error.name,
            errorResponse.response.data.error.message,
            [{ text: 'OK', onPress: () => {} }],
            { cancelable: false },
          );
        });
    };

    this.onOfferIgnore = async (offer) => {
      const { id: routeId } = offer;
      const { routeOffers } = this.state;

      let listOfIgnoredRouteOffers = await SecureStore.getItemAsync(
        'listOfIgnoredRouteOffers',
      );
      listOfIgnoredRouteOffers = listOfIgnoredRouteOffers
        ? JSON.parse(listOfIgnoredRouteOffers)
        : [];
      listOfIgnoredRouteOffers.push(routeId);
      SecureStore.setItemAsync(
        'listOfIgnoredRouteOffers',
        JSON.stringify(listOfIgnoredRouteOffers),
      );
      const offers = routeOffers.filter(
        routeOffer => !listOfIgnoredRouteOffers.find((routeIdBeingSearched) => {
          const routeIdIgnored = parseInt(routeIdBeingSearched, 10);
          const routeOfferId = parseInt(routeOffer.id, 10);
          return routeIdIgnored === routeOfferId;
        }),
      );
      this.setState({ routeOffers: offers });
    };

    this.onPressOverview = async (offer) => {
      const { id: routeId } = offer;
      const {
        navigation: { navigate },
      } = this.props;
      navigate('RouteOverview', { routeId });
    };

    this.checkAgentTags = (agentTags, requiredAgentTags) => {
      let matchesCount = 0;

      for (let i = 0; i < agentTags.length; i += 1) {
        for (let j = 0; j < requiredAgentTags.length; j += 1) {
          if (requiredAgentTags[j].id === agentTags[i].id) {
            matchesCount += 1;
          }
        }
      }

      if (matchesCount > 0) {
        return true;
      }

      return false;
    };
  }

  async componentDidMount() {
    this.API = await API.instantiate();

    const agent = await new AuthProvider().getAgent();

    const routes = await this.API.get(`/agents/${agent.id}/route-offers`);

    let listOfIgnoredRouteOffers = await SecureStore.getItemAsync(
      'listOfIgnoredRouteOffers',
    );

    listOfIgnoredRouteOffers = listOfIgnoredRouteOffers
      ? JSON.parse(listOfIgnoredRouteOffers)
      : [];

    const routeOffers = routes.data
      // Hide offers the agent has ignored before.
      .filter(
        routeOffer => !listOfIgnoredRouteOffers.find(
          routeId => parseInt(routeId, 10) === parseInt(routeOffer.id, 10),
        ),
      );

    this.setState({
      routeOffers,
      isLoading: false,
    });
  }

  render() {
    const { routeOffers, isLoading } = this.state;

    return (
      <View style={styles.container}>
        <FlatList
          style={styles.container}
          contentContainerStyle={styles.contentContainerStyle}
          data={routeOffers}
          keyExtractor={offer => String(offer.id)}
          renderItem={({ item: offer }) => (
            <OfferCard
              onPressAccept={this.onOfferAccept}
              onPressIgnore={this.onOfferIgnore}
              onPressOverview={this.onPressOverview}
              offer={offer}
              key={offer.id}
            />
          )}
        />
        <Loader visible={isLoading} />
      </View>
    );
  }
}

export default withAuth(RouteOffers);
