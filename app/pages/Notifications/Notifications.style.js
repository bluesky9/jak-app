import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  headerRightButton: {
    position: 'absolute',
    marginTop: 0,
    marginLeft: 0,
    width: 16,
    height: 16,
    borderRadius: 8,
    borderWidth: 1,
    borderColor: '#FFF',
    backgroundColor: '#ED174B',
  },
  headerRightCountText: {
    fontSize: 11,
    lineHeight: 14,
    textAlign: 'center',
    textAlignVertical: 'center',
    color: '#FFFFFF',
  },
  headerRightButtonContainer: {
    height: 30,
    width: 32,
    paddingLeft: 2,
  },
  list: {
    flex: 1,
  },
  listContentContainer: {
    padding: 20,
  },
});
