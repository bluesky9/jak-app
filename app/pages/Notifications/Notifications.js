import React from 'react';
import {
  View, FlatList, Platform, Text,
} from 'react-native';
import moment from 'moment';
import Icon from 'react-native-vector-icons/MaterialIcons';
import PropTypes from 'prop-types';
import NotificationCard from '../../components/NotificationCard/NotificationCard';
import { AuthProvider } from '../../components/Auth/AuthProvider';
import API from '../../components/API/API';
import Loader from '../../components/Loader';
import styles from './Notifications.style';

class Notifications extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      data: [],
      isLoading: true,
    };

    this.onPressNotification = (notification) => {
      if (notification.hasBeenRead) return;

      this.setState({ isLoading: true });
      const { navigation } = this.props;

      this.API.get(`notifications/${notification.id}/mark-as-read`)
        .then(() => {
          const { data } = this.state;
          const updatedNotifications = data.map(
            not => (not.id === notification.id ? { ...not, hasBeenRead: true } : not),
          );
          const count = updatedNotifications.filter(item => !item.hasBeenRead)
            .length;
          navigation.setParams({ notificationCount: count });
          this.setState({ data: updatedNotifications, isLoading: false });
        })
        .catch(() => {
          this.setState({ isLoading: false });
        });
    };
  }

  async componentDidMount() {
    try {
      this.setState({ isLoading: true });
      const { navigation } = this.props;
      this.API = await API.instantiate();
      const agent = await new AuthProvider().getAgent();
      const response = await this.API.get(`/agents/${agent.id}/notifications`);
      const { data } = response;
      data.forEach((entry) => {
        const notification = entry;
        notification.time = moment(notification.createdAt).format('hh:mm A');
        notification.date = moment(notification.createdAt).format('DD/MM/YYYY');
      });
      data.sort((first, second) => {
        if (first.createdAt < second.createdAt) {
          return Platform.OS === 'android' ? 1 : true;
        }
        return Platform.OS === 'android' ? -1 : false;
      });
      const count = data.filter(item => !item.hasBeenRead).length;
      navigation.setParams({ notificationCount: count });
      this.setState({ data, isLoading: false });
    } catch (error) {
      this.setState({ isLoading: false });
    }
  }

  render() {
    const { data, isLoading } = this.state;
    return (
      <View style={styles.container}>
        <FlatList
          style={styles.list}
          contentContainerStyle={styles.listContentContainer}
          data={data}
          keyExtractor={item => String(item.id)}
          renderItem={({ item }) => (
            <NotificationCard
              data={item}
              onPressNotification={this.onPressNotification}
            />
          )}
        />
        <Loader visible={isLoading} />
      </View>
    );
  }
}

Notifications.propTypes = {
  navigation: PropTypes.shape({
    setParams: PropTypes.func.isRequired,
  }).isRequired,
};

Notifications.navigationOptions = ({
  navigation: {
    state: { params },
  },
}) => ({
  headerRight:
    params && params.notificationCount ? (
      <View style={styles.headerRightButtonContainer}>
        <Icon name="notifications" size={30} color="#ED174B" />
        <View style={styles.headerRightButton}>
          <Text style={styles.headerRightCountText}>
            {params.notificationCount}
          </Text>
        </View>
      </View>
    ) : (
      <View style={styles.headerRightButtonContainer}>
        <Icon name="notifications-none" size={30} color="#888" />
      </View>
    ),
});

export default Notifications;
