import React from 'react';
import { View, Text, Button } from 'react-native';
import styles from './APIExample.style';
import API from '../../components/API/API';
import Loader from '../../components/Loader';

export default class APIExample extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      response: null,
      isLoading: false,
    };

    this.apiCall = () => {
      const data = {
        username: 'admin',
        password: '123',
      };

      this.setState({ isLoading: true });

      this.API.post('/users/login', data).then((response) => {
        this.setState({
          response: response.data,
          isLoading: false,
        });
      });
    };
  }

  async componentDidMount() {
    this.API = await API.instantiate();
  }

  render() {
    const { response, isLoading } = this.state;
    return (
      <View style={styles.container}>
        <Text>Press the button below</Text>
        <Button
          title="Call API"
          onPress={this.apiCall}
        />
        {response && (
          <Text>
            {response.id}
          </Text>
        )}
        <Loader visible={isLoading} />
      </View>
    );
  }
}
