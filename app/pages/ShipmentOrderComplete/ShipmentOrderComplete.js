import React from 'react';
import {
  View, Text, Image, TouchableOpacity, ImageBackground,
} from 'react-native';
import PropTypes from 'prop-types'; //
import styles from './ShipmentOrderComplete.style';
import icDoneImg from '../../assets/img/ic_done.png';
import icShapesImg from '../../assets/img/ic_shapes_bg.png';

const ShipmentOrderComplete = props => (
  <ImageBackground
    source={icShapesImg}
    style={styles.imgBackground}
    resizeMode="center"
  >
    <View style={styles.container}>
      <Image
        style={styles.icDone}
        source={icDoneImg}
      />
      <Text style={styles.wellDoneTxt}>Well Done!</Text>
      <Text style={styles.routeCompleteTxt}>Route is complete</Text>
      <TouchableOpacity
        style={styles.goToDashboardBtn}
        onPress={() => props.navigation.navigate('PageOne')}
      >
        <Text style={styles.goToDashboardTxt}>Go to Dashboard</Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.viewPendingOrdersBtn}
        onPress={() => props.navigation.navigate('PageOne')}
      >
        <Text style={styles.viewPendingOrdersTxt}>View Pending Orders</Text>
      </TouchableOpacity>
    </View>
  </ImageBackground>
);

ShipmentOrderComplete.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func.isRequired,
  }).isRequired,
};


export default ShipmentOrderComplete;
