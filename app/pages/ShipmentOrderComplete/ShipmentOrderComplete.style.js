import { StyleSheet } from 'react-native';
import variables from '../../assets/styles/variables';

export default StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  imgBackground: {
    backgroundColor: 'white',
    width: '100%',
    height: '100%',
  },
  icDone: {
    width: 100,
    height: 100,
    marginBottom: 10,
  },
  wellDoneTxt: {
    color: variables.colors.magenta,
    height: 40,
    lineHeight: 40,
    fontSize: 36,
    fontFamily: 'dubai-bold',
    marginTop: 20,
  },
  routeCompleteTxt: {
    color: variables.colors.purple,
    height: 27,
    lineHeight: 27,
    fontSize: 16,
    fontFamily: 'dubai-medium',
    marginBottom: 20,
  },
  goToDashboardBtn: {
    backgroundColor: variables.colors.purple,
    padding: 10,
    alignItems: 'center',
    borderRadius: 22.5,
    height: 45,
    width: 320,
    marginTop: 20,
  },
  goToDashboardTxt: {
    color: 'white',
    fontSize: 16,
    fontFamily: 'dubai-bold',
  },
  viewPendingOrdersBtn: {
    backgroundColor: 'white',
    padding: 10,
    alignItems: 'center',
    borderRadius: 22.5,
    height: 45,
    width: 320,
    marginTop: 20,
    borderWidth: 1,
    borderStyle: 'solid',
    borderColor: variables.colors.purple,
  },
  viewPendingOrdersTxt: {
    color: variables.colors.purple,
    fontSize: 16,
    fontFamily: 'dubai-medium',
  },
});
