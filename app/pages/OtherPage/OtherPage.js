import React from 'react';
import { View, Text } from 'react-native';
import styles from './OtherPage.style';

const OtherPage = () => (
  <View style={styles.container}>
    <Text>Other page</Text>
  </View>
);

export default OtherPage;
