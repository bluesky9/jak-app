import React from 'react';
import { View, Text, FlatList } from 'react-native';
import moment from 'moment';
import T from 'i18n-react';
import API from '../../components/API/API';
import { AuthProvider } from '../../components/Auth/AuthProvider';
import Loader from '../../components/Loader';
import Picker from '../../components/Picker';
import styles from './FinancialDetails.style';

class FinancialDetails extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      filterOptions: [
        {
          value: '1000000',
          label: T.translate('financials.details.filter.viewAll'),
        },
        { value: '1', label: T.translate('financials.details.filter.last24h') },
        { value: '3', label: T.translate('financials.details.filter.last3d') },
        {
          value: '7',
          label: T.translate('financials.details.filter.lastWeek'),
        },
        {
          value: 'currentMonth',
          label: T.translate('financials.details.filter.currentMonth'),
        },
        {
          value: 'lastMonth',
          label: T.translate('financials.details.filter.lastMonth'),
        },
        { value: 'year', label: T.translate('financials.details.filter.year') },
      ],
      selectedFilter: {
        value: '1000000',
        label: T.translate('financials.details.filter.viewAll'),
      },
      earnings: [],
      isLoading: false,
    };

    this.filter2date = (value) => {
      if (!value) {
        return undefined;
      }

      if (value === 'currentMonth') {
        return moment
          .utc()
          .date(1)
          .toDate();
      }

      if (value === 'lastMonth') {
        return moment
          .utc()
          .subtract(1, 'months')
          .date(1)
          .toDate();
      }

      if (value === 'year') {
        return moment
          .utc()
          .subtract(1, 'years')
          .month(1)
          .date(1)
          .toDate();
      }

      return moment
        .utc()
        .subtract(value, 'days')
        .toDate();
    };

    this.validate = () => {
      const { oldPassword, newPassword, retypePassword } = this.state;

      if (oldPassword === newPassword) return -1;

      if (newPassword !== retypePassword) return -2;

      return 1;
    };
  }

  async componentDidMount() {
    this.API = await API.instantiate();

    const agent = await new AuthProvider().getAgent();
    const agentType = await this.API.get(`/agents/${agent.id}/type`);

    this.setState({ isLoading: true });

    this.API.get(`/agents/${agent.id}/routes`, {
      params: {
        filter: {
          include: ['agent', 'status', 'bundle'],
        },
      },
    })
      .then((response) => {
        const temps = [];
        response.data.forEach((data) => {
          let routeValue;

          if (agentType.name === 'Freelancer') {
            if (data.value !== null) {
              routeValue = data.shipments.length * agent.agreedCommissionPerShipment;
            } else {
              routeValue = data.value;
            }
          } else {
            /* Full-timer */
            routeValue = data.totalCoD;
          }

          const date = new Date(data.updatedAt);
          const temp = {
            type: data.bundle.name,
            amount: routeValue,
            status: data.status.name,
            date: moment(date).format('DD/MM/YYYY'),
          };
          temps.push(temp);
        });
        this.setState({ earnings: temps, isLoading: false });
      })
      .catch(() => {
        this.setState({ isLoading: false });
      });
  }

  render() {
    const {
      earnings, filterOptions, isLoading, selectedFilter,
    } = this.state;
    let totalAmount = 0;
    const deadline = this.filter2date(selectedFilter.value);

    const filteredEarnings = earnings.filter((item) => {
      if (moment(item.date, 'DD/MM/YYYY').isSameOrAfter(deadline)) {
        return true;
      }
      return false;
    });
    if (filteredEarnings.length > 0) {
      totalAmount = filteredEarnings
        .map(data => parseFloat(data.amount))
        .reduce((acc, curr) => acc + curr);
    }

    return (
      <View style={styles.container}>
        <View style={styles.earningsTop}>
          <Text style={styles.earningsText}>{`${totalAmount} SAR`}</Text>
        </View>

        <View style={styles.pickerContainer}>
          <Text style={styles.filterText}>
            {T.translate('financials.details.filterBy')}
          </Text>
          <Picker
            items={filterOptions}
            selectedItem={selectedFilter}
            onValueChange={selectedFil => this.setState({ selectedFilter: selectedFil })
            }
          />
        </View>
        <FlatList
          style={styles.list}
          data={filteredEarnings}
          ListHeaderComponent={() => <View style={styles.divider} />}
          ListFooterComponent={() => <View style={styles.divider} />}
          ItemSeparatorComponent={() => <View style={styles.divider} />}
          keyExtractor={item => `${item.type} ${item.amount} ${item.status} ${item.date}`
          }
          renderItem={({ item }) => (
            <View style={styles.boxes}>
              <View>
                <Text style={styles.typeText}>{item.type}</Text>
                <Text style={styles.statusText}>
                  {`${item.status}    ${item.date}`}
                </Text>
              </View>
              <View>
                <Text style={styles.moneyText}>{`${item.amount} SAR`}</Text>
              </View>
            </View>
          )}
        />
        <Loader visible={isLoading} />
      </View>
    );
  }
}

FinancialDetails.navigationOptions = () => ({
  title: T.translate('financials.details.title'),
  headerTintColor: 'white',
  headerBackStyle: {
    color: 'white',
  },
  headerTitleStyle: {
    color: 'white',
  },
  headerStyle: {
    backgroundColor: '#ED174B',
    shadowColor: 'transparent',
    elevation: 0,
    shadowOpacity: 0,
    borderBottomWidth: 0,
  },
});

export default FinancialDetails;
