import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    flexDirection: 'column',
    backgroundColor: 'white',
  },
  earningsTop: {
    width: '100%',
    height: 100,
    backgroundColor: '#ED174B',
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 10,
  },
  earningsText: {
    color: 'white',
    fontSize: 36,
    lineHeight: 36,
    fontWeight: 'bold',
  },
  pickerContainer: {
    width: '100%',
    paddingHorizontal: 20,
    marginBottom: 20,
  },
  filterText: {
    color: '#969696',
    fontSize: 14,
    fontFamily: 'dubai-regular',
    lineHeight: 24,
    height: 24,
    paddingLeft: 10,
  },
  typeText: {
    marginBottom: 6,
    color: '#2E1A46',
    fontSize: 14,
    fontWeight: 'bold',
    paddingLeft: 20,
  },
  statusText: {
    color: '#646464',
    fontSize: 14,
    lineHeight: 16,
    paddingLeft: 20,
  },
  list: {},
  divider: {
    height: 1,
    backgroundColor: '#E6E6E6',
  },
  boxes: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
    height: 80,
    paddingVertical: 19,
    borderStyle: 'solid',
  },
  moneyText: {
    color: '#2E1A46',
    fontSize: 14,
    fontWeight: 'bold',
    lineHeight: 16,
    paddingRight: 20,
  },
});
