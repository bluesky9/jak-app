import React from 'react';
import PropTypes from 'prop-types';
import { View, TouchableOpacity, Text } from 'react-native';
import T from 'i18n-react';
import styles from './Financials.style';
import API from '../../components/API/API';
import Loader from '../../components/Loader';
import { AuthProvider } from '../../components/Auth/AuthProvider';

class Financials extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      totalEarnings: 0,
      cashAtHand: 0,
      totalTransactions: 0,
      isLoading: true,
    };
  }

  async componentDidMount() {
    this.API = await API.instantiate();
    const agent = await new AuthProvider().getAgent();

    Promise.all([
      this.API.get(`/agents/${agent.id}/total-earnings`),
      this.API.get(`/agents/${agent.id}/cash-at-hand`),
      this.API.get(`/agents/${agent.id}/total-transactions`),
    ])
      .then((results) => {
        const [
          totalEarningsRequest,
          cashAtHandRequest,
          totalTransactionsRequest,
        ] = results;
        this.setState({
          totalEarnings: totalEarningsRequest.data,
          cashAtHand: cashAtHandRequest.data,
          totalTransactions: totalTransactionsRequest.data,
          isLoading: false,
        });
      })
      .catch(() => {
        this.setState({ isLoading: false });
      });
  }

  render() {
    const {
      totalEarnings,
      cashAtHand,
      totalTransactions,
      isLoading,
    } = this.state;
    const { navigation } = this.props;
    const { navigate } = navigation;

    return (
      <View style={styles.container}>
        <TouchableOpacity
          style={[styles.content, styles.earning]}
          onPress={() => navigate('FinancialDetails')}
        >
          <Text style={styles.label}>
            {T.translate('financials.summary.myEarnings')}
          </Text>
          <Text style={styles.money}>{`${totalEarnings} SAR`}</Text>
        </TouchableOpacity>
        <TouchableOpacity style={[styles.content, styles.cashAtHand]}>
          <Text style={styles.label}>
            {T.translate('financials.summary.cashAtHand')}
          </Text>
          <Text style={styles.money}>{`${cashAtHand} SAR`}</Text>
        </TouchableOpacity>
        <TouchableOpacity style={[styles.content, styles.transaction]}>
          <Text style={styles.label}>
            {T.translate('financials.summary.totalTransactions')}
          </Text>
          <Text style={styles.money}>{`${totalTransactions} SAR`}</Text>
        </TouchableOpacity>
        <Loader visible={isLoading} />
      </View>
    );
  }
}

Financials.propTypes = PropTypes.shape({
  navigation: PropTypes.object.isRequired,
}).isRequired;

export default Financials;
