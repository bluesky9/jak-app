import { StyleSheet } from 'react-native';
import variables from '../../assets/styles/variables';

export default StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    paddingTop: 20,
    paddingHorizontal: 11,
  },
  content: {
    width: '100%',
    borderRadius: 5,
    shadowOffset: { width: 0, height: 3 },
    shadowRadius: 4,
    shadowColor: 'rgba(0, 0, 0, 0.25)',
    shadowOpacity: 1.0,
    marginBottom: 20,
    paddingLeft: 20,
    paddingBottom: 19,
    paddingTop: 27,
  },
  earning: {
    backgroundColor: variables.colors.magenta,
  },
  cashAtHand: {
    backgroundColor: variables.colors.limeGreen,
  },
  transaction: {
    backgroundColor: variables.colors.lightBlue,
  },
  label: {
    color: 'white',
    fontSize: 15,
    fontFamily: 'dubai-bold',
    marginBottom: 2,
  },
  money: {
    color: 'white',
    fontSize: 28,
    fontFamily: 'dubai-bold',
  },
});
