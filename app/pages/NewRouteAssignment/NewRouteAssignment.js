import React from 'react';
import { View, Button } from 'react-native';
import styles from './NewRouteAssignment.style';
import NewRouteAssignmentModal from '../../components/Modals/NewRouteAssignmentModal';
import API from '../../components/API/API';
import Loader from '../../components/Loader';
import { withAuth } from '../../components/Auth/AuthProvider';

const routeId = 1;

class NewRouteAssignment extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      modalVisible: false,
      currentItem: {},
      isLoading: true,
    };

    this.openModal = () => {
      this.setState({ modalVisible: true });
    };

    this.closeModal = () => {
      this.setState({ modalVisible: false });
    };
  }

  async componentDidMount() {
    this.API = await API.instantiate();

    this.API.get(`/routes/${routeId}`, {
      params: {
        filter: {
          include: ['shipments'],
        },
      },
    })
      .then((response) => {
        this.setState({
          currentItem: response.data,
          isLoading: false,
        });
      })
      .catch(() => {
        this.setState({ isLoading: false });
      });
  }

  render() {
    const { modalVisible, currentItem, isLoading } = this.state;
    return (
      <View style={styles.container}>
        <Button
          title="Open New Route Assignment Detail"
          onPress={this.openModal}
        />
        <NewRouteAssignmentModal
          visible={modalVisible}
          route={currentItem}
          onDismissModal={this.closeModal}
        />
        <Loader visible={isLoading} />
      </View>
    );
  }
}

export default withAuth(NewRouteAssignment);
