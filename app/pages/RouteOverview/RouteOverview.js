import React, { Component } from 'react';
import { Dimensions, View } from 'react-native';
import { MapView } from 'expo';
import MapViewDirections from 'react-native-maps-directions';
import FIcon from 'react-native-vector-icons/FontAwesome';
import { GOOGLE_MAPS_API_KEY } from 'react-native-dotenv';
import styles from './RouteOverview.style';
import API from '../../components/API/API';
import Loader from '../../components/Loader';

const { width, height } = Dimensions.get('window');

class RouteOverview extends Component {
  constructor(props) {
    super(props);

    this.state = {
      latitude: 24.7136,
      longitude: 46.6753,
      coordinates: [],
      wayPoints: [],
      isLoading: true,
    };
  }

  async componentDidMount() {
    this.API = await API.instantiate();
    const {
      navigation: {
        state: {
          params: { routeId },
        },
      },
    } = this.props;

    this.API.get(`/routes/${routeId}/ordered-waypoints`)
      .then((response) => {
        const coordinates = response.data.map(wayPoint => ({
          latitude: wayPoint.lat,
          longitude: wayPoint.lng,
        }));

        this.setState({
          coordinates,
          wayPoints: response.data,
          isLoading: false,
        });
      })
      .catch(() => {
        this.setState({ isLoading: false });
      });
  }

  render() {
    const {
      latitude,
      longitude,
      isLoading,
      coordinates,
      wayPoints,
    } = this.state;

    return (
      <View style={styles.container}>
        <MapView
          ref={(ref) => {
            this.mapView = ref;
          }}
          style={styles.map}
          provider={MapView.PROVIDER_GOOGLE}
          showsMyLocationButton
          loadingEnabled
          initialRegion={{
            latitude,
            longitude,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
          }}
        >
          {wayPoints.map(wayPoint => (
            <MapView.Marker
              key={`${wayPoint.shipmentId} ${wayPoint.lat} ${wayPoint.lng}`}
              coordinate={{
                latitude: wayPoint.lat,
                longitude: wayPoint.lng,
              }}
            >
              {wayPoint.type === 'Pickup' ? (
                <FIcon name="circle" size={12} color="yellow" />
              ) : (
                <FIcon name="circle" size={12} color="green" />
              )}
            </MapView.Marker>
          ))}
          <MapViewDirections
            origin={coordinates[0]}
            waypoints={coordinates.length > 2 ? coordinates.slice(1, -1) : null}
            destination={coordinates[coordinates.length - 1]}
            apikey={GOOGLE_MAPS_API_KEY}
            strokeWidth={3}
            strokeColor="skyblue"
            onReady={(result) => {
              this.mapView.fitToCoordinates(result.coordinates, {
                edgePadding: {
                  right: width / 20,
                  bottom: height / 20,
                  left: width / 20,
                  top: height / 20,
                },
              });
            }}
          />
        </MapView>
        <Loader visible={isLoading} />
      </View>
    );
  }
}

RouteOverview.navigationOptions = {
  title: 'Route Overview',
  headerBackTitle: null,
  headerTintColor: 'white',
  headerBackStyle: {
    color: 'white',
  },
  headerTitleStyle: {
    color: 'white',
  },
  headerStyle: {
    backgroundColor: '#ED174B',
    shadowColor: 'transparent',
    elevation: 0,
    shadowOpacity: 0,
  },
};

export default RouteOverview;
