import React from 'react';
import { createStackNavigator, createDrawerNavigator } from 'react-navigation';
import cacheAssetsAsync from './helpers/cacheAssetsAsync';
import { AuthProvider } from './components/Auth/AuthProvider';
import I18nInitializer from './components/I18n/I18nInitializer';
import SideMenu from './components/SideMenu/SideMenu';
import LoadingScreen from './components/LoadingScreen/LoadingScreen';
import routes from './routes/index';
import assets from './assets/index';
import Login from './pages/Login/Login';
import NavigationService from './components/Navigation/NavigationService';

class App extends React.Component {
  constructor() {
    super();

    this.state = {
      assetsLoaded: false,
    };

    this.loadAssetsAsync = async () => {
      try {
        await cacheAssetsAsync(assets);
      } finally {
        this.setState({ assetsLoaded: true });
      }
    };
  }

  componentWillMount() {
    this.loadAssetsAsync();
    I18nInitializer();
  }

  render() {
    const { assetsLoaded } = this.state;
    if (assetsLoaded) {
      const Root = createStackNavigator({
        Login: {
          screen: Login,
          navigationOptions: {
            header: null,
            headerMode: 'none',
          },
        },
        Drawer: {
          screen: createDrawerNavigator(routes, {
            contentComponent: SideMenu,
          }),
          navigationOptions: {
            title: 'Jak Agent',
            header: null,
            headerMode: 'none',
          },
        },
      });
      return (
        <AuthProvider>
          <Root
            ref={(navigatorRef) => {
              NavigationService.setTopLevelNavigator(navigatorRef);
            }}
          />
        </AuthProvider>
      );
    }
    return <LoadingScreen />;
  }
}

export default App;
