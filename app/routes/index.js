import { MaterialIcons, MaterialCommunityIcons } from '@expo/vector-icons';
import { createStackNavigator } from 'react-navigation';
import T from 'i18n-react';
import HeaderParams from '../components/Header/HeaderParams';
import LightHeader from '../components/Header/LightHeader.style';
import DrawerIcon from '../components/SideMenu/DrawerIcon';
import Dashboard from '../pages/Dashboard/Dashboard';
import RoutesHistory from '../pages/RoutesHistory/RoutesHistory';
import Financials from '../pages/Financials/Financials';
import FinancialDetails from '../pages/Financials/FinancialDetails';
import RouteOffers from '../pages/RouteOffers/RouteOffers';
import RouteOverview from '../pages/RouteOverview';
import MyAccount from '../pages/MyAccount/MyAccount';
import ChangePassword from '../pages/MyAccount/ChangePassword';
import Notifications from '../pages/Notifications/Notifications';
import LanguageSelector from '../pages/LanguageSelector/LanguageSelector';
import Logout from '../pages/Logout/Logout';

export default {
  DashboardStack: {
    screen: createStackNavigator({
      Dashboard: {
        screen: Dashboard,
        navigationOptions: () => ({
          title: T.translate('dashboard.title'),
          ...HeaderParams(LightHeader),
        }),
      },
      RouteOverview: {
        screen: RouteOverview,
      },
    }),
    navigationOptions: () => ({
      title: T.translate('menu.dashboard'),
      drawerIcon: ({ tintColor }) => DrawerIcon(tintColor, MaterialIcons, 'home'),
    }),
  },
  RouteOffersStack: {
    screen: createStackNavigator({
      RouteOffers: {
        screen: RouteOffers,
        navigationOptions: () => ({
          title: T.translate('routeOffers.title'),
          ...HeaderParams(LightHeader),
        }),
      },
      RouteOverview: {
        screen: RouteOverview,
      },
    }),
    navigationOptions: () => ({
      title: T.translate('menu.routeOffers'),
      drawerIcon: ({ tintColor }) => DrawerIcon(tintColor, MaterialCommunityIcons, 'tag'),
    }),
  },
  AccountStack: {
    screen: createStackNavigator({
      MyAccount: {
        screen: MyAccount,
        navigationOptions: () => ({
          title: T.translate('myAccount.title'),
          ...HeaderParams(LightHeader),
        }),
      },
      ChangePassword: {
        screen: ChangePassword,
        navigationOptions: () => ({
          title: T.translate('changePassword.title'),
          ...HeaderParams(LightHeader),
        }),
      },
    }),
    navigationOptions: () => ({
      title: T.translate('menu.myAccount'),
      drawerIcon: ({ tintColor }) => DrawerIcon(tintColor, MaterialIcons, 'account-circle'),
    }),
  },
  RoutesHistoryStack: {
    screen: createStackNavigator({
      RoutesHistory: {
        screen: RoutesHistory,
        navigationOptions: () => ({
          title: T.translate('routesHistory.title'),
          ...HeaderParams(LightHeader),
        }),
      },
      RouteOverview: {
        screen: RouteOverview,
      },
    }),
    navigationOptions: () => ({
      title: T.translate('menu.routesHistory'),
      drawerIcon: ({ tintColor }) => DrawerIcon(tintColor, MaterialIcons, 'history'),
    }),
  },
  FinancialsStack: {
    screen: createStackNavigator({
      Financials: {
        screen: Financials,
        navigationOptions: () => ({
          title: T.translate('financials.title'),
          ...HeaderParams(LightHeader),
        }),
      },
      FinancialDetails: {
        screen: FinancialDetails,
      },
    }),
    navigationOptions: () => ({
      title: T.translate('menu.financials'),
      drawerIcon: ({ tintColor }) => DrawerIcon(tintColor, MaterialIcons, 'account-balance-wallet'),
    }),
  },
  Notifications: {
    screen: createStackNavigator({
      RoutesHistory: {
        screen: Notifications,
        navigationOptions: () => ({
          title: T.translate('notifications.title'),
          ...HeaderParams(LightHeader),
        }),
      },
    }),
    navigationOptions: () => ({
      title: T.translate('menu.notifications'),
      drawerIcon: ({ tintColor }) => DrawerIcon(tintColor, MaterialIcons, 'notifications'),
    }),
  },
  LanguageSelector: {
    screen: LanguageSelector,
    navigationOptions: () => ({
      title: T.translate('menu.toggleLanguage'),
      drawerIcon: ({ tintColor }) => DrawerIcon(tintColor, MaterialIcons, 'language'),
    }),
  },
  Logout: {
    screen: Logout,
    navigationOptions: () => ({
      title: T.translate('menu.logout'),
      drawerIcon: ({ tintColor }) => DrawerIcon(tintColor, MaterialIcons, 'exit-to-app'),
    }),
  },
};
