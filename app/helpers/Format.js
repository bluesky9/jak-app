import moment from 'moment';
import numeral from 'numeral';

class Format {
  static date(dt, format = 'DD/MM/YYYY') {
    return moment(dt).format(format);
  }

  static time(tm, format = 'h:mm A') {
    return moment(tm).format(format);
  }

  static money(value) {
    return numeral(value).format('0.00');
  }
}

export default Format;
