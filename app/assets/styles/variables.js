const colors = {
  white: '#ffffff',
  magenta: '#ed174b',
  lightMagenta: '#ff1a50',
  purple: '#2e1a46',
  lightBlue: '#5fd0df',
  babyBlue: '#f7fbfc',
  orange: '#ffcf01',
  gray: '#969696',
  lightGray: '#F7F6F8',
  limeGreen: '#76bc21',
  redPigment: '#ea2027',
};

export default {
  colors,
  colorContext: {
    primary: colors.magenta,
    secondary: colors.purple,
    success: colors.limeGreen,
    warning: colors.orange,
    info: colors.lightBlue,
    danger: colors.redPigment,
    light: colors.lightGray,
  },
  shadowColor: '#2E1A46',
  lightBackground: '#fafafb',
};
