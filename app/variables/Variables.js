import {
  API_PROTOCOL, API_HOSTNAME, API_PORT, GOOGLE_MAPS_API_KEY,
} from 'react-native-dotenv';

export const apiBaseUrl = `${API_PROTOCOL}://${API_HOSTNAME}${API_PORT ? `:${API_PORT}` : ''}/api`;
export const googleMapsApiKey = GOOGLE_MAPS_API_KEY;

export default {
  apiBaseUrl,
  googleMapsApiKey,
};
