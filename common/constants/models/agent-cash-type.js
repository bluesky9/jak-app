export const AGENT_CASH_TYPE = {
  CASH_COLLECTED: { id: 1, name: "Cash Collected", isProfitForCompany: true },
  INSURANCE: { id: 2, name: "Insurance", isProfitForCompany: true },
  COMMISSION: { id: 3, name: "Commission", isProfitForCompany: false },
  SALARY: { id: 4, name: "Salary", isProfitForCompany: false }
};
