export const SHIPMENT_TYPE = {
  NORMAL: { id: 1, name: "Normal" },
  GENERIC: { id: 2, name: "Generic" },
  WAREHOUSE: { id: 3, name: "Warehouse" }
};
