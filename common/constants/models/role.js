export const ROLE = {
  ADMIN: { id: 1, name: "admin", description: "Has access to everything." },
  OPERATIONS_TEAM_MEMBER: {
    id: 2,
    name: "operationsTeamMember",
    description: "Has access to the Operations Team Panel and Accountant Panel."
  },
  ACCOUNTANT: {
    id: 3,
    name: "accountant",
    description: "Has access to the Accountant Panel."
  },
  AGENT: { id: 4, name: "agent", description: "Has access to the Agent App." },
  PARTNER: {
    id: 5,
    name: "partner",
    description: "Has access to Partner Panel."
  }
};
