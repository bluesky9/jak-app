export const PARTNER_CASH_DIRECTION = {
  INBOUND: { id: 1, name: "Inbound" },
  OUTBOUND: { id: 2, name: "Outbound" }
};
