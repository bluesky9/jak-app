export const AGENT_TYPE = {
  FULL_TIMER: {
    id: 1,
    name: "Full-timer",
    description:
      "Has routes assigned to them without their choice, as long as they are available and within their working hours."
  },
  FREELANCER: {
    id: 2,
    name: "Freelancer",
    description: "Has the freedom to accept or refuse route offers."
  }
};
