export const SETTING = {
  ROUTE_OFFER_START_RADIUS: {
    id: 1,
    name: "Route Offer Start Radius",
    value: "15"
  },
  ROUTE_OFFER_RADIUS_INCREASE_RATE: {
    id: 2,
    name: "Route Offer Radius Increase Rate",
    value: "5"
  },
  ROUTE_OFFER_SECONDS_INTERVAL_TO_INCREASE_RADIUS: {
    id: 3,
    name: "Route Offer Seconds Interval To Increase Radius",
    value: "60"
  },
  ROUTE_OFFER_ACCEPTANCE_TIMEOUT_IN_SECONDS: {
    id: 4,
    name: "Route Offer Acceptance Timeout In Seconds",
    value: "600"
  }
};
