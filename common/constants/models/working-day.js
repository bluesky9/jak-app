export const WORKING_DAY = {
  SUNDAY: { id: 1, name: "Sunday" },
  MONDAY: { id: 2, name: "Monday" },
  TUESDAY: { id: 3, name: "Tuesday" },
  WEDNESDAY: { id: 4, name: "Wednesday" },
  THURSDAY: { id: 5, name: "Thursday" },
  FRIDAY: { id: 6, name: "Friday" },
  SATURDAY: { id: 7, name: "Saturday" }
};
