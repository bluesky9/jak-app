export const AGENT_STATUS = {
  AVAILABLE: { id: 1, name: "Available" },
  NOT_AVAILABLE: { id: 2, name: "Not Available" },
  ON_ROUTE: { id: 3, name: "On Route" }
};
