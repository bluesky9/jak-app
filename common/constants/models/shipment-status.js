export const SHIPMENT_STATUS = {
  CREATED: { id: 1, name: "Created" },
  PICKED_UP: { id: 2, name: "Picked Up" },
  ON_ROUTE: { id: 3, name: "On Route" },
  DELIVERED: { id: 4, name: "Delivered" },
  CANCELED: { id: 5, name: "Canceled" }
};
