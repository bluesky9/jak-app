export const ROUTE_BUNDLE = {
  GENERIC: { id: 1, name: "Generic" },
  BULK: { id: 2, name: "Bulk" },
  MIX: { id: 3, name: "Mix" },
  DIRECTED_BULK: { id: 4, name: "Directed-Bulk" }
};
