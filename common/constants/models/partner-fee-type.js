export const PARTNER_FEE_TYPE = {
  FLAT: { id: 1, name: "Flat" },
  PERCENTAGE: { id: 2, name: "Percentage" }
};
