export const ROUTE_STATUS = {
  CREATED: { id: 1, name: "Created" },
  ASSIGNED: { id: 2, name: "Assigned" },
  COMPLETED: { id: 3, name: "Completed" },
  STARTED: { id: 4, name: "Started" }
};
