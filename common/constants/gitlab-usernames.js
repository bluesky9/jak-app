/**
 * A flat list of developers usernames on GitLab.
 * @type {string[]}
 */
export const GITLAB_USERNAMES = [
  "aliz",
  "apalmer",
  "dmitryg",
  "gfranciscani",
  "kirylk",
  "matheusa",
  "saad",
  "santiagof",
  "thiagop",
  "victorn"
];
