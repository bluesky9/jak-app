export const removeProtocolFromUrl = url => {
  return url.replace(/(^\w+:|^)\/\//, "");
};
