#!/bin/bash
SCRIPT_PATH=$(dirname "$(readlink -f "$0")")
cd "${SCRIPT_PATH}/../../app/" && npm run lint
cd "${SCRIPT_PATH}/../../back-end/" && npm run lint
cd "${SCRIPT_PATH}/../../front-end/operations-team-panel/" && npm run lint
cd "${SCRIPT_PATH}/../../front-end/partners-panel/" && npm run lint
cd "${SCRIPT_PATH}/../../front-end/accountant-panel/" && npm run lint
cd "${SCRIPT_PATH}/../../front-end/pin-location-on-map/" && npm run lint