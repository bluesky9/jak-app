#!/bin/bash
SCRIPT_PATH=$(dirname "$(readlink -f "$0")")
cd "${SCRIPT_PATH}" && git reset --hard && git pull
cd "${SCRIPT_PATH}/../../back-end/" && npm install && npm install ../common && node "./node_scripts/autoupdate-database.js" && pm2 reload ecosystem.config.js
cd "${SCRIPT_PATH}/../../front-end/operations-team-panel/" && (npm install; npm run build)
cd "${SCRIPT_PATH}/../../front-end/partners-panel/" && (npm install; npm run build)
cd "${SCRIPT_PATH}/../../front-end/accountant-panel/" && (npm install; npm run build)
cd "${SCRIPT_PATH}/../../front-end/pin-location-on-map/" && (npm install; npm run build)