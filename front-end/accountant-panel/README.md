# Jak Logistics - Accountant Panel

Welcome to the Accountant Panel.

This project is based on React and makes use of the [Bootstrap 4.1](http://getbootstrap.com/docs/4.1/getting-started/introduction/) and [reactstrap](https://reactstrap.github.io/).

## Quick Start

1. Run: `npm start`
2. Navigate to `http://localhost:3000/`
