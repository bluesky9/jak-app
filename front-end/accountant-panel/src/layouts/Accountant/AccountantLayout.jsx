import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Container } from 'reactstrap';

import Header from '../../components/Header/Header';
import Footer from '../../components/Footer/Footer';

export default class AccountantLayout extends Component {
  componentDidUpdate(e) {
    if (e.history && e.history.action === 'PUSH') {
      document.documentElement.scrollTop = 0;
      document.scrollingElement.scrollTop = 0;
      this.mainPanel.scrollTop = 0;
    }
  }

  render() {
    const { children } = this.props;
    return [
      <Header key="accountant-header" />,
      <Container className="accountant-container" key="accountant-container">
        <div style={{ minHeight: 'calc(100vh - 150px)' }} className="py-3">
          {children}
        </div>
        <Footer />
      </Container>,
    ];
  }
}

AccountantLayout.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};

AccountantLayout.defaultProps = {
  children: null,
};
