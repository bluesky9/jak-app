import React, { Component } from 'react';
import { Container, Table, Button } from 'reactstrap';
import { Link } from 'react-router-dom';
import Moment from 'react-moment';
import T from 'i18n-react';
import SweetAlert from 'sweetalert2-react';
import API from '../../components/API/API';
import { Pagination } from '../../components/Pagination/Pagination';

class CashInstancesList extends Component {
  constructor(props) {
    super(props);

    this.API = new API();

    this.pagination = React.createRef();

    this.state = {
      resourceNameOnApi: 'partnerCashInstances',
      cashInstances: [],
      showSettleConfirmation: false,
      selectedCashInstanceToBeSettled: {},
    };

    /**
     * Mark an Cash Instance as settled
     * @param id Cash Instance object ID
     */
    this.markAsSettled = async (id) => {
      const { resourceNameOnApi } = this.state;
      await this.API.patch(`/${resourceNameOnApi}/${id}`, {
        isSettled: true,
      });
      this.pagination.current.fetchItems();
    };
  }

  render() {
    const DIRECTION_INBOUND = 2;

    const {
      cashInstances,
      resourceNameOnApi,
      showSettleConfirmation,
      selectedCashInstanceToBeSettled,
    } = this.state;

    return [
      <Container key="cash-instances-list-container" fluid>
        <h3 className="text-secondary font-weight-bold mt-3">
          {T.translate('cashInstances.list.title')}
        </h3>
        <h4 className="text-secondary font-weight-light mb-3">
          {T.translate('cashInstances.list.caption')}
        </h4>
        <Table responsive striped hover>
          <thead>
            <tr>
              <th>{T.translate('cashInstances.fields.direction')}</th>
              <th>{T.translate('cashInstances.fields.amount')}</th>
              <th>{T.translate('cashInstances.fields.type')}</th>
              <th>{T.translate('cashInstances.fields.dueDatetime')}</th>
              <th>{T.translate('cashInstances.fields.isSettled')}</th>
              <th>{T.translate('cashInstances.fields.partner')}</th>
              <th>{T.translate('cashInstances.fields.shipment')}</th>
              <th>{T.translate('cashInstances.list.actions')}</th>
            </tr>
          </thead>
          <tbody>
            {cashInstances.map(cashInstance => (
              <tr key={cashInstance.id}>
                <td
                  className={`text-nowrap text-${
                    cashInstance.directionId === DIRECTION_INBOUND
                      ? 'success'
                      : 'danger'
                  }`}
                >
                  <i
                    className={`mx-1 fas fa-arrow-${
                      cashInstance.directionId === DIRECTION_INBOUND
                        ? 'down'
                        : 'up'
                    }`}
                  />
                  {T.translate(
                    `cashInstances.fields.directions.${
                      cashInstance.direction.name
                    }`,
                  )}
                </td>
                <td>{cashInstance.amount}</td>
                <td>
                  {T.translate(
                    `cashInstances.fields.types.${cashInstance.type.name}`,
                  )}
                </td>
                <td>
                  <Moment date={cashInstance.dueDatetime} />
                </td>
                <td className="text-center">
                  <i
                    className={`fas fa-${
                      cashInstance.isSettled
                        ? 'check text-success'
                        : 'times text-danger'
                    }`}
                    title={
                      cashInstance.isSettled
                        ? T.translate('defaults.yes')
                        : T.translate('defaults.no')
                    }
                  />
                </td>
                <td>
                  <Link
                    to={`/partners/details/${cashInstance.partner.id}`}
                    title={T.translate(
                      'cashInstances.list.viewPartnerTooltip',
                      {
                        name: cashInstance.partner.name,
                      },
                    )}
                  >
                    {cashInstance.partner.name}
                  </Link>
                </td>
                <td>
                  <Link
                    to={`/shipments/details/${cashInstance.shipmentId}`}
                    title={T.translate(
                      'cashInstances.list.viewShipmentTooltip',
                      {
                        trackingId: cashInstance.shipment.trackingId,
                      },
                    )}
                  >
                    {cashInstance.shipment.trackingId}
                  </Link>
                </td>
                <td className="text-center">
                  <div className="btn-group" role="group">
                    {!cashInstance.isSettled && (
                      <Button
                        color="success"
                        size="sm"
                        title={T.translate('cashInstances.list.settleTooltip')}
                        className={`px-2${
                          cashInstance.isSettled ? ' disabled' : ''
                        }`}
                        onClick={() => {
                          this.setState({
                            selectedCashInstanceToBeSettled: cashInstance,
                            showSettleConfirmation: true,
                          });
                        }}
                      >
                        <i className="fas fa-check fa-fw" />
                      </Button>
                    )}
                    {cashInstance.isSettled && (
                      <Link
                        to={`/invoices/details/${cashInstance.id}`}
                        title={T.translate('cashInstances.list.invoiceTooltip')}
                        className={`px-2 btn btn-sm btn-secondary${
                          !cashInstance.isSettled ? ' disabled' : ''
                        }`}
                      >
                        <i className="fas fa-file-invoice fa-fw" />
                      </Link>
                    )}
                  </div>
                </td>
              </tr>
            ))}
          </tbody>
        </Table>
        <Pagination
          ref={this.pagination}
          resourceNameOnApi={`${resourceNameOnApi}`}
          filter={{
            include: ['direction', 'type', 'shipment', 'partner'],
            order: 'dueDatetime ASC',
          }}
          onItemsReceived={(cashInstanceList) => {
            this.setState({ cashInstances: cashInstanceList });
          }}
        />
      </Container>,
      <SweetAlert
        key="sweet-alert"
        show={showSettleConfirmation}
        title={T.translate('cashInstances.list.settleWarning.title')}
        text={T.translate('cashInstances.list.settleWarning.message')}
        type="warning"
        showCancelButton
        buttonsStyling={false}
        confirmButtonText={T.translate(
          'cashInstances.list.settleWarning.confirmButton',
        )}
        confirmButtonClass="btn btn-primary btn-lg btn-rounded px-5 mx-2"
        cancelButtonText={T.translate(
          'cashInstances.list.settleWarning.cancelButton',
        )}
        cancelButtonClass="btn btn-secondary btn-lg btn-rounded px-5 mx-2"
        onConfirm={() => {
          this.markAsSettled(selectedCashInstanceToBeSettled.id);
          this.setState({ showSettleConfirmation: false });
        }}
      />,
    ];
  }
}

export default CashInstancesList;
