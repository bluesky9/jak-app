import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Moment from 'react-moment';
import { Container, Row, Col } from 'reactstrap';
import { Link } from 'react-router-dom';
import T from 'i18n-react';

import API from '../../components/API/API';

export default class PartnerDetails extends Component {
  constructor(props) {
    super(props);

    this.API = new API();

    this.state = {
      resourceNameOnApi: 'partners',
      resource: {},
      hiddenPropertyNamesOnDetail: ['id', 'codFeeTypeId'],
    };
  }

  componentDidMount() {
    const { resourceNameOnApi } = this.state;
    const { match } = this.props;

    this.API.get(`/${resourceNameOnApi}/${match.params.id}`, {
      params: {
        filter: {
          include: ['codFeeType'],
        },
      },
    }).then((response) => {
      this.setState({
        resource: response.data,
      });
    });
  }

  render() {
    const { resource, hiddenPropertyNamesOnDetail } = this.state;

    return (
      <Container fluid>
        <h3 className="text-secondary font-weight-bold mt-3">
          {T.translate('partners.detail.title')}
        </h3>
        <h4 className="text-secondary font-weight-light mb-3">
          {T.translate('partners.detail.caption', { name: resource.name })}
        </h4>
        <hr />
        {Object.keys(resource).map((property) => {
          if (hiddenPropertyNamesOnDetail.includes(property)) {
            return null;
          }

          let propertyValue;

          if (['createdAt', 'updatedAt'].includes(property)) {
            propertyValue = (
              <span>
                <Moment date={resource[property]} />
              </span>
            );
          } else if (property === 'website') {
            propertyValue = (
              <span>
                <a
                  href={resource[property]}
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  {resource[property]}
                </a>
              </span>
            );
          } else if (property === 'codFeeType') {
            propertyValue = (
              <span>
                {T.translate(
                  `partners.fields.codFeeTypes.${resource.codFeeType.name}`,
                )}
              </span>
            );
          } else if (['codIsCollect', 'isActive'].includes(property)) {
            propertyValue = (
              <span>
                {resource[property]
                  ? T.translate('defaults.yes')
                  : T.translate('defaults.no')}
              </span>
            );
          } else {
            propertyValue = (
              <span>
                {resource[property] || T.translate('defaults.notSet')}
              </span>
            );
          }

          return (
            <Row className="mb-3" key={property}>
              <Col md={4} className="font-weight-bold">
                <span>{T.translate(`partners.fields.${property}`)}</span>
              </Col>
              <Col md={8}>{propertyValue}</Col>
            </Row>
          );
        })}
        <hr />
        <div className="clearfix">
          <Link
            to="/cashInstances/list"
            className="btn btn-rounded btn-lg btn-outline-secondary float-md-left d-block d-md-inline-block px-5"
          >
            {T.translate('partners.detail.backButton')}
          </Link>
        </div>
      </Container>
    );
  }
}

PartnerDetails.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string.isRequired,
    }),
  }),
};

PartnerDetails.defaultProps = {
  match: {
    params: {
      id: '',
    },
  },
};
