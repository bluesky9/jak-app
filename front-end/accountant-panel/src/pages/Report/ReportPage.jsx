import React, { Component } from 'react';
import {
  Container,
  Row,
  Col,
  Card,
  CardBody,
  CardTitle,
  CardSubtitle,
  Table,
  FormGroup,
  Label,
  Input,
} from 'reactstrap';
import { Link } from 'react-router-dom';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import Moment from 'react-moment';
import T from 'i18n-react';
import 'react-datepicker/dist/react-datepicker.css';
import API from '../../components/API/API';
import { Pagination } from '../../components/Pagination/Pagination';

class ReportPage extends Component {
  constructor(props) {
    super(props);

    this.API = new API();

    this.pagination = React.createRef();

    this.state = {
      resourceNameOnApi: 'partnerCashInstances',
      cashInstances: [],
      types: [],
      filter: {
        from: moment({ hour: 0, minute: 0 }),
        to: moment({ hour: 23, minute: 59, second: 59 }),
        type: null,
      },
      report: {
        settled: {
          count: '-',
          amount: {
            total: '-',
            inbound: '-',
            outbound: '-',
          },
        },
        notSettled: {
          count: '-',
          amount: {
            total: '-',
            inbound: '-',
            outbound: '-',
          },
        },
      },
    };

    /**
     * Loads from API all available cash instance types, to build up the select options in the form.
     */
    this.loadAvailableTypes = () => {
      this.API.get('/partnerCashTypes').then((response) => {
        this.setState({
          types: response.data,
        });
      });
    };

    /**
     * Loads from API the report with a given cash instance type and due dates.
     */
    this.loadReport = () => {
      const { resourceNameOnApi, filter } = this.state;
      this.API.post(`${resourceNameOnApi}/report`, {
        [filter.type ? 'typeId' : null]: filter.type,
        from: filter.from,
        to: filter.to,
      }).then((response) => {
        this.setState({
          report: response.data,
        });
      });
    };

    /**
     * Handle type select change event
     * @param event
     */
    this.handleChangeType = (event) => {
      const { value } = event.target;
      const { filter } = this.state;
      this.setState(
        {
          filter: {
            ...filter,
            type: value === 'false' ? null : parseInt(value, 10),
          },
        },
        () => {
          this.pagination.current.refreshPagination();
          this.loadReport();
        },
      );
    };

    /**
     * Handle date picker change event
     * @param input
     * @param date
     */
    this.handleDateChange = (input, date) => {
      const { filter } = this.state;
      this.setState(
        {
          filter: {
            ...filter,
            [input]: date,
          },
        },
        () => {
          this.pagination.current.refreshPagination();
          this.loadReport();
        },
      );
    };
  }

  componentDidMount() {
    this.loadAvailableTypes();
    this.loadReport();
  }

  render() {
    const DIRECTION_INBOUND = 2;
    const {
      types,
      filter,
      report,
      cashInstances,
      resourceNameOnApi,
    } = this.state;

    return (
      <Container fluid>
        <h3 className="text-secondary font-weight-bold mt-3">
          {T.translate('reports.title')}
        </h3>
        <h4 className="text-secondary font-weight-light mb-3">
          {T.translate('reports.caption')}
        </h4>
        <Card className="bg-light my-1">
          <CardBody>
            <Row>
              <Col md={4}>
                <FormGroup>
                  <Label>{T.translate('reports.filter.type')}</Label>
                  <Input type="select" onChange={this.handleChangeType}>
                    <option value={false}>
                      {T.translate('reports.filter.allTypes')}
                    </option>
                    {types.map(type => (
                      <option key={type.id} value={type.id}>
                        {T.translate(`cashInstances.fields.types.${type.name}`)}
                      </option>
                    ))}
                  </Input>
                </FormGroup>
              </Col>
              <Col md={4}>
                <FormGroup>
                  <Label>{T.translate('reports.filter.from')}</Label>
                  <DatePicker
                    selected={filter.from}
                    dateFormat="LL"
                    className="form-control"
                    onChange={(date) => {
                      this.handleDateChange('from', date);
                    }}
                  />
                </FormGroup>
              </Col>
              <Col md={4}>
                <FormGroup>
                  <Label>{T.translate('reports.filter.to')}</Label>
                  <DatePicker
                    selected={filter.to}
                    dateFormat="LL"
                    className="form-control"
                    onChange={(date) => {
                      this.handleDateChange('to', date);
                    }}
                  />
                </FormGroup>
              </Col>
            </Row>
          </CardBody>
        </Card>
        <Card>
          <CardBody>
            <Row>
              <Col sm={3}>
                <h1
                  style={{ fontSize: '45px' }}
                  className="float-left mr-3 mb-0"
                >
                  <span className="fa-layers fa-fw">
                    <i className="text-muted fas fa-money-bill-wave" />
                    <i
                      className="text-success fas fa-circle"
                      data-fa-transform="shrink-7 down-5 right-8"
                    />
                    <i
                      className="fas fa-check fa-inverse"
                      data-fa-transform="shrink-10 down-5 right-8"
                    />
                  </span>
                </h1>
                <CardTitle className="h6 font-weight-light text-muted">
                  {T.translate('reports.statistics.settled.count')}
                </CardTitle>
                <CardSubtitle className="h4">
                  {report.settled.count}
                </CardSubtitle>
              </Col>
              <Col sm={3}>
                <h1
                  style={{ fontSize: '45px' }}
                  className="float-left mr-3 mb-0"
                >
                  <span className="fa-layers fa-fw">
                    <i className="text-muted fas fa-money-bill-wave" />
                    <i
                      className="text-success fas fa-circle"
                      data-fa-transform="shrink-7 down-5 right-8"
                    />
                    <i
                      className="fas fa-dollar-sign fa-inverse"
                      data-fa-transform="shrink-10 down-5 right-8"
                    />
                  </span>
                </h1>
                <CardTitle className="h6 font-weight-light text-muted">
                  {T.translate('reports.statistics.settled.amount.total')}
                </CardTitle>
                <CardSubtitle className="h4">
                  {report.settled.amount.total}
                </CardSubtitle>
              </Col>
              <Col sm={3}>
                <h1
                  style={{ fontSize: '45px' }}
                  className="float-left mr-3 mb-0"
                >
                  <span className="fa-layers fa-fw">
                    <i className="text-muted fas fa-money-bill-wave" />
                    <i
                      className="text-danger fas fa-circle"
                      data-fa-transform="shrink-7 down-5 right-8"
                    />
                    <i
                      className="fas fa-times fa-inverse"
                      data-fa-transform="shrink-10 down-5 right-8"
                    />
                  </span>
                </h1>
                <CardTitle className="h6 font-weight-light text-muted">
                  {T.translate('reports.statistics.notSettled.count')}
                </CardTitle>
                <CardSubtitle className="h4">
                  {report.notSettled.count}
                </CardSubtitle>
              </Col>
              <Col sm={3}>
                <h1
                  style={{ fontSize: '45px' }}
                  className="float-left mr-3 mb-0"
                >
                  <span className="fa-layers fa-fw">
                    <i className="text-muted fas fa-money-bill-wave" />
                    <i
                      className="text-danger fas fa-circle"
                      data-fa-transform="shrink-7 down-5 right-8"
                    />
                    <i
                      className="fas fa-dollar-sign fa-inverse"
                      data-fa-transform="shrink-10 down-5 right-8"
                    />
                  </span>
                </h1>
                <CardTitle className="h6 font-weight-light text-muted">
                  {T.translate('reports.statistics.notSettled.amount.total')}
                </CardTitle>
                <CardSubtitle className="h4">
                  {report.notSettled.amount.total}
                </CardSubtitle>
              </Col>
            </Row>
            <hr />
            <Row>
              <Col sm={3}>
                <h1
                  style={{ fontSize: '45px' }}
                  className="float-left mr-3 mb-0"
                >
                  <i className="text-danger fas fa-arrow-alt-circle-up" />
                </h1>
                <CardTitle className="h6 font-weight-light text-muted">
                  {T.translate('reports.statistics.notSettled.amount.outbound')}
                </CardTitle>
                <CardSubtitle className="h4">
                  {report.notSettled.amount.outbound}
                </CardSubtitle>
              </Col>
              <Col sm={3}>
                <h1
                  style={{ fontSize: '45px' }}
                  className="float-left mr-3 mb-0"
                >
                  <i className="text-success fas fa-arrow-alt-circle-up" />
                </h1>
                <CardTitle className="h6 font-weight-light text-muted">
                  {T.translate('reports.statistics.settled.amount.outbound')}
                </CardTitle>
                <CardSubtitle className="h4">
                  {report.settled.amount.outbound}
                </CardSubtitle>
              </Col>
              <Col sm={3}>
                <h1
                  style={{ fontSize: '45px' }}
                  className="float-left mr-3 mb-0"
                >
                  <i className="text-danger fas fa-arrow-alt-circle-down" />
                </h1>
                <CardTitle className="h6 font-weight-light text-muted">
                  {T.translate('reports.statistics.notSettled.amount.inbound')}
                </CardTitle>
                <CardSubtitle className="h4">
                  {report.notSettled.amount.inbound}
                </CardSubtitle>
              </Col>
              <Col sm={3}>
                <h1
                  style={{ fontSize: '45px' }}
                  className="float-left mr-3 mb-0"
                >
                  <i className="text-success fas fa-arrow-alt-circle-down" />
                </h1>
                <CardTitle className="h6 font-weight-light text-muted">
                  {T.translate('reports.statistics.settled.amount.inbound')}
                </CardTitle>
                <CardSubtitle className="h4">
                  {report.settled.amount.inbound}
                </CardSubtitle>
              </Col>
            </Row>
          </CardBody>
        </Card>
        <Table responsive striped hover>
          <thead>
            <tr>
              <th>{T.translate('cashInstances.fields.direction')}</th>
              <th>{T.translate('cashInstances.fields.amount')}</th>
              <th>{T.translate('cashInstances.fields.type')}</th>
              <th>{T.translate('cashInstances.fields.dueDatetime')}</th>
              <th>{T.translate('cashInstances.fields.isSettled')}</th>
              <th>{T.translate('cashInstances.fields.partner')}</th>
              <th>{T.translate('cashInstances.fields.shipment')}</th>
            </tr>
          </thead>
          <tbody>
            {cashInstances.map(cashInstance => (
              <tr key={cashInstance.id}>
                <td
                  className={`text-nowrap text-${
                    cashInstance.directionId === DIRECTION_INBOUND
                      ? 'success'
                      : 'danger'
                  }`}
                >
                  <i
                    className={`mx-2 fas fa-arrow-${
                      cashInstance.directionId === DIRECTION_INBOUND
                        ? 'down'
                        : 'up'
                    }`}
                  />
                  {T.translate(
                    `cashInstances.fields.directions.${
                      cashInstance.direction.name
                    }`,
                  )}
                </td>
                <td>{cashInstance.amount}</td>
                <td>
                  {T.translate(
                    `cashInstances.fields.types.${cashInstance.type.name}`,
                  )}
                </td>
                <td>
                  <Moment date={cashInstance.dueDatetime} />
                </td>
                <td className="text-center">
                  <i
                    className={`fas fa-${
                      cashInstance.isSettled
                        ? 'check text-success'
                        : 'times text-danger'
                    }`}
                    title={
                      cashInstance.isSettled
                        ? T.translate('defaults.yes')
                        : T.translate('defaults.no')
                    }
                  />
                </td>
                <td>
                  <Link
                    to={`/partners/details/${cashInstance.partner.id}`}
                    title={T.translate(
                      'cashInstances.list.viewPartnerTooltip',
                      {
                        name: cashInstance.partner.name,
                      },
                    )}
                  >
                    {cashInstance.partner.name}
                  </Link>
                </td>
                <td>
                  <Link
                    to={`/shipments/details/${cashInstance.shipmentId}`}
                    title={T.translate(
                      'cashInstances.list.viewShipmentTooltip',
                      {
                        trackingId: cashInstance.shipment.trackingId,
                      },
                    )}
                  >
                    {cashInstance.shipment.trackingId}
                  </Link>
                </td>
              </tr>
            ))}
          </tbody>
        </Table>
        <Pagination
          ref={this.pagination}
          resourceNameOnApi={`${resourceNameOnApi}`}
          where={{
            [filter.type ? 'typeId' : null]: filter.type,
            and: [
              {
                dueDatetime: {
                  gt: filter.from,
                },
              },
              {
                dueDatetime: {
                  lt: filter.to,
                },
              },
            ],
          }}
          filter={{
            include: ['direction', 'type', 'shipment', 'partner'],
            order: 'dueDatetime ASC',
          }}
          onItemsReceived={(cashInstanceList) => {
            this.setState({ cashInstances: cashInstanceList });
          }}
        />
      </Container>
    );
  }
}

export default ReportPage;
