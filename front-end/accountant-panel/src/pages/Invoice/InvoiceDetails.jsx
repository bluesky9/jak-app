import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Moment from 'react-moment';
import { Container, Row, Col } from 'reactstrap';
import { Link } from 'react-router-dom';
import T from 'i18n-react';

import API from '../../components/API/API';

export default class InvoiceDetails extends Component {
  constructor(props) {
    super(props);

    this.API = new API();

    this.state = {
      resourceNameOnApi: 'partnerCashInstances',
      resource: {
        type: {},
        partner: {},
        shipment: {},
        direction: {},
      },
    };
  }

  componentDidMount() {
    const { resourceNameOnApi } = this.state;
    const { match } = this.props;
    this.API.get(`/${resourceNameOnApi}/${match.params.id}`, {
      params: {
        filter: {
          include: ['direction', 'type', 'shipment', 'partner'],
        },
      },
    }).then((response) => {
      this.setState({
        resource: response.data,
      });
    });
  }

  render() {
    const { resource } = this.state;
    return (
      <Container fluid>
        <h3 className="text-secondary font-weight-bold mt-3">
          {T.translate('invoices.detail.title')}
        </h3>
        <h4 className="text-secondary font-weight-light mb-3">
          {T.translate('invoices.detail.caption', {
            name: resource.partner.name,
          })}
        </h4>
        <hr />
        <Row className="mb-3">
          <Col md={4} className="font-weight-bold">
            <span>{T.translate('cashInstances.fields.direction')}</span>
          </Col>
          <Col md={8}>
            <span>{resource.direction.name}</span>
          </Col>
        </Row>
        <Row className="mb-3">
          <Col md={4} className="font-weight-bold">
            <span>{T.translate('cashInstances.fields.type')}</span>
          </Col>
          <Col md={8}>
            <span>{resource.type.name}</span>
          </Col>
        </Row>
        <Row className="mb-3">
          <Col md={4} className="font-weight-bold">
            <span>{T.translate('cashInstances.fields.shipment')}</span>
          </Col>
          <Col md={8}>
            <span>{resource.shipment.trackingId}</span>
          </Col>
        </Row>
        <Row className="mb-3">
          <Col md={4} className="font-weight-bold">
            <span>{T.translate('cashInstances.fields.amount')}</span>
          </Col>
          <Col md={8}>
            <span>{resource.amount}</span>
          </Col>
        </Row>
        <Row className="mb-3">
          <Col md={4} className="font-weight-bold">
            <span>{T.translate('cashInstances.fields.settledAt')}</span>
          </Col>
          <Col md={8}>
            <span>
              <Moment date={resource.settledAt} />
            </span>
          </Col>
        </Row>
        <hr />
        <div className="clearfix">
          <Link
            to="/cashInstances/list"
            className="btn btn-rounded btn-lg btn-outline-secondary float-md-left d-block d-md-inline-block px-5"
          >
            {T.translate('invoices.detail.backButton')}
          </Link>
        </div>
      </Container>
    );
  }
}

InvoiceDetails.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string.isRequired,
    }),
  }),
};

InvoiceDetails.defaultProps = {
  match: {
    params: {
      id: '',
    },
  },
};
