import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Moment from 'react-moment';
import { Container, Row, Col } from 'reactstrap';
import { Link } from 'react-router-dom';
import T from 'i18n-react';

import API from '../../components/API/API';
import { MapWithMarkers } from '../../components/MapWithMarkers/MapWithMarkers';

export default class ShipmentDetails extends Component {
  constructor(props) {
    super(props);

    this.API = new API();

    this.state = {
      resourceNameOnApi: 'shipments',
      resource: {
        trackingId: '',
      },
      hiddenPropertyNamesOnDetail: [
        'id',
        'statusId',
        'typeId',
        'routeId',
        'partnerId',
        'senderAreaId',
        'recipientAreaId',
      ],
    };
  }

  componentDidMount() {
    const { resourceNameOnApi } = this.state;
    const { match } = this.props;
    this.API.get(`/${resourceNameOnApi}/${match.params.id}`, {
      params: {
        filter: {
          include: ['type', 'status', 'route', 'recipientArea', 'senderArea'],
        },
      },
    }).then((response) => {
      this.setState({
        resource: response.data,
      });
    });
  }

  render() {
    const { resource, hiddenPropertyNamesOnDetail } = this.state;

    return (
      <Container fluid>
        <h3 className="text-secondary font-weight-bold mt-3">
          {T.translate('shipments.detail.title')}
        </h3>
        <h4 className="text-secondary font-weight-light mb-3">
          {T.translate('shipments.detail.caption', {
            trackingId: resource.trackingId,
          })}
        </h4>
        <hr />
        {Object.keys(resource).map((property) => {
          if (hiddenPropertyNamesOnDetail.includes(property)) {
            return null;
          }

          let propertyValue;

          if (
            [
              'createdAt',
              'updatedAt',
              'pickupDatetime',
              'deliveryDatetime',
              'recipientAddressConfirmationDatetime',
            ].includes(property)
          ) {
            propertyValue = (
              <span>
                <Moment date={resource[property]} />
              </span>
            );
          } else if (property === 'status') {
            propertyValue = (
              <span>
                {T.translate(
                  `shipments.fields.statuses.${resource.status.name}`,
                )}
              </span>
            );
          } else if (property === 'type') {
            propertyValue = (
              <span>
                {T.translate(`shipments.fields.types.${resource.type.name}`)}
              </span>
            );
          } else if (property === 'recipientAddressConfirmed') {
            propertyValue = (
              <span>
                {resource[property]
                  ? T.translate('defaults.yes')
                  : T.translate('defaults.no')}
              </span>
            );
          } else if (property === 'route') {
            propertyValue = (
              <span>
                {T.translate('shipments.detail.route', {
                  routeId: resource[property].id,
                  routeStartDatetime: resource.route.startDatetime,
                })}
              </span>
            );
          } else if (['recipientArea', 'senderArea'].includes(property)) {
            propertyValue = (
              <span>
                {resource[property].code}
                {' '}
                <em className="text-muted">
(
                  {resource[property].name}
)
                </em>
              </span>
            );
          } else if (
            ['recipientGeoPoint', 'senderGeoPoint'].includes(property)
          ) {
            propertyValue = (
              <div>
                <span>
                  {resource[property].lat}
,
                  {resource[property].lng}
                </span>
                <MapWithMarkers
                  mapProps={{
                    defaultCenter: {
                      lat: resource[property].lat,
                      lng: resource[property].lng,
                    },
                    defaultOptions: {
                      zoom: 15,
                      scrollwheel: false,
                      zoomControl: true,
                    },
                  }}
                  containerHeight="150px"
                  markers={[
                    {
                      position: {
                        lat: resource[property].lat,
                        lng: resource[property].lng,
                      },
                    },
                  ]}
                />
              </div>
            );
          } else {
            propertyValue = (
              <span>
                {resource[property] || T.translate('defaults.notSet')}
              </span>
            );
          }

          return (
            <Row className="mb-3" key={property}>
              <Col md={4} className="font-weight-bold">
                <span>{T.translate(`shipments.fields.${property}`)}</span>
              </Col>
              <Col md={8}>{propertyValue}</Col>
            </Row>
          );
        })}
        <hr />
        <div className="clearfix">
          <Link
            to="/cashInstances/list"
            className="btn btn-rounded btn-lg btn-outline-secondary float-md-left d-block d-md-inline-block px-5"
          >
            {T.translate('shipments.detail.backButton')}
          </Link>
        </div>
      </Container>
    );
  }
}

ShipmentDetails.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string.isRequired,
    }),
  }),
};

ShipmentDetails.defaultProps = {
  match: {
    params: {
      id: '',
    },
  },
};
