import React, { Component } from 'react';
import {
  Container,
  Row,
  Col,
  Card,
  CardBody,
  CardTitle,
  CardSubtitle,
} from 'reactstrap';
import { Link } from 'react-router-dom';
import T from 'i18n-react';
import API from '../../components/API/API';

class Dashboard extends Component {
  constructor(props) {
    super(props);

    this.API = new API();

    this.state = {
      resourceNameOnApi: 'partnerCashInstances',
      notSettledCount: 0,
      cashInstancesCount: 0,
    };
  }

  componentDidMount() {
    const { resourceNameOnApi } = this.state;
    // Fetch not settled cash instances count
    this.API.get(`/${resourceNameOnApi}/count`, {
      params: {
        where: {
          isSettled: false,
        },
      },
    }).then((response) => {
      this.setState({
        notSettledCount: response.data.count,
      });
    });

    // Fetch total cash instances count
    this.API.get(`/${resourceNameOnApi}/count`).then((response) => {
      this.setState({
        cashInstancesCount: response.data.count,
      });
    });
  }

  render() {
    const { cashInstancesCount, notSettledCount } = this.state;
    return (
      <Container fluid>
        <Row className="my-4">
          <Col lg={6} md={12}>
            <h3 className="text-secondary font-weight-bold mt-3">
              {T.translate('dashboard.title')}
            </h3>
            <h4 className="text-secondary font-weight-light mb-3">
              {T.translate('dashboard.caption')}
            </h4>
          </Col>
          <Col lg={6} md={12}>
            <Card>
              <CardBody>
                <Row>
                  <Col sm={6}>
                    <h1
                      style={{ fontSize: '45px' }}
                      className="float-left mr-3 mb-0"
                    >
                      <i className="text-muted fas fa-money-bill-wave" />
                    </h1>
                    <CardTitle className="h6 font-weight-light text-muted">
                      {T.translate('dashboard.statistics.total')}
                    </CardTitle>
                    <CardSubtitle className="h4">
                      {cashInstancesCount}
                    </CardSubtitle>
                  </Col>
                  <Col sm={6}>
                    <h1
                      style={{ fontSize: '45px' }}
                      className="float-left mr-3 mb-0"
                    >
                      <span className="fa-layers fa-fw">
                        <i className="text-muted fas fa-money-bill-wave" />
                        <i
                          className="text-danger fas fa-circle"
                          data-fa-transform="shrink-7 down-5 right-8"
                        />
                        <i
                          className="fas fa-times fa-inverse"
                          data-fa-transform="shrink-10 down-5 right-8"
                        />
                      </span>
                    </h1>
                    <CardTitle className="h6 font-weight-light text-muted">
                      {T.translate('dashboard.statistics.notSettled')}
                    </CardTitle>
                    <CardSubtitle className="h4">
                      {notSettledCount}
                    </CardSubtitle>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
        </Row>
        <hr />
        <Row>
          <Col lg={{ size: 4, offset: 2 }}>
            <Link
              to="/cashInstances/list"
              className="btn btn-block btn-primary btn-rounded my-3 clearfix d-flex align-items-center"
            >
              <h1 style={{ fontSize: '30px' }} className="my-1 ml-2">
                <i className="fas fa-money-bill-wave" />
              </h1>
              <span className="flex-grow-1 text-center">
                {T.translate('dashboard.cashInstances')}
              </span>
            </Link>
          </Col>
          <Col lg={4}>
            <Link
              to="/reports"
              className="btn btn-block btn-primary btn-rounded my-3 clearfix d-flex align-items-center"
            >
              <h1 style={{ fontSize: '30px' }} className="my-1 ml-2">
                <i className="fas fa-chart-line" />
              </h1>
              <span className="flex-grow-1 text-center">
                {T.translate('dashboard.reports')}
              </span>
            </Link>
          </Col>
        </Row>
      </Container>
    );
  }
}

export default Dashboard;
