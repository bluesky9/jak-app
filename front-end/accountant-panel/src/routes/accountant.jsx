import Dashboard from '../pages/Dashboard/Dashboard';
import CashInstancesList from '../pages/CashInstances/CashInstancesList';
import ShipmentDetails from '../pages/Shipment/ShipmentDetails';
import PartnerDetails from '../pages/Partner/PartnerDetails';
import InvoiceDetails from '../pages/Invoice/InvoiceDetails';
import ReportPage from '../pages/Report/ReportPage';

/**
 * Define routes and sidebar links at the same time.
 * Note that only items with 'icon' property and
 * without 'redirect' property will be rendered on sidebar.
 * @type {Object[]}
 */
const accountantRoutes = [
  {
    path: '/dashboard',
    name: 'menu.dashboard',
    component: Dashboard,
    authRequired: true,
  },
  {
    path: '/cashInstances/list',
    name: 'menu.cashInstances.list',
    component: CashInstancesList,
    authRequired: true,
  },
  {
    path: '/shipments/details/:id',
    name: 'menu.shipments.details',
    component: ShipmentDetails,
    authRequired: true,
  },
  {
    path: '/partners/details/:id',
    name: 'menu.partners.details',
    component: PartnerDetails,
    authRequired: true,
  },
  {
    path: '/invoices/details/:id',
    name: 'menu.invoices.details',
    component: InvoiceDetails,
    authRequired: true,
  },
  {
    path: '/reports',
    name: 'menu.reports',
    component: ReportPage,
    authRequired: true,
  },
  {
    redirect: true,
    path: '/',
    to: '/dashboard',
    name: 'menu.dashboard.redirect',
  },
];

export default accountantRoutes;
