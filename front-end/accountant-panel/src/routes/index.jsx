import AccountantLayout from '../layouts/Accountant/AccountantLayout';
import accountantRoutes from './accountant';
import CleanLayout from '../layouts/Clean/CleanLayout';
import cleanRoutes from './clean';

const indexRoutes = Array.prototype.concat(
  cleanRoutes.map(route => ({ layout: CleanLayout, ...route })),
  accountantRoutes.map(route => ({ layout: AccountantLayout, ...route })),
);

export default indexRoutes;
