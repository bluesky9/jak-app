import React, { Component } from 'react';
import PropTypes from 'prop-types';
import T from 'i18n-react';
import API from '../API/API';

export class Pagination extends Component {
  constructor(props) {
    super(props);

    this.API = new API();

    this.state = {
      currentPage: 1,
      pageCount: 0,
    };

    this.refreshPagination = () => {
      this.setState(
        {
          currentPage: 1,
        },
        async () => {
          await this.fetchItemCount();
          await this.fetchItems();
        },
      );
    };

    this.fetchItemCount = async () => {
      const { resourceNameOnApi, where, itemsPerPage } = this.props;
      this.API.get(`/${resourceNameOnApi}/count`, {
        params: {
          where,
        },
      }).then((response) => {
        this.setState({
          pageCount: Math.ceil(response.data.count / itemsPerPage),
        });
      });
    };

    this.fetchItems = async () => {
      const {
        itemsPerPage,
        filter: propsFilter,
        where,
        params: propsParams,
        customEndpoint,
        resourceNameOnApi,
        onItemsReceived,
      } = this.props;
      const { currentPage } = this.state;
      const currentSkip = itemsPerPage * (currentPage - 1);
      const filter = {
        ...propsFilter,
        where,
        limit: itemsPerPage,
        skip: currentSkip,
      };
      const params = {
        ...propsParams,
        filter,
      };
      const endpoint = customEndpoint || `/${resourceNameOnApi}`;
      this.API.get(endpoint, { params }).then((response) => {
        onItemsReceived(response.data);
      });
    };

    this.goToPage = (page) => {
      const { currentPage, pageCount } = this.state;
      const actualPage = currentPage;
      switch (page) {
        case 'first':
          this.setState({ currentPage: 1 });
          break;
        case 'last':
          this.setState({ currentPage: pageCount });
          break;
        case 'next':
          this.setState({
            currentPage: currentPage < pageCount ? currentPage + 1 : pageCount,
          });
          break;
        case 'previous':
          this.setState({
            currentPage: currentPage > 1 ? currentPage - 1 : 1,
          });
          break;
        default:
          if (page > 0 && page <= pageCount) this.setState({ currentPage: page });
          break;
      }
      if (actualPage !== currentPage) {
        this.fetchItems();
      }
    };
  }

  componentWillMount() {
    const { initialPage } = this.props;
    this.setState({
      currentPage: initialPage,
    });
  }

  componentDidMount() {
    this.fetchItemCount();
    this.fetchItems();
  }

  componentDidUpdate(prevProps, prevState) {
    const { currentPage } = this.state;
    if (prevState.currentPage !== currentPage) this.fetchItems();
  }

  render() {
    const showPageButtons = () => {
      const { pageCount, currentPage } = this.state;
      const { maxPagesShown } = this.props;
      const pagesToShow = [];
      let firstPageShown;
      let lastPageShown;
      if (pageCount <= maxPagesShown) {
        firstPageShown = 1;
        lastPageShown = pageCount;
      } else {
        firstPageShown = currentPage - Math.floor(maxPagesShown / 2);
        lastPageShown = currentPage + Math.floor(maxPagesShown / 2);
        if (firstPageShown < 1) {
          lastPageShown = maxPagesShown;
          firstPageShown = 1;
        }
        if (lastPageShown > pageCount) {
          firstPageShown = pageCount - maxPagesShown + 1;
          lastPageShown = pageCount;
        }
      }
      for (let i = firstPageShown; i <= lastPageShown; i += 1) {
        pagesToShow.push(i);
      }

      return pagesToShow.map(page => (
        <li
          key={page}
          className={`page-item${currentPage === page ? ' active' : ''}`}
        >
          <button
            type="button"
            className="page-link"
            onClick={() => {
              this.goToPage(page);
            }}
          >
            <span aria-hidden="true">{page}</span>
          </button>
        </li>
      ));
    };

    const { pageCount, currentPage } = this.state;

    return (
      <nav aria-label={T.translate('defaults.pagination.navigation')}>
        <ul className="pagination pagination-rounded justify-content-center">
          <li className={`page-item${currentPage === 1 ? ' disabled' : ''}`}>
            <button
              type="button"
              className="page-link"
              onClick={() => {
                this.goToPage('first');
              }}
              title={T.translate('defaults.pagination.firstPage')}
            >
              <i className="fas fa-angle-double-left" />
            </button>
          </li>
          <li className={`page-item${currentPage === 1 ? ' disabled' : ''}`}>
            <button
              type="button"
              className="page-link"
              onClick={() => {
                this.goToPage('previous');
              }}
              title={T.translate('defaults.pagination.previous')}
            >
              <i className="fas fa-angle-left" />
            </button>
          </li>
          {showPageButtons()}
          <li
            className={`page-item${
              currentPage === pageCount ? ' disabled' : ''
            }`}
          >
            <button
              type="button"
              className="page-link"
              onClick={() => {
                this.goToPage('next');
              }}
              title={T.translate('defaults.pagination.next')}
            >
              <i className="fas fa-angle-right" />
            </button>
          </li>
          <li
            className={`page-item${
              currentPage === pageCount ? ' disabled' : ''
            }`}
          >
            <button
              type="button"
              className="page-link"
              onClick={() => {
                this.goToPage('last');
              }}
              title={T.translate('defaults.pagination.lastPage')}
            >
              <i className="fas fa-angle-double-right" />
            </button>
          </li>
        </ul>
      </nav>
    );
  }
}

Pagination.propTypes = {
  resourceNameOnApi: PropTypes.string.isRequired,
  onItemsReceived: PropTypes.func.isRequired,
  params: PropTypes.objectOf(PropTypes.object),
  filter: PropTypes.objectOf(PropTypes.any),
  where: PropTypes.objectOf(PropTypes.any),
  initialPage: PropTypes.number,
  itemsPerPage: PropTypes.number,
  maxPagesShown: PropTypes.number,
  customEndpoint: PropTypes.string,
};

Pagination.defaultProps = {
  params: {},
  filter: {},
  where: {},
  initialPage: 1,
  itemsPerPage: 10,
  maxPagesShown: 5,
  customEndpoint: null,
};

export default Pagination;
