import React, { Component } from 'react';
import {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
  Marker,
} from 'react-google-maps';

const apiKey = process.env.REACT_APP_GOOGLE_MAPS_API_KEY;

export class MapWithMarkers extends Component {
  constructor(props) {
    super(props);

    this.mapRef = React.createRef();

    this.init = false;
  }

  setCenterAndZoom() {
    const { autoCenter, markers } = this.props;
    if (this.init === false && autoCenter === true && markers.length > 0) {
      const bounds = new window.google.maps.LatLngBounds();
      markers.forEach((marker) => {
        bounds.extend(
          new window.google.maps.LatLng(
            marker.position.lat,
            marker.position.lng,
          ),
        );
      });
      this.mapRef.current.fitBounds(bounds);
    }
    this.init = true;
  }

  render() {
    let GMap = withGoogleMap(() => {
      const { mapProps, markers } = this.props;

      return (
        <GoogleMap
          ref={this.mapRef}
          {...mapProps}
          onTilesLoaded={() => this.setCenterAndZoom()}
        >
          {markers.map((markerProps, index) => (
            <Marker key={`marker-${index + 1}`} {...markerProps} />
          ))}
        </GoogleMap>
      );
    });

    const { autoLoad, containerHeight } = this.props;

    if (autoLoad !== false) GMap = withScriptjs(GMap);

    return (
      <GMap
        googleMapURL={`https://maps.googleapis.com/maps/api/js${
          apiKey ? `?key=${apiKey}` : '?'
        }&libraries=geocode`}
        loadingElement={<div style={{ height: '100%' }} />}
        containerElement={(
          <div
            style={{ height: containerHeight || '50vh', marginBottom: '20px' }}
          />
)}
        mapElement={<div style={{ height: '100%' }} />}
      />
    );
  }
}

export default MapWithMarkers;
