import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
  Marker,
} from 'react-google-maps';

const apiKey = process.env.REACT_APP_GOOGLE_MAPS_API_KEY;

function BuildMap({ mapProps, markerProps, autoLoad }) {
  let GMap = withGoogleMap(() => (
    <GoogleMap {...mapProps}>
      <Marker {...markerProps} />
    </GoogleMap>
  ));

  if (autoLoad !== false) {
    GMap = withScriptjs(GMap);
  }

  return (
    <GMap
      googleMapURL={`https://maps.googleapis.com/maps/api/js${
        apiKey ? `?key=${apiKey}` : '?'
      }&libraries=geocode`}
      loadingElement={<div style={{ height: '100%' }} />}
      containerElement={
        <div style={{ height: '50vh', marginBottom: '20px' }} />
      }
      mapElement={<div style={{ height: '100%' }} />}
    />
  );
}

export class LocationMap extends Component {
  shouldComponentUpdate(nextProps) {
    const { markerProps, mapProps } = this.props;
    return !(
      markerProps.position.lat === nextProps.markerProps.position.lat
      || markerProps.position.lng === nextProps.markerProps.position.lng
      || mapProps.defaultCenter.lat === nextProps.mapProps.defaultCenter.lat
      || mapProps.defaultCenter.lng === nextProps.mapProps.defaultCenter.lng
    );
  }

  render() {
    return <BuildMap {...this.props} />;
  }
}

BuildMap.propTypes = {
  mapProps: PropTypes.objectOf(PropTypes.object),
  markerProps: PropTypes.objectOf(PropTypes.object),
  autoLoad: PropTypes.bool,
};

BuildMap.defaultProps = {
  mapProps: {},
  markerProps: {},
  autoLoad: true,
};

export default LocationMap;
