import React from 'react';
import { Container, Button } from 'reactstrap';

const onClickLanguageButton = (event) => {
  localStorage.setItem('language', event.target.dataset.language);
  window.location.reload();
};

const languageButtons = [
  {
    label: 'ENGLISH',
    languageCode: 'en-US',
  },
  {
    label: 'ARABIC',
    languageCode: 'ar-SA',
  },
];

export const Footer = () => (
  <footer className="footer">
    <Container fluid>
      <p
        className="copyright float-left"
        style={{ fontSize: '10px', height: '12px' }}
      >
        {languageButtons.map(button => (
          <Button
            key={button.languageCode}
            data-language={button.languageCode}
            onClick={onClickLanguageButton}
            color="secondary"
            outline
            style={{
              fontSize: '14px',
              border: 0,
              marginRight: '5px',
            }}
          >
            {button.label}
          </Button>
        ))}
      </p>
      <p className="copyright float-right">
        &copy;
        {' '}
        {new Date().getFullYear()}
        {' '}
        <a href="https://jakapp.co">Jak Logistics</a>
      </p>
      <div className="clearfix" />
    </Container>
  </footer>
);

export default Footer;
