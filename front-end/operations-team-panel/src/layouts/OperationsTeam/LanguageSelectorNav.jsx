import React from 'react';
import {
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
} from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const onClickLanguageButton = (event) => {
  localStorage.setItem('language', event.target.dataset.language);
  window.location.reload();
};

const languageButtons = [
  {
    short: 'EN',
    label: 'English',
    languageCode: 'en-US',
  },
  {
    short: 'AR',
    label: 'Arabic',
    languageCode: 'ar-SA',
  },
];

export const LanguageSelectorNav = props => (
  <UncontrolledDropdown nav inNavbar {...props}>
    <DropdownToggle nav caret>
      {languageButtons
        .filter(lang => lang.languageCode === localStorage.getItem('language'))
        .map(language => language.short)}
      {' '}
    </DropdownToggle>
    <DropdownMenu right className="py-0 shadow">
      {languageButtons.map(language => (
        <DropdownItem
          key={language.languageCode}
          className={
            language.languageCode === localStorage.getItem('language')
              ? 'active bg-secondary'
              : ''
          }
          data-language={language.languageCode}
          onClick={onClickLanguageButton}
        >
          {language.languageCode === localStorage.getItem('language') && (
            <FontAwesomeIcon icon="check" className="float-right mt-1" />
          )}
          {language.label}
        </DropdownItem>
      ))}
    </DropdownMenu>
  </UncontrolledDropdown>
);

export default LanguageSelectorNav;
