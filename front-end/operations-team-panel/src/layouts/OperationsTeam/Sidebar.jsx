import React, { Component } from 'react';
import { Nav } from 'reactstrap';
import { Link, withRouter } from 'react-router-dom';
import T from 'i18n-react';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { withAuth, AuthProvider } from '../../components/Auth/AuthProvider';
import operationsTeamRoutes from '../../routes/operationsTeam';

import logo from '../../assets/img/logo.png';

class Sidebar extends Component {
  constructor(props) {
    super(props);

    this.hasRights = (routeRoles) => {
      if (routeRoles && routeRoles.length) {
        const userRoles = new AuthProvider()
          .getUser()
          .roles.map(role => role.name);
        let hasRights = false;
        routeRoles.forEach((role) => {
          if (hasRights) return;
          hasRights = userRoles.includes(role);
        });
        return hasRights;
      }
      return true;
    };
  }

  putActiveClass(route) {
    const { location } = this.props;
    let isActive = '';
    if (typeof route === 'string') {
      isActive = location.pathname.indexOf(route) > -1 ? 'active' : '';
    } else {
      route.forEach((r) => {
        if (isActive === '') {
          isActive = this.putActiveClass(r);
        }
      });
    }
    return isActive;
  }

  render() {
    return (
      <div className="operations-team-sidebar">
        <div className="sidebar-brand">
          <Link to="/">
            <img src={logo} alt="Jak Logistics" />
          </Link>
          <div className="text-center p-1 small text-uppercase text-secondary font-weight-bold mt-2">
            {T.translate('login.dashboardTitle')}
          </div>
        </div>

        <Nav vertical className="">
          {operationsTeamRoutes.map((prop) => {
            if (!prop.redirect && prop.icon && this.hasRights(prop.roles)) {
              return (
                <li
                  key={`sidebar_${prop.name}`}
                  className={`nav-item ${this.putActiveClass(prop.path)}`}
                >
                  <Link to={prop.path} replace className="nav-link">
                    <FontAwesomeIcon
                      icon={prop.icon}
                      transform="grow-8"
                      className="sidebar-icon mr-3"
                      fixedWidth
                    />
                    <span>{T.translate(prop.name)}</span>
                  </Link>
                </li>
              );
            }
            return null;
          })}
        </Nav>
      </div>
    );
  }
}

Sidebar.propTypes = {
  location: PropTypes.shape({ pathname: PropTypes.string.isRequired })
    .isRequired,
};

export default withAuth(withRouter(Sidebar));
