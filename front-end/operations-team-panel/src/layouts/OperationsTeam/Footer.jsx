import React from 'react';
import { Container } from 'reactstrap';
import PropTypes from 'prop-types';

export const Footer = (props) => {
  const { className } = props;
  return (
    <Container>
      <footer
        className={`text-right text-muted small border-top py-3 ${className}`}
      >
        <Container>
          &copy; 2018
          {' '}
          <a
            href="https://jakapp.co"
            className="text-muted"
            target="_blank"
            rel="noopener noreferrer"
          >
            Jak Logistics
          </a>
        </Container>
      </footer>
    </Container>
  );
};

Footer.propTypes = {
  className: PropTypes.string,
};

Footer.defaultProps = {
  className: null,
};

export default Footer;
