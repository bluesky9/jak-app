import React from 'react';
import { Navbar, Nav } from 'reactstrap';
import { LanguageSelectorNav } from './LanguageSelectorNav';
import UserNav from './UserNav';

export const Header = () => (
  <Navbar
    color="light"
    light
    expand="md"
    sticky="top"
    className="operations-team-header"
  >
    <Nav className="ml-auto" navbar>
      <LanguageSelectorNav className="mr-3" />
      <UserNav />
    </Nav>
  </Navbar>
);

export default Header;
