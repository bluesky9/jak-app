import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { Header } from './Header';
import { Footer } from './Footer';
import Sidebar from './Sidebar';

export default class OperationsTeamLayout extends Component {
  componentDidUpdate(e) {
    if (e.history && e.history.action === 'PUSH') {
      document.documentElement.scrollTop = 0;
      document.scrollingElement.scrollTop = 0;
      this.mainPanel.scrollTop = 0;
    }
  }

  render() {
    const { children } = this.props;
    return (
      <div className="operations-team-wrapper">
        <Sidebar />
        <div
          className="operations-team-container"
          ref={(component) => {
            this.mainPanel = component;
          }}
        >
          <Header />
          <div className="operations-team-content py-3">{children}</div>
          <Footer />
        </div>
      </div>
    );
  }
}

OperationsTeamLayout.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};

OperationsTeamLayout.defaultProps = {
  children: null,
};
