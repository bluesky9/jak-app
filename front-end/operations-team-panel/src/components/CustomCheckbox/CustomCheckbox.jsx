import React, { Component } from 'react';

class CustomCheckbox extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isCheckedFromProps: !!props.isChecked,
    };
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    this.setState(prevState => ({
      isCheckedFromProps: !prevState.isCheckedFromProps,
    }));
  }

  render() {
    const {
      isChecked, number, label, inline, ...rest
    } = this.props;
    const { isCheckedFromProps } = this.state;
    const classes = inline !== undefined ? 'checkbox checkbox-inline' : 'checkbox';
    return (
      <div className={classes}>
        <label htmlFor={number}>
          <input
            id={number}
            type="checkbox"
            onChange={this.handleClick}
            checked={isCheckedFromProps}
            {...rest}
          />
          {label}
        </label>
      </div>
    );
  }
}

export default CustomCheckbox;
