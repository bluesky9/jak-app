import React from 'react';
import PropTypes from 'prop-types';

export const UserCard = (props) => {
  const {
    bgImage, avatar, name, userName, description, socials,
  } = props;

  return (
    <div className="card card-user">
      <div className="image">
        <img src={bgImage} alt="..." />
      </div>
      <div className="content">
        <div className="author">
          <a href="#pablo">
            <img
              className="avatar border-gray"
              src={avatar}
              alt="..."
            />
            <h4 className="title">
              {name}
              <br />
              <small>
                {userName}
              </small>
            </h4>
          </a>
        </div>
        <p className="description text-center">
          {description}
        </p>
      </div>
      <hr />
      <div className="text-center">
        {socials}
      </div>
    </div>
  );
};

UserCard.propTypes = {
  bgImage: PropTypes.string.isRequired,
  avatar: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  userName: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  socials: PropTypes.string.isRequired,
};

export default UserCard;
