import React from 'react';
import PropTypes from 'prop-types';
import { Tooltip } from 'react-bootstrap';

const tooltipForOverlayTrigger = ({ message }) => (
  <Tooltip id="tooltip">
    {message}
  </Tooltip>
);

tooltipForOverlayTrigger.propTypes = {
  message: PropTypes.string.isRequired,
};

export default tooltipForOverlayTrigger;
