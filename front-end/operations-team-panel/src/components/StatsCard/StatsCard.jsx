import React from 'react';
import PropTypes from 'prop-types';
import {
  Row, Col, Card, CardBody, CardTitle, CardSubtitle,
} from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

export const StatsCard = (props) => {
  const {
    icon, className, statsText, statsValue,
  } = props;
  return (
    <Card className="border-0">
      <CardBody>
        <Row>
          <Col>
            <h1
              style={{ fontSize: '45px' }}
              className="float-left mr-3 mb-0 text-muted"
            >
              <FontAwesomeIcon icon={icon} className={className} />
            </h1>
            <CardTitle className="h6 font-weight-light text-muted">
              {statsText}
            </CardTitle>
            <CardSubtitle className="h4">{statsValue}</CardSubtitle>
          </Col>
        </Row>
      </CardBody>
    </Card>
  );
};

StatsCard.propTypes = {
  icon: PropTypes.string.isRequired,
  className: PropTypes.string.isRequired,
  statsText: PropTypes.string.isRequired,
  statsValue: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
    .isRequired,
};

export default StatsCard;
