import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class SearchBy extends Component {
  constructor(props) {
    super(props);

    this.state = {
      opened: false,
      selected: props.filters[0] || '',
    };
  }

  toggleDropdown() {
    const { opened } = this.state;
    this.setState({ opened: !opened });
  }

  selectFilter(filter) {
    const { handleChange } = this.props;

    this.setState({ selected: filter });
    handleChange(filter, 'dropdown');
  }

  render() {
    const { filters, handleChange, classes } = this.props;
    const { opened, selected } = this.state;
    const dropdownClass = opened ? 'show' : '';

    return (
      <div className={`search-component ${classes.map(d => d).join(' ')}`}>
        <div className="input-group">
          <input
            type="hidden"
            name="search_param"
            value="all"
            id="search_param"
          />
          <input
            type="text"
            className="form-control input has-dropdown"
            placeholder="Search term by..."
            onChange={e => handleChange(e.target.value, 'input')}
          />

          <div className="input-group-btn search-panel dropdown-wrapper">
            <div
              title={`Search By ${selected}`}
              className="btn-group"
              onClick={this.toggleDropdown.bind(this)}
              role="button"
              tabIndex={-1}
              onKeyPress={() => {}}
            >
              <button
                type="button"
                className="btn btn-danger dropdown-toggle width_100"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
              >
                {selected}
              </button>

              <div className={`dropdown-menu ${dropdownClass}`}>
                {filters.map(d => (
                  <button
                    className="dropdown-item"
                    key={d}
                    onKeyPress={() => {}}
                    onClick={() => this.selectFilter(d)}
                    type="button"
                  >
                    {d}
                  </button>
                ))}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

SearchBy.propTypes = {
  filters: PropTypes.instanceOf(Array).isRequired,
  classes: PropTypes.instanceOf(Array),
  handleChange: PropTypes.func.isRequired,
};

SearchBy.defaultProps = {
  classes: [],
};
