import React from 'react';
import SweetAlert from 'sweetalert2-react';
import { Route, Redirect } from 'react-router-dom';
import { withAuth, AuthProvider } from '../Auth/AuthProvider';
import Loader from '../Loader/Loader';

class AppRoute extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showAlert: false,
      errorText: '',
    };

    this.onCloseAlert = () => {
      this.setState({
        showAlert: false,
        errorText: '',
        showLoader: false,
      });
    };

    this.onShowAlert = (error) => {
      const { showAlert, errorText } = this.state;
      if (showAlert === false || errorText !== error) {
        this.setState({ showAlert: true, errorText: error });
      }
    };

    this.onShowLoader = (showLoader) => {
      this.setState({ showLoader });
    };
  }

  componentWillMount() {
    const { emitter } = this.props;
    emitter.on('ShowAlert', this.onShowAlert);
    emitter.on('ShowLoader', this.onShowLoader);
  }

  componentWillUnmount() {
    const { emitter } = this.props;
    emitter.removeListener('ShowAlert', this.onShowAlert);
    emitter.removeListener('ShowLoader', this.onShowLoader);
  }

  render() {
    const { component: Component, layout: Layout, ...routeProps } = this.props;
    const { showAlert, errorText, showLoader } = this.state;
    if (routeProps.auth && routeProps.authRequired) {
      if (!routeProps.auth.isAuth) {
        return (
          <Redirect
            from={routeProps.path}
            to={`/login?redirect=${routeProps.path}`}
          />
        );
      }
      if (routeProps.roles && routeProps.roles.length) {
        const userRoles = new AuthProvider()
          .getUser()
          .roles.map(role => role.name);
        let hasRights = false;
        routeProps.roles.forEach((role) => {
          if (hasRights) return;
          hasRights = userRoles.includes(role);
        });
        if (!hasRights) {
          return <Redirect from={routeProps.path} to="/forbidden" />;
        }
      }
    }

    if (routeProps.redirect) {
      return <Redirect from={routeProps.path} to={routeProps.to} />;
    }
    return (
      <Route
        {...routeProps}
        render={props => (
          <Layout>
            <SweetAlert
              show={showAlert}
              title="Error"
              text={errorText}
              type="error"
              confirmButtonText="Ok"
              confirmButtonClass="btn btn-primary btn-rounded mx-2 btn-lg px-5"
              buttonsStyling={false}
              onConfirm={this.onCloseAlert}
            />
            <Component {...props} />
            <Loader visible={showLoader} />
          </Layout>
        )}
      />
    );
  }
}

export default withAuth(AppRoute);
