import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Tooltip } from 'reactstrap';

class TooltipCompent extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      tooltipOpen: false,
    };
  }

  toggle() {
    this.setState(prevState => ({
      tooltipOpen: !prevState.tooltipOpen,
    }));
  }

  render() {
    const { message, target } = this.props;
    const { tooltipOpen } = this.state;

    return (
      <Tooltip isOpen={tooltipOpen} placement="top" id="tooltip" target={target} toggle={this.toggle}>
        {message}
      </Tooltip>
    );
  }
}


TooltipCompent.propTypes = {
  message: PropTypes.string.isRequired,
  target: PropTypes.string.isRequired,
};

export default TooltipCompent;
