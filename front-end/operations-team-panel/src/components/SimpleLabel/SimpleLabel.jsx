import React from 'react';
import PropTypes from 'prop-types';

const SimpleLabel = (props) => {
  const {
    colour, text, width, title,
  } = props;

  return (
    <div
      title={title}
      style={{ width: `${width}px` }}
      className={`simple-label-component bg-${colour || 'blue'}`}
    >
      {text}
    </div>
  );
};

SimpleLabel.propTypes = {
  colour: PropTypes.string.isRequired,
  text: PropTypes.oneOfType([PropTypes.string, PropTypes.object]).isRequired,
  title: PropTypes.string,
  width: PropTypes.number,
};

SimpleLabel.defaultProps = {
  width: 100,
  title: '',
};

export default SimpleLabel;
