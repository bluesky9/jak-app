import React from 'react';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const Loader = (props) => {
  const { visible } = props;
  if (!visible) return null;
  return (
    <div className="loader-overlay">
      <FontAwesomeIcon icon="circle-notch" spin />
    </div>
  );
};

Loader.propTypes = {
  visible: PropTypes.bool,
};

Loader.defaultProps = {
  visible: false,
};

export default Loader;
