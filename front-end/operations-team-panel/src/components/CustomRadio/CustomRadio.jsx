import React from 'react';

export const CustomRadio = (props) => {
  const {
    number, label, option, name, ...rest
  } = props;

  return (
    <div className="radio">
      <label htmlFor={number}>
        <input id={number} name={name} type="radio" value={option} {...rest} />
        {label}
      </label>
    </div>
  );
};

export default CustomRadio;
