import React from 'react';
import PropTypes from 'prop-types';
import {
  GoogleMap,
  Marker,
  withScriptjs,
  withGoogleMap,
  DirectionsRenderer,
} from 'react-google-maps';

const MapComponentWrapper = withScriptjs(withGoogleMap(props => (
  <GoogleMap
    defaultZoom={8}
    defaultCenter={props.defaultCenter}
  >
    {props.markers.map(
      marker => <Marker key={marker.lat} position={{ lat: marker.lat, lng: marker.lng }} />,
    )}
    {props.directions != null && <DirectionsRenderer directions={props.directions} />}
  </GoogleMap>
)));

const Map = (props) => {
  const { markers, directions, centerPoint } = props;
  return (
    <MapComponentWrapper
      markers={markers}
      googleMapURL={`https://maps.googleapis.com/maps/api/js?key=${process.env.REACT_APP_GOOGLE_MAPS_API_KEY}&libraries=geometry,drawing,places`}
      loadingElement={<div style={{ height: '100%' }} />}
      containerElement={<div style={{ height: '400px' }} />}
      mapElement={<div style={{ height: '100%' }} />}
      directions={directions}
      defaultCenter={centerPoint}
    />
  );
};

Map.propTypes = {
  markers: PropTypes.arrayOf(PropTypes.objectOf(PropTypes.number)),
  directions: PropTypes.objectOf(PropTypes.object),
  centerPoint: PropTypes.objectOf(PropTypes.number),
};

Map.defaultProps = {
  markers: [],
  directions: null,
  centerPoint: { lat: 24.6037022, lng: 46.6622325 },
};
export default Map;
