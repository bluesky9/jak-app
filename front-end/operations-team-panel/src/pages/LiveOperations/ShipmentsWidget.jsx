import React, { Component } from 'react';
import {
  Card, CardHeader, CardBody, Table,
} from 'reactstrap';
import { Link } from 'react-router-dom';
import T from 'i18n-react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import API from '../../components/API/API';
import InlineLoader from '../../components/Loader/InlineLoader';
import SimpleLabel from '../../components/SimpleLabel/SimpleLabel';
import SearchBy from '../../components/Search/SearchBy';

class ShipmentsWidget extends Component {
  constructor(props) {
    super(props);

    this.searchFields = [
      { name: 'Status', field: 'statusName' },
      { name: 'Partner', field: 'partnerName' },
      { name: 'Tracking ID', field: 'trackingId' },
    ];

    this.state = {
      resourceNameOnApi: 'shipments',
      listItems: [],
      backupList: [],
      isLoading: false,
      searchBy: this.searchFields[0].name,
      searchValue: '',
    };

    this.API = new API();

    this.searchTimeout = null;

    this.handleSearchChange = (value, type) => {
      clearTimeout(this.searchTimeout);
      if (type.toLowerCase() === 'input') {
        this.setState({
          searchValue: value,
        }, () => {
          this.performSearch();
        });
      } else if (type.toLowerCase() === 'dropdown') {
        this.setState({
          searchBy: value,
        }, () => {
          this.performSearch();
        });
      }
    };

    this.performSearch = () => {
      const { searchBy, searchValue, backupList } = this.state;
      const { field } = this.searchFields.find(d => d.name === searchBy) || {};

      if (!field) return;

      this.searchTimeout = setTimeout(async () => {
        const newList = backupList.filter((shipment) => {
          const valueInLowerCase = searchValue.toLowerCase();
          let filtered;

          switch (field) {
            case 'statusName':
              if (shipment.status) {
                filtered = shipment.status.name
                  .toLowerCase()
                  .includes(valueInLowerCase);
              }
              break;
            case 'partnerName':
              if (shipment.partner) {
                filtered = shipment.partner.name
                  .toLowerCase()
                  .includes(valueInLowerCase);
              }
              break;
            default:
              filtered = String(shipment[field])
                .toLowerCase()
                .includes(valueInLowerCase);
              break;
          }

          return filtered;
        });
        newList.sort((first, second) => {
          let firstComp = '';
          let secondComp = '';
          switch (field) {
            case 'statusName':
              if (first.status) {
                firstComp = first.status.name.toLowerCase();
              }
              if (second.status) {
                secondComp = second.status.name.toLowerCase();
              }
              break;
            case 'partnerName':
              if (first.partner) {
                firstComp = first.partner.name.toLowerCase();
              }
              if (second.partner) {
                secondComp = second.partner.name.toLowerCase();
              }
              break;
            default:
              firstComp = String(first[field]).toLowerCase();
              secondComp = String(second[field]).toLowerCase();
              break;
          }
          if (firstComp < secondComp) {
            return -1;
          }
          if (firstComp > secondComp) {
            return 1;
          }
          return 0;
        });
        if (!this.willUnmount) this.setState({ listItems: newList });
      }, 500);
    };

    this.fetchShipments = () => {
      const { resourceNameOnApi } = this.state;
      const date = new Date();
      const year = date.getFullYear();
      const month = date.getMonth() + 1;
      const day = date.getDate();
      const startDate = `${year}-${month}-${day}`;
      const endDate = `${year}-${month}-${day + 1}`;

      if (!this.willUnmount) this.setState({ isLoading: true });

      this.API.get(`/${resourceNameOnApi}`, {
        showLoader: false,
        params: {
          filter: {
            include: ['status', 'partner'],
            where: {
              or: [
                {
                  createdAt: {
                    between: [startDate, endDate],
                  },
                },
                {
                  pickupDatetime: {
                    between: [startDate, endDate],
                  },
                },
                {
                  deliveryDatetime: {
                    between: [startDate, endDate],
                  },
                },
              ],
            },
          },
        },
      }).then((response) => {
        if (!this.willUnmount) {
          this.setState({
            backupList: response.data,
            isLoading: false,
          });
          this.performSearch();
        }
      });
    };
  }

  componentDidMount() {
    this.fetchShipments();
    this.fetchShipmentsHandle = setInterval(this.fetchShipments, 10000);
  }

  componentWillUnmount() {
    this.willUnmount = true;
    clearTimeout(this.searchTimeout);
    clearInterval(this.fetchShipmentsHandle);
  }

  render() {
    const {
      searchBy, listItems, isLoading, resourceNameOnApi,
    } = this.state;

    return (
      <Card className="h-100">
        <CardHeader className="p-2">
          <h6 className="font-weight-light mb-0">
            <FontAwesomeIcon icon="shipping-fast" className="text-success mr-2" />
            {T.translate('liveOperations.shipments.title')}
            <InlineLoader visible={isLoading} className="float-right" />
          </h6>
          <div className="header-button">
            <SimpleLabel
              text={(
                <Link className="link" to={`/${resourceNameOnApi}/create`}>
                  <FontAwesomeIcon icon="plus" style={{ marginRight: '8px' }} />
                  {T.translate('shipments.list.add')}
                </Link>
              )}
              colour="deep-purple"
              width={150}
              title={T.translate('shipments.list.add')}
            />
          </div>
        </CardHeader>
        <SearchBy
          filters={this.searchFields.map(d => d.name)}
          default={searchBy}
          handleChange={this.handleSearchChange}
          classes={['on-top', 'no-borders']}
        />
        <CardBody className="table-widget">
          <Table responsive striped hover size="sm">
            <thead>
              <tr>
                <th className="text-center">Tracking ID</th>
                <th>Status</th>
                <th>Creator</th>
              </tr>
            </thead>
            <tbody>
              {listItems
                && listItems.length > 0
                && listItems.map(item => (
                  <tr key={item.trackingId}>
                    <td className="text-center">
                      <Link to={`/shipments/details/${item.id}`}>
                        {item.trackingId}
                      </Link>
                    </td>
                    <td>{!!item.status && item.status.name}</td>
                    <td>
                      {!!item.partner && (
                        <Link to={`/partners/details/${item.partner.id}`}>
                          {item.partner.name}
                        </Link>
                      )}
                      {!item.partner && (
                        <em className="font-weight-light text-muted">
                          Created Manually
                        </em>
                      )}
                    </td>
                  </tr>
                ))}
              {(!listItems || listItems.length === 0) && (
                <tr>
                  <td colSpan="3">
                    {T.translate('liveOperations.shipments.noResults')}
                  </td>
                </tr>
              )}
            </tbody>
          </Table>
        </CardBody>
      </Card>
    );
  }
}

export default ShipmentsWidget;
