import React, { Component } from 'react';
import {
  Card, CardHeader, CardBody, Table,
} from 'reactstrap';
import T from 'i18n-react';
import PropTypes from 'prop-types';
import {
  ROUTE_BUNDLE,
  SHIPMENT_STATUS,
  ROUTE_STATUS,
} from 'common/constants/models';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import API from '../../components/API/API';
import InlineLoader from '../../components/Loader/InlineLoader';
import SearchBy from '../../components/Search/SearchBy';

class JourneyGuruWidget extends Component {
  constructor(props) {
    super(props);

    this.API = new API();

    this.searchFields = [
      { name: 'Name', field: 'name' },
      { name: 'ID', field: 'friendlyId' },
    ];

    this.state = {
      isLoading: false,
      onRouteAgentStatusId: null,
      activeAgents: [],
      backupList: [],
      searchBy: this.searchFields[0].name,
    };

    this.searchTimeout = null;

    this.handleSearchChange = (value, type) => {
      const { searchBy, backupList } = this.state;
      switch (type.toLowerCase()) {
        case 'input':
          clearTimeout(this.searchTimeout);
          this.fetchAgentsHandle('clear');

          if (value) {
            const { field } = this.searchFields.find(d => d.name === searchBy) || {};
            if (!field) return;
            this.searchTimeout = setTimeout(async () => {
              this.setState({
                activeAgents: backupList.filter(d => String(d[field])
                  .toLowerCase()
                  .includes(String(value).toLowerCase())),
              });
            }, 500);
          } else {
            this.setState(
              prevState => ({
                activeAgents: prevState.backupList,
              }),
              () => this.fetchAgentsHandle('trigger'),
            );
          }
          break;
        case 'dropdown':
          this.setState(
            prevState => ({
              searchBy: value,
              activeAgents: prevState.backupList,
            }),
            () => this.fetchAgentsHandle('trigger'),
          );
          break;
        default:
          break;
      }
    };

    this.fetchOnRouteAgentStatusId = () => {
      const params = {
        filter: {
          fields: ['id'],
          where: { name: 'On Route' },
        },
      };
      this.API.get('/agentStatuses', { showLoader: false, params }).then(
        (response) => {
          const [firstArrayElement] = response.data;
          const { id: onRouteAgentStatusId } = firstArrayElement;
          this.setState({ onRouteAgentStatusId }, () => {
            this.fetchAgents();
          });
        },
      ).catch(() => { });
    };

    this.fetchAgents = () => {
      const { onRouteAgentStatusId } = this.state;

      if (!onRouteAgentStatusId) return;

      const params = {
        filter: {
          fields: ['id', 'friendlyId', 'name'],
          include: { routes: 'shipments' },
          where: { statusId: onRouteAgentStatusId },
        },
      };

      this.API.get('/agents', { showLoader: false, params }).then((response) => {
        this.setState({
          activeAgents: response.data,
          backupList: response.data,
        });
      }).catch(() => { });
    };
  }

  componentDidMount() {
    this.fetchOnRouteAgentStatusId();
    this.fetchAgentsHandle = (flag) => {
      if (flag === 'trigger') {
        clearInterval(this.fetchInterval);
        this.fetchInterval = setInterval(this.fetchAgents, 10000);
      } else if (flag === 'clear') {
        clearInterval(this.fetchInterval);
      }
    };

    this.fetchAgentsHandle('trigger');
  }

  componentWillUnmount() {
    this.fetchAgentsHandle('clear');
    this.API.cancelAllRequests();
  }

  render() {
    const { isLoading, activeAgents, searchBy } = this.state;

    const { onRowClick } = this.props;

    let journeyGuruList;
    if (activeAgents && activeAgents.length > 0) {
      const sortField = searchBy.toLowerCase();
      activeAgents.sort((a, b) => (a[sortField] > b[sortField] ? 1 : -1));
      journeyGuruList = activeAgents.map((item) => {
        const agentId = item.friendlyId;
        const agentName = item.name;

        const activeRoute = item.routes.find(
          route => route.statusId === ROUTE_STATUS.ASSIGNED.id,
        );

        let activeRouteBundle;
        if (activeRoute && activeRoute.bundleId === ROUTE_BUNDLE.GENERIC.id) {
          activeRouteBundle = 'Generic';
        } else if (
          activeRoute
          && activeRoute.bundleId === ROUTE_BUNDLE.BULK.id
        ) {
          activeRouteBundle = 'Bulk';
        } else if (
          activeRoute
          && activeRoute.bundleId === ROUTE_BUNDLE.MIX.id
        ) {
          activeRouteBundle = 'Mix';
        }

        let activeShipmentIndex;
        const activeShipment = activeRoute
          && activeRoute.shipments.find((shipment, index) => {
            if (shipment.statusId !== 4 && shipment.statusId !== 5) {
              activeShipmentIndex = index + 1;
              return true;
            }
            return false;
          });

        let indexSuffix;
        switch (activeShipmentIndex) {
          case 1:
            indexSuffix = 'st';
            break;
          case 2:
            indexSuffix = 'nd';
            break;
          case 3:
            indexSuffix = 'rd';
            break;
          default:
            indexSuffix = 'th';
            break;
        }

        const totalShipments = activeRoute && activeRoute.shipments.length;

        let activeShipmentStatus;
        if (activeShipment) {
          if (activeShipment.statusId === SHIPMENT_STATUS.CREATED.id) {
            activeShipmentStatus = 'Picking-up';
          } else if (
            [
              SHIPMENT_STATUS.PICKED_UP.id,
              SHIPMENT_STATUS.ON_ROUTE.id,
            ].includes(activeShipment.statusId)
          ) {
            activeShipmentStatus = 'Delivering';
          }
        }

        return (
          <tr
            onClick={e => onRowClick(e, item)}
            onKeyPress={() => { }}
            key={item.id}
            style={{ cursor: 'pointer' }}
          >
            <td className="text-center">{agentId}</td>
            <td>{agentName}</td>
            <td>
              {activeShipment
                ? `${activeShipmentStatus} the ${activeShipmentIndex}${indexSuffix}
                   of ${totalShipments}, on a ${activeRouteBundle}-Route`
                : 'Shipment information not available'}
            </td>
          </tr>
        );
      });
    }

    return (
      <Card className="h-100">
        <CardHeader className="p-2">
          <h6 className="font-weight-light mb-0">
            <FontAwesomeIcon icon="user-tie" className="text-info mr-2" />
            {T.translate('liveOperations.journeyGuru.title')}
            <InlineLoader visible={isLoading} className="float-right" />
          </h6>
        </CardHeader>
        <SearchBy
          filters={this.searchFields.map(d => d.name)}
          default={searchBy}
          handleChange={this.handleSearchChange}
          classes={['on-top', 'no-borders']}
        />
        <CardBody className="table-widget">
          <Table responsive striped hover size="sm">
            <thead>
              <tr>
                <th className="text-center">ID</th>
                <th>Agent</th>
                <th>Shipments</th>
              </tr>
            </thead>
            <tbody>
              {activeAgents && activeAgents.length > 0 ? (journeyGuruList) : (
                <tr>
                  <td colSpan="3">
                    {T.translate('liveOperations.journeyGuru.noResults')}
                  </td>
                </tr>
              )}
            </tbody>
          </Table>
        </CardBody>
      </Card>
    );
  }
}

JourneyGuruWidget.propTypes = {
  onRowClick: PropTypes.func,
};

JourneyGuruWidget.defaultProps = {
  onRowClick: () => { },
};

export default JourneyGuruWidget;
