import React, { Component } from 'react';
import { Card, CardHeader, CardBody } from 'reactstrap';
import { Redirect } from 'react-router-dom';
import T from 'i18n-react';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import API from '../../components/API/API';
import { MapWithMarkers } from '../../components/Maps/MapWithMarkers';
import InlineLoader from '../../components/Loader/InlineLoader';
import AgentInfoWindow from './AgentInfoWindow';

class LiveViewWidget extends Component {
  constructor(props) {
    super(props);
    const { onPressMarker } = props;

    this.API = new API();

    this.state = {
      agentMarkers: [],
      redirectTo: '',
      onRouteAgentStatusId: null,
      isLoading: false,
    };

    this.fetchOnRouteAgentStatusId = () => {
      const params = {
        filter: {
          fields: ['id'],
          where: { name: 'On Route' },
        },
      };
      this.setState({ isLoading: true }, () => {
        this.API.get('/agentStatuses', { showLoader: false, params }).then(
          (response) => {
            const [firstArrayElement] = response.data;
            const { id: onRouteAgentStatusId } = firstArrayElement;
            this.setState({ onRouteAgentStatusId }, () => {
              this.fetchAgents();
            });
          },
        ).catch(() => { });
      });
    };

    this.fetchAgents = () => {
      const { onRouteAgentStatusId } = this.state;

      if (!onRouteAgentStatusId) return;

      const params = {
        filter: {
          where: { statusId: onRouteAgentStatusId },
          include: ['status', 'routes'],
        },
      };

      this.API.get('/agents', { showLoader: false, params }).then((response) => {
        this.setState({
          isLoading: false,
          agentMarkers: response.data.map(agent => ({
            position: agent.currentGeoPoint,
            onClick: event => onPressMarker(event, agent),
            infoWindowId: agent.id,
            infoWindow: <AgentInfoWindow agent={agent} />,
          })),
        });
      }).catch(() => { });
    };
  }

  componentDidMount() {
    this.fetchOnRouteAgentStatusId();
    this.fetchAgentsHandle = setInterval(this.fetchAgents, 10000);
  }

  componentWillUnmount() {
    clearInterval(this.fetchAgentsHandle);
    this.API.cancelAllRequests();
  }

  render() {
    const { redirectTo, agentMarkers, isLoading } = this.state;

    const { selectedMarker } = this.props;

    if (redirectTo) return <Redirect to={redirectTo} />;

    return (
      <Card className="h-100">
        <CardHeader className="p-2">
          <h6 className="font-weight-light mb-0">
            <FontAwesomeIcon icon="route" className="text-danger mr-2" />
            {T.translate('liveOperations.liveView.title')}
            <InlineLoader visible={isLoading} className="float-right" />
          </h6>
        </CardHeader>
        <CardBody className="p-0 h-100">
          <MapWithMarkers
            markers={agentMarkers}
            activeInfoWindow={selectedMarker}
            autoCenter
            mapProps={{
              defaultCenter: {
                lat: 41.9,
                lng: -87.624,
              },
              defaultOptions: {
                zoom: 15,
                scrollwheel: false,
                zoomControl: true,
                fullscreenControl: false,
              },
            }}
            containerHeight="100%"
          />
        </CardBody>
      </Card>
    );
  }
}

LiveViewWidget.propTypes = {
  onPressMarker: PropTypes.func.isRequired,
  selectedMarker: PropTypes.number,
};

LiveViewWidget.defaultProps = {
  selectedMarker: null,
};

export default LiveViewWidget;
