import React from 'react';
import PropTypes from 'prop-types';
import { Button } from 'reactstrap';
import { Link } from 'react-router-dom';
import T from 'i18n-react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const AgentInfoWindow = (props) => {
  const { agent } = props;
  const {
    id, name, friendlyId, phone, status,
  } = agent;

  return (
    <div>
      <strong>Name: </strong>
      {name}
      <br />

      <strong>Identifier: </strong>
      {friendlyId}
      <br />

      <strong>Phone: </strong>
      {phone}
      <br />

      <strong>Status: </strong>
      {status.name}

      <hr className="my-2" />

      <Button color="primary" size="sm" block>
        <Link
          className="agent-info-button"
          to={`/agents/details/${id}`}
          target="_blank"
        >
          <FontAwesomeIcon icon="user" />
          {' '}
          {T.translate('liveOperations.liveView.agentDetailsButton')}
        </Link>
      </Button>
    </div>
  );
};

AgentInfoWindow.propTypes = {
  agent: PropTypes.shape({
    name: PropTypes.string,
    friendlyId: PropTypes.string,
    phone: PropTypes.string,
    status: PropTypes.shape({
      id: PropTypes.number,
      name: PropTypes.string,
    }),
  }).isRequired,
};

export default AgentInfoWindow;
