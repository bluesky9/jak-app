import React, { Component } from 'react';
import { Card, CardHeader, CardBody } from 'reactstrap';
import T from 'i18n-react';
import STATUS from 'http-status';
import get from 'get-value';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { StatsCard } from '../../components/StatsCard/StatsCard';
import API from '../../components/API/API';
import { AuthProvider } from '../../components/Auth/AuthProvider';
import InlineLoader from '../../components/Loader/InlineLoader';

class StatisticsWidget extends Component {
  constructor(props) {
    super(props);

    this.API = new API();
    this.account = new AuthProvider().getUser();

    this.state = {
      isLoading: false,
      shipmentsCount: '',
      agentsCount: '',
      areasCount: '',
      routesCount: '',
    };

    this.fetchStatistics = () => {
      const resourcesToCont = ['shipments', 'agents', 'areas', 'routes'];
      this.setState({ isLoading: true });
      resourcesToCont.forEach((resource) => {
        const stateToChange = {
          isLoading: false,
        };
        this.API.get(`/${resource}/count`, { showLoader: false })
          .then((res) => {
            stateToChange[`${resource}Count`] = res.data.count;
          })
          .catch((reason) => {
            if (get(reason, 'response.status', 0) === STATUS.NOT_FOUND) {
              stateToChange[`${resource}Count`] = '0';
            } else {
              stateToChange[`${resource}Count`] = T.translate(
                'liveOperations.statistics.notAvailable',
              );
            }
          })
          .finally(() => {
            if (!this.willUnmount) this.setState(stateToChange);
          });
      });
    };
  }

  componentDidMount() {
    this.fetchStatistics();
    this.fetchStatisticsHandle = setInterval(this.fetchStatistics, 10000);
  }

  componentWillUnmount() {
    this.willUnmount = true;
    clearInterval(this.fetchStatisticsHandle);
    this.API.cancelAllRequests();
  }

  render() {
    const {
      shipmentsCount,
      agentsCount,
      areasCount,
      routesCount,
      isLoading,
    } = this.state;
    return (
      <Card className="h-100">
        <CardHeader className="p-2">
          <h6 className="font-weight-light mb-0">
            <FontAwesomeIcon icon="chart-bar" className="text-primary mr-2" />
            {T.translate('liveOperations.statistics.title')}
            <InlineLoader visible={isLoading} className="float-right" />
          </h6>
        </CardHeader>
        <CardBody className="p-0 scroll d-flex flex-wrap flex-row justify-content-around align-items-center">
          <StatsCard
            icon="shipping-fast"
            className="text-success"
            statsText={T.translate('liveOperations.statistics.shipments')}
            statsValue={shipmentsCount}
          />
          <StatsCard
            icon="user-friends"
            className="text-info"
            statsText={T.translate('liveOperations.statistics.agents')}
            statsValue={agentsCount}
          />
          <StatsCard
            icon="map-marked-alt"
            className="text-warning"
            statsText={T.translate('liveOperations.statistics.areas')}
            statsValue={areasCount}
          />
          <StatsCard
            icon="route"
            className="text-danger"
            statsText={T.translate('liveOperations.statistics.routes')}
            statsValue={routesCount}
          />
        </CardBody>
      </Card>
    );
  }
}

export default StatisticsWidget;
