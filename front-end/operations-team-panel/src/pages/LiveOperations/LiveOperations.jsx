import React, { Component } from 'react';
import {
  Container, Row, Col, Button,
} from 'reactstrap';
import T from 'i18n-react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Fullscreen from 'react-full-screen';
import LiveViewWidget from './LiveViewWidget';
import JourneyGuruWidget from './JourneyGuruWidget';
import AlertsAndEventsWidget from './AlertsAndEventsWidget';
import ShipmentsWidget from './ShipmentsWidget';
import StatisticsWidget from './StatisticsWidget';

class LiveOperations extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isFullScreen: false,
      selectedMarker: null,
    };

    this.focusMarker = (event, agent) => {
      this.setState({
        selectedMarker: agent.id,
      });
    };

    this.toggleFullScreen = () => {
      const { isFullScreen } = this.state;
      this.setState({ isFullScreen: !isFullScreen });
    };
  }

  render() {
    const { isFullScreen: isFull, selectedMarker } = this.state;

    return (
      <div className="operations-room-container">
        <Fullscreen
          enabled={isFull}
          onChange={isFullScreen => this.setState({ isFullScreen })}
        >
          <Button
            className="fullscreen-button"
            onClick={this.toggleFullScreen}
            color="dark"
            title={T.translate('liveOperations.fullScreen')}
          >
            <FontAwesomeIcon icon={isFull ? 'compress' : 'expand'} />
          </Button>
          <Container
            fluid
            className={`fullscreen-container ${isFull ? 'active' : ''}`}
          >
            <Row className="align-items-stretch">
              <Col md={12}>
                <LiveViewWidget
                  onPressMarker={this.focusMarker}
                  selectedMarker={selectedMarker}
                />
              </Col>
            </Row>
            <Row className="align-items-stretch">
              <Col md={6}>
                <AlertsAndEventsWidget />
              </Col>
              <Col md={6}>
                <JourneyGuruWidget onRowClick={this.focusMarker} />
              </Col>
            </Row>
            <Row className="align-items-stretch">
              <Col md={6}>
                <ShipmentsWidget />
              </Col>
              <Col md={6}>
                <StatisticsWidget />
              </Col>
            </Row>
          </Container>
        </Fullscreen>
      </div>
    );
  }
}

export default LiveOperations;
