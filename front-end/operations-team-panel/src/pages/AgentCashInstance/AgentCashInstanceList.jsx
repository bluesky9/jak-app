import React, { Component } from 'react';
import {
  Container, Row, Col, Table, Button,
} from 'reactstrap';
import Moment from 'react-moment';
import { Link } from 'react-router-dom';
import SweetAlert from 'sweetalert2-react';
import T from 'i18n-react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { Pagination } from '../../components/Pagination/Pagination';
import API from '../../components/API/API';
import SearchBy from '../../components/Search/SearchBy';

export default class AgentCashInstanceList extends Component {
  constructor(props) {
    super(props);

    this.API = new API();

    this.pagination = React.createRef();

    this.searchFields = [
      { name: 'Agent', field: 'agent' },
      { name: 'Type', field: 'type' },
      { name: 'Due Date', field: 'dueDatetime' },
      { name: 'Amount', field: 'amount' },
    ];

    this.sortFields = [
      { name: 'agent', field: 'agentId' },
      { name: 'type', field: 'typeId' },
      { name: 'dueDatetime', field: 'dueDatetime' },
      { name: 'amount', field: 'amount' },
    ];

    this.getIcon = (name) => {
      const { sortBy } = this.state;
      const { field } = this.sortFields.find(d => d.name === name);
      const icon = field === sortBy.field ? sortBy.icon : 'sort';
      return <FontAwesomeIcon className="sort-icon" icon={icon} />;
    };

    this.state = {
      resourceNameOnApi: 'agentCashInstances',
      listItems: [],
      sortBy: {
        field: '',
        type: '',
        icon: '',
      },
      order: 'agentId ASC',
      backupList: [],
      searchBy: this.searchFields[0].name,
      searchParam: {
        filter: {
          include: ['agent', 'type'],
        },
      },
    };

    this.handleSortChange = (name) => {
      const { sortBy } = this.state;
      const { type, field: previousField } = sortBy;
      const { field } = this.sortFields.find(d => d.name === name);
      let newType = 'DESC';

      if (field === previousField) {
        newType = type === 'ASC' ? 'DESC' : 'ASC';
      }

      this.setState(
        {
          sortBy: {
            field,
            type: newType,
            icon: newType === 'ASC' ? 'sort-up' : 'sort-down',
          },
          order: `${field} ${newType}`,
        },
        () => {
          this.pagination.current.fetchItemCount();
        },
      );
    };

    this.delete = (id) => {
      const { resourceNameOnApi } = this.state;
      this.API.delete(`/${resourceNameOnApi}/${id}`).then(() => {
        this.pagination.current.fetchItems();
      });
    };

    this.searchTimeout = null;

    this.handleSearchChange = (value, type) => {
      if (type.toLowerCase() === 'input') {
        const { searchBy } = this.state;
        clearTimeout(this.searchTimeout);

        if (value) {
          const key = this.searchFields.find(d => d.name === searchBy).field;
          const val = value.toLowerCase();

          this.searchTimeout = setTimeout(async () => {
            let params;
            let makeRequest = true;

            switch (key) {
              case 'dueDatetime': {
                const date = new Date(value);

                // will only query if date is valid
                if (date.toDateString().toLowerCase() === 'invalid date') {
                  clearTimeout(this.searchTimeout);
                  makeRequest = false;
                }

                const year = date.getFullYear();
                const month = date.getMonth() + 1;
                const day = date.getDate();

                const startDate = `${year}-${month}-${day}`;
                const endDate = `${year}-${month}-${day + 1}`;

                params = {
                  filter: {
                    where: {
                      [key]: {
                        between: [startDate, endDate],
                      },
                    },
                  },
                };

                break;
              }

              case 'amount': {
                let operation;

                if (val.startsWith('>=') || val.startsWith('=>')) {
                  operation = 'gte';
                } else if (val.startsWith('>')) {
                  operation = 'gt';
                } else if (val.startsWith('<=') || val.startsWith('=<')) {
                  operation = 'lte';
                } else if (val.startsWith('<')) {
                  operation = 'lt';
                } else if (val.startsWith('=')) {
                  operation = 'eq';
                }

                // removes mathematical symbols
                const strippedValue = val.replace(/>|=|</g, '');

                if (operation && operation !== 'eq') {
                  params = {
                    filter: {
                      where: {
                        [key]: {
                          [operation]: strippedValue,
                        },
                      },
                    },
                  };
                } else {
                  params = {
                    filter: {
                      where: {
                        [key]: strippedValue,
                      },
                    },
                  };
                }

                break;
              }

              case 'agent': {
                const agentsRequest = await this.API.get('/agents', {
                  params: {
                    filter: {
                      fields: ['id'],
                      where: { name: { ilike: `%${val}%` } },
                    },
                  },
                });

                const agentIds = agentsRequest.data.map(agent => agent.id);

                params = {
                  filter: {
                    where: {
                      agentId: {
                        inq: agentIds,
                      },
                    },
                  },
                };

                break;
              }

              case 'type': {
                const typesRequest = await this.API.get('/agentCashTypes', {
                  params: {
                    filter: {
                      fields: ['id'],
                      where: { name: { ilike: `%${val}%` } },
                    },
                  },
                });

                const typeIds = typesRequest.data.map(
                  agentCashType => agentCashType.id,
                );

                params = {
                  filter: {
                    where: {
                      typeId: {
                        inq: typeIds,
                      },
                    },
                  },
                };

                break;
              }

              default:
                break;
            }

            params.filter.include = ['agent', 'type'];

            if (makeRequest) {
              this.setState({ searchParam: params }, () => {
                this.pagination.current.fetchItemCount();
              });
            }
          }, 500);
        } else {
          const params = {
            filter: {
              include: ['agent', 'type'],
            },
          };

          this.setState({ searchParam: params }, () => {
            this.pagination.current.fetchItemCount();
          });
        }
      } else if (type.toLowerCase() === 'dropdown') {
        this.setState(prevState => ({
          searchBy: value,
          listItems: prevState.backupList,
        }));
      }
    };
  }

  render() {
    const {
      resourceNameOnApi,
      listItems,
      showDeletionConfirmation,
      selectedItemToBeDeleted,
      searchBy,
      searchParam,
      order,
    } = this.state;

    return [
      <Container key="agent-cash-instance-list-container">
        <Row className="mb-3">
          <Col>
            <h3 className="text-secondary font-weight-bold">
              {T.translate('agentCashInstances.list.title')}
            </h3>
            <h4 className="text-secondary font-weight-light mb-0">
              {T.translate('agentCashInstances.list.description')}
            </h4>
          </Col>
          <Col
            md={3}
            className="d-flex justify-content-end align-items-end search-wrapper"
            style={{ marginRight: '20px' }}
          >
            <SearchBy
              filters={this.searchFields.map(d => d.name)}
              default={searchBy}
              resourceNameOnApi={resourceNameOnApi}
              handleChange={this.handleSearchChange}
            />
          </Col>
          <Col md={3} className="d-flex justify-content-end align-items-end">
            <Link
              to={`/${resourceNameOnApi}/create`}
              className="btn btn btn-secondary btn-rounded px-3"
            >
              <FontAwesomeIcon icon="plus" />
              {' '}
              {T.translate('agentCashInstances.list.add')}
            </Link>
          </Col>
        </Row>
        <Table responsive striped hover className="border">
          <thead>
            <tr>
              <th>
                <span
                  className="table-header"
                  onKeyPress={() => { }}
                  role="button"
                  tabIndex={-1}
                  onClick={() => this.handleSortChange('dueDatetime')}
                >
                  {T.translate('agentCashInstances.fields.dueDatetime')}
                  {this.getIcon('dueDatetime')}
                </span>
              </th>
              <th>
                <span
                  className="table-header"
                  onKeyPress={() => { }}
                  role="button"
                  tabIndex={-1}
                  onClick={() => this.handleSortChange('amount')}
                >
                  {T.translate('agentCashInstances.fields.amount')}
                  {this.getIcon('amount')}
                </span>
              </th>
              <th>
                <span
                  className="table-header"
                  onKeyPress={() => { }}
                  role="button"
                  tabIndex={-1}
                  onClick={() => this.handleSortChange('agent')}
                >
                  {T.translate('agentCashInstances.fields.agent')}
                  {this.getIcon('agent')}
                </span>
              </th>
              <th colSpan={2}>
                <span
                  className="table-header"
                  onKeyPress={() => { }}
                  role="button"
                  tabIndex={-1}
                  onClick={() => this.handleSortChange('type')}
                >
                  {T.translate('agentCashInstances.fields.type')}
                  {this.getIcon('type')}
                </span>
              </th>
            </tr>
          </thead>
          <tbody>
            {listItems.map(item => (
              <tr key={item.id}>
                <td>
                  <Moment date={item.dueDatetime} />
                </td>
                <td>{item.amount}</td>
                <td>
                  {item.agent ? (
                    <Link
                      to={{
                        pathname: `/agents/details/${item.agentId}`,
                        state: { from: 'agentCashInstances' },
                      }}
                    >
                      {item.agent.name}
                    </Link>
                  ) : (
                    T.translate('defaults.notSet')
                  )}
                </td>
                <td>
                  <i
                    className={
                      item.type && item.type.isProfitForCompany
                        ? 'pe-7s-plus text-success'
                        : 'pe-7s-less text-danger'
                    }
                  />
                  &nbsp;
                  {item.type ? item.type.name : T.translate('defaults.notSet')}
                </td>
                <td className="text-right py-0 align-middle">
                  <div className="btn-group" role="group">
                    <Link
                      to={`/${resourceNameOnApi}/details/${item.id}`}
                      className="btn btn-primary btn-sm"
                      title={T.translate('agentCashInstances.list.viewTooltip')}
                    >
                      <FontAwesomeIcon icon="eye" fixedWidth />
                    </Link>
                    <Link
                      className="btn btn-primary btn-sm"
                      to={`/${resourceNameOnApi}/update/${item.id}`}
                      title={T.translate('agentCashInstances.list.editTooltip')}
                    >
                      <FontAwesomeIcon icon="pencil-alt" fixedWidth />
                    </Link>
                    <Button
                      size="sm"
                      color="primary"
                      title={T.translate(
                        'agentCashInstances.list.deleteTooltip',
                      )}
                      onClick={() => {
                        this.setState({
                          selectedItemToBeDeleted: item,
                          showDeletionConfirmation: true,
                        });
                      }}
                    >
                      <FontAwesomeIcon icon="trash-alt" fixedWidth />
                    </Button>
                  </div>
                </td>
              </tr>
            ))}
          </tbody>
        </Table>
        <Pagination
          ref={this.pagination}
          resourceNameOnApi={resourceNameOnApi}
          filter={{ ...searchParam.filter, order }}
          onItemsReceived={(listItemsReceived) => {
            this.setState({
              listItems: listItemsReceived,
              backupList: listItemsReceived,
            });
          }}
        />
      </Container>,
      <SweetAlert
        key="sweet-alert"
        show={showDeletionConfirmation}
        title={T.translate('agentCashInstances.list.deleteWarning.title', {
          itemToBeDeleted: selectedItemToBeDeleted
            ? selectedItemToBeDeleted.name
            : '',
        })}
        text={T.translate('agentCashInstances.list.deleteWarning.message')}
        type="warning"
        showCancelButton
        confirmButtonText={T.translate(
          'agentCashInstances.list.deleteWarning.confirmButton',
        )}
        cancelButtonText={T.translate(
          'agentCashInstances.list.deleteWarning.cancelButton',
        )}
        confirmButtonClass="btn btn-primary btn-rounded mx-2 btn-lg px-5"
        cancelButtonClass="btn btn-secondary btn-rounded mx-2 btn-lg px-5"
        buttonsStyling={false}
        onConfirm={() => {
          this.delete(selectedItemToBeDeleted.id);
          this.setState({ showDeletionConfirmation: false });
        }}
      />,
    ];
  }
}
