import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Moment from 'react-moment';
import {
  Container,
  Row,
  Col,
  Card,
  CardHeader,
  CardBody,
  Button,
} from 'reactstrap';
import { Link, withRouter } from 'react-router-dom';
import T from 'i18n-react';

import API from '../../components/API/API';

class AgentCashInstanceDetails extends Component {
  constructor(props) {
    super(props);

    this.API = new API();

    this.state = {
      resourceNameOnApi: 'agentCashInstances',
      resource: {},
      hiddenPropertyNamesOnDetail: ['id', 'typeId', 'agentId'],
    };
  }

  componentDidMount() {
    const { resourceNameOnApi } = this.state;
    const { match } = this.props;
    this.API.get(`/${resourceNameOnApi}/${match.params.id}`, {
      params: {
        filter: {
          include: ['type', 'agent'],
        },
      },
    }).then((response) => {
      this.setState({
        resource: response.data,
      });
    });
  }

  render() {
    const {
      resourceNameOnApi,
      resource,
      hiddenPropertyNamesOnDetail,
    } = this.state;
    const { history } = this.props;
    return (
      <Container>
        <h3 className="text-secondary font-weight-bold">
          {T.translate('agentCashInstances.detail.title')}
        </h3>
        <h4 className="text-secondary font-weight-light mb-0">
          {T.translate('agentCashInstances.detail.description')}
        </h4>
        <hr />
        {Object.keys(resource).map((property) => {
          if (hiddenPropertyNamesOnDetail.includes(property)) {
            return null;
          }

          let propertyValue;

          if (
            ['createdAt', 'updatedAt', 'dueDatetime', 'settledAt'].includes(
              property,
            )
          ) {
            propertyValue = (
              <span>
                <Moment date={resource[property]} />
              </span>
            );
          } else if (property === 'agent') {
            propertyValue = <span>{resource[property].name}</span>;
          } else if (property === 'type') {
            propertyValue = (
              <span>
                {resource[property].name}
                {' '}
                <em className="text-muted">
                  (
                  {resource[property].isProfitForCompany
                    ? 'This agent owes Jak'
                    : 'Jak owes this agent'}
                  )
                </em>
              </span>
            );
          } else if (['isSettled', 'isProfitForCompany'].includes(property)) {
            propertyValue = (
              <span>
                {resource[property]
                  ? T.translate('defaults.yes')
                  : T.translate('defaults.no')}
              </span>
            );
          } else {
            propertyValue = (
              <span>
                {resource[property] || T.translate('defaults.notSet')}
              </span>
            );
          }

          return (
            <Row className="mb-3" key={property}>
              <Col md={4} className="font-weight-bold">
                <span>
                  {T.translate(`agentCashInstances.fields.${property}`)}
                </span>
              </Col>
              <Col md={8}>{propertyValue}</Col>
            </Row>
          );
        })}
        <hr />
        <Row>
          {resource.agentId && (
            <Col md={6}>
              <Card className="mb-3">
                <CardHeader className="p-3">
                  <h5 className="text-secondary font-weight-bold mb-1">
                    {T.translate('agentCashInstances.detail.agent.title')}
                  </h5>
                  <h6 className="text-secondary font-weight-light mb-0">
                    {T.translate('agentCashInstances.detail.agent.description')}
                  </h6>
                </CardHeader>
                <CardBody className="p-3">
                  {Object.keys(resource.agent).map((property) => {
                    if (['name', 'phone'].includes(property)) {
                      return (
                        <Row className="mb-3" key={`agent_${property}`}>
                          <Col md={4} className="font-weight-bold">
                            <span>
                              {T.translate(`agents.fields.${property}`)}
                            </span>
                          </Col>
                          <Col md={8}>
                            {resource.agent[property]
                              || T.translate('defaults.notSet')}
                          </Col>
                        </Row>
                      );
                    }
                    return null;
                  })}
                  <div className="clearfix text-center">
                    <Link
                      to={`/agents/details/${resource.agentId}`}
                      className="btn btn-rounded btn-outline-primary btn-block"
                    >
                      {T.translate('agentCashInstances.detail.agent.viewAgent')}
                    </Link>
                  </div>
                </CardBody>
              </Card>
            </Col>
          )}
        </Row>
        <hr className="mt-0" />
        <div className="clearfix text-center">
          <Button
            onClick={history.goBack}
            className="btn btn-rounded btn-lg btn-secondary float-md-left px-5"
          >
            {T.translate('defaults.goBack')}
          </Button>
          <Link
            to={`/${resourceNameOnApi}/update/${resource.id}`}
            className="btn btn-rounded btn-lg btn-primary float-md-right px-5"
          >
            {T.translate('agentCashInstances.detail.editButton')}
          </Link>
        </div>
      </Container>
    );
  }
}

AgentCashInstanceDetails.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string.isRequired,
    }),
  }),
  history: PropTypes.shape({
    goBack: PropTypes.func.isRequired,
  }).isRequired,
};

AgentCashInstanceDetails.defaultProps = {
  match: {
    params: {
      id: '',
    },
  },
};

export default withRouter(AgentCashInstanceDetails);
