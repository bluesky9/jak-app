import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Moment from 'react-moment';
import {
  Container, Row, Col, Button,
} from 'reactstrap';
import { Link, withRouter } from 'react-router-dom';
import T from 'i18n-react';

import QRCode from 'qrcode.react';
import API from '../../components/API/API';
import { MapWithMarkers } from '../../components/Maps/MapWithMarkers';
import ManyToManyRelationManager from '../../components/ManyToManyRelationManager/ManyToManyRelationManager';
import HasManyRelationManager from '../../components/HasManyRelationManager/HasManyRelationManager';

class ShipmentDetails extends Component {
  constructor(props) {
    super(props);

    this.API = new API();

    this.state = {
      resourceNameOnApi: 'shipments',
      resource: {},
      hiddenPropertyNamesOnDetail: [
        'id',
        'statusId',
        'routeId',
        'typeId',
        'senderAreaId',
        'recipientAreaId',
        'partnerId',
      ],
    };

    this.renderQRCode = (value, size) => (
      <div className="qrcode-container">
        <QRCode value={value} renderAs="svg" size={size} />
      </div>
    );
  }

  componentDidMount() {
    const { resourceNameOnApi } = this.state;
    const { match } = this.props;
    this.API.get(`/${resourceNameOnApi}/${match.params.id}`, {
      params: {
        filter: {
          include: [
            'type',
            'status',
            'route',
            'partner',
            'senderArea',
            'recipientArea',
            'partner',
          ],
        },
      },
    }).then((response) => {
      this.setState({
        resource: response.data,
      });
    });
  }

  render() {
    const {
      resourceNameOnApi,
      resource,
      hiddenPropertyNamesOnDetail,
    } = this.state;
    const { history } = this.props;

    return (
      <Container>
        <h3 className="text-secondary font-weight-bold">
          {T.translate('shipments.detail.title')}
        </h3>
        <h4 className="text-secondary font-weight-light mb-0">
          {T.translate('shipments.detail.description')}
        </h4>
        <hr />
        {Object.keys(resource).map((property) => {
          let qr = '';

          if (hiddenPropertyNamesOnDetail.includes(property)) {
            return null;
          }

          let propertyValue;

          if (['createdAt', 'updatedAt'].includes(property)) {
            propertyValue = (
              <span>
                <Moment date={resource[property]} />
              </span>
            );
          } else if (property === 'type' && resource.type) {
            propertyValue = (
              <span>
                {T.translate(`shipments.fields.types.${resource.type.name}`)}
              </span>
            );
          } else if (property === 'status' && resource.status) {
            propertyValue = (
              <span>
                {T.translate(
                  `shipments.fields.statuses.${resource.status.name}`,
                )}
              </span>
            );
          } else if (property === 'recipientAddressConfirmed') {
            propertyValue = (
              <span>
                {resource[property]
                  ? T.translate('defaults.yes')
                  : T.translate('defaults.no')}
              </span>
            );
          } else if (property === 'route') {
            propertyValue = (
              <span>
                <Link to={`/routes/details/${resource[property].id}`}>
                  {T.translate('shipments.detail.viewRoute')}
                </Link>
              </span>
            );
          } else if (
            ['recipientGeoPoint', 'senderGeoPoint'].includes(property)
            && resource[property]
          ) {
            // Don't display sender address map if there's no SenderAddress set.
            if (property === 'senderGeoPoint' && !resource.senderAddress) return null;

            propertyValue = [
              <em key={`${property}_coordinates`}>
                {resource[property].lat}
                {', '}
                {resource[property].lng}
              </em>,
              <MapWithMarkers
                key={`${property}_marker`}
                mapProps={{
                  defaultCenter: {
                    lat: resource[property].lat,
                    lng: resource[property].lng,
                  },
                  defaultOptions: {
                    zoom: 15,
                    scrollwheel: false,
                    zoomControl: true,
                  },
                }}
                containerHeight="150px"
                markers={[
                  {
                    position: {
                      lat: resource[property].lat,
                      lng: resource[property].lng,
                    },
                  },
                ]}
                autoCenter={false}
              />,
            ];
          } else if (
            ['recipientArea', 'senderArea', 'partner'].includes(property)
            && resource[property]
          ) {
            propertyValue = <span>{resource[property].name}</span>;
          } else {
            propertyValue = (
              <span>
                {resource[property] || T.translate('defaults.notSet')}
              </span>
            );

            if (property === 'trackingId' && resource[property]) {
              qr = (
                <Col md={4}>
                  {this.renderQRCode(
                    resource[property],
                    resource.senderAddress ? 180 : 280,
                  )}
                </Col>
              );
            }
          }

          return (
            <Row className="mb-3" key={property}>
              <Col md={4} className="font-weight-bold">
                <span>{T.translate(`shipments.fields.${property}`)}</span>
              </Col>
              <Col md={property.toLowerCase() === 'trackingid' ? 4 : 8}>
                {propertyValue}
              </Col>
              {qr}
            </Row>
          );
        })}
        <hr />
        <Row>
          <Col md={6}>
            {resource.id && (
              <ManyToManyRelationManager
                resourceEndPoint={resourceNameOnApi}
                resourceId={resource.id}
                relationEndPoint="events"
                relationLabel={T.translate('shipments.detail.events.label')}
                title={T.translate('shipments.detail.events.title')}
                category={T.translate('shipments.detail.events.description')}
                itemText={T.translate('shipments.detail.events.itemText', {
                  statusName: '{{status.name}}',
                  createdAt: '{{createdAt}}',
                })}
                readOnly
              />
            )}
          </Col>
          <Col md={6}>
            {resource.id && (
              <HasManyRelationManager
                resourceEndPoint={resourceNameOnApi}
                resourceId={resource.id}
                relationEndPoint="cashInstances"
                relationAttribute="shipmentId"
                relationLabel={T.translate(
                  'shipments.detail.cashInstances.label',
                )}
                title={T.translate('shipments.detail.cashInstances.title')}
                category={T.translate(
                  'shipments.detail.cashInstances.description',
                )}
                itemText={T.translate(
                  'shipments.detail.cashInstances.itemText',
                  {
                    direction: '{{direction.name}}',
                    amount: '{{amount}}',
                  },
                )}
                relationDetailRoute="/partnerCashInstances/details"
                readOnly
              />
            )}
          </Col>
        </Row>
        <hr className="mt-0" />
        <div className="clearfix text-center">
          <Button
            onClick={history.goBack}
            className="btn btn-rounded btn-lg btn-secondary float-md-left px-5"
          >
            {T.translate('defaults.goBack')}
          </Button>
          <Link
            to={`/${resourceNameOnApi}/update/${resource.id}`}
            className="btn btn-rounded btn-lg btn-primary float-md-right px-5"
          >
            {T.translate('shipments.detail.editButton')}
          </Link>
        </div>
      </Container>
    );
  }
}

ShipmentDetails.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string.isRequired,
    }),
  }),
  history: PropTypes.shape({
    goBack: PropTypes.func.isRequired,
  }).isRequired,
};

ShipmentDetails.defaultProps = {
  match: {
    params: {
      id: '',
    },
  },
};

export default withRouter(ShipmentDetails);
