import React, { Component } from 'react';
import {
  Container, Row, Col, Table, Button,
} from 'reactstrap';
import { Link } from 'react-router-dom';
import SweetAlert from 'sweetalert2-react';
import T from 'i18n-react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { Pagination } from '../../components/Pagination/Pagination';
import API from '../../components/API/API';
import SearchBy from '../../components/Search/SearchBy';

export default class ShipmentList extends Component {
  constructor(props) {
    super(props);

    this.API = new API();

    this.pagination = React.createRef();

    this.sortFields = [
      { name: 'trackingId', field: 'trackingId' },
      { name: 'status', field: 'statusId' },
      { name: 'type', field: 'typeId' },
      { name: 'partner', field: 'partnerId' },
    ];

    this.searchFields = [
      { name: 'Tracking Id', field: 'trackingId' },
      { name: 'Status', field: 'statusId' },
      { name: 'Type', field: 'typeId' },
      { name: 'Sender', field: 'senderName' },
      { name: 'Recipient', field: 'recipientName' },
      { name: 'Creation Date', field: 'createdAt' },
      { name: 'Delivery Date', field: 'deliveryDatetime' },
      { name: 'Belongs To', field: 'partnerId' },
    ];

    this.getIcon = (name) => {
      const { sortBy } = this.state;

      const element = this.sortFields.find(d => d.name === name);
      if (!element) {
        return '';
      }

      const { field } = this.sortFields.find(d => d.name === name);
      const icon = field === sortBy.field ? sortBy.icon : 'sort';
      return <FontAwesomeIcon className="sort-icon" icon={icon} />;
    };

    this.state = {
      resourceNameOnApi: 'shipments',
      listItems: [],
      sortBy: {
        field: '',
        type: '',
        icon: '',
      },
      order: 'trackingId ASC',
      backupList: [],
      searchBy: this.searchFields[0].name,
      searchParam: {
        filter: {
          include: ['partner', 'status', 'type'],
        },
      },
    };

    this.delete = (id) => {
      const { resourceNameOnApi } = this.state;
      this.API.delete(`/${resourceNameOnApi}/${id}`).then(() => {
        this.pagination.current.fetchItems();
      });
    };

    this.searchTimeout = null;

    this.handleSortChange = (name) => {
      const element = this.sortFields.find(d => d.name === name);
      if (!element) {
        return;
      }

      const { sortBy } = this.state;
      const { type, field: previousField } = sortBy;
      const { field } = element;

      let newType = 'DESC';

      if (field === previousField) {
        newType = type === 'ASC' ? 'DESC' : 'ASC';
      }

      this.setState(
        {
          sortBy: {
            field,
            type: newType,
            icon: newType === 'ASC' ? 'sort-up' : 'sort-down',
          },
          order: `${field} ${newType}`,
        },
        () => {
          this.pagination.current.fetchItemCount();
        },
      );
    };

    this.handleSearchChange = (value, type) => {
      if (type.toLowerCase() === 'input') {
        const { searchBy } = this.state;
        clearTimeout(this.searchTimeout);

        if (value) {
          const { field } = this.searchFields.find(d => d.name === searchBy);

          this.searchTimeout = setTimeout(async () => {
            let params;
            let makeRequest = true;

            switch (field) {
              case 'createdAt':
              case 'deliveryDatetime': {
                const date = new Date(value);

                // will only query if date is valid
                if (date.toDateString().toLowerCase() === 'invalid date') {
                  clearTimeout(this.searchTimeout);
                  makeRequest = false;
                }

                const year = date.getFullYear();
                const month = date.getMonth() + 1;
                const day = date.getDate();

                const startDate = `${year}-${month}-${day}`;
                const endDate = `${year}-${month}-${day + 1}`;

                params = {
                  filter: {
                    where: {
                      [field]: {
                        between: [startDate, endDate],
                      },
                    },
                  },
                };

                break;
              }

              case 'typeId':
              case 'partnerId':
              case 'statusId': {
                let endpoint;

                if (field === 'statusId') {
                  endpoint = '/shipmentStatuses';
                } else if (field === 'typeId') {
                  endpoint = '/shipmentTypes';
                } else if (field === 'partnerId') {
                  endpoint = '/partners';
                }

                const response = await this.API.get(endpoint, {
                  params: {
                    filter: {
                      fields: ['id'],
                      where: { name: { ilike: `%${value}%` } },
                    },
                  },
                });

                const ids = response.data.map(agent => agent.id);

                params = {
                  filter: {
                    where: {
                      [field]: {
                        inq: ids,
                      },
                    },
                  },
                };

                break;
              }

              default: {
                params = {
                  filter: {
                    where: {
                      [field]: {
                        ilike: `%${value}%`,
                      },
                    },
                  },
                };

                break;
              }
            }

            params.filter.include = ['status', 'type', 'partner'];

            if (makeRequest) {
              this.setState({ searchParam: params }, () => {
                this.pagination.current.fetchItemCount();
              });
            }
          }, 500);
        } else {
          const params = {
            filter: {
              include: ['partner', 'status', 'type'],
            },
          };

          this.setState({ searchParam: params }, () => {
            this.pagination.current.fetchItemCount();
          });
        }
      } else if (type.toLowerCase() === 'dropdown') {
        this.setState(prevState => ({
          searchBy: value,
          listItems: prevState.backupList,
        }));
      }
    };
  }

  render() {
    const {
      resourceNameOnApi,
      listItems,
      showDeletionConfirmation,
      selectedItemToBeDeleted,
      order,
      searchParam,
    } = this.state;

    return [
      <Container key="shipment-list-container">
        <Row className="mb-3">
          <Col>
            <h3 className="text-secondary font-weight-bold">
              {T.translate('shipments.list.title')}
            </h3>
            <h4 className="text-secondary font-weight-light mb-0">
              {T.translate('shipments.list.description')}
            </h4>
          </Col>
          <Col
            className="d-flex justify-content-end align-items-end search-wrapper"
            style={{ marginRight: '20px' }}
          >
            <SearchBy
              filters={this.searchFields.map(d => d.name)}
              handleChange={this.handleSearchChange}
            />
          </Col>
          <Col md={2} className="d-flex justify-content-end align-items-end">
            <Link
              to={`/${resourceNameOnApi}/create`}
              className="btn btn btn-secondary btn-rounded px-3"
            >
              <FontAwesomeIcon icon="plus" />
              {' '}
              {T.translate('shipments.list.add')}
            </Link>
          </Col>
        </Row>
        <Table responsive striped hover className="border">
          <thead>
            <tr>
              <th>
                <span
                  className="table-header"
                  onKeyPress={() => { }}
                  role="button"
                  tabIndex={-1}
                  onClick={() => this.handleSortChange('trackingId')}
                >
                  {T.translate('shipments.fields.trackingId')}
                  {this.getIcon('trackingId')}
                </span>
              </th>
              <th>
                <span
                  className="table-header"
                  onKeyPress={() => { }}
                  role="button"
                  tabIndex={-1}
                  onClick={() => this.handleSortChange('partner')}
                >
                  {T.translate('shipments.fields.belongsTo')}
                  {this.getIcon('partner')}
                </span>
              </th>
              <th>
                <span
                  className="table-header"
                  onKeyPress={() => { }}
                  role="button"
                  tabIndex={-1}
                  onClick={() => this.handleSortChange('status')}
                >
                  {T.translate('shipments.fields.status')}
                  {this.getIcon('status')}
                </span>
              </th>
              <th colSpan={2}>
                <span
                  className="table-header"
                  onKeyPress={() => { }}
                  role="button"
                  tabIndex={-1}
                  onClick={() => this.handleSortChange('type')}
                >
                  {T.translate('shipments.fields.type')}
                  {this.getIcon('type')}
                </span>
              </th>
            </tr>
          </thead>
          <tbody>
            {listItems.map(item => (
              <tr key={item.id}>
                <td>{item.trackingId}</td>
                <td>
                  {item.partner ? (
                    <Link
                      to={{
                        pathname: `/partners/details/${item.partnerId}`,
                        state: { from: 'shipments' },
                      }}
                    >
                      {item.partner.name}
                    </Link>
                  ) : (
                    '--'
                  )}
                </td>
                <td>
                  {item.status
                    ? T.translate(
                      `shipments.fields.statuses.${item.status.name}`,
                    )
                    : '--'}
                </td>
                <td>
                  {item.type
                    ? T.translate(`shipments.fields.types.${item.type.name}`)
                    : '--'}
                </td>
                <td className="text-right py-0 align-middle">
                  <div className="btn-group" role="group">
                    <Link
                      to={`/${resourceNameOnApi}/details/${item.id}`}
                      className="btn btn-primary btn-sm"
                      title={T.translate('shipments.list.viewTooltip')}
                    >
                      <FontAwesomeIcon icon="eye" fixedWidth />
                    </Link>
                    <Link
                      className="btn btn-primary btn-sm"
                      to={`/${resourceNameOnApi}/update/${item.id}`}
                      title={T.translate('shipments.list.editTooltip')}
                    >
                      <FontAwesomeIcon icon="pencil-alt" fixedWidth />
                    </Link>
                    <Button
                      size="sm"
                      color="primary"
                      title={T.translate('shipments.list.deleteTooltip')}
                      onClick={() => {
                        this.setState({
                          selectedItemToBeDeleted: item,
                          showDeletionConfirmation: true,
                        });
                      }}
                    >
                      <FontAwesomeIcon icon="trash-alt" fixedWidth />
                    </Button>
                  </div>
                </td>
              </tr>
            ))}
          </tbody>
        </Table>
        <Pagination
          ref={this.pagination}
          resourceNameOnApi={resourceNameOnApi}
          filter={{ ...searchParam.filter, order }}
          onItemsReceived={async (listItemsReceived) => {
            this.setState({
              listItems: listItemsReceived,
              backupList: listItemsReceived,
            });
          }}
        />
      </Container>,
      <SweetAlert
        key="sweet-alert"
        show={showDeletionConfirmation}
        title={T.translate('shipments.list.deleteWarning.title', {
          itemToBeDeleted: selectedItemToBeDeleted
            ? selectedItemToBeDeleted.trackingId
            : '',
        })}
        text={T.translate('shipments.list.deleteWarning.message')}
        type="warning"
        showCancelButton
        confirmButtonText={T.translate(
          'shipments.list.deleteWarning.confirmButton',
        )}
        cancelButtonText={T.translate(
          'shipments.list.deleteWarning.cancelButton',
        )}
        confirmButtonClass="btn btn-primary btn-rounded mx-2 btn-lg px-5"
        cancelButtonClass="btn btn-secondary btn-rounded mx-2 btn-lg px-5"
        buttonsStyling={false}
        onConfirm={() => {
          this.delete(selectedItemToBeDeleted.id);
          this.setState({ showDeletionConfirmation: false });
        }}
      />,
    ];
  }
}
