import React, { Component } from 'react';
import {
  Container, Row, Col, Table, Button,
} from 'reactstrap';
import { Link } from 'react-router-dom';
import SweetAlert from 'sweetalert2-react';
import T from 'i18n-react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { Pagination } from '../../components/Pagination/Pagination';
import API from '../../components/API/API';
import Dropdown from '../../components/Dropdown/Dropdown';
import SearchBy from '../../components/Search/SearchBy';

export default class UserList extends Component {
  constructor(props) {
    super(props);

    this.API = new API();

    this.pagination = React.createRef();

    this.dateOptions = [
      'View all',
      'Last 24 hours',
      'Last 3 days',
      'Last week',
      'This month',
      'Last month',
      'This year',
      'Last year',
    ];

    this.searchFields = [
      { name: 'Username', field: 'username' },
      { name: 'Email', field: 'email' },
    ];

    this.sortFields = [
      { name: 'username', field: 'username' },
      // { name: 'role', field: 'roleId' },
    ];

    this.getIcon = (name) => {
      const { sortBy } = this.state;

      const element = this.sortFields.find(d => d.name === name);
      if (!element) {
        return '';
      }

      const { field } = this.sortFields.find(d => d.name === name);
      const icon = field === sortBy.field ? sortBy.icon : 'sort';
      return <FontAwesomeIcon className="sort-icon" icon={icon} />;
    };

    this.state = {
      resourceNameOnApi: 'users',
      listItems: [],
      backupList: [],
      sortBy: {
        field: '',
        type: '',
        icon: '',
      },
      order: 'username ASC',
      searchBy: this.searchFields[0].name,
      dateOption: this.dateOptions[0],
      searchParam: {
        filter: {
          where: {},
        },
      },
      searchKeyword: '',
    };

    this.delete = (id) => {
      const { resourceNameOnApi } = this.state;
      this.API.delete(`/${resourceNameOnApi}/${id}`).then(() => {
        this.pagination.current.fetchItems();
      });
    };

    this.handleSortChange = (name) => {
      const element = this.sortFields.find(d => d.name === name);
      if (!element) {
        return;
      }

      const { sortBy } = this.state;
      const { type, field: previousField } = sortBy;
      const { field } = element;

      let newType = 'DESC';

      if (field === previousField) {
        newType = type === 'ASC' ? 'DESC' : 'ASC';
      }

      this.setState(
        {
          sortBy: {
            field,
            type: newType,
            icon: newType === 'ASC' ? 'sort-up' : 'sort-down',
          },
          order: `${field} ${newType}`,
        },
        () => {
          this.pagination.current.fetchItemCount();
        },
      );
    };

    this.getDateLimits = (dateOption) => {
      const today = new Date();

      const day = today.getDate();
      const month = today.getMonth() + 1;
      const year = today.getFullYear();

      let startDate;
      let endDate;

      switch (dateOption.toLowerCase()) {
        case 'last 24 hours': {
          startDate = `${year}-${month}-${day - 1}`;
          endDate = `${year}-${month}-${day}`;
          break;
        }

        case 'last 3 days': {
          startDate = `${year}-${month}-${day - 3}`;
          endDate = `${year}-${month}-${day}`;
          break;
        }

        case 'last week': {
          startDate = `${year}-${month}-${day - 7}`;
          endDate = `${year}-${month}-${day}`;
          break;
        }

        case 'this month': {
          startDate = `${year}-${month}-01`;
          endDate = `${year}-${month}-${day}`;
          break;
        }

        case 'last month': {
          startDate = `${year}-${month - 1}-01`;
          endDate = `${year}-${month - 1}-31`;
          break;
        }

        case 'this year': {
          startDate = `${year}-01-01`;
          endDate = `${year}-${month}-${day}`;
          break;
        }

        case 'last year': {
          startDate = `${year - 1}-01-01`;
          endDate = `${year}-01-01`;
          break;
        }

        default:
          break;
      }

      return { startDate, endDate };
    };

    this.handleDateChange = (value) => {
      const { searchBy, searchKeyword } = this.state;
      if (value !== this.dateOptions[0]) {
        const { startDate, endDate } = this.getDateLimits(value);
        const key = this.searchFields.find(d => d.name === searchBy).field;
        const params = {
          filter: {
            where: searchKeyword
              ? {
                [key]: { ilike: `%${searchKeyword}%` },
              }
              : {},
          },
        };
        params.filter.where.createdAt = { between: [startDate, endDate] };
        this.setState({ searchParam: params }, () => {
          this.pagination.current.fetchItemCount();
        });
      } else {
        const key = this.searchFields.find(d => d.name === searchBy).field;
        const params = {
          filter: {
            where: searchKeyword
              ? {
                [key]: { ilike: `%${searchKeyword}%` },
              }
              : {},
          },
        };

        this.setState({ searchParam: params }, () => {
          this.pagination.current.fetchItemCount();
        });
      }

      this.setState({ dateOption: value });
    };

    this.searchTimeout = null;
    this.handleSearchChange = (value, type) => {
      if (type.toLowerCase() === 'input') {
        const { searchBy, dateOption } = this.state;
        clearTimeout(this.searchTimeout);

        if (value) {
          const key = this.searchFields.find(d => d.name === searchBy).field;

          this.searchTimeout = setTimeout(() => {
            const params = {
              filter: {
                where: {
                  [key]: { ilike: `%${value}%` },
                },
              },
            };

            if (dateOption !== this.dateOptions[0]) {
              const { startDate, endDate } = this.getDateLimits(dateOption);
              params.filter.where.createdAt = { between: [startDate, endDate] };
            }
            this.setState(
              {
                searchParam: params,
                searchKeyword: value,
              },
              () => {
                this.pagination.current.fetchItemCount();
              },
            );
          }, 500);
        } else {
          const params = {
            filter: {
              where: {},
            },
          };

          this.setState(
            {
              searchParam: params,
              searchKeyword: value,
            },
            () => {
              this.pagination.current.fetchItemCount();
            },
          );
        }
      } else if (type.toLowerCase() === 'dropdown') {
        this.setState(prevState => ({
          searchBy: value,
          listItems: prevState.backupList,
        }));
      }
    };
  }

  render() {
    const {
      resourceNameOnApi,
      listItems,
      showDeletionConfirmation,
      selectedItemToBeDeleted,
      searchParam,
      order,
    } = this.state;

    return [
      <Container key="user-list-container">
        <Row className="mb-3">
          <Col>
            <h3 className="text-secondary font-weight-bold">
              {T.translate('users.list.title')}
            </h3>
            <h4 className="text-secondary font-weight-light mb-0">
              {T.translate('users.list.description')}
            </h4>
          </Col>
          <Col md={5} style={{ marginRight: '10px' }}>
            <Row>
              <SearchBy
                filters={this.searchFields.map(d => d.name)}
                handleChange={this.handleSearchChange}
              />
            </Row>
            <Row
              className="d-flex justify-content-end"
              style={{ marginTop: '16px' }}
            >
              <Dropdown
                title="Created At"
                list={this.dateOptions}
                handleChange={this.handleDateChange}
              />
            </Row>
          </Col>
          <Col md={2} className="d-flex justify-content-end align-items-end">
            <Link
              to={`/${resourceNameOnApi}/create`}
              className="btn btn btn-secondary btn-rounded px-3"
            >
              <FontAwesomeIcon icon="plus" />
              {' '}
              {T.translate('users.list.add')}
            </Link>
          </Col>
        </Row>
        <Table responsive striped hover className="border">
          <thead>
            <tr>
              <th>
                <span
                  className="table-header"
                  onKeyPress={() => {}}
                  role="button"
                  tabIndex={-1}
                  onClick={() => this.handleSortChange('username')}
                >
                  {T.translate('users.fields.username')}
                  {this.getIcon('username')}
                </span>
              </th>
              <th colSpan={2}>
                <span
                  className="table-header"
                  onKeyPress={() => {}}
                  role="button"
                  tabIndex={-1}
                  onClick={() => this.handleSortChange('roles')}
                >
                  {T.translate('users.fields.roles')}
                  {this.getIcon('roles')}
                </span>
              </th>
            </tr>
          </thead>
          <tbody>
            {listItems.map(item => (
              <tr key={item.id}>
                <td>{item.username}</td>
                <td>
                  {T.translate(
                    `users.fields.roleList.${
                      item.roles[0] ? item.roles[0].name : 'user'
                    }`,
                  )}
                </td>
                <td className="text-right py-0 align-middle">
                  <div className="btn-group" role="group">
                    <Link
                      to={`/${resourceNameOnApi}/details/${item.id}`}
                      className="btn btn-primary btn-sm"
                      title={T.translate('users.list.viewTooltip')}
                    >
                      <FontAwesomeIcon icon="eye" fixedWidth />
                    </Link>
                    <Link
                      className="btn btn-primary btn-sm"
                      to={`/${resourceNameOnApi}/update/${item.id}`}
                      title={T.translate('users.list.editTooltip')}
                    >
                      <FontAwesomeIcon icon="pencil-alt" fixedWidth />
                    </Link>
                    <Button
                      size="sm"
                      color="primary"
                      title={T.translate('users.list.deleteTooltip')}
                      onClick={() => {
                        this.setState({
                          selectedItemToBeDeleted: item,
                          showDeletionConfirmation: true,
                        });
                      }}
                    >
                      <FontAwesomeIcon icon="trash-alt" fixedWidth />
                    </Button>
                  </div>
                </td>
              </tr>
            ))}
          </tbody>
        </Table>
        <Pagination
          ref={this.pagination}
          resourceNameOnApi={resourceNameOnApi}
          filter={{ ...searchParam.filter, order }}
          onItemsReceived={(listItemsReceived) => {
            this.setState({
              listItems: listItemsReceived,
              backupList: listItemsReceived,
            });
          }}
        />
      </Container>,
      <SweetAlert
        key="sweet-alert"
        show={showDeletionConfirmation}
        title={T.translate('users.list.deleteWarning.title', {
          itemToBeDeleted: selectedItemToBeDeleted
            ? selectedItemToBeDeleted.username
            : '',
        })}
        text={T.translate('users.list.deleteWarning.message')}
        type="warning"
        showCancelButton
        confirmButtonText={T.translate(
          'users.list.deleteWarning.confirmButton',
        )}
        cancelButtonText={T.translate('users.list.deleteWarning.cancelButton')}
        confirmButtonClass="btn btn-primary btn-rounded mx-2 btn-lg px-5"
        cancelButtonClass="btn btn-secondary btn-rounded mx-2 btn-lg px-5"
        buttonsStyling={false}
        onConfirm={() => {
          this.delete(selectedItemToBeDeleted.id);
          this.setState({ showDeletionConfirmation: false });
        }}
      />,
    ];
  }
}
