import React, { Component } from 'react';
import { Container, Row, Col } from 'reactstrap';
import T from 'i18n-react';

import { StatsCard } from '../../components/StatsCard/StatsCard';
import API from '../../components/API/API';
import { AuthProvider } from '../../components/Auth/AuthProvider';
import Loader from '../../components/Loader/Loader';

class Dashboard extends Component {
  constructor(props) {
    super(props);

    this.API = new API();
    this.account = new AuthProvider().getUser();

    this.state = {
      isLoading: false,
      shipmentsCount: '',
      agentsCount: '',
      areasCount: '',
      routesCount: '',
    };
  }

  componentDidMount() {
    const resourcesToCont = ['shipments', 'agents', 'areas', 'routes'];
    this.setState({ isLoading: true });
    resourcesToCont.forEach((resource) => {
      const stateToChange = {
        isLoading: false,
      };
      this.API.get(`/${resource}/count`).then((res) => {
        stateToChange[`${resource}Count`] = res.data.count;
      }).catch((reason) => {
        if (reason.response && reason.response.status && reason.response.status === 404) {
          stateToChange[`${resource}Count`] = '0';
        } else {
          stateToChange[`${resource}Count`] = T.translate('dashboard.notAvailable');
        }
      }).finally(() => {
        this.setState(stateToChange);
      });
    });
  }

  render() {
    const {
      shipmentsCount, agentsCount, areasCount, routesCount, isLoading,
    } = this.state;
    return (
      <Container>
        <h3 className="text-secondary font-weight-bold">
          {T.translate('dashboard.title', { name: this.account.username })}
        </h3>
        <h4 className="text-secondary font-weight-light mb-3">
          {T.translate('dashboard.description')}
        </h4>
        <Row>
          <Col lg={3} sm={6}>
            <StatsCard
              bigIcon="fas fa-shipping-fast text-success"
              statsText={T.translate('dashboard.shipments.text')}
              statsValue={shipmentsCount}
              statsIcon="far fa-chart-bar"
              statsIconText={T.translate('dashboard.shipments.iconText')}
            />
          </Col>
          <Col lg={3} sm={6}>
            <StatsCard
              bigIcon="fas fa-user-friends text-info"
              statsText={T.translate('dashboard.agents.text')}
              statsValue={agentsCount}
              statsIcon="far fa-chart-bar"
              statsIconText={T.translate('dashboard.agents.iconText')}
            />
          </Col>
          <Col lg={3} sm={6}>
            <StatsCard
              bigIcon="fas fa-map-marked-alt text-warning"
              statsText={T.translate('dashboard.areas.text')}
              statsValue={areasCount}
              statsIcon="far fa-chart-bar"
              statsIconText={T.translate('dashboard.areas.iconText')}
            />
          </Col>
          <Col lg={3} sm={6}>
            <StatsCard
              bigIcon="fas fa-route text-danger"
              statsText={T.translate('dashboard.routes.text')}
              statsValue={routesCount}
              statsIcon="far fa-chart-bar"
              statsIconText={T.translate('dashboard.routes.iconText')}
            />
          </Col>
        </Row>
        <Loader visible={isLoading} />
      </Container>
    );
  }
}

export default Dashboard;
