import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Moment from 'react-moment';
import {
  Container, Row, Col, Button,
} from 'reactstrap';
import { Link, withRouter } from 'react-router-dom';
import SweetAlert from 'sweetalert2-react';
import T from 'i18n-react';

import API from '../../components/API/API';

class PartnerDetails extends Component {
  constructor(props) {
    super(props);

    this.API = new API();

    this.state = {
      resourceNameOnApi: 'Partners',
      resource: {},
      hiddenPropertyNamesOnDetail: ['id', 'codFeeTypeId'],
      eternalAccessToken: '',
      showRenewConfirmation: false,
    };
  }

  componentDidMount() {
    const { resourceNameOnApi } = this.state;
    const { match } = this.props;
    this.API.get(`/${resourceNameOnApi}/${match.params.id}`, {
      params: {
        filter: {
          include: ['codFeeType'],
        },
      },
    }).then((response) => {
      this.setState({
        resource: response.data,
      });
    });
    this.API.get(
      `/${resourceNameOnApi}/${match.params.id}/eternal-access-token`,
    ).then((response) => {
      this.setState({ eternalAccessToken: response.data });
    });

    this.onRenewConfirmation = () => {
      this.API.patch(
        `/${resourceNameOnApi}/${match.params.id}/eternal-access-token`,
      ).then((response) => {
        this.setState({
          eternalAccessToken: response.data,
          showRenewConfirmation: false,
        });
      });
    };
  }

  render() {
    const {
      resource,
      hiddenPropertyNamesOnDetail,
      resourceNameOnApi,
      eternalAccessToken,
      showRenewConfirmation,
    } = this.state;
    const { history } = this.props;

    return [
      <Container key="Main Component">
        <h3 className="text-secondary font-weight-bold">
          {T.translate('partners.detail.title')}
        </h3>
        <h4 className="text-secondary font-weight-light mb-0">
          {T.translate('partners.detail.description')}
        </h4>
        <hr />
        {Object.keys(resource).map((property) => {
          if (hiddenPropertyNamesOnDetail.includes(property)) {
            return null;
          }

          let propertyValue;

          if (['createdAt', 'updatedAt'].includes(property)) {
            propertyValue = (
              <span>
                <Moment date={resource[property]} />
              </span>
            );
          } else if (property === 'codFeeType') {
            propertyValue = (
              <span>
                {T.translate(
                  `partners.fields.codFeeTypes.${resource.codFeeType.name}`,
                )}
              </span>
            );
          } else if (['isActive', 'codIsCollect'].includes(property)) {
            propertyValue = (
              <span>
                {resource[property]
                  ? T.translate('defaults.yes')
                  : T.translate('defaults.no')}
              </span>
            );
          } else {
            propertyValue = (
              <span>
                {resource[property] || T.translate('defaults.notSet')}
              </span>
            );
          }

          return (
            <Row className="mb-3" key={property}>
              <Col md={4} className="font-weight-bold">
                <span>{T.translate(`partners.fields.${property}`)}</span>
              </Col>
              <Col md={8}>{propertyValue}</Col>
            </Row>
          );
        })}
        <Row className="mb-3" key="eternalAccessToken">
          <Col md={4} className="font-weight-bold">
            <span className="line-height-col">
              {T.translate('partners.fields.accessToken')}
            </span>
          </Col>
          <Col md={8}>
            <div className="flex-direction-row">
              <span className="line-height-col mr-16">
                {eternalAccessToken}
              </span>

              <Button
                size="sm"
                color="primary"
                title={T.translate('partners.detail.renewAccessToken.title')}
                onClick={() => {
                  this.setState({
                    showRenewConfirmation: true,
                  });
                }}
              >
                {T.translate('partners.detail.renewAccessToken.title')}
              </Button>
            </div>
          </Col>
        </Row>
        <hr />
        <div className="clearfix text-center">
          <Button
            onClick={history.goBack}
            className="btn btn-rounded btn-lg btn-secondary float-md-left px-5"
          >
            {T.translate('defaults.goBack')}
          </Button>
          <Link
            to={`/${resourceNameOnApi}/update/${resource.id}`}
            className="btn btn-rounded btn-lg btn-primary float-md-right px-5"
          >
            {T.translate('partners.detail.editButton')}
          </Link>
        </div>
      </Container>,
      <SweetAlert
        key="sweet-alert-deletion-confirmation"
        show={showRenewConfirmation}
        title={T.translate('partners.detail.renewAccessToken.title')}
        text={T.translate('partners.detail.renewAccessToken.message')}
        type="warning"
        showCancelButton
        confirmButtonText={T.translate(
          'partners.detail.renewAccessToken.confirmButton',
        )}
        cancelButtonText={T.translate(
          'partners.detail.renewAccessToken.cancelButton',
        )}
        confirmButtonClass="btn btn-primary btn-rounded mx-2 btn-lg px-5"
        cancelButtonClass="btn btn-secondary btn-rounded mx-2 btn-lg px-5"
        buttonsStyling={false}
        onConfirm={this.onRenewConfirmation}
      />,
    ];
  }
}

PartnerDetails.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string.isRequired,
    }),
  }),
  location: PropTypes.shape({}).isRequired,
  history: PropTypes.shape({
    goBack: PropTypes.func.isRequired,
  }).isRequired,
};

PartnerDetails.defaultProps = {
  match: {
    params: {
      id: '',
    },
  },
};

export default withRouter(PartnerDetails);
