import React, { Component } from 'react';
import {
  Container, Row, Col, Label, FormGroup, Button,
  Card, CardHeader, CardBody, Alert, Table,
} from 'reactstrap';
import Select from 'react-select';
import T from 'i18n-react';
import moment from 'moment';

import API from '../../components/API/API';
import MapWithDirections from '../../components/Maps/MapWithDirections';
import { FormInputs } from '../../components/FormInputs/FormInputs';

// TODO: Fetch this ID via API, as this ID can change in future.
const SHIPMENT_DELIVERED_STATUS_ID = 4;

export default class JourneyGuru extends Component {
  constructor(props) {
    super(props);
    this.API = new API();

    this.state = {
      resourceNameOnApi: 'routes',
      resource: {},
      selectedRoute: null,
      routeSelectOptions: [],
      markers: [],
      otpNumber: 0,
      journeyInstructions: {
        shipment: {
          id: 0,
        },
        title: '',
        message: '',
      },
      error: null,
    };

    /**
     * Returns the select option (used in react-select) component,
     * based on the resource retrieved from database.
     * @param route
     * @return {{value: *, label: string, data: *}}
     */
    this.buildRoutesOptionFromTheResource = route => ({
      value: route.id,
      label: T.translate('shipments.form.routeSelectLabel', { routeBundle: route.bundle.name, routeId: route.id, routeCreatedAt: moment(route.createdAt).format('dddd, MMMM Do YYYY, h:mm:ss a') }),
      data: route,
    });

    /**
     * Callback function to when user selects some value on Route
     * form field. Saves route to this component state.
     * @param selectedRoute
     */
    this.handleChangeOnRoute = (selectedRoute) => {
      this.setState({ selectedRoute });
      if (selectedRoute === null) {
        this.setState(prevState => ({
          resource: {
            ...prevState.resource,
            route: null,
            routeId: null,
          },
          journeyInstructions: {},
          error: null,
          otpNumber: 0,
        }));
      } else {
        this.setState(prevState => ({
          resource: {
            ...prevState.resource,
            route: selectedRoute.data,
            routeId: selectedRoute.data.id,
          },
          error: null,
          otpNumber: 0,
        }));
        this.getInstructions();
        this.buildMarkers();
      }
    };

    /**
     * Callback for when user input some data on form fields.
     * It saves the data in their component state.
     * @param event
     */
    this.handleInputChange = (event) => {
      const { value } = event.target;
      this.setState({
        otpNumber: value,
        error: null,
      });
    };

    /**
     * Function to build all map markers according
     * shipment recipient geolocation point.
     */
    this.buildMarkers = () => {
      const { resource } = this.state;
      const { shipments } = resource.route;
      const markers = [];
      shipments.forEach((shipment) => {
        markers.push(shipment.recipientGeoPoint);
      });
      this.setState({ markers });
    };

    /**
     * Function to retrieve the current instructions
     * of the selected route.
     */
    this.getInstructions = () => {
      const { resourceNameOnApi, resource } = this.state;
      this.API.get(`/${resourceNameOnApi}/${resource.routeId}/journey-instructions`, {})
        .then((response) => {
          this.setState({
            journeyInstructions: response.data,
          });
        })
        .catch(() => this.setState({ journeyInstructions: null }));
    };

    /**
     * Function to check the current instruction as completed
     * and get the next one.
     */
    this.getNextInstructions = () => {
      const { journeyInstructions } = this.state;
      const currentInstruction = journeyInstructions.title;
      let completeInstruction = null;
      switch (currentInstruction) {
        case 'Contact Sender':
          completeInstruction = this.markSenderAsContacted;
          break;
        case 'Contact':
          completeInstruction = this.markRecipientAsContacted;
          break;
        case 'Scan Shipment':
        case 'Pickup And Scan All Shipments':
          completeInstruction = this.markAsScanBeforePickup;
          break;
        case 'Re-scan the shipment':
          completeInstruction = this.markAsScannedAfterContactingRecipient;
          break;
        case 'OTP Confirmation':
          completeInstruction = this.confirmOtpNumber;
          break;
        default: return;
      }
      completeInstruction()
        .then(() => {
          const { resourceNameOnApi, resource } = this.state;
          this.API.get(`/${resourceNameOnApi}/${resource.routeId}/journey-instructions`, {
            params: {},
          }).then((response) => {
            this.setState({
              journeyInstructions: response.data,
            });
          })
            .catch(() => this.setState({ journeyInstructions: null }));
        });
    };
    /**
     * Function to mark the current shipment as scanned
     */
    this.markAsScanBeforePickup = () => {
      const { journeyInstructions } = this.state;
      const { shipment } = journeyInstructions;
      return this.API.get(`/shipments/${shipment.id}/mark-as-scanned-before-pickup`);
    };
    /**
     * Function to mark that the current shipment recipient
     * was contacted
     */
    this.markRecipientAsContacted = () => {
      const { journeyInstructions } = this.state;
      const { shipment } = journeyInstructions;
      return this.API.get(`/shipments/${shipment.id}/mark-recipient-as-contacted`);
    };

    /**
     * Function to mark that the current shipment recipient
     * was contacted
     */
    this.markSenderAsContacted = () => {
      const { journeyInstructions } = this.state;
      const { shipment } = journeyInstructions;
      return this.API.get(`/shipments/${shipment.id}/mark-sender-as-contacted`);
    };

    /**
     * Function to mark the current shipment as scanned before contacting
     * the recipient
     */
    this.markAsScannedAfterContactingRecipient = () => {
      const { journeyInstructions } = this.state;
      const { shipment } = journeyInstructions;
      return this.API.get(`/shipments/${shipment.id}/mark-as-scanned-after-contacting-recipient`);
    };
    /**
     * Function to confirm the opt number of the delivery
     */
    this.confirmOtpNumber = () => {
      const { journeyInstructions } = this.state;
      const { shipment } = journeyInstructions;
      let { otpNumber } = this.state;
      otpNumber = parseInt(otpNumber, 10);
      return this.API.post(`/shipments/${shipment.id}/confirm-otp-number`, { otpNumber })
        .then(() => {
          this.updateShipmentStatus(shipment.id, { id: SHIPMENT_DELIVERED_STATUS_ID, name: 'Delivered' });
          this.setState({
            otpNumber: '',
            error: null,
          });
        })
        .catch(() => {
          this.setState({
            error: {
              title: 'Wrong OTP number',
              message: 'The OTP Number provided doesn\'t match this shipment\'s OTP Number.',
            },
          });
        });
    };

    this.getShipmentRowStyle = (shipment) => {
      const { journeyInstructions } = this.state;
      if (shipment.status.id === SHIPMENT_DELIVERED_STATUS_ID) {
        return 'text-muted';
      }
      if (journeyInstructions
        && journeyInstructions.shipment
        && journeyInstructions.shipment.id === shipment.id) {
        return 'font-weight-bold text-light bg-primary';
      }
      return '';
    };

    this.updateShipmentStatus = (id, status) => {
      const { selectedRoute } = this.state;
      const { shipments } = selectedRoute.data;
      shipments.find(shipment => shipment.id === id).status = status;
      this.setState(prevState => ({ selectedRoute: { ...prevState.selectedRoute, shipments } }));
    };
  }

  componentDidMount() {
    const { resourceNameOnApi } = this.state;
    this.API.get(`/${resourceNameOnApi}`, {
      params: {
        filter: {
          include: [
            'bundle',
            { shipments: ['status'] },
          ],
        },
      },
    }).then((response) => {
      this.setState({
        resource: response.data,
      });
      const routeOptions = [];
      response.data.forEach((item) => {
        const routeOption = this.buildRoutesOptionFromTheResource(item);
        routeOptions.push(routeOption);
      });
      this.setState({
        routeSelectOptions: routeOptions,
      });
    });
  }


  render() {
    const {
      journeyInstructions,
      selectedRoute,
      routeSelectOptions,
      resource,
      markers,
      error,
      otpNumber,
    } = this.state;

    return (
      <Container>
        <Row className="mb-3">
          <Col>
            <h3 className="text-secondary font-weight-bold">
              {T.translate('journeyGuru.title')}
            </h3>
            <h4 className="text-secondary font-weight-light mb-0">
              {T.translate('journeyGuru.description')}
            </h4>
          </Col>
        </Row>
        <hr />
        <form>
          <FormGroup>
            <Label className="h5 text-secondary font-weight-bold">
              {T.translate('routes.list.title')}
            </Label>
            <Select
              name="route"
              value={selectedRoute}
              onChange={this.handleChangeOnRoute}
              options={routeSelectOptions}
              placeholder={T.translate('defaults.placeholder.select')}
              isClearable={false}
              className="react-select-container"
              classNamePrefix="react-select"
            />
            {(resource.routeId && journeyInstructions)
              && (
                <div>
                  <Card className="my-3 bg-light">
                    <CardBody>
                      <h4 className="text-secondary text-center font-weight-light mb-0">
                        {journeyInstructions.message}
                      </h4>
                    </CardBody>
                  </Card>
                  <Row className="justify-content-center">
                    {journeyInstructions.title === 'OTP Confirmation' && (
                      <Col md={9}>
                        <FormInputs
                          key="otpNumber"
                          ncols={['col-md-12']}
                          proprieties={[
                            {
                              value: otpNumber || '',
                              name: 'otpNumber',
                              label: T.translate('journeyGuru.otpNumber'),
                              placeholder: '',
                              type: 'number',
                              bsClass: 'form-control',
                              onChange: (event) => {
                                this.handleInputChange(event);
                              },
                            },
                          ]}
                        />
                      </Col>
                    )}
                    <Col md={3} className="align-self-end pb-3">
                      <Button
                        block
                        className="btn-rounded px-5"
                        color="primary"
                        onClick={this.getNextInstructions}
                        disabled={!journeyInstructions.shipment}
                      >
                        {T.translate('journeyGuru.nextButton')}
                      </Button>
                    </Col>
                  </Row>
                  {error && (
                    <Alert color="danger">
                      {error.message}
                    </Alert>
                  )}
                  <hr className="mt-0" />
                </div>
              )}

          </FormGroup>
        </form>
        <h5 className="text-secondary font-weight-bold">
          {T.translate('journeyGuru.directions')}
        </h5>
        <div className="border rounded">
          <MapWithDirections markers={markers} />
        </div>
        {selectedRoute && selectedRoute.data && selectedRoute.data.shipments.length !== 0 && (
          <Row>
            <Col md={12}>
              <Card className="my-3">
                <CardHeader className="p-3">
                  <h5 className="text-secondary font-weight-bold mb-1">
                    {T.translate('shipments.list.title')}
                  </h5>
                  <h6 className="text-secondary font-weight-light mb-0">
                    {T.translate('journeyGuru.listDescription')}
                  </h6>
                </CardHeader>
                <CardBody className="p-0">
                  <Table striped hover className="mb-0">
                    <thead>
                      <tr>
                        <th>
                          {T.translate('shipments.fields.trackingId')}
                        </th>
                        <th>
                          {T.translate('shipments.fields.status')}
                        </th>
                        <th>
                          {T.translate('shipments.fields.senderName')}
                        </th>
                        <th>
                          {T.translate('shipments.fields.senderAddress')}
                        </th>
                        <th>
                          {T.translate('shipments.fields.senderPhone')}
                        </th>
                        <th>
                          {T.translate('shipments.fields.recipientName')}
                        </th>
                        <th>
                          {T.translate('shipments.fields.recipientAddress')}
                        </th>
                        <th>
                          {T.translate('shipments.fields.recipientPhone')}
                        </th>
                      </tr>
                    </thead>
                    <tbody>
                      {selectedRoute.data.shipments.map(item => (
                        <tr key={`shipment_${item.id}`} className={this.getShipmentRowStyle(item)}>
                          <td>
                            {item.trackingId}
                          </td>
                          <td>
                            {T.translate(`shipments.fields.statuses.${item.status.name}`)}
                          </td>
                          <td>
                            {item.senderName}
                          </td>
                          <td>
                            {item.senderAddress}
                          </td>
                          <td>
                            {item.senderPhone}
                          </td>
                          <td>
                            {item.recipientName}
                          </td>
                          <td>
                            {item.recipientPhone}
                          </td>
                          <td>
                            {item.recipientPhone}
                          </td>
                        </tr>
                      ))}
                    </tbody>
                  </Table>
                </CardBody>
              </Card>
            </Col>
          </Row>
        )}
      </Container>
    );
  }
}
