import React, { Component } from 'react';
import {
  Container, Row, Col, Table, Button,
} from 'reactstrap';
import { Link } from 'react-router-dom';
import SweetAlert from 'sweetalert2-react';
import T from 'i18n-react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { Pagination } from '../../components/Pagination/Pagination';
import API from '../../components/API/API';
import SearchBy from '../../components/Search/SearchBy';

export default class AgentList extends Component {
  constructor(props) {
    super(props);

    this.API = new API();

    this.pagination = React.createRef();

    this.searchFields = [
      { name: 'Name', field: 'name' },
      { name: 'Friendly ID', field: 'friendlyId' },
      { name: 'Phone', field: 'phone' },
      { name: 'Type', field: 'typeId' },
      { name: 'Status', field: 'statusId' },
    ];

    this.sortFields = [
      { name: 'name', field: 'name' },
      { name: 'friendlyId', field: 'friendlyId' },
      { name: 'phone', field: 'phone' },
      { name: 'type', field: 'typeId' },
      { name: 'status', field: 'statusId' },
    ];

    this.getIcon = (name) => {
      const element = this.sortFields.find(d => d.name === name);
      if (!element) {
        return '';
      }

      const { sortBy } = this.state;
      const { field } = this.sortFields.find(d => d.name === name);
      const icon = field === sortBy.field ? sortBy.icon : 'sort';
      return <FontAwesomeIcon className="sort-icon" icon={icon} />;
    };

    this.state = {
      resourceNameOnApi: 'agents',
      listItems: [],
      backupList: [],
      sortBy: {
        field: '',
        type: '',
        icon: '',
      },
      order: 'name ASC',
      searchBy: this.searchFields[0].name,
      searchParam: {
        filter: {
          where: {},
          include: ['type', 'status'],
        },
      },
    };

    this.delete = (id) => {
      const { resourceNameOnApi } = this.state;
      this.API.delete(`/${resourceNameOnApi}/${id}`).then(() => {
        this.pagination.current.fetchItems();
      });
    };

    this.handleSortChange = (name) => {
      const element = this.sortFields.find(d => d.name === name);
      if (!element) {
        return;
      }

      const { sortBy } = this.state;
      const { type, field: previousField } = sortBy;
      const { field } = this.sortFields.find(d => d.name === name);
      let newType = 'DESC';

      if (field === previousField) {
        newType = type === 'ASC' ? 'DESC' : 'ASC';
      }

      this.setState(
        {
          sortBy: {
            field,
            type: newType,
            icon: newType === 'ASC' ? 'sort-up' : 'sort-down',
          },
          order: `${field} ${newType}`,
        },
        () => {
          this.pagination.current.fetchItemCount();
        },
      );
    };

    this.searchTimeout = null;

    this.handleSearchChange = (value, type) => {
      if (type.toLowerCase() === 'input') {
        const { searchBy } = this.state;
        clearTimeout(this.searchTimeout);

        if (value) {
          const { field } = this.searchFields.find(d => d.name === searchBy);

          this.searchTimeout = setTimeout(async () => {
            let params;

            switch (field) {
              case 'typeId':
              case 'statusId': {
                let endpoint;

                if (field === 'statusId') {
                  endpoint = '/agentStatuses';
                } else if (field === 'typeId') {
                  endpoint = '/agentTypes';
                }

                const response = await this.API.get(endpoint, {
                  params: {
                    filter: {
                      fields: ['id'],
                      where: { name: { ilike: `%${value}%` } },
                    },
                  },
                });

                const ids = response.data.map(agent => agent.id);

                params = {
                  filter: {
                    where: {
                      [field]: {
                        inq: ids,
                      },
                    },
                  },
                };

                break;
              }

              default: {
                params = {
                  filter: {
                    where: {
                      [field]: {
                        ilike: `%${value}%`,
                      },
                    },
                  },
                };

                break;
              }
            }

            params.filter.include = ['status', 'type'];

            this.setState({ searchParam: params }, () => {
              this.pagination.current.fetchItemCount();
            });
          }, 500);
        } else {
          const params = {
            filter: {
              include: ['status', 'type'],
            },
          };

          this.setState({ searchParam: params }, () => {
            this.pagination.current.fetchItemCount();
          });
        }
      } else if (type.toLowerCase() === 'dropdown') {
        this.setState(prevState => ({
          searchBy: value,
          listItems: prevState.backupList,
        }));
      }
    };
  }

  render() {
    const {
      resourceNameOnApi,
      listItems,
      showDeletionConfirmation,
      selectedItemToBeDeleted,
      searchBy,
      searchParam,
      order,
    } = this.state;

    return [
      <Container key="agent-list-container">
        <Row className="mb-3">
          <Col>
            <h3 className="text-secondary font-weight-bold">
              {T.translate('agents.list.title')}
            </h3>
            <h4 className="text-secondary font-weight-light mb-0">
              {T.translate('agents.list.description')}
            </h4>
          </Col>
          <Col
            className="d-flex justify-content-end align-items-end search-wrapper"
            style={{ marginRight: '20px' }}
          >
            <SearchBy
              filters={this.searchFields.map(d => d.name)}
              default={searchBy}
              resourceNameOnApi={resourceNameOnApi}
              handleChange={this.handleSearchChange}
            />
          </Col>
          <Col md={2} className="d-flex justify-content-end align-items-end">
            <Link
              to={`/${resourceNameOnApi}/create`}
              className="btn btn btn-secondary btn-rounded px-3"
            >
              <FontAwesomeIcon icon="plus" />
              {' '}
              {T.translate('agents.list.add')}
            </Link>
          </Col>
        </Row>
        <Table responsive striped hover className="border">
          <thead>
            <tr>
              <th>
                <span
                  className="table-header"
                  onClick={() => this.handleSortChange('name')}
                  onKeyPress={() => { }}
                  role="button"
                  tabIndex={-1}
                >
                  {T.translate('agents.fields.name')}
                  {this.getIcon('name')}
                </span>
              </th>
              <th>
                <span
                  className="table-header"
                  onClick={() => this.handleSortChange('friendlyId')}
                  onKeyPress={() => { }}
                  role="button"
                  tabIndex={-1}
                >
                  {T.translate('agents.fields.friendlyId')}
                  {this.getIcon('friendlyId')}
                </span>
              </th>
              <th>
                <span
                  className="table-header"
                  onClick={() => this.handleSortChange('type')}
                  onKeyPress={() => { }}
                  role="button"
                  tabIndex={-1}
                >
                  {T.translate('agents.fields.type')}
                  {this.getIcon('type')}
                </span>
              </th>
              <th>
                <span
                  className="table-header"
                  onClick={() => this.handleSortChange('status')}
                  onKeyPress={() => { }}
                  role="button"
                  tabIndex={-1}
                >
                  {T.translate('agents.fields.status')}
                  {this.getIcon('status')}
                </span>
              </th>
              <th colSpan={2}>
                <span
                  className="table-header"
                  onClick={() => this.handleSortChange('phone')}
                  onKeyPress={() => { }}
                  role="button"
                  tabIndex={-1}
                >
                  {T.translate('agents.fields.phone')}
                  {this.getIcon('phone')}
                </span>
              </th>
            </tr>
          </thead>
          <tbody>
            {listItems.map(item => (
              <tr key={item.id}>
                <td>{item.name}</td>
                <td>{item.friendlyId}</td>
                <td>{item.type.name}</td>
                <td>{item.status.name}</td>
                <td>{item.phone}</td>
                <td className="text-right py-0 align-middle">
                  <div className="btn-group" role="group">
                    <Link
                      to={{
                        pathname: `/${resourceNameOnApi}/details/${item.id}`,
                        state: { from: 'agents' },
                      }}
                      className="btn btn-primary btn-sm"
                      title={T.translate('agents.list.viewTooltip')}
                    >
                      <FontAwesomeIcon icon="eye" fixedWidth />
                    </Link>
                    <Link
                      className="btn btn-primary btn-sm"
                      to={`/${resourceNameOnApi}/update/${item.id}`}
                      title={T.translate('agents.list.editTooltip')}
                    >
                      <FontAwesomeIcon icon="pencil-alt" fixedWidth />
                    </Link>
                    <Button
                      size="sm"
                      color="primary"
                      title={T.translate('agents.list.deleteTooltip')}
                      onClick={() => {
                        this.setState({
                          selectedItemToBeDeleted: item,
                          showDeletionConfirmation: true,
                        });
                      }}
                    >
                      <FontAwesomeIcon icon="trash-alt" fixedWidth />
                    </Button>
                  </div>
                </td>
              </tr>
            ))}
          </tbody>
        </Table>
        <Pagination
          ref={this.pagination}
          resourceNameOnApi={resourceNameOnApi}
          filter={{ ...searchParam.filter, order }}
          onItemsReceived={(listItemsReceived) => {
            this.setState({
              listItems: listItemsReceived,
              backupList: listItemsReceived,
            });
          }}
        />
      </Container>,
      <SweetAlert
        key="sweet-alert"
        show={showDeletionConfirmation}
        title={T.translate('agents.list.deleteWarning.title', {
          itemToBeDeleted: selectedItemToBeDeleted
            ? selectedItemToBeDeleted.name
            : '',
        })}
        text={T.translate('agents.list.deleteWarning.message')}
        type="warning"
        showCancelButton
        confirmButtonText={T.translate(
          'agents.list.deleteWarning.confirmButton',
        )}
        cancelButtonText={T.translate('agents.list.deleteWarning.cancelButton')}
        confirmButtonClass="btn btn-primary btn-rounded mx-2 btn-lg px-5"
        cancelButtonClass="btn btn-secondary btn-rounded mx-2 btn-lg px-5"
        buttonsStyling={false}
        onConfirm={() => {
          this.delete(selectedItemToBeDeleted.id);
          this.setState({ showDeletionConfirmation: false });
        }}
      />,
    ];
  }
}
