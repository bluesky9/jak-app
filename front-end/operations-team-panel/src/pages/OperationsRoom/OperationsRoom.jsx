import React, { Component } from 'react';
import {
  Container, Row, Col, Button,
} from 'reactstrap';
import T from 'i18n-react';
import Fullscreen from 'react-full-screen';
import LiveViewWidget from './LiveViewWidget';
import JourneyGuruWidget from './JourneyGuruWidget';
import AlertsAndEventsWidget from './AlertsAndEventsWidget';
import ShipmentsWidget from './ShipmentsWidget';
import StatisticsWidget from './StatisticsWidget';

class OperationsRoom extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isFullScreen: false,
    };

    this.toggleFullScreen = () => {
      const { isFullScreen } = this.state;
      this.setState({ isFullScreen: !isFullScreen });
    };
  }

  render() {
    const { isFullScreen: isFull } = this.state;

    return (
      <div className="operations-room-container">
        <Fullscreen
          enabled={isFull}
          onChange={isFullScreen => this.setState({ isFullScreen })}
        >
          <Button
            className="fullscreen-button"
            onClick={this.toggleFullScreen}
            color="dark"
            title={T.translate('operationsRoom.fullScreen')}
          >
            <i className={`fas ${isFull ? 'fa-compress' : 'fa-expand'}`} />
          </Button>
          <Container fluid className={`fullscreen-container ${isFull ? 'active' : ''}`}>
            <Row className="align-items-stretch">
              <Col md={12}>
                <LiveViewWidget />
              </Col>
            </Row>
            <Row className="align-items-stretch">
              <Col md={6}>
                <AlertsAndEventsWidget />
              </Col>
              <Col md={6}>
                <JourneyGuruWidget />
              </Col>
            </Row>
            <Row className="align-items-stretch">
              <Col md={6}>
                <ShipmentsWidget />
              </Col>
              <Col md={6}>
                <StatisticsWidget />
              </Col>
            </Row>
          </Container>
        </Fullscreen>
      </div>
    );
  }
}

export default OperationsRoom;
