import React, { Component } from 'react';
import { Card, CardHeader, CardBody } from 'reactstrap';
import T from 'i18n-react';

import { StatsCard } from '../../components/StatsCard/StatsCard';
import API from '../../components/API/API';
import { AuthProvider } from '../../components/Auth/AuthProvider';
import InlineLoader from '../../components/Loader/InlineLoader';

class StatisticsWidget extends Component {
  constructor(props) {
    super(props);

    this.API = new API();
    this.account = new AuthProvider().getUser();

    this.state = {
      isLoading: false,
      shipmentsCount: '',
      agentsCount: '',
      areasCount: '',
      routesCount: '',
    };

    this.fetchStatistics = () => {
      const resourcesToCont = ['shipments', 'agents', 'areas', 'routes'];
      this.setState({ isLoading: true });
      resourcesToCont.forEach((resource) => {
        const stateToChange = {
          isLoading: false,
        };
        this.API.get(`/${resource}/count`).then((res) => {
          stateToChange[`${resource}Count`] = res.data.count;
        }).catch((reason) => {
          if (reason.response && reason.response.status && reason.response.status === 404) {
            stateToChange[`${resource}Count`] = '0';
          } else {
            stateToChange[`${resource}Count`] = T.translate('operationsRoom.statistics.notAvailable');
          }
        }).finally(() => {
          this.setState(stateToChange);
        });
      });
    };
  }

  componentDidMount() {
    this.fetchStatistics();
    this.fetchStatisticsHandle = setInterval(this.fetchStatistics, 10000);
  }

  componentWillUnmount() {
    clearInterval(this.fetchStatisticsHandle);
  }

  render() {
    const {
      shipmentsCount, agentsCount, areasCount, routesCount, isLoading,
    } = this.state;
    return (
      <Card className="h-100">
        <CardHeader className="p-2">
          <h6 className="font-weight-light mb-0">
            <i className="text-primary fas fa-chart-bar mr-2" />
            {T.translate('operationsRoom.statistics.title')}
            <InlineLoader
              visible={isLoading}
              className="float-right"
            />
          </h6>
        </CardHeader>
        <CardBody className="p-0 scroll d-flex flex-wrap flex-row justify-content-around align-items-center">
          <StatsCard
            bigIcon="fas fa-shipping-fast text-success"
            statsText={T.translate('operationsRoom.statistics.shipments')}
            statsValue={shipmentsCount}
          />
          <StatsCard
            bigIcon="fas fa-user-friends text-info"
            statsText={T.translate('operationsRoom.statistics.agents')}
            statsValue={agentsCount}
          />
          <StatsCard
            bigIcon="fas fa-map-marked-alt text-warning"
            statsText={T.translate('operationsRoom.statistics.areas')}
            statsValue={areasCount}
          />
          <StatsCard
            bigIcon="fas fa-route text-danger"
            statsText={T.translate('operationsRoom.statistics.routes')}
            statsValue={routesCount}
          />
        </CardBody>
      </Card>
    );
  }
}

export default StatisticsWidget;
