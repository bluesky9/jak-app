import React, { Component } from 'react';
import {
  Card, CardHeader, CardBody, Table,
} from 'reactstrap';
import T from 'i18n-react';

import API from '../../components/API/API';
import SimpleLabel from '../../components/SimpleLabel/SimpleLabel';
import InlineLoader from '../../components/Loader/InlineLoader';

class AlertsAndEventsWidget extends Component {
  constructor(props) {
    super(props);

    this.API = new API();

    this.state = {
      isLoading: false,
    };
  }

  componentDidMount() {
    // Fetch Data
  }

  render() {
    const { isLoading } = this.state;

    return (
      <Card className="h-100">
        <CardHeader className="p-2">
          <h6 className="font-weight-light mb-0">
            <i className="text-warning fas fa-exclamation-triangle mr-2" />
            {T.translate('operationsRoom.alertsAndEvents.title')}
            <InlineLoader
              visible={isLoading}
              className="float-right"
            />
          </h6>
        </CardHeader>
        <CardBody className="table-widget">
          <Table responsive striped hover size="sm">
            <thead>
              <tr>
                <th>
                 Type
                </th>
                <th>
                 Event
                </th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td style={{ width: '150px', paddingLeft: '28px' }}>
                  <SimpleLabel text="Warning" colour="yellow" />
                </td>
                <td>
                  First Row
                </td>
              </tr>
              <tr>
                <td style={{ width: '150px', paddingLeft: '28px' }}>
                  <SimpleLabel text="Event Overflow" colour="cyan" />
                </td>
                <td>
                  Second row
                </td>
              </tr>
              <tr>
                <td style={{ width: '150px', paddingLeft: '28px' }}>
                  <SimpleLabel text="Event" colour="deep-purple" />
                </td>
                <td>
                  Third row
                </td>
              </tr>
              <tr>
                <td style={{ width: '150px', paddingLeft: '28px' }}>
                  ...
                </td>
                <td>
                  ...
                </td>
              </tr>
              <tr>
                <td style={{ width: '150px', paddingLeft: '28px' }}>
                  ...
                </td>
                <td>
                  ...
                </td>
              </tr>
              <tr>
                <td style={{ width: '150px', paddingLeft: '28px' }}>
                  ...
                </td>
                <td>
                  ...
                </td>
              </tr>
              <tr>
                <td style={{ width: '150px', paddingLeft: '28px' }}>
                  ...
                </td>
                <td>
                  ...
                </td>
              </tr>
              <tr>
                <td style={{ width: '150px', paddingLeft: '28px' }}>
                  <SimpleLabel text="Warning" colour="red" />
                </td>
                <td>
                  Last Row
                </td>
              </tr>
            </tbody>
          </Table>
        </CardBody>
      </Card>
    );
  }
}

export default AlertsAndEventsWidget;
