import React, { Component } from 'react';
import {
  Card, CardHeader, CardBody,
} from 'reactstrap';
import { Redirect } from 'react-router-dom';
import T from 'i18n-react';
import API from '../../components/API/API';
import { MapWithMarkers } from '../../components/Maps/MapWithMarkers';
import InlineLoader from '../../components/Loader/InlineLoader';
import AgentInfoWindow from './AgentInfoWindow';

class LiveViewWidget extends Component {
  constructor(props) {
    super(props);

    this.API = new API();

    this.state = {
      agentMarkers: [],
      redirectTo: '',
      onRouteAgentStatusId: null,
      isLoading: false,
      selectedMarker: null,
    };

    this.onPressMarker = (event, agent) => {
      this.setState({
        selectedMarker: agent.id,
      });
    };

    this.fetchOnRouteAgentStatusId = () => {
      const params = {
        filter: {
          fields: ['id'],
          where: { name: 'On Route' },
        },
      };
      this.setState({ isLoading: true }, () => {
        this.API.get('/agentStatuses', { params }).then((response) => {
          const [firstArrayElement] = response.data;
          const { id: onRouteAgentStatusId } = firstArrayElement;
          this.setState({ onRouteAgentStatusId }, () => {
            this.fetchAgents();
          });
        });
      });
    };

    this.fetchAgents = () => {
      const { onRouteAgentStatusId } = this.state;

      if (!onRouteAgentStatusId) return;

      const params = {
        filter: {
          where: { statusId: onRouteAgentStatusId },
          include: ['status', 'routes'],
        },
      };

      this.API.get('/agents', { params }).then((response) => {
        this.setState({
          isLoading: false,
          agentMarkers: response.data.map(agent => ({
            position: agent.currentGeoPoint,
            onClick: event => this.onPressMarker(event, agent),
            infoWindowId: agent.id,
            infoWindow: <AgentInfoWindow agent={agent} />,
          })),
        });
      });
    };
  }

  componentDidMount() {
    this.fetchOnRouteAgentStatusId();
    this.fetchAgentsHandle = setInterval(this.fetchAgents, 10000);
  }

  componentWillUnmount() {
    clearInterval(this.fetchAgentsHandle);
  }

  render() {
    const {
      redirectTo, agentMarkers, isLoading, selectedMarker,
    } = this.state;

    if (redirectTo) return <Redirect to={redirectTo} />;

    return (
      <Card className="h-100">
        <CardHeader className="p-2">
          <h6 className="font-weight-light mb-0">
            <i className="text-danger fas fa-route mr-2" />
            {T.translate('operationsRoom.liveView.title')}
            <InlineLoader
              visible={isLoading}
              className="float-right"
            />
          </h6>
        </CardHeader>
        <CardBody className="p-0 h-100">
          <MapWithMarkers
            markers={agentMarkers}
            activeInfoWindow={selectedMarker}
            mapProps={{
              defaultCenter: {
                lat: 41.9,
                lng: -87.624,
              },
              defaultOptions: {
                zoom: 15,
                scrollwheel: false,
                zoomControl: true,
                fullscreenControl: false,
              },
            }}
            containerHeight="100%"
          />
        </CardBody>
      </Card>
    );
  }
}

export default LiveViewWidget;
