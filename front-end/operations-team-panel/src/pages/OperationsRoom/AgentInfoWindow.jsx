import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Button } from 'reactstrap';
import { Redirect } from 'react-router-dom';
import T from 'i18n-react';

class AgentInfoWindow extends Component {
  constructor(props) {
    super(props);

    this.state = {
      redirectTo: null,
    };
  }

  render() {
    const { redirectTo } = this.state;
    const { agent } = this.props;
    const {
      id, name, friendlyId, phone, status,
    } = agent;

    if (redirectTo) return <Redirect to={redirectTo} />;

    return (
      <div>
        <strong>Name: </strong>
        {name}
        <br />

        <strong>Identifier: </strong>
        {friendlyId}
        <br />

        <strong>Phone: </strong>
        {phone}
        <br />

        <strong>Status: </strong>
        {status.name}

        <hr className="my-2" />

        <Button
          color="primary"
          size="sm"
          block
          onClick={() => {
            this.setState({
              redirectTo: `/agents/details/${id}`,
            });
          }}
        >
          <i className="fas fa-user" />
          {' '}
          {T.translate('operationsRoom.liveView.agentDetailsButton')}
        </Button>
      </div>
    );
  }
}

AgentInfoWindow.propTypes = {
  agent: PropTypes.shape({
    name: PropTypes.string,
    friendlyId: PropTypes.string,
    phone: PropTypes.string,
    status: PropTypes.shape({
      id: PropTypes.number,
      name: PropTypes.string,
    }),
  }).isRequired,
};

export default AgentInfoWindow;
