import React, { Component } from 'react';
import {
  Card, CardHeader, CardBody, Table,
} from 'reactstrap';
import T from 'i18n-react';

import API from '../../components/API/API';
import InlineLoader from '../../components/Loader/InlineLoader';

class ShipmentsWidget extends Component {
  constructor(props) {
    super(props);

    this.state = {
      resourceNameOnApi: 'shipments',
      listItems: [],
      isLoading: false,
    };

    this.API = new API();
  }

  componentDidMount() {
    const { resourceNameOnApi } = this.state;
    const date = new Date();
    const year = date.getFullYear();
    const month = date.getMonth() + 1;
    const day = date.getDate();
    const startDate = `${year}-${month}-${day}`;
    const endDate = `${year}-${month}-${day + 1}`;

    this.setState({ isLoading: true });
    this.API.get(`/${resourceNameOnApi}`, {
      params: {
        filter: {
          include: ['status'],
          where: {
            or: [
              {
                createdAt: {
                  between: [startDate, endDate],
                },
              },
              {
                pickupDatetime: {
                  between: [startDate, endDate],
                },
              },
              {
                deliveryDatetime: {
                  between: [startDate, endDate],
                },
              },
            ],
          },
        },
      },
    }).then((response) => {
      this.setState({
        listItems: response.data,
        isLoading: false,
      });
    });
  }

  render() {
    const { listItems, isLoading } = this.state;

    return (
      <Card className="h-100">
        <CardHeader className="p-2">
          <h6 className="font-weight-light mb-0">
            <i className="text-success fas fa-shipping-fast mr-2" />
            {T.translate('operationsRoom.shipments.title')}
            <InlineLoader
              visible={isLoading}
              className="float-right"
            />
          </h6>
        </CardHeader>
        <CardBody className="table-widget">
          <Table responsive striped hover size="sm">
            <thead>
              <tr>
                <th>
                  Shipment ID
                </th>
                <th>
                  Status
                </th>
              </tr>
            </thead>
            <tbody>
              {listItems && listItems.length > 0 && listItems.map(item => (
                <tr key={item.id}>
                  <td>
                    {item.id}
                  </td>
                  <td>
                    {item.status.name}
                  </td>
                </tr>
              ))}
              {(!listItems || listItems.length === 0)
              && (
              <tr>
                <td colSpan="2">
                  {T.translate('operationsRoom.shipments.noResults')}
                </td>
              </tr>
              )
            }
            </tbody>
          </Table>
        </CardBody>
      </Card>
    );
  }
}

export default ShipmentsWidget;
