import React, { Component } from 'react';
import {
  Card, CardHeader, CardBody, Button,
} from 'reactstrap';
import T from 'i18n-react';

import API from '../../components/API/API';

class ShortcutsWidget extends Component {
  constructor(props) {
    super(props);

    this.API = new API();
  }

  componentDidMount() {
    // Fetch Data
  }

  render() {
    return (
      <Card className="h-100">
        <CardHeader className="p-2">
          <h6 className="font-weight-light mb-0">
            <i className="text-primary fas fa-grip-horizontal mr-2" />
            {T.translate('operationsRoom.shortcuts.title')}
          </h6>
        </CardHeader>
        <CardBody className="p-2 scroll d-flex flex-wrap flex-row justify-content-around">
          <Button
            color="primary"
            className="text-center flex-fill m-2"
          >
            <i className="d-block m-auto h2 far fa-square" />
            {' '}
            Shortcut
          </Button>
          <Button
            color="primary"
            className="text-center flex-fill m-2"
          >
            <i className="d-block m-auto h2 far fa-square" />
            {' '}
            Shortcut
          </Button>
          <Button
            color="primary"
            className="text-center flex-fill m-2"
          >
            <i className="d-block m-auto h2 far fa-square" />
            {' '}
            Shortcut
          </Button>
          <div className="w-100" />
          <Button
            color="primary"
            className="text-center flex-fill m-2"
          >
            <i className="d-block m-auto h2 far fa-square" />
            {' '}
            Shortcut
          </Button>
          <Button
            color="primary"
            className="text-center flex-fill m-2"
          >
            <i className="d-block m-auto h2 far fa-square" />
            {' '}
            Shortcut
          </Button>
          <Button
            color="primary"
            className="text-center flex-fill m-2"
          >
            <i className="d-block m-auto h2 far fa-square" />
            {' '}
            Shortcut
          </Button>
        </CardBody>
      </Card>
    );
  }
}

export default ShortcutsWidget;
