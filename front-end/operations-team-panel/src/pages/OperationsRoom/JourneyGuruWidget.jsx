import React, { Component } from 'react';
import {
  Card, CardHeader, CardBody, Table,
} from 'reactstrap';
import T from 'i18n-react';

import API from '../../components/API/API';
import InlineLoader from '../../components/Loader/InlineLoader';

class JourneyGuruWidget extends Component {
  constructor(props) {
    super(props);

    this.API = new API();

    this.state = {
      isLoading: false,
    };
  }

  componentDidMount() {
    // Fetch Data
  }

  render() {
    const { isLoading } = this.state;

    return (
      <Card className="h-100">
        <CardHeader className="p-2">
          <h6 className="font-weight-light mb-0">
            <i className="text-info fas fa-user-tie mr-2" />
            {T.translate('operationsRoom.journeyGuru.title')}
            <InlineLoader
              visible={isLoading}
              className="float-right"
            />
          </h6>
        </CardHeader>
        <CardBody className="table-widget">
          <Table responsive striped hover size="sm">
            <thead>
              <tr>
                <th>
                  Route
                </th>
                <th>
                  Directions
                </th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>
                  #1
                </td>
                <td>
                  First row
                </td>
              </tr>
              <tr>
                <td>
                  #2
                </td>
                <td>
                  Second row
                </td>
              </tr>
              <tr>
                <td>
                  #3
                </td>
                <td>
                  Third row
                </td>
              </tr>
              <tr>
                <td>
                  ...
                </td>
                <td>
                  ...
                </td>
              </tr>
              <tr>
                <td>
                  ...
                </td>
                <td>
                  ...
                </td>
              </tr>
              <tr>
                <td>
                  ...
                </td>
                <td>
                  ...
                </td>
              </tr>
              <tr>
                <td>
                  ...
                </td>
                <td>
                  ...
                </td>
              </tr>
              <tr>
                <td>
                  #99
                </td>
                <td>
                  Last Row
                </td>
              </tr>
            </tbody>
          </Table>
        </CardBody>
      </Card>
    );
  }
}

export default JourneyGuruWidget;
