import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Moment from 'react-moment';
import {
  Container,
  Row,
  Col,
  Card,
  CardHeader,
  CardBody,
  Button,
} from 'reactstrap';
import { Link, withRouter } from 'react-router-dom';
import T from 'i18n-react';

import API from '../../components/API/API';
import HasManyRelationManager from '../../components/HasManyRelationManager/HasManyRelationManager';
import ManyToManyRelationManager from '../../components/ManyToManyRelationManager/ManyToManyRelationManager';

class RouteDetails extends Component {
  constructor(props) {
    super(props);

    this.API = new API();

    this.state = {
      resourceNameOnApi: 'routes',
      resource: {},
      hiddenPropertyNamesOnDetail: [
        'id',
        'agentId',
        'agent',
        'shipments',
        'bundleId',
        'statusId',
        'agentCashInstanceId',
      ],
    };
  }

  async componentDidMount() {
    const { resourceNameOnApi } = this.state;
    const { match } = this.props;
    const response = await this.API.get(
      `/${resourceNameOnApi}/${match.params.id}`,
      {
        params: {
          filter: {
            include: ['agent', 'shipments', 'bundle', 'status'],
          },
        },
      },
    );
    this.setState({
      resource: response.data,
    });
  }

  render() {
    const {
      resourceNameOnApi,
      resource,
      hiddenPropertyNamesOnDetail,
    } = this.state;
    const { history } = this.props;

    return (
      <Container>
        <h3 className="text-secondary font-weight-bold">
          {T.translate('routes.detail.title')}
        </h3>
        <h4 className="text-secondary font-weight-light mb-0">
          {T.translate('routes.detail.description')}
        </h4>
        <hr />
        {Object.keys(resource).map((property) => {
          if (hiddenPropertyNamesOnDetail.includes(property)) {
            return null;
          }

          let propertyValue = (
            <span>{resource[property] || T.translate('defaults.notSet')}</span>
          );

          if (['isOffered'].includes(property)) {
            propertyValue = (
              <span>
                {!resource[property]
                  ? T.translate('defaults.no')
                  : T.translate('defaults.yes')}
              </span>
            );
          }

          if (
            ['startDatetime', 'endDatetime', 'createdAt', 'updatedAt'].includes(
              property,
            )
          ) {
            propertyValue = (
              <span>
                <Moment date={resource[property]} />
              </span>
            );
          }

          if (['bundle', 'status'].includes(property)) {
            propertyValue = <span>{resource[property].name}</span>;
          }

          return (
            <Row className="mb-3" key={property}>
              <Col md={4} className="font-weight-bold">
                <span>{T.translate(`routes.fields.${property}`)}</span>
              </Col>
              <Col md={8}>{propertyValue}</Col>
            </Row>
          );
        })}
        <hr />
        <Row>
          {resource.agent && (
            <Col md={6}>
              <Card className="mb-3">
                <CardHeader className="p-3">
                  <h5 className="text-secondary font-weight-bold mb-1">
                    {T.translate('agentCashInstances.detail.agent.title')}
                  </h5>
                  <h6 className="text-secondary font-weight-light mb-0">
                    {T.translate('agentCashInstances.detail.agent.description')}
                  </h6>
                </CardHeader>
                <CardBody className="p-3">
                  {Object.keys(resource.agent).map((property) => {
                    if (['name', 'phone'].includes(property)) {
                      return (
                        <Row className="mb-3" key={`agent_${property}`}>
                          <Col md={4} className="font-weight-bold">
                            <span>
                              {T.translate(`agents.fields.${property}`)}
                            </span>
                          </Col>
                          <Col md={8}>
                            {resource.agent[property]
                              || T.translate('defaults.notSet')}
                          </Col>
                        </Row>
                      );
                    }
                    return null;
                  })}
                  <div className="clearfix text-center">
                    <Link
                      to={`/agents/details/${resource.agentId}`}
                      className="btn btn-rounded btn-outline-primary btn-block"
                    >
                      {T.translate('agentCashInstances.detail.agent.viewAgent')}
                    </Link>
                  </div>
                </CardBody>
              </Card>
            </Col>
          )}
          {resource.id && (
            <Col md={6}>
              <HasManyRelationManager
                resourceEndPoint={resourceNameOnApi}
                resourceId={resource.id}
                relationEndPoint="shipments"
                relationAttribute="routeId"
                relationLabel={T.translate('routes.detail.shipments.label')}
                relationDetailRoute="/shipments/details"
                title={T.translate('routes.detail.shipments.title')}
                category={T.translate('routes.detail.shipments.description')}
                itemText="{{trackingId}}"
                nameProperty="trackingId"
              />
            </Col>
          )}
          {resource.id && (
            <Col md={6}>
              <ManyToManyRelationManager
                resourceEndPoint={resourceNameOnApi}
                resourceId={resource.id}
                relationEndPoint="requiredAgentTags"
                relationLabel={T.translate(
                  'routes.detail.requiredAgentTags.label',
                )}
                title={T.translate('routes.detail.requiredAgentTags.title')}
                category={T.translate(
                  'routes.detail.requiredAgentTags.description',
                )}
              />
            </Col>
          )}
        </Row>
        <hr className="mt-0" />
        <div className="clearfix text-center">
          <Button
            onClick={history.goBack}
            className="btn btn-rounded btn-lg btn-secondary float-md-left px-5"
          >
            {T.translate('defaults.goBack')}
          </Button>
          <Link
            to={`/${resourceNameOnApi}/update/${resource.id}`}
            className="btn btn-rounded btn-lg btn-primary float-md-right px-5"
          >
            {T.translate('routes.detail.editButton')}
          </Link>
        </div>
      </Container>
    );
  }
}

RouteDetails.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string.isRequired,
    }),
  }),
  history: PropTypes.shape({
    goBack: PropTypes.func.isRequired,
  }).isRequired,
};

RouteDetails.defaultProps = {
  match: {
    params: {
      id: '',
    },
  },
};

export default withRouter(RouteDetails);
