import React, { Component } from 'react';
import {
  Container, Row, Col, Table, Button,
} from 'reactstrap';
import { Link } from 'react-router-dom';
import SweetAlert from 'sweetalert2-react';
import T from 'i18n-react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { Pagination } from '../../components/Pagination/Pagination';
import API from '../../components/API/API';
import SearchBy from '../../components/Search/SearchBy';

export default class RouteList extends Component {
  constructor(props) {
    super(props);

    this.API = new API();

    this.pagination = React.createRef();

    this.searchFields = [
      { name: 'Agent', field: 'agentId' },
      { name: 'Bundle', field: 'bundleId' },
      { name: 'Status', field: 'statusId' },
    ];

    this.sortFields = [
      { name: 'agent', field: 'agentId' },
      { name: 'bundle', field: 'bundleId' },
      { name: 'status', field: 'statusId' },
      // { name: 'shipments', field: 'totalShipments' },
      { name: 'value', field: 'value' },
    ];

    this.getIcon = (name) => {
      const { sortBy } = this.state;

      const element = this.sortFields.find(d => d.name === name);
      if (!element) {
        return '';
      }

      const { field } = this.sortFields.find(d => d.name === name);
      const icon = field === sortBy.field ? sortBy.icon : 'sort';
      return <FontAwesomeIcon className="sort-icon" icon={icon} />;
    };

    this.state = {
      resourceNameOnApi: 'routes',
      listItems: [],
      autoAssignResponse: null,
      autoAssignError: null,
      showAutoAssignSuccessAlert: false,
      showAutoAssignErrorAlert: false,
      autoAssignIsRunning: false,
      backupList: [],
      sortBy: {
        field: '',
        type: '',
        icon: '',
      },
      order: 'agentId ASC',
      searchBy: this.searchFields[0].name,
      searchParam: {
        filter: {
          include: ['agent', 'bundle', 'status'],
        },
      },
      routeGenerationIsRunning: false,
      routeGenerationResponse: null,
    };

    this.routeStatusesEndpoint = 'routeStatuses';
    this.routeBundlesEndpoint = 'routeBundles';

    this.handleSortChange = (name) => {
      const element = this.sortFields.find(d => d.name === name);
      if (!element) {
        return;
      }

      const { sortBy } = this.state;
      const { type, field: previousField } = sortBy;
      const { field } = element;

      let newType = 'DESC';

      if (field === previousField) {
        newType = type === 'ASC' ? 'DESC' : 'ASC';
      }

      this.setState(
        {
          sortBy: {
            field,
            type: newType,
            icon: newType === 'ASC' ? 'sort-up' : 'sort-down',
          },
          order: `${field} ${newType}`,
        },
        () => {
          this.pagination.current.fetchItemCount();
        },
      );
    };

    this.searchTimeout = null;

    this.handleSearchChange = (value, type) => {
      if (type.toLowerCase() === 'input') {
        const { searchBy } = this.state;
        clearTimeout(this.searchTimeout);

        if (value) {
          const { field } = this.searchFields.find(d => d.name === searchBy);

          this.searchTimeout = setTimeout(async () => {
            let params;

            switch (field) {
              case 'bundleId':
              case 'statusId':
              case 'agentId': {
                let endpoint;

                if (field === 'statusId') {
                  endpoint = `/${this.routeStatusesEndpoint}`;
                } else if (field === 'bundleId') {
                  endpoint = `/${this.routeBundlesEndpoint}`;
                } else if (field === 'agentId') {
                  endpoint = '/agents';
                }

                const response = await this.API.get(endpoint, {
                  params: {
                    filter: {
                      fields: ['id'],
                      where: { name: { ilike: `%${value}%` } },
                    },
                  },
                });

                const ids = response.data.map(agent => agent.id);

                params = {
                  filter: {
                    where: {
                      [field]: {
                        inq: ids,
                      },
                    },
                  },
                };

                break;
              }

              default:
                break;
            }

            params.filter.include = ['agent'];
            this.setState({ searchParam: params }, () => {
              this.pagination.current.fetchItemCount();
            });
          }, 500);
        } else {
          const params = {
            filter: {
              include: ['agent'],
            },
          };

          this.setState({ searchParam: params }, () => {
            this.pagination.current.fetchItemCount();
          });
        }
      } else if (type.toLowerCase() === 'dropdown') {
        this.setState(prevState => ({
          searchBy: value,
          listItems: prevState.backupList,
        }));
      }
    };

    /**
     * @param {number[]} [specificRouteIds]
     */
    this.updateListFilter = (specificRouteIds) => {
      const filter = {
        include: [
          {
            relation: 'agent',
            scope: {
              fields: ['name'],
            },
          },
        ],
      };

      if (specificRouteIds) {
        filter.where = { id: { inq: specificRouteIds } };
      }
      this.setState({ searchParam: { filter } });
    };

    this.delete = (id) => {
      const { resourceNameOnApi } = this.state;
      this.API.delete(`/${resourceNameOnApi}/${id}`).then(() => {
        this.updateListFilter();
        this.pagination.current.fetchItems();
      });
    };

    this.onClickAutoAssignButton = () => {
      this.setState({ autoAssignIsRunning: true });

      const { resourceNameOnApi } = this.state;
      this.API.get(`/${resourceNameOnApi}/auto-assign`)
        .then((response) => {
          this.setState({
            autoAssignResponse: response.data,
            showAutoAssignSuccessAlert: true,
            autoAssignIsRunning: false,
          });

          const specificRouteIds = response.data.map(route => route.id);
          this.updateListFilter(specificRouteIds);
          this.pagination.current.fetchItems();
        })
        .catch((error) => {
          this.setState({
            autoAssignError: error.response
              ? error.response.data
              : error.message,
            showAutoAssignErrorAlert: true,
            autoAssignIsRunning: false,
          });
        });
    };

    this.onClickGenerateRoutesButton = async () => {
      this.setState({ routeGenerationIsRunning: true });
      const { resourceNameOnApi } = this.state;
      const response = await this.API.get(`/${resourceNameOnApi}/generate`);
      this.setState({
        routeGenerationResponse: response.data,
        showRouteGenerationSuccessAlert: true,
        routeGenerationIsRunning: false,
      });
      const specificRouteIds = response.data.map(route => route.id);
      this.updateListFilter(specificRouteIds);
      this.pagination.current.fetchItems();
    };
  }

  componentDidMount() {
    this.updateListFilter();
  }

  render() {
    const {
      resourceNameOnApi,
      listItems,
      showDeletionConfirmation,
      selectedItemToBeDeleted,
      autoAssignIsRunning,
      showAutoAssignSuccessAlert,
      showAutoAssignErrorAlert,
      autoAssignError,
      autoAssignResponse,
      searchBy,
      searchParam,
      order,
      showRouteGenerationSuccessAlert,
      routeGenerationResponse,
      routeGenerationIsRunning,
    } = this.state;

    return [
      <Container key="route-list-container">
        <Row className="mb-3">
          <Col>
            <h3 className="text-secondary font-weight-bold">
              {T.translate('routes.list.title')}
            </h3>
            <h4 className="text-secondary font-weight-light mb-0">
              {T.translate('routes.list.description')}
            </h4>
          </Col>
          <Col
            className="d-flex justify-content-end align-items-end search-wrapper"
            style={{ marginRight: '30px' }}
          >
            <SearchBy
              filters={this.searchFields.map(d => d.name)}
              default={searchBy}
              resourceNameOnApi={resourceNameOnApi}
              handleChange={this.handleSearchChange}
            />
          </Col>
          <Col md={3} className="d-flex justify-content-end align-items-end">
            <Link
              to={`/${resourceNameOnApi}/create`}
              className="btn btn btn-secondary btn-rounded px-3"
            >
              <FontAwesomeIcon icon="plus" />
              {' '}
              {T.translate('routes.list.add')}
            </Link>
          </Col>
        </Row>
        <Table responsive striped hover className="border">
          <thead>
            <tr>
              <th>
                <span
                  className="table-header"
                  onKeyPress={() => { }}
                  role="button"
                  tabIndex={-1}
                  onClick={() => this.handleSortChange('agent')}
                >
                  {T.translate('routes.fields.agent')}
                  {this.getIcon('agent')}
                </span>
              </th>
              <th>
                <span
                  className="table-header"
                  onKeyPress={() => { }}
                  role="button"
                  tabIndex={-1}
                  onClick={() => this.handleSortChange('bundle')}
                >
                  {T.translate('routes.fields.bundle')}
                  {this.getIcon('bundle')}
                </span>
              </th>
              <th>
                <span
                  className="table-header"
                  onKeyPress={() => { }}
                  role="button"
                  tabIndex={-1}
                  onClick={() => this.handleSortChange('status')}
                >
                  {T.translate('routes.fields.status')}
                  {this.getIcon('status')}
                </span>
              </th>
              <th className="text-center">
                <span
                  className="table-header"
                  onKeyPress={() => { }}
                  role="button"
                  tabIndex={-1}
                  onClick={() => this.handleSortChange('value')}
                >
                  {T.translate('routes.fields.value')}
                  {this.getIcon('value')}
                </span>
              </th>
              <th className="text-center">
                <FontAwesomeIcon icon="cubes" title="Total Shipments" />
              </th>
              <th className="text-center">
                <FontAwesomeIcon
                  icon="clipboard-list"
                  title="Visibility on Route Offers page"
                />
              </th>
              <th />
            </tr>
          </thead>
          <tbody>
            {listItems.map(item => (
              <tr key={item.id}>
                <td>
                  {item.agent ? (
                    <Link
                      to={{
                        pathname: `/agents/details/${item.agentId}`,
                        state: { from: 'routes' },
                      }}
                    >
                      {item.agent.name}
                    </Link>
                  ) : (
                    '(Not Set)'
                  )}
                </td>
                <td>{item.bundle}</td>
                <td>{item.status}</td>
                <td className="text-center">{item.value || '(Not Set)'}</td>
                <td className="text-center">{item.totalShipments}</td>
                <td className="text-center">
                  <FontAwesomeIcon
                    icon={item.isOffered ? 'eye' : 'eye-slash'}
                    title={`${
                      item.isOffered ? 'Displayed on' : 'Hidden from'
                    } Route Offers page on Agent App`}
                  />
                </td>
                <td className="text-right py-0 align-middle">
                  <div className="btn-group" role="group">
                    <Link
                      to={`/${resourceNameOnApi}/details/${item.id}`}
                      className="btn btn-primary btn-sm"
                      title={T.translate('routes.list.viewTooltip')}
                    >
                      <FontAwesomeIcon icon="eye" fixedWidth />
                    </Link>
                    <Link
                      className="btn btn-primary btn-sm"
                      to={`/${resourceNameOnApi}/update/${item.id}`}
                      title={T.translate('routes.list.editTooltip')}
                    >
                      <FontAwesomeIcon icon="pencil-alt" fixedWidth />
                    </Link>
                    <Button
                      size="sm"
                      color="primary"
                      title={T.translate('routes.list.deleteTooltip')}
                      onClick={() => {
                        this.setState({
                          selectedItemToBeDeleted: item,
                          showDeletionConfirmation: true,
                        });
                      }}
                    >
                      <FontAwesomeIcon icon="trash-alt" fixedWidth />
                    </Button>
                  </div>
                </td>
              </tr>
            ))}
          </tbody>
        </Table>
        <Row className="mt-3 mb-3">
          <Col>
            <Button
              color="secondary"
              outline
              onClick={this.onClickGenerateRoutesButton}
              disabled={autoAssignIsRunning}
              className="btn-rounded px-3 mx-2"
            >
              <FontAwesomeIcon icon="atom" />
              {' '}
              {T.translate('routes.list.generateRoutes')}
            </Button>
            <Button
              color="secondary"
              outline
              onClick={this.onClickAutoAssignButton}
              disabled={routeGenerationIsRunning}
              className="btn-rounded px-3 mx-2"
            >
              <FontAwesomeIcon icon="magic" />
              {' '}
              {T.translate('routes.list.autoAssign')}
            </Button>
          </Col>
          <Col>
            <Pagination
              ref={this.pagination}
              resourceNameOnApi={resourceNameOnApi}
              filter={{ ...searchParam.filter, order }}
              onItemsReceived={async (listItemsReceived) => {
                const routeStatuses = await this.API.get(
                  `/${this.routeStatusesEndpoint}`,
                );
                const routeBundles = await this.API.get(
                  `/${this.routeBundlesEndpoint}`,
                );

                const newList = listItemsReceived.map((d) => {
                  const element = { ...d };

                  if (routeStatuses.data) {
                    const statusFound = routeStatuses.data.find(
                      r => r.id === d.statusId,
                    );
                    if (statusFound) element.status = statusFound.name;
                  }

                  if (routeBundles.data) {
                    const routeBundleFound = routeBundles.data.find(
                      r => r.id === d.bundleId,
                    );
                    if (routeBundleFound) element.bundle = routeBundleFound.name;
                  }

                  return element;
                });

                this.setState({
                  listItems: newList,
                  backupList: newList,
                });
              }}
            />
          </Col>
        </Row>
      </Container>,
      <SweetAlert
        key="sweet-alert-deletion-confirmation"
        show={showDeletionConfirmation}
        title={T.translate('routes.list.deleteWarning.title', {
          itemToBeDeleted: selectedItemToBeDeleted
            ? selectedItemToBeDeleted.name
            : '',
        })}
        text={T.translate('routes.list.deleteWarning.message')}
        type="warning"
        showCancelButton
        confirmButtonText={T.translate(
          'routes.list.deleteWarning.confirmButton',
        )}
        cancelButtonText={T.translate('routes.list.deleteWarning.cancelButton')}
        confirmButtonClass="btn btn-primary btn-rounded mx-2 btn-lg px-5"
        cancelButtonClass="btn btn-secondary btn-rounded mx-2 btn-lg px-5"
        buttonsStyling={false}
        onConfirm={() => {
          this.delete(selectedItemToBeDeleted.id);
          this.setState({ showDeletionConfirmation: false });
        }}
      />,
      <SweetAlert
        key="sweet-alert-auto-assign-success"
        show={showAutoAssignSuccessAlert}
        title={T.translate('routes.list.autoAssignSuccessAlert.title')}
        text={T.translate('routes.list.autoAssignSuccessAlert.text', {
          numberOfRoutesAssigned: autoAssignResponse
            ? autoAssignResponse.length
            : 0,
        })}
        type="success"
        confirmButtonText={T.translate(
          'routes.list.autoAssignSuccessAlert.confirmButton',
        )}
        confirmButtonClass="btn btn-primary btn-rounded mx-2 btn-lg px-5"
        buttonsStyling={false}
        onConfirm={() => {
          this.setState({ showAutoAssignSuccessAlert: false });
        }}
      />,
      <SweetAlert
        key="sweet-alert-auto-assign-error"
        show={showAutoAssignErrorAlert}
        title={T.translate('routes.list.autoAssignErrorAlert.title')}
        text={JSON.stringify(autoAssignError)}
        type="warning"
        confirmButtonText={T.translate(
          'routes.list.autoAssignErrorAlert.confirmButton',
        )}
        confirmButtonClass="btn btn-primary btn-rounded mx-2 btn-lg px-5"
        buttonsStyling={false}
        onConfirm={() => {
          this.setState({ showAutoAssignErrorAlert: false });
        }}
      />,
      <SweetAlert
        key="sweet-alert-route-generation-success"
        show={showRouteGenerationSuccessAlert}
        title={T.translate('routes.list.routeGenerationSuccessAlert.title')}
        text={T.translate('routes.list.routeGenerationSuccessAlert.text', {
          numberOfRoutesGenerated: routeGenerationResponse
            ? routeGenerationResponse.length
            : 0,
        })}
        type="success"
        confirmButtonText={T.translate('defaults.ok')}
        confirmButtonClass="btn btn-primary btn-rounded mx-2 btn-lg px-5"
        buttonsStyling={false}
        onConfirm={() => {
          this.setState({ showRouteGenerationSuccessAlert: false });
        }}
      />,
    ];
  }
}
