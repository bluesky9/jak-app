import React, { Component } from 'react';
import {
  Container, Row, Col, Table, Button,
} from 'reactstrap';
import Moment from 'react-moment';
import { Link } from 'react-router-dom';
import SweetAlert from 'sweetalert2-react';
import T from 'i18n-react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { PARTNER_CASH_DIRECTION } from 'common/constants/models';

import { Pagination } from '../../components/Pagination/Pagination';
import API from '../../components/API/API';
import SearchBy from '../../components/Search/SearchBy';

export default class PartnerCashInstanceList extends Component {
  constructor(props) {
    super(props);

    this.API = new API();

    this.pagination = React.createRef();

    this.searchFields = [
      { name: 'Partner', field: 'partnerId' },
      { name: 'Due Date', field: 'dueDatetime' },
      { name: 'Amount', field: 'amount' },
    ];

    this.sortFields = [
      { name: 'partner', field: 'partnerId' },
      { name: 'dueDatetime', field: 'dueDatetime' },
      { name: 'amount', field: 'amount' },
    ];

    this.getIcon = (name) => {
      const { sortBy } = this.state;

      const element = this.sortFields.find(d => d.name === name);
      if (!element) {
        return '';
      }

      const { field } = this.sortFields.find(d => d.name === name);
      const icon = field === sortBy.field ? sortBy.icon : 'sort';
      return <FontAwesomeIcon className="sort-icon" icon={icon} />;
    };

    this.state = {
      resourceNameOnApi: 'partnerCashInstances',
      listItems: [],
      backupList: [],
      sortBy: {
        field: '',
        type: '',
        icon: '',
      },
      order: 'partnerId ASC',
      searchBy: this.searchFields[0].name,
      searchParam: {
        filter: {
          include: ['partner'],
        },
      },
    };

    this.delete = (id) => {
      const { resourceNameOnApi } = this.state;
      this.API.delete(`/${resourceNameOnApi}/${id}`).then(() => {
        this.pagination.current.fetchItems();
      });
    };

    this.handleSortChange = (name) => {
      const element = this.sortFields.find(d => d.name === name);
      if (!element) {
        return;
      }

      const { sortBy } = this.state;
      const { type, field: previousField } = sortBy;
      const { field } = element;

      let newType = 'DESC';

      if (field === previousField) {
        newType = type === 'ASC' ? 'DESC' : 'ASC';
      }

      this.setState(
        {
          sortBy: {
            field,
            type: newType,
            icon: newType === 'ASC' ? 'sort-up' : 'sort-down',
          },
          order: `${field} ${newType}`,
        },
        () => {
          this.pagination.current.fetchItemCount();
        },
      );
    };

    this.searchTimeout = null;

    this.handleSearchChange = (value, type) => {
      if (type.toLowerCase() === 'input') {
        const { searchBy } = this.state;
        clearTimeout(this.searchTimeout);

        if (value) {
          const key = this.searchFields.find(d => d.name === searchBy).field;
          const val = value.toLowerCase();

          this.searchTimeout = setTimeout(async () => {
            let params;
            let makeRequest = true;

            switch (key) {
              case 'dueDatetime': {
                const date = new Date(value);

                // will only query if date is valid
                if (date.toDateString().toLowerCase() === 'invalid date') {
                  clearTimeout(this.searchTimeout);
                  makeRequest = false;
                }

                const year = date.getFullYear();
                const month = date.getMonth() + 1;
                const day = date.getDate();

                const startDate = `${year}-${month}-${day}`;
                const endDate = `${year}-${month}-${day + 1}`;

                params = {
                  filter: {
                    where: {
                      [key]: {
                        between: [startDate, endDate],
                      },
                    },
                  },
                };

                break;
              }

              case 'amount': {
                let operation;

                if (val.startsWith('>=') || val.startsWith('=>')) {
                  operation = 'gte';
                } else if (val.startsWith('>')) {
                  operation = 'gt';
                } else if (val.startsWith('<=') || val.startsWith('=<')) {
                  operation = 'lte';
                } else if (val.startsWith('<')) {
                  operation = 'lt';
                } else if (val.startsWith('=')) {
                  operation = 'eq';
                }

                // removes mathematical symbols
                const strippedValue = val.replace(/>|=|</g, '');

                if (operation && operation !== 'eq') {
                  params = {
                    filter: {
                      where: {
                        [key]: {
                          [operation]: strippedValue,
                        },
                      },
                    },
                  };
                } else {
                  params = {
                    filter: {
                      where: {
                        [key]: strippedValue,
                      },
                    },
                  };
                }

                break;
              }

              case 'partnerId': {
                const partnersRequest = await this.API.get('/partners', {
                  params: {
                    filter: {
                      fields: ['id'],
                      where: { name: { ilike: `%${val}%` } },
                    },
                  },
                });

                const partnerIds = partnersRequest.data.map(
                  partner => partner.id,
                );

                params = {
                  filter: {
                    where: {
                      [key]: {
                        inq: partnerIds,
                      },
                    },
                  },
                };

                break;
              }

              default:
                break;
            }

            params.filter.include = 'partner';

            if (makeRequest) {
              this.setState({ searchParam: params }, () => {
                this.pagination.current.fetchItemCount();
              });
            }
          }, 500);
        } else {
          const params = {
            filter: {
              include: ['partner'],
            },
          };

          this.setState({ searchParam: params }, () => {
            this.pagination.current.fetchItemCount();
          });
        }
      } else if (type.toLowerCase() === 'dropdown') {
        this.setState(prevState => ({
          searchBy: value,
          listItems: prevState.backupList,
        }));
      }
    };
  }

  render() {
    const {
      resourceNameOnApi,
      listItems,
      showDeletionConfirmation,
      selectedItemToBeDeleted,
      searchBy,
      searchParam,
      order,
    } = this.state;

    return [
      <Container key="partner-cash-instance-list-container">
        <Row className="mb-3">
          <Col>
            <h3 className="text-secondary font-weight-bold">
              {T.translate('partnerCashInstances.list.title')}
            </h3>
            <h4 className="text-secondary font-weight-light mb-0">
              {T.translate('partnerCashInstances.list.description')}
            </h4>
          </Col>
          <Col
            md={3}
            className="d-flex justify-content-end align-items-end search-wrapper"
            style={{ marginRight: '20px' }}
          >
            <SearchBy
              filters={this.searchFields.map(d => d.name)}
              default={searchBy}
              resourceNameOnApi={resourceNameOnApi}
              handleChange={this.handleSearchChange}
            />
          </Col>
          <Col md={3} className="d-flex justify-content-end align-items-end">
            <Link
              to={`/${resourceNameOnApi}/create`}
              className="btn btn btn-secondary btn-rounded px-3"
            >
              <FontAwesomeIcon icon="plus" />
              {' '}
              {T.translate('partnerCashInstances.list.add')}
            </Link>
          </Col>
        </Row>
        <Table responsive striped hover className="border">
          <thead>
            <tr>
              <th>
                <span
                  className="table-header"
                  onKeyPress={() => { }}
                  role="button"
                  tabIndex={-1}
                  onClick={() => this.handleSortChange('dueDatetime')}
                >
                  {T.translate('partnerCashInstances.fields.dueDatetime')}
                  {this.getIcon('dueDatetime')}
                </span>
              </th>
              <th>
                <span
                  className="table-header"
                  onKeyPress={() => { }}
                  role="button"
                  tabIndex={-1}
                  onClick={() => this.handleSortChange('amount')}
                >
                  {T.translate('partnerCashInstances.fields.amount')}
                  {this.getIcon('amount')}
                </span>
              </th>
              <th colSpan={2}>
                <span
                  className="table-header"
                  onKeyPress={() => { }}
                  role="button"
                  tabIndex={-1}
                  onClick={() => this.handleSortChange('partner')}
                >
                  {T.translate('partnerCashInstances.fields.partner')}
                  {this.getIcon('partner')}
                </span>
              </th>
            </tr>
          </thead>
          <tbody>
            {listItems.map(item => (
              <tr key={item.id}>
                <td>
                  <Moment date={item.dueDatetime} />
                </td>
                <td>
                  <i
                    className={
                      item.directionId === PARTNER_CASH_DIRECTION.INBOUND.id
                        ? 'pe-7s-plus text-success'
                        : 'pe-7s-less text-danger'
                    }
                  />
                  &nbsp;
                  {item.amount}
                </td>
                <td>
                  <Link
                    to={{
                      pathname: `/partners/details/${item.partnerId}`,
                      state: { from: 'partnerCashInstances' },
                    }}
                  >
                    {item.partner
                      ? item.partner.name
                      : T.translate('defaults.notSet')}
                  </Link>
                </td>
                <td className="text-right py-0 align-middle">
                  <div className="btn-group" role="group">
                    <Link
                      to={`/${resourceNameOnApi}/details/${item.id}`}
                      className="btn btn-primary btn-sm"
                      title={T.translate(
                        'partnerCashInstances.list.viewTooltip',
                      )}
                    >
                      <FontAwesomeIcon icon="eye" fixedWidth />
                    </Link>
                    <Link
                      className="btn btn-primary btn-sm"
                      to={`/${resourceNameOnApi}/update/${item.id}`}
                      title={T.translate(
                        'partnerCashInstances.list.editTooltip',
                      )}
                    >
                      <FontAwesomeIcon icon="pencil-alt" fixedWidth />
                    </Link>
                    <Button
                      size="sm"
                      color="primary"
                      title={T.translate(
                        'partnerCashInstances.list.deleteTooltip',
                      )}
                      onClick={() => {
                        this.setState({
                          selectedItemToBeDeleted: item,
                          showDeletionConfirmation: true,
                        });
                      }}
                    >
                      <FontAwesomeIcon icon="trash-alt" fixedWidth />
                    </Button>
                  </div>
                </td>
              </tr>
            ))}
          </tbody>
        </Table>
        <Pagination
          ref={this.pagination}
          resourceNameOnApi={resourceNameOnApi}
          filter={{ ...searchParam.filter, order }}
          onItemsReceived={(listItemsReceived) => {
            this.setState({
              listItems: listItemsReceived,
              backupList: listItemsReceived,
            });
          }}
        />
      </Container>,
      <SweetAlert
        key="sweet-alert"
        show={showDeletionConfirmation}
        title={T.translate('partnerCashInstances.list.deleteWarning.title', {
          itemToBeDeleted: selectedItemToBeDeleted
            ? selectedItemToBeDeleted.name
            : '',
        })}
        text={T.translate('partnerCashInstances.list.deleteWarning.message')}
        type="warning"
        showCancelButton
        confirmButtonText={T.translate(
          'partnerCashInstances.list.deleteWarning.confirmButton',
        )}
        cancelButtonText={T.translate(
          'partnerCashInstances.list.deleteWarning.cancelButton',
        )}
        confirmButtonClass="btn btn-primary btn-rounded mx-2 btn-lg px-5"
        cancelButtonClass="btn btn-secondary btn-rounded mx-2 btn-lg px-5"
        buttonsStyling={false}
        onConfirm={() => {
          this.delete(selectedItemToBeDeleted.id);
          this.setState({ showDeletionConfirmation: false });
        }}
      />,
    ];
  }
}
