import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Moment from 'react-moment';
import {
  Container,
  Row,
  Col,
  Card,
  CardHeader,
  CardBody,
  Button,
} from 'reactstrap';
import { Link, withRouter } from 'react-router-dom';
import T from 'i18n-react';

import API from '../../components/API/API';

class PartnerCashInstanceDetails extends Component {
  constructor(props) {
    super(props);

    this.API = new API();

    this.state = {
      resourceNameOnApi: 'partnerCashInstances',
      resource: {},
      hiddenPropertyNamesOnDetail: [
        'id',
        'shipmentId',
        'directionId',
        'typeId',
        'partnerId',
      ],
    };
  }

  componentDidMount() {
    const { resourceNameOnApi } = this.state;
    const { match } = this.props;
    this.API.get(`/${resourceNameOnApi}/${match.params.id}`, {
      params: {
        filter: {
          include: ['shipment', 'direction', 'type', 'partner'],
        },
      },
    }).then((response) => {
      this.setState({
        resource: response.data,
      });
    });
  }

  render() {
    const {
      resourceNameOnApi,
      resource,
      hiddenPropertyNamesOnDetail,
    } = this.state;

    const { history } = this.props;

    return (
      <Container>
        <h3 className="text-secondary font-weight-bold">
          {T.translate('partnerCashInstances.detail.title')}
        </h3>
        <h4 className="text-secondary font-weight-light mb-0">
          {T.translate('partnerCashInstances.detail.description')}
        </h4>
        <hr />
        {Object.keys(resource).map((property) => {
          if (hiddenPropertyNamesOnDetail.includes(property)) {
            return null;
          }

          let propertyValue;

          if (
            ['createdAt', 'updatedAt', 'dueDatetime', 'settledAt'].includes(
              property,
            )
          ) {
            propertyValue = (
              <span>
                <Moment date={resource[property]} />
              </span>
            );
          } else if (['type', 'direction'].includes(property)) {
            propertyValue = (
              <span>
                {T.translate(
                  `partnerCashInstances.fields.${property}s.${
                    resource[property].name
                  }`,
                )}
              </span>
            );
          } else if (property === 'partner') {
            propertyValue = (
              <span>
                <Link to={`/partners/details/${resource[property].id}`}>
                  {resource[property].name}
                </Link>
              </span>
            );
          } else if (property === 'shipment') {
            propertyValue = (
              <span>
                <Link to={`/shipments/details/${resource[property].id}`}>
                  {resource[property].trackingId}
                </Link>
              </span>
            );
          } else if (property === 'isSettled') {
            propertyValue = (
              <span>
                {resource[property]
                  ? T.translate('defaults.yes')
                  : T.translate('defaults.no')}
              </span>
            );
          } else {
            propertyValue = (
              <span>
                {resource[property] || T.translate('defaults.notSet')}
              </span>
            );
          }

          return (
            <Row className="mb-3" key={property}>
              <Col md={4} className="font-weight-bold">
                <span>
                  {T.translate(`partnerCashInstances.fields.${property}`)}
                </span>
              </Col>
              <Col md={8}>{propertyValue}</Col>
            </Row>
          );
        })}
        <hr />
        <Row>
          {resource.partnerId && (
            <Col md={6}>
              <Card className="mb-3">
                <CardHeader className="p-3">
                  <h5 className="text-secondary font-weight-bold mb-1">
                    {T.translate('partnerCashInstances.detail.partner.title')}
                  </h5>
                  <h6 className="text-secondary font-weight-light mb-0">
                    {T.translate(
                      'partnerCashInstances.detail.partner.description',
                    )}
                  </h6>
                </CardHeader>
                <CardBody className="p-3">
                  {Object.keys(resource.partner).map((property) => {
                    if (['name', 'phone'].includes(property)) {
                      return (
                        <Row className="mb-3" key={`partner_${property}`}>
                          <Col md={4} className="font-weight-bold">
                            <span>
                              {T.translate(`partners.fields.${property}`)}
                            </span>
                          </Col>
                          <Col md={8}>
                            {resource.partner[property]
                              || T.translate('defaults.notSet')}
                          </Col>
                        </Row>
                      );
                    }
                    return null;
                  })}
                  <div className="clearfix text-center">
                    <Link
                      to={`/partners/details/${resource.partnerId}`}
                      className="btn btn-rounded btn-outline-primary btn-block"
                    >
                      {T.translate(
                        'partnerCashInstances.detail.partner.viewPartner',
                      )}
                    </Link>
                  </div>
                </CardBody>
              </Card>
            </Col>
          )}
          {resource.shipmentId && (
            <Col md={6}>
              <Card className="mb-3">
                <CardHeader className="p-3">
                  <h5 className="text-secondary font-weight-bold mb-1">
                    {T.translate('partnerCashInstances.detail.shipment.title')}
                  </h5>
                  <h6 className="text-secondary font-weight-light mb-0">
                    {T.translate(
                      'partnerCashInstances.detail.shipment.description',
                    )}
                  </h6>
                </CardHeader>
                <CardBody className="p-3">
                  {Object.keys(resource.shipment).map((property) => {
                    let propertyValue;

                    if (
                      ['trackingId', 'createdAt', 'status', 'routeId'].includes(
                        property,
                      )
                    ) {
                      if (property === 'routeId') {
                        propertyValue = (
                          <span>
                            {resource.shipment.routeId ? (
                              <Link
                                to={`/routes/details/${
                                  resource.shipment[property]
                                }`}
                              >
                                {T.translate(
                                  'partnerCashInstances.detail.shipment.viewRoute',
                                )}
                              </Link>
                            ) : (
                              T.translate('defaults.notSet')
                            )}
                          </span>
                        );
                      } else if (property === 'createdAt') {
                        propertyValue = (
                          <span>
                            <Moment date={resource.shipment[property]} />
                          </span>
                        );
                      } else {
                        propertyValue = (
                          <span>
                            {resource.shipment[property]
                              || T.translate('defaults.notSet')}
                          </span>
                        );
                      }

                      return (
                        <Row className="mb-3" key={`shipment_${property}`}>
                          <Col md={4} className="font-weight-bold">
                            <span>
                              {T.translate(
                                `shipments.fields.${
                                  property === 'routeId' ? 'route' : property
                                }`,
                              )}
                            </span>
                          </Col>
                          <Col md={8}>{propertyValue}</Col>
                        </Row>
                      );
                    }
                    return null;
                  })}
                  <div className="clearfix text-center">
                    <Link
                      to={`/shipments/details/${resource.shipmentId}`}
                      className="btn btn-rounded btn-outline-primary btn-block"
                    >
                      {T.translate(
                        'partnerCashInstances.detail.shipment.viewShipment',
                      )}
                    </Link>
                  </div>
                </CardBody>
              </Card>
            </Col>
          )}
        </Row>
        <hr className="mt-0" />
        <div className="clearfix text-center">
          <Button
            onClick={history.goBack}
            className="btn btn-rounded btn-lg btn-secondary float-md-left px-5"
          >
            {T.translate('defaults.goBack')}
          </Button>
          <Link
            to={`/${resourceNameOnApi}/update/${resource.id}`}
            className="btn btn-rounded btn-lg btn-primary float-md-right px-5"
          >
            {T.translate('partnerCashInstances.detail.editButton')}
          </Link>
        </div>
      </Container>
    );
  }
}

PartnerCashInstanceDetails.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string.isRequired,
    }),
  }),
  history: PropTypes.shape({
    goBack: PropTypes.func.isRequired,
  }).isRequired,
};

PartnerCashInstanceDetails.defaultProps = {
  match: {
    params: {
      id: '',
    },
  },
};

export default withRouter(PartnerCashInstanceDetails);
