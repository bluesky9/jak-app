import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Moment from 'react-moment';
import {
  Container,
  Row,
  Col,
  Card,
  CardHeader,
  CardBody,
  Button,
} from 'reactstrap';
import { Link, withRouter } from 'react-router-dom';
import T from 'i18n-react';

import API from '../../components/API/API';
import ManyToManyRelationManager from '../../components/ManyToManyRelationManager/ManyToManyRelationManager';
import { MapWithMarkers } from '../../components/Maps/MapWithMarkers';

class AreaDetails extends Component {
  constructor(props) {
    super(props);

    this.API = new API();

    this.state = {
      resourceNameOnApi: 'areas',
      resource: {},
      hiddenPropertyNamesOnDetail: ['id'],
    };
  }

  componentDidMount() {
    const { resourceNameOnApi } = this.state;
    const { match } = this.props;
    this.API.get(`/${resourceNameOnApi}/${match.params.id}`).then((response) => {
      this.setState({
        resource: response.data,
      });
    });
  }

  render() {
    const {
      resourceNameOnApi,
      resource,
      hiddenPropertyNamesOnDetail,
    } = this.state;
    const { history } = this.props;

    return (
      <Container>
        <h3 className="text-secondary font-weight-bold">
          {T.translate('areas.detail.title')}
        </h3>
        <h4 className="text-secondary font-weight-light mb-0">
          {T.translate('areas.detail.description')}
        </h4>
        <hr />
        {Object.keys(resource).map((property) => {
          if (hiddenPropertyNamesOnDetail.includes(property)) {
            return null;
          }

          let propertyValue;

          if (['createdAt', 'updatedAt'].includes(property)) {
            propertyValue = (
              <span>
                <Moment date={resource[property]} />
              </span>
            );
          }
          if (property === 'centerGeoPoint') {
            propertyValue = (
              <span>
                (
                {resource.centerGeoPoint.lat}
,
                {resource.centerGeoPoint.lng}
)
              </span>
            );
          } else {
            propertyValue = (
              <span>
                {resource[property] || T.translate('defaults.notSet')}
              </span>
            );
          }

          return (
            <Row className="mb-3" key={property}>
              <Col md={4} className="font-weight-bold">
                <span>{T.translate(`areas.fields.${property}`)}</span>
              </Col>
              <Col md={8}>{propertyValue}</Col>
            </Row>
          );
        })}
        <hr />
        <Row>
          {Object.prototype.hasOwnProperty.call(resource, 'centerGeoPoint') && (
            <Col md={6}>
              <Card className="mb-3">
                <CardHeader className="p-3">
                  <h5 className="text-secondary font-weight-bold mb-1">
                    {T.translate('areas.detail.centerGeoPoint.title')}
                  </h5>
                  <h6 className="text-secondary font-weight-light mb-0">
                    {T.translate('areas.detail.centerGeoPoint.description')}
                  </h6>
                </CardHeader>
                <CardBody className="p-0">
                  <MapWithMarkers
                    mapProps={{
                      defaultCenter: {
                        lat: resource.centerGeoPoint.lat,
                        lng: resource.centerGeoPoint.lng,
                      },
                      defaultOptions: {
                        zoom: 13,
                        scrollwheel: false,
                        zoomControl: true,
                      },
                    }}
                    containerHeight="450px"
                    markers={[
                      {
                        position: {
                          lat: resource.centerGeoPoint.lat,
                          lng: resource.centerGeoPoint.lng,
                        },
                      },
                    ]}
                    autoCenter={false}
                  />
                </CardBody>
              </Card>
            </Col>
          )}
          <Col md={6}>
            {resource.id && (
              <ManyToManyRelationManager
                resourceEndPoint={resourceNameOnApi}
                resourceId={resource.id}
                relationEndPoint="agents"
                relationLabel={T.translate('areas.detail.agents.label')}
                title={T.translate('areas.detail.agents.title')}
                category={T.translate('areas.detail.agents.description')}
              />
            )}
          </Col>
        </Row>
        <hr className="mt-0" />
        <div className="clearfix text-center">
          <Button
            onClick={history.goBack}
            className="btn btn-rounded btn-lg btn-secondary float-md-left px-5"
          >
            {T.translate('defaults.goBack')}
          </Button>
          <Link
            to={`/${resourceNameOnApi}/update/${resource.id}`}
            className="btn btn-rounded btn-lg btn-primary float-md-right px-5"
          >
            {T.translate('areas.detail.editButton')}
          </Link>
        </div>
      </Container>
    );
  }
}

AreaDetails.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string.isRequired,
    }),
  }),
  history: PropTypes.shape({
    goBack: PropTypes.func.isRequired,
  }).isRequired,
};

AreaDetails.defaultProps = {
  match: {
    params: {
      id: '',
    },
  },
};

export default withRouter(AreaDetails);
