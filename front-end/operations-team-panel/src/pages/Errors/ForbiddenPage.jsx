import React from 'react';
import { Link } from 'react-router-dom';
import {
  Container,
  Row,
  Col,
  Card,
  CardBody,
  CardTitle,
  CardSubtitle,
} from 'reactstrap';
import T from 'i18n-react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

export const ForbiddenPage = () => (
  <Container fluid>
    <Row className="justify-content-center">
      <Col md={8} lg={6}>
        <Card style={{ marginTop: '50px' }}>
          <CardBody className="text-center py-4">
            <h1 style={{ fontSize: '50px' }}>
              <FontAwesomeIcon icon="user-lock" className="text-muted" />
            </h1>
            <CardTitle>{T.translate('error.forbidden.title')}</CardTitle>
            <CardSubtitle>
              {T.translate('error.forbidden.message')}
            </CardSubtitle>
            <Link to="/" className="btn btn-primary btn-rounded px-5 mt-4">
              {T.translate('defaults.goBack')}
            </Link>
          </CardBody>
        </Card>
      </Col>
    </Row>
  </Container>
);

export default ForbiddenPage;
