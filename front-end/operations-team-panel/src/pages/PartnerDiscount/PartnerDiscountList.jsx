import React, { Component } from 'react';
import {
  Container, Row, Col, Table, Button,
} from 'reactstrap';
import { Link } from 'react-router-dom';
import SweetAlert from 'sweetalert2-react';
import T from 'i18n-react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { Pagination } from '../../components/Pagination/Pagination';
import API from '../../components/API/API';
import SearchBy from '../../components/Search/SearchBy';

export default class PartnerDiscountList extends Component {
  constructor(props) {
    super(props);

    this.API = new API();

    this.pagination = React.createRef();

    this.searchFields = [
      { name: 'Partner', field: 'partnerId' },
      { name: 'Code', field: 'code' },
    ];

    this.sortFields = [
      { name: 'partner', field: 'partnerId' },
      { name: 'code', field: 'code' },
    ];

    this.getIcon = (name) => {
      const { sortBy } = this.state;

      const element = this.sortFields.find(d => d.name === name);
      if (!element) {
        return '';
      }

      const { field } = this.sortFields.find(d => d.name === name);
      const icon = field === sortBy.field ? sortBy.icon : 'sort';
      return <FontAwesomeIcon className="sort-icon" icon={icon} />;
    };

    this.state = {
      resourceNameOnApi: 'partnerDiscounts',
      listItems: [],
      backupList: [],
      sortBy: this.sortFields[0].name,
      order: 'partnerId ASC',
      searchBy: this.searchFields[0].name,
      searchParam: {
        filter: {
          include: ['partner'],
        },
      },
    };

    this.delete = (id) => {
      const { resourceNameOnApi } = this.state;
      this.API.delete(`/${resourceNameOnApi}/${id}`).then(() => {
        this.pagination.current.fetchItems();
      });
    };

    this.handleSortChange = (name) => {
      const element = this.sortFields.find(d => d.name === name);
      if (!element) {
        return;
      }

      const { sortBy } = this.state;
      const { type, field: previousField } = sortBy;
      const { field } = element;

      let newType = 'DESC';

      if (field === previousField) {
        newType = type === 'ASC' ? 'DESC' : 'ASC';
      }

      this.setState(
        {
          sortBy: {
            field,
            type: newType,
            icon: newType === 'ASC' ? 'sort-up' : 'sort-down',
          },
          order: `${field} ${newType}`,
        },
        () => {
          this.pagination.current.fetchItemCount();
        },
      );
    };

    this.searchTimeout = null;

    this.handleSearchChange = (value, type) => {
      if (type.toLowerCase() === 'input') {
        const { searchBy } = this.state;
        clearTimeout(this.searchTimeout);

        if (value) {
          const key = this.searchFields.find(d => d.name === searchBy).field;
          const val = value.toLowerCase();

          this.searchTimeout = setTimeout(async () => {
            let params = {};

            switch (key) {
              case 'partnerId': {
                const partnersRequest = await this.API.get('/partners', {
                  params: {
                    filter: {
                      fields: ['id'],
                      where: { name: { ilike: `%${val}%` } },
                    },
                  },
                });

                const partnerIds = partnersRequest.data.map(
                  partner => partner.id,
                );

                params = {
                  filter: {
                    where: {
                      [key]: {
                        inq: partnerIds,
                      },
                    },
                  },
                };

                break;
              }

              default: {
                params = {
                  filter: {
                    where: {
                      [key]: {
                        ilike: `%${val}%`,
                      },
                    },
                  },
                };
              }
            }

            params.filter.include = 'partner';

            this.setState({ searchParam: params }, () => {
              this.pagination.current.fetchItemCount();
            });
          }, 500);
        } else {
          const params = {
            filter: {
              include: ['partner'],
            },
          };

          this.setState({ searchParam: params }, () => {
            this.pagination.current.fetchItemCount();
          });
        }
      } else if (type.toLowerCase() === 'dropdown') {
        this.setState(prevState => ({
          searchBy: value,
          listItems: prevState.backupList,
        }));
      }
    };
  }

  render() {
    const {
      resourceNameOnApi,
      listItems,
      showDeletionConfirmation,
      selectedItemToBeDeleted,
      searchBy,
      searchParam,
      order,
    } = this.state;

    return [
      <Container key="partner-discount-container">
        <Row className="mb-3">
          <Col>
            <h3 className="text-secondary font-weight-bold">
              {T.translate('partnerDiscounts.list.title')}
            </h3>
            <h4 className="text-secondary font-weight-light mb-0">
              {T.translate('partnerDiscounts.list.description')}
            </h4>
          </Col>
          <Col
            md={3}
            className="d-flex justify-content-end align-items-end search-wrapper"
            style={{ marginRight: '20px' }}
          >
            <SearchBy
              filters={this.searchFields.map(d => d.name)}
              default={searchBy}
              resourceNameOnApi={resourceNameOnApi}
              handleChange={this.handleSearchChange}
            />
          </Col>
          <Col md={3} className="d-flex justify-content-end align-items-end">
            <Link
              to={`/${resourceNameOnApi}/create`}
              className="btn btn btn-secondary btn-rounded px-3"
            >
              <FontAwesomeIcon icon="plus" />
              {' '}
              {T.translate('partnerDiscounts.list.add')}
            </Link>
          </Col>
        </Row>
        <Table responsive striped hover className="border">
          <thead>
            <tr>
              <th>
                <span
                  className="table-header"
                  onKeyPress={() => { }}
                  role="button"
                  tabIndex={-1}
                  onClick={() => this.handleSortChange('code')}
                >
                  {T.translate('partnerDiscounts.fields.code')}
                  {this.getIcon('code')}
                </span>
              </th>
              <th colSpan={2}>
                <span
                  className="table-header"
                  onKeyPress={() => { }}
                  role="button"
                  tabIndex={-1}
                  onClick={() => this.handleSortChange('partner')}
                >
                  {T.translate('partnerDiscounts.fields.partner')}
                  {this.getIcon('partner')}
                </span>
              </th>
            </tr>
          </thead>
          <tbody>
            {listItems.map(item => (
              <tr key={item.id}>
                <td>{item.code}</td>
                <td>
                  <Link
                    to={{
                      pathname: `/partners/details/${item.partnerId}`,
                      state: { from: 'partnerDiscounts' },
                    }}
                  >
                    {item.partner
                      ? item.partner.name
                      : T.translate('defaults.notSet')}
                  </Link>
                </td>
                <td className="text-right py-0 align-middle">
                  <div className="btn-group" role="group">
                    <Link
                      to={`/${resourceNameOnApi}/details/${item.id}`}
                      className="btn btn-primary btn-sm"
                      title={T.translate('partnerDiscounts.list.viewTooltip')}
                    >
                      <FontAwesomeIcon icon="eye" fixedWidth />
                    </Link>
                    <Link
                      className="btn btn-primary btn-sm"
                      to={`/${resourceNameOnApi}/update/${item.id}`}
                      title={T.translate('partnerDiscounts.list.editTooltip')}
                    >
                      <FontAwesomeIcon icon="pencil-alt" fixedWidth />
                    </Link>
                    <Button
                      size="sm"
                      color="primary"
                      title={T.translate('partnerDiscounts.list.deleteTooltip')}
                      onClick={() => {
                        this.setState({
                          selectedItemToBeDeleted: item,
                          showDeletionConfirmation: true,
                        });
                      }}
                    >
                      <FontAwesomeIcon icon="trash-alt" fixedWidth />
                    </Button>
                  </div>
                </td>
              </tr>
            ))}
          </tbody>
        </Table>
        <Pagination
          ref={this.pagination}
          resourceNameOnApi={resourceNameOnApi}
          filter={{ ...searchParam.filter, order }}
          onItemsReceived={(listItemsReceived) => {
            this.setState({
              listItems: listItemsReceived,
              backupList: listItemsReceived,
            });
          }}
        />
      </Container>,
      <SweetAlert
        key="sweet-alert"
        show={showDeletionConfirmation}
        title={T.translate('partnerDiscounts.list.deleteWarning.title', {
          itemToBeDeleted: selectedItemToBeDeleted
            ? selectedItemToBeDeleted.code
            : '',
        })}
        text={T.translate('partnerDiscounts.list.deleteWarning.message')}
        type="warning"
        showCancelButton
        confirmButtonText={T.translate(
          'partnerDiscounts.list.deleteWarning.confirmButton',
        )}
        cancelButtonText={T.translate(
          'partnerDiscounts.list.deleteWarning.cancelButton',
        )}
        confirmButtonClass="btn btn-primary btn-rounded mx-2 btn-lg px-5"
        cancelButtonClass="btn btn-secondary btn-rounded mx-2 btn-lg px-5"
        buttonsStyling={false}
        onConfirm={() => {
          this.delete(selectedItemToBeDeleted.id);
          this.setState({ showDeletionConfirmation: false });
        }}
      />,
    ];
  }
}
