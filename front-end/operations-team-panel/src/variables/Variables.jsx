const {
  REACT_APP_API_PROTOCOL,
  REACT_APP_API_HOSTNAME,
  REACT_APP_API_PORT,
} = process.env;

const apiBaseUrl = `${REACT_APP_API_PROTOCOL}://${REACT_APP_API_HOSTNAME}${
  REACT_APP_API_PORT ? `:${REACT_APP_API_PORT}` : ''
}/api`;

module.exports = {
  apiBaseUrl,
};
