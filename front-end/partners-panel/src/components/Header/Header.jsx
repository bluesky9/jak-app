import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import {
  Container, Collapse, Navbar, NavbarToggler, Nav,
} from 'reactstrap';
import { Link, withRouter } from 'react-router-dom';
import T from 'i18n-react';

import logo from '../../assets/img/logo.png';

class Header extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false,
    };
  }

  putActiveClass(route) {
    let isActive = '';
    const { location } = this.props;
    if (typeof route === 'string') {
      isActive = location.pathname.indexOf(route) > -1 ? 'active' : '';
    } else {
      route.forEach((r) => {
        if (isActive === '') {
          isActive = this.putActiveClass(r);
        }
      });
    }
    return isActive;
  }

  toggle() {
    const { isOpen } = this.state;
    this.setState({
      isOpen: !isOpen,
    });
  }

  render() {
    const { isOpen } = this.state;
    const { location } = this.props;
    return (
      <Navbar color="light" light expand="md" fixed="top">
        <Container>
          <Link to="/dashboard" className="navbar-brand">
            <img src={logo} alt="Jak Logistics" style={{ height: '1.5rem' }} />
          </Link>
          <NavbarToggler onClick={this.toggle} />
          <Collapse isOpen={isOpen} navbar>
            {// Not in dashboard page
            location.pathname.indexOf('/dashboard') === -1 && (
              <Nav className="mr-auto" navbar>
                <li className={`nav-item ${this.putActiveClass('/dashboard')}`}>
                  <Link to="/dashboard" className="nav-link">
                    {T.translate('menu.dashboard')}
                  </Link>
                </li>
                <li
                  className={`nav-item ${this.putActiveClass([
                    '/shipments/list',
                    '/shipments/update',
                    '/shipments/detail',
                  ])}`}
                >
                  <Link to="/shipments/list" className="nav-link">
                    {T.translate('menu.shipments')}
                  </Link>
                </li>
                <li
                  className={`nav-item ${this.putActiveClass(
                    '/shipments/upload',
                  )}`}
                >
                  <Link to="/shipments/upload" className="nav-link">
                    {T.translate('menu.bulkUpload')}
                  </Link>
                </li>
                <li className={`nav-item ${this.putActiveClass('/billing')}`}>
                  <Link to="/billing" className="nav-link">
                    {T.translate('menu.billing')}
                  </Link>
                </li>
                <li className={`nav-item ${this.putActiveClass('/webhook')}`}>
                  <Link to="/webhook" className="nav-link">
                    {T.translate('menu.webhook')}
                  </Link>
                </li>
              </Nav>
            )}
            {// Inside dashboard page
            location.pathname.indexOf('/dashboard') > -1 && (
              <Nav className="mr-auto" navbar>
                <li className={`nav-item ${this.putActiveClass('/webhook')}`}>
                  <Link to="/webhook" className="nav-link">
                    {T.translate('menu.webhook')}
                  </Link>
                </li>
              </Nav>
            )}
            <Nav className="ml-auto" navbar>
              <li className="nav-item">
                <Link to="/logout" className="nav-link">
                  {T.translate('menu.logout')}
                </Link>
              </li>
            </Nav>
          </Collapse>
        </Container>
      </Navbar>
    );
  }
}

Header.propTypes = {
  location: PropTypes.shape({
    pathname: PropTypes.string.isRequired,
  }).isRequired,
};

export default withRouter(Header);
