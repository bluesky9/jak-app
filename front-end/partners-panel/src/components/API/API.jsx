import axios from 'axios';
import createHistory from 'history/createBrowserHistory';
import { apiBaseUrl } from '../../variables/Variables';
import { AuthProvider } from '../Auth/AuthProvider';

const API = () => {
  const history = createHistory();
  const auth = new AuthProvider();
  const accessToken = auth.getAccessToken();

  const errorResponseHandler = (error) => {
    if (
      typeof error.config.errorHandle === 'undefined'
      || error.config.errorHandle === true
    ) {
      switch (error.response.status) {
        case 401:
          history.replace('/#/login');
          window.location.reload();
          break;
        default:
      }
    }

    return Promise.reject(error);
  };

  const axiosConf = {
    baseURL: apiBaseUrl,
  };

  if (accessToken) {
    axiosConf.headers = {
      Authorization: `${accessToken}`,
    };
  }

  const axiosInstance = axios.create(axiosConf);

  axiosInstance.interceptors.response.use(
    response => response,
    errorResponseHandler,
  );

  return axiosInstance;
};

export default API;
