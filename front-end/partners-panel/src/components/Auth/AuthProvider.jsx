import React, { Component } from 'react';
import PropTypes from 'prop-types';

const AuthContext = React.createContext();

class AuthProvider extends Component {
  constructor(props) {
    super(props);

    this.login = (accessToken, remember) => {
      const encoded = btoa(accessToken);
      if (remember) {
        localStorage.setItem('_jak_at', encoded);
      } else {
        sessionStorage.setItem('_jak_at', encoded);
      }
      this.setState({ isAuth: true });
    };

    this.logout = () => {
      localStorage.removeItem('_jak_at');
      sessionStorage.removeItem('_jak_at');
      localStorage.removeItem('partner_id');
      sessionStorage.removeItem('partner_id');
      this.setState({ isAuth: false });
    };

    this.getAccessToken = () => {
      const raw = localStorage.getItem('_jak_at') || sessionStorage.getItem('_jak_at');
      return raw ? atob(raw) : null;
    };

    this.state = {
      isAuth: !!this.getAccessToken(),
    };
  }

  render() {
    const { isAuth } = this.state;
    const { children } = this.props;
    return (
      <AuthContext.Provider
        value={{
          isAuth,
          login: this.login,
          logout: this.logout,
        }}
      >
        {children}
      </AuthContext.Provider>
    );
  }
}

AuthProvider.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};

AuthProvider.defaultProps = {
  children: null,
};

const AuthConsumer = AuthContext.Consumer;

const withAuth = (ChildComponent) => {
  const AuthComponent = props => (
    <AuthContext.Consumer>
      {value => <ChildComponent {...props} auth={value} />}
    </AuthContext.Consumer>
  );
  return AuthComponent;
};

const AuthProps = PropTypes.shape({
  login: PropTypes.func.isRequired,
  logout: PropTypes.func.isRequired,
  isAuth: PropTypes.bool.isRequired,
});

export {
  AuthProvider, AuthConsumer, withAuth, AuthProps,
};
export default AuthProvider;
