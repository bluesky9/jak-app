import PartnerLayout from '../layouts/Partner/PartnerLayout';
import partnerRoutes from './partner';
import CleanLayout from '../layouts/Clean/CleanLayout';
import cleanRoutes from './clean';

const indexRoutes = Array.prototype.concat(
  cleanRoutes.map(route => ({ layout: CleanLayout, ...route })),
  partnerRoutes.map(route => ({ layout: PartnerLayout, ...route })),
);

export default indexRoutes;
