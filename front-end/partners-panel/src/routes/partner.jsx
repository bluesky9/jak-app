import Dashboard from '../pages/Dashboard/Dashboard';
import Billing from '../pages/Billing/Billing';
import ShipmentList from '../pages/Shipment/ShipmentList';
import ShipmentForm from '../pages/Shipment/ShipmentForm';
import ShipmentDetails from '../pages/Shipment/ShipmentDetails';
import ShipmentBulkUpload from '../pages/Shipment/ShipmentBulkUpload';
import WebhookForm from '../pages/Webhook/WebhookForm';

/**
 * Define routes and sidebar links at the same time.
 * Note that only items with 'icon' property and
 * without 'redirect' property will be rendered on sidebar.
 * @type {Object[]}
 */
const partnerRoutes = [
  {
    path: '/dashboard',
    name: 'menu.dashboard',
    component: Dashboard,
    authRequired: true,
  },
  {
    path: '/billing',
    name: 'menu.billing',
    component: Billing,
    authRequired: true,
  },
  {
    path: '/shipments/list',
    name: 'menu.shipments',
    component: ShipmentList,
    authRequired: true,
  },
  {
    path: '/shipments/update/:id',
    name: 'menu.shipments.update',
    component: ShipmentForm,
    authRequired: true,
  },
  {
    path: '/shipments/details/:id',
    name: 'menu.shipments.details',
    component: ShipmentDetails,
    authRequired: true,
  },
  {
    path: '/shipments/upload',
    name: 'menu.shipments.bulkUpload',
    component: ShipmentBulkUpload,
    authRequired: true,
  },
  {
    path: '/webhook',
    name: 'menu.webhook',
    component: WebhookForm,
    authRequired: true,
  },
  {
    redirect: true,
    path: '/',
    to: '/dashboard',
    name: 'menu.dashboard.redirect',
  },
];

export default partnerRoutes;
