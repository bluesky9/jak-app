import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Container } from 'reactstrap';

import Header from '../../components/Header/Header';
import Footer from '../../components/Footer/Footer';

export default class PartnerLayout extends Component {
  componentDidUpdate(e) {
    if (e.history && e.history.action === 'PUSH') {
      document.documentElement.scrollTop = 0;
      document.scrollingElement.scrollTop = 0;
      this.mainPanel.scrollTop = 0;
    }
  }

  render() {
    const { children } = this.props;
    return [
      <Header key="partner-header" />,
      <Container className="partner-container" key="partner-container">
        <div style={{ minHeight: 'calc(100vh - 150px)' }} className="py-3">
          {children}
        </div>
        <Footer />
      </Container>,
    ];
  }
}

PartnerLayout.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};

PartnerLayout.defaultProps = {
  children: null,
};
