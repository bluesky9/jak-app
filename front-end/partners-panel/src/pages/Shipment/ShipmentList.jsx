import React, { Component } from 'react';
import { Container, Table } from 'reactstrap';
import { Link } from 'react-router-dom';
import T from 'i18n-react';
import API from '../../components/API/API';
import { Pagination } from '../../components/Pagination/Pagination';

const SHIPMENT_STATUS_WAITING_FOR_PICKUP = 1;

class Shipment extends Component {
  constructor(props) {
    super(props);

    this.API = new API();

    this.pagination = React.createRef();

    this.partner_id = localStorage.getItem('partner_id');

    this.state = {
      resourceNameOnApi: 'partners',
      shipments: [],
    };
  }

  render() {
    const { shipments, resourceNameOnApi } = this.state;
    return (
      <Container fluid>
        <h3 className="text-secondary font-weight-bold mt-3">
          {T.translate('shipments.list.title')}
        </h3>
        <h4 className="text-secondary font-weight-light mb-3">
          {T.translate('shipments.list.caption')}
        </h4>
        <Table responsive striped hover>
          <thead>
            <tr>
              <th>{T.translate('shipments.fields.trackingId')}</th>
              <th>{T.translate('shipments.fields.status')}</th>
              <th>{T.translate('shipments.fields.type')}</th>
              <th className="text-center">
                {T.translate('shipments.list.actions')}
              </th>
            </tr>
          </thead>
          <tbody>
            {shipments.map(shipment => (
              <tr key={shipment.id}>
                <td>{shipment.trackingId}</td>
                <td>
                  {T.translate(
                    `shipments.fields.statuses.${shipment.status.name}`,
                  )}
                </td>
                <td>
                  {T.translate(`shipments.fields.types.${shipment.type.name}`)}
                </td>
                <td className="text-center">
                  <div className="btn-group" role="group">
                    <Link
                      to={`/shipments/details/${shipment.id}`}
                      className="btn btn-primary btn-sm"
                      title={T.translate('shipments.list.viewTooltip')}
                    >
                      <i className="fas fa-eye" />
                    </Link>
                    <Link
                      to={`/shipments/update/${shipment.id}`}
                      className={`btn btn-primary btn-sm${
                        shipment.statusId !== SHIPMENT_STATUS_WAITING_FOR_PICKUP
                          ? ' disabled'
                          : ''
                      }`}
                      title={T.translate('shipments.list.editTooltip')}
                    >
                      <i className="fas fa-pencil-alt" />
                    </Link>
                  </div>
                </td>
              </tr>
            ))}
          </tbody>
        </Table>
        <Pagination
          ref={this.pagination}
          resourceNameOnApi={`${resourceNameOnApi}/${
            this.partner_id
          }/shipments`}
          filter={{ include: ['status', 'type'] }}
          onItemsReceived={(shipmentList) => {
            this.setState({ shipments: shipmentList });
          }}
        />
      </Container>
    );
  }
}

export default Shipment;
