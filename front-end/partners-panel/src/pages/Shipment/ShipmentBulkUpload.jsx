import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import {
  Container,
  Row,
  Col,
  Card,
  CardBody,
  CardFooter,
  Label,
  Input,
  Button,
} from 'reactstrap';
import T from 'i18n-react';

import API from '../../components/API/API';

export default class ShipmentBulkUpload extends Component {
  constructor(props) {
    super(props);

    this.API = new API();

    this.partner_id = parseInt(localStorage.getItem('partner_id'), 10);

    this.state = {
      redirectTo: '',
    };

    // TODO: Implement upload logic based on bulk upload API.
  }

  render() {
    const { redirectTo, originalTrackingId } = this.state;

    if (redirectTo) return <Redirect to={redirectTo} />;

    return (
      <Container fluid>
        <h3 className="text-secondary font-weight-bold mt-3">
          {T.translate('shipments.bulkUpload.title')}
        </h3>
        <h4 className="text-secondary font-weight-light mb-3">
          {T.translate('shipments.bulkUpload.caption', {
            trackingId: originalTrackingId,
          })}
        </h4>
        <Row>
          <Col md={{ size: 8, offset: 2 }} lg={{ size: 6, offset: 3 }}>
            <form className="text-center mt-3">
              <Card className="w-100">
                <Label className="custom-file-upload">
                  {/* TODO: Implement UI change after file selection */}
                  <Input type="file" name="file" />
                  <CardBody className="drag-body">
                    <h1
                      style={{ fontSize: '50px' }}
                      className="my-1 ml-2 text-primary"
                    >
                      <i className="fas fa-cloud-upload-alt" />
                    </h1>
                    <h5>{T.translate('shipments.bulkUpload.fileCaption')}</h5>
                  </CardBody>
                </Label>
                <CardFooter className="small">
                  {T.translate('shipments.bulkUpload.fileInfo')}
                  <br />
                  <a href="#templateDownload">
                    {T.translate('shipments.bulkUpload.templateDownload')}
                  </a>
                </CardFooter>
              </Card>
              <Button
                size="lg"
                color="primary"
                className="btn-rounded d-block d-md-inline-block px-5 mx-auto mt-3"
              >
                {T.translate('shipments.bulkUpload.uploadButton')}
              </Button>
            </form>
          </Col>
        </Row>
      </Container>
    );
  }
}
