import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link, Redirect } from 'react-router-dom';
import {
  Container,
  Row,
  Col,
  FormGroup,
  Label,
  Input,
  Button,
} from 'reactstrap';
import Select from 'react-select';
import moment from 'moment';
import T from 'i18n-react';

import API from '../../components/API/API';
import { LocationMap } from '../../components/LocationMap/LocationMap';

const SHIPMENT_STATUS_WAITING_FOR_PICKUP = 1;

export default class ShipmentForm extends Component {
  constructor(props) {
    super(props);

    this.API = new API();

    this.partner_id = parseInt(localStorage.getItem('partner_id'), 10);

    this.state = {
      initialLat: {
        recipientGeoPoint: 24.736817,
        senderGeoPoint: 24.736817,
      },
      initialLng: {
        recipientGeoPoint: 46.684993,
        senderGeoPoint: 46.684993,
      },
      redirectTo: '',
      resourceNameOnApi: 'shipments',
      resource: {
        trackingId: '',
        senderShipmentId: '',
        senderName: '',
        senderAddress: '',
        senderGeoPoint: {
          lat: '',
          lng: '',
        },
        senderPhone: '',
        recipientName: '',
        recipientAddress: '',
        recipientGeoPoint: {
          lat: '',
          lng: '',
        },
        recipientPhone: '',
        pickupDatetime: '',
        deliveryDatetime: '',
        numberOfPackages: '',
        cashToCollectOnDelivery: '',
        recipientAddressConfirmed: false,
        recipientAddressConfirmationDatetime: '',
      },
      hiddenPropertyNamesOnForm: [
        'id',
        'type',
        'status',
        'createdAt',
        'updatedAt',
        'route',
        'partnerId',
      ],
      selectedType: null,
      typeSelectOptions: [],
      selectedStatus: null,
      statusSelectOptions: [],
      selectedRoute: null,
      routeSelectOptions: [],
      originalTrackingId: '',
    };

    /**
     * Callback for when user input some data on form fields.
     * It saves the data in their component state.
     * @param event
     */
    this.handleInputChange = (event) => {
      const { target } = event;
      const { name, type } = target;
      let { value } = target;

      switch (type) {
        case 'number':
          value = parseFloat(target.value);
          break;
        case 'checkbox':
          value = target.checked;
          break;
        default:
          break;
      }

      const { resource } = this.state;
      this.setState({ resource: { ...resource, [name]: value } });
    };

    /**
     * Callback for when user submits the form.
     * It sends the data to database via API.
     * @param event
     */
    this.handleSubmit = (event) => {
      event.preventDefault();
      const { resource } = this.state;
      this.save(resource);
    };

    /**
     * Checks if there's an ID set (on URL). If so, updates the record. Otherwise creates one.
     * @param data
     */
    this.save = async (data) => {
      const { resourceNameOnApi, resource } = this.state;
      const response = await this.API.put(
        `/${resourceNameOnApi}/${resource.id}`,
        data,
      );
      this.setState({ redirectTo: `/shipments/details/${response.data.id}` });
    };

    /**
     * Loads in the form the data from resource to be updated.
     */
    this.loadResource = () => {
      const { match } = this.props;
      const { resourceNameOnApi } = this.state;
      if (match.params.id) {
        this.API.get(`/${resourceNameOnApi}/${match.params.id}`, {
          params: {
            filter: {
              include: ['type', 'status', 'route'],
            },
          },
        }).then((response) => {
          if (
            response.data.partnerId !== this.partner_id
            || response.data.statusId !== SHIPMENT_STATUS_WAITING_FOR_PICKUP
          ) {
            this.setState({
              redirectTo: '/shipments/list',
            });
            return;
          }
          const initialLat = {
            recipientGeoPoint: response.data.recipientGeoPoint.lat,
            senderGeoPoint: response.data.senderGeoPoint.lat,
          };
          const initialLng = {
            recipientGeoPoint: response.data.recipientGeoPoint.lng,
            senderGeoPoint: response.data.senderGeoPoint.lng,
          };

          this.setState(
            {
              resource: response.data,
              originalTrackingId: response.data.trackingId,
              initialLat,
              initialLng,
            },
            () => {
              const { resource } = this.state;
              this.setState({
                selectedStatus: this.buildStatusOptionFromTheResource(
                  resource.status,
                ),
                selectedType: this.buildTypeOptionFromTheResource(
                  resource.type,
                ),
              });
              if (resource.routeId !== null) {
                this.setState({
                  selectedRoute: this.buildRoutesOptionFromTheResource(
                    resource.route,
                  ),
                });
              }
            },
          );
        });
      }
    };

    /**
     * Returns the select option (used in react-select) component,
     * based on the resource retrieved from database.
     * @param status
     * @return {{value: *, label: string, data: *}}
     */
    this.buildStatusOptionFromTheResource = status => ({
      value: status.id,
      label: T.translate(`shipments.fields.statuses.${status.name}`),
      data: status,
    });

    /**
     * Loads from API all available status, to build up the select options in the form.
     */
    this.loadAvailableStatuses = () => {
      const statusOptions = [];
      this.API.get('/shipmentStatuses').then((response) => {
        response.data.forEach((item) => {
          const statusOption = this.buildStatusOptionFromTheResource(item);
          statusOptions.push(statusOption);
        });
        this.setState({
          statusSelectOptions: statusOptions,
        });
      });
    };

    /**
     * Callback function to when user selects some value on Status
     * form field. Saves status to this component state.
     * @param selectedStatus
     */
    this.handleChangeOnStatus = (selectedStatus) => {
      this.setState({ selectedStatus });
      const { resource } = this.state;
      resource.status = selectedStatus.data;
    };

    /**
     * Returns the select option (used in react-select) component,
     * based on the resource retrieved from database.
     * @param type
     * @return {{value: *, label: string, data: *}}
     */
    this.buildTypeOptionFromTheResource = type => ({
      value: type.id,
      label: T.translate(`shipments.fields.types.${type.name}`),
      data: type,
    });

    /**
     * Loads from API all available types, to build up the select options in the form.
     */
    this.loadAvailableTypes = () => {
      this.API.get('/shipmentTypes').then((response) => {
        const typeOptions = [];
        response.data.forEach((item) => {
          const typeOption = this.buildTypeOptionFromTheResource(item);
          typeOptions.push(typeOption);
        });
        this.setState({
          typeSelectOptions: typeOptions,
        });
      });
    };

    /**
     * Callback function to when user selects some value on Status
     * form field. Saves status to this component state.
     * @param selectedType
     */
    this.handleChangeOnType = (selectedType) => {
      this.setState({ selectedType });
      const { resource } = this.state;
      resource.status = selectedType.data;
    };

    /**
     * Returns the select option (used in react-select) component,
     * based on the resource retrieved from database.
     * @param route
     * @return {{value: *, label: string, data: *}}
     */
    this.buildRoutesOptionFromTheResource = route => ({
      value: route.id,
      label: T.translate('shipments.form.routeSelectLabel', {
        routeId: route.id,
        routeStartDatetime: moment(route.startDatetime).format(
          'dddd, MMMM Do YYYY, h:mm:ss a',
        ),
      }),
      data: route,
    });

    /**
     * Loads from API all available routes, to build up the select options in the form.
     */
    this.loadAvailableRoutes = () => {
      this.API.get('/routes').then((response) => {
        const routeOptions = [];
        response.data.forEach((item) => {
          const statusOption = this.buildRoutesOptionFromTheResource(item);
          routeOptions.push(statusOption);
        });
        this.setState({
          routeSelectOptions: routeOptions,
        });
      });
    };

    /**
     * Callback function to when user selects some value on Route
     * form field. Saves route to this component state.
     * @param selectedRoute
     */
    this.handleChangeOnRoute = (selectedRoute) => {
      this.setState({ selectedRoute });
      const { resource } = this.state;
      if (selectedRoute === null) {
        resource.route = null;
        resource.routeId = null;
      } else {
        resource.route = selectedRoute.data;
        resource.routeId = selectedRoute.data.id;
      }
    };

    /**
     * Callback function for when the user drags the pin on the map,
     * the new latitude and longitudes will be saved in state.
     * @param propertyName
     * @param event
     */
    this.processLocation = (propertyName, event) => {
      const { resource } = this.state;
      this.setState({
        resource: {
          ...resource,
          [propertyName]: {
            lat: event.latLng.lat().toFixed(6),
            lng: event.latLng.lng().toFixed(6),
          },
        },
      });
    };
  }

  componentDidMount() {
    this.loadResource();
    this.loadAvailableStatuses();
    this.loadAvailableRoutes();
    this.loadAvailableTypes();
  }

  render() {
    const {
      redirectTo,
      originalTrackingId,
      resource,
      hiddenPropertyNamesOnForm,
      initialLat,
      initialLng,
      selectedType,
      typeSelectOptions,
      selectedStatus,
      statusSelectOptions,
      selectedRoute,
      routeSelectOptions,
    } = this.state;

    if (redirectTo) return <Redirect to={redirectTo} />;

    return (
      <Container fluid>
        <h3 className="text-secondary font-weight-bold mt-3">
          {T.translate('shipments.form.title')}
        </h3>
        <h4 className="text-secondary font-weight-light mb-3">
          {T.translate('shipments.form.caption', {
            trackingId: originalTrackingId,
          })}
        </h4>
        <hr />
        <form onSubmit={event => this.handleSubmit(event)}>
          {Object.keys(resource).map((propertyName) => {
            if (hiddenPropertyNamesOnForm.includes(propertyName)) {
              return null;
            }
            if (
              [
                'numberOfPackages',
                'cashToCollectOnDelivery',
              ].includes(propertyName)
            ) {
              return (
                <FormGroup key={propertyName}>
                  <Label>
                    {T.translate(`shipments.fields.${propertyName}`)}
                  </Label>
                  <Input
                    type="number"
                    name={propertyName}
                    value={resource[propertyName]}
                    onChange={(event) => {
                      this.handleInputChange(event);
                    }}
                  />
                </FormGroup>
              );
            }
            if (propertyName === 'typeId') {
              return (
                <FormGroup key={propertyName}>
                  <Label>{T.translate('shipments.fields.type')}</Label>
                  <Select
                    name="form-field-name"
                    value={selectedType}
                    onChange={this.handleChangeOnType}
                    options={typeSelectOptions}
                    placeholder={T.translate('defaults.placeholder.select')}
                    isClearable={false}
                    className="react-select-container"
                    classNamePrefix="react-select"
                  />
                </FormGroup>
              );
            }
            if (propertyName === 'statusId') {
              return (
                <FormGroup key={propertyName}>
                  <Label>{T.translate('shipments.fields.status')}</Label>
                  <Select
                    name="form-field-name"
                    value={selectedStatus}
                    onChange={this.handleChangeOnStatus}
                    options={statusSelectOptions}
                    placeholder={T.translate('defaults.placeholder.select')}
                    isClearable={false}
                    className="react-select-container"
                    classNamePrefix="react-select"
                  />
                </FormGroup>
              );
            }
            if (propertyName === 'routeId') {
              return (
                <FormGroup key={propertyName}>
                  <Label>{T.translate('shipments.fields.route')}</Label>
                  <Select
                    name="form-field-name"
                    value={selectedRoute}
                    onChange={this.handleChangeOnRoute}
                    options={routeSelectOptions}
                    placeholder={T.translate('defaults.placeholder.select')}
                    className="react-select-container"
                    classNamePrefix="react-select"
                  />
                </FormGroup>
              );
            }
            if (
              ['recipientGeoPoint', 'senderGeoPoint'].includes(propertyName)
            ) {
              return (
                <div key={propertyName}>
                  <Row>
                    <Col md={6}>
                      <FormGroup>
                        <Label>
                          {T.translate(`shipments.fields.${propertyName}.lat`)}
                        </Label>
                        <Input
                          type="text"
                          name={propertyName}
                          value={resource[propertyName].lat || ''}
                          onChange={(event) => {
                            this.handleInputChange(event);
                          }}
                        />
                      </FormGroup>
                    </Col>
                    <Col md={6}>
                      <FormGroup>
                        <Label>
                          {T.translate(`shipments.fields.${propertyName}.lng`)}
                        </Label>
                        <Input
                          type="text"
                          name={propertyName}
                          value={resource[propertyName].lng || ''}
                          onChange={(event) => {
                            this.handleInputChange(event);
                          }}
                        />
                      </FormGroup>
                    </Col>
                  </Row>
                  <Row>
                    <Col>
                      <LocationMap
                        mapProps={{
                          defaultZoom: 16,
                          defaultCenter: {
                            lat: initialLat[propertyName],
                            lng: initialLng[propertyName],
                          },
                          defaultOptions: {
                            scrollwheel: false,
                            zoomControl: true,
                          },
                        }}
                        markerProps={{
                          position: {
                            lat: initialLat[propertyName],
                            lng: initialLng[propertyName],
                          },
                          draggable: true,
                          onDragEnd: (event) => {
                            this.processLocation(propertyName, event);
                          },
                        }}
                      />
                    </Col>
                  </Row>
                </div>
              );
            }
            if (propertyName === 'recipientAddressConfirmed') {
              return (
                <FormGroup key={propertyName}>
                  <Label>
                    {T.translate(`shipments.fields.${propertyName}`)}
                  </Label>
                  <Input
                    type="select"
                    name={propertyName}
                    value={resource[propertyName]}
                    placeholder={T.translate('defaults.placeholder.select')}
                    onChange={(event) => {
                      this.handleInputChange(event);
                    }}
                  >
                    <option value>{T.translate('defaults.yes')}</option>
                    <option value={false}>{T.translate('defaults.no')}</option>
                  </Input>
                </FormGroup>
              );
            }
            return (
              <FormGroup key={propertyName}>
                <Label>{T.translate(`shipments.fields.${propertyName}`)}</Label>
                <Input
                  type="text"
                  name={propertyName}
                  value={resource[propertyName] || ''}
                  onChange={(event) => {
                    this.handleInputChange(event);
                  }}
                />
              </FormGroup>
            );
          })}
          <hr />
          <div className="clearfix text-center">
            <Link
              to={`/shipments/details/${resource.id}`}
              className="btn btn-rounded btn-lg btn-outline-secondary float-md-left d-block d-md-inline-block px-5"
            >
              {T.translate('shipments.form.cancelButton')}
            </Link>
            <Button
              size="lg"
              color="primary"
              className="btn-rounded float-md-right d-block d-md-inline-block m-auto px-5"
            >
              {T.translate('shipments.form.updateButton')}
            </Button>
          </div>
        </form>
      </Container>
    );
  }
}

ShipmentForm.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string,
    }),
  }),
};

ShipmentForm.defaultProps = {
  match: {
    params: {
      id: '',
    },
  },
};
