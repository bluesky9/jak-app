import React, { Component } from 'react';
import { Container, Table } from 'reactstrap';
import { Link } from 'react-router-dom';
import Moment from 'react-moment';
import T from 'i18n-react';
import API from '../../components/API/API';
import { Pagination } from '../../components/Pagination/Pagination';

class Billing extends Component {
  constructor(props) {
    super(props);

    this.API = new API();

    this.pagination = React.createRef();

    this.partner_id = localStorage.getItem('partner_id');

    this.state = {
      resourceNameOnApi: 'partners',
      cashInstances: [],
    };
  }

  render() {
    const { cashInstances, resourceNameOnApi } = this.state;
    return (
      <Container fluid>
        <h3 className="text-secondary font-weight-bold mt-3">
          {T.translate('billing.title')}
        </h3>
        <h4 className="text-secondary font-weight-light mb-3">
          {T.translate('billing.caption')}
        </h4>
        <Table responsive striped hover>
          <thead>
            <tr>
              <th>{T.translate('billing.fields.direction')}</th>
              <th>{T.translate('billing.fields.amount')}</th>
              <th>{T.translate('billing.fields.type')}</th>
              <th>{T.translate('billing.fields.dueDatetime')}</th>
              <th>{T.translate('billing.fields.isSettled')}</th>
              <th>{T.translate('billing.fields.shipment')}</th>
            </tr>
          </thead>
          <tbody>
            {cashInstances.map(cashInstance => (
              <tr key={cashInstance.id}>
                <td>
                  {T.translate(
                    `billing.fields.directions.${cashInstance.direction.name}`,
                  )}
                </td>
                <td>{cashInstance.amount}</td>
                <td>
                  {T.translate(
                    `billing.fields.types.${cashInstance.type.name}`,
                  )}
                </td>
                <td>
                  <Moment date={cashInstance.dueDatetime} />
                </td>
                <td>
                  {cashInstance.isSettled
                    ? T.translate('defaults.yes')
                    : T.translate('defaults.no')}
                </td>
                <td>
                  <Link
                    to={`/shipments/details/${cashInstance.shipmentId}`}
                    title={T.translate('billing.viewShipmentTooltip', {
                      trackingId: cashInstance.shipment.trackingId,
                    })}
                  >
                    {cashInstance.shipment.trackingId}
                  </Link>
                </td>
              </tr>
            ))}
          </tbody>
        </Table>
        <Pagination
          ref={this.pagination}
          resourceNameOnApi={`${resourceNameOnApi}/${
            this.partner_id
          }/cashInstances`}
          filter={{ include: ['direction', 'type', 'shipment'] }}
          onItemsReceived={(cashInstanceList) => {
            this.setState({ cashInstances: cashInstanceList });
          }}
        />
      </Container>
    );
  }
}

export default Billing;
