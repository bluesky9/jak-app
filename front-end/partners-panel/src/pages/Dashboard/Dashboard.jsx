import React, { Component } from 'react';
import {
  Container,
  Row,
  Col,
  Card,
  CardBody,
  CardTitle,
  CardSubtitle,
} from 'reactstrap';
import { Link } from 'react-router-dom';
import T from 'i18n-react';
import API from '../../components/API/API';
import { MapWithMarkers } from '../../components/MapWithMarkers/MapWithMarkers';

const SHIPMENT_STATUS_ON_ROUTE = 3;
const SHIPMENT_STATUS_DELIVERED = 4;

class Dashboard extends Component {
  constructor(props) {
    super(props);

    this.API = new API();

    this.partner_id = localStorage.getItem('partner_id');

    this.state = {
      resourceNameOnApi: 'partners',
      resource: {},
      deliveredCount: 0,
      shipmentsOnRoute: [],
    };
  }

  componentDidMount() {
    const { resourceNameOnApi } = this.state;
    // Fetch Partner data
    this.API.get(`/${resourceNameOnApi}/${this.partner_id}`, {
      params: {
        filter: {
          counts: 'shipments',
          include: [],
        },
      },
    }).then((response) => {
      this.setState({
        resource: response.data,
      });
    });

    // Fetch delivered shipment count
    this.API.get(`/${resourceNameOnApi}/${this.partner_id}/shipments/count`, {
      params: {
        where: {
          statusId: SHIPMENT_STATUS_DELIVERED,
        },
      },
    }).then((response) => {
      this.setState({
        deliveredCount: response.data.count,
      });
    });

    // Fetch partner's shipments currently on route
    this.API.get(`/${resourceNameOnApi}/${this.partner_id}/shipments`, {
      params: {
        filter: {
          where: {
            statusId: SHIPMENT_STATUS_ON_ROUTE,
          },
        },
      },
    }).then((response) => {
      this.setState({
        shipmentsOnRoute: response.data,
      });
    });
  }

  render() {
    const { resource, deliveredCount, shipmentsOnRoute } = this.state;
    return (
      <Container fluid>
        {resource.id && (
          <div>
            <Row className="my-4">
              <Col lg={6} md={12}>
                <h3 className="text-secondary font-weight-bold mt-3">
                  {T.translate('dashboard.title', { name: resource.name })}
                </h3>
                <h4 className="text-secondary font-weight-light mb-3">
                  {T.translate('dashboard.caption')}
                </h4>
              </Col>
              <Col lg={6} md={12}>
                <Card>
                  <CardBody>
                    <Row>
                      <Col sm={6}>
                        <h1
                          style={{ fontSize: '45px' }}
                          className="float-left mr-3 mb-0"
                        >
                          <i className="text-muted fas fa-shipping-fast" />
                        </h1>
                        <CardTitle className="h6 font-weight-light text-muted">
                          {T.translate('dashboard.statistics.registered')}
                        </CardTitle>
                        <CardSubtitle className="h4">
                          {resource.shipmentsCount}
                        </CardSubtitle>
                      </Col>
                      <Col sm={6}>
                        <h1
                          style={{ fontSize: '45px' }}
                          className="float-left mr-3 mb-0"
                        >
                          <i className="text-muted fas fa-box" />
                        </h1>
                        <CardTitle className="h6 font-weight-light text-muted">
                          {T.translate('dashboard.statistics.delivered')}
                        </CardTitle>
                        <CardSubtitle className="h4">
                          {deliveredCount}
                        </CardSubtitle>
                      </Col>
                    </Row>
                  </CardBody>
                </Card>
              </Col>
            </Row>
            <hr />
            <Row>
              <Col lg={4}>
                <Link
                  to="/shipments/upload"
                  className="btn btn-block btn-primary btn-rounded my-3 clearfix d-flex align-items-center"
                >
                  <h1 style={{ fontSize: '30px' }} className="my-1 ml-2">
                    <i className="fas fa-cubes" />
                  </h1>
                  <span className="flex-grow-1 text-center">
                    {T.translate('dashboard.bulkUpload')}
                  </span>
                </Link>
              </Col>
              <Col lg={4}>
                <Link
                  to="/shipments/list"
                  className="btn btn-block btn-primary btn-rounded my-3 clearfix d-flex align-items-center"
                >
                  <h1 style={{ fontSize: '30px' }} className="my-1 ml-2">
                    <i className="fas fa-cube" />
                  </h1>
                  <span className="flex-grow-1 text-center">
                    {T.translate('dashboard.shipmentManagement')}
                  </span>
                </Link>
              </Col>
              <Col lg={4}>
                <Link
                  to="/billing"
                  className="btn btn-block btn-primary btn-rounded my-3 clearfix d-flex align-items-center"
                >
                  <h1 style={{ fontSize: '30px' }} className="my-1 ml-2">
                    <i className="fas fa-credit-card" />
                  </h1>
                  <span className="flex-grow-1 text-center">
                    {T.translate('dashboard.billing')}
                  </span>
                </Link>
              </Col>
            </Row>
            <hr />
          </div>
        )}
        {shipmentsOnRoute.length > 0 && (
          <div>
            <h4 className="text-muted text-center font-weight-light mb-3">
              {T.translate('dashboard.shipmentsOnRoute', {
                count: shipmentsOnRoute.length,
              })}
            </h4>
            <MapWithMarkers
              mapProps={{
                defaultCenter: {
                  lat: shipmentsOnRoute[0].recipientGeoPoint.lat,
                  lng: shipmentsOnRoute[0].recipientGeoPoint.lng,
                },
                defaultOptions: {
                  zoom: 15,
                  scrollwheel: false,
                  zoomControl: true,
                },
              }}
              autoCenter
              markers={shipmentsOnRoute.map(shipment => ({
                position: {
                  lat: shipment.recipientGeoPoint.lat,
                  lng: shipment.recipientGeoPoint.lng,
                },
              }))}
            />
          </div>
        )}
      </Container>
    );
  }
}

export default Dashboard;
