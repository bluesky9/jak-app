import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import {
  Container, FormGroup, Label, Input, Button, Alert,
} from 'reactstrap';
import T from 'i18n-react';

import API from '../../components/API/API';

export default class WebhookForm extends Component {
  constructor(props) {
    super(props);

    this.API = new API();

    this.partner_id = parseInt(localStorage.getItem('partner_id'), 10);

    this.state = {
      resourceNameOnApi: 'partners',
      resource: {
        webhookUrl: '',
      },
      success: false,
    };

    /**
     * Callback for when user input some data on form fields.
     * It saves the data in their component state.
     * @param event
     */
    this.handleInputChange = (event) => {
      const { target } = event;
      const { name, type } = target;
      let { value } = target;

      switch (type) {
        case 'number':
          value = parseFloat(target.value);
          break;
        case 'checkbox':
          value = target.checked;
          break;
        default:
          break;
      }

      const { resource } = this.state;
      this.setState({ resource: { ...resource, [name]: value } });
    };

    /**
     * Callback for when user submits the form.
     * It sends the data to database via API.
     * @param event
     */
    this.handleSubmit = (event) => {
      event.preventDefault();
      const { resource } = this.state;
      this.save(resource);
    };

    /**
     * Checks if there's an ID set (on URL). If so, updates the record. Otherwise creates one.
     * @param data
     */
    this.save = async (data) => {
      const { resourceNameOnApi } = this.state;
      await this.API.patch(`/${resourceNameOnApi}/${this.partner_id}`, data);
      this.setState({ success: true });
    };

    /**
     * Loads in the form the data from resource to be updated.
     */
    this.loadResource = () => {
      const { resourceNameOnApi } = this.state;
      this.API.get(`/${resourceNameOnApi}/${this.partner_id}`, {
        params: {
          filter: {
            fields: ['webhookUrl'],
          },
        },
      }).then((response) => {
        this.setState({
          resource: response.data,
        });
      });
    };
  }

  componentDidMount() {
    this.loadResource();
  }

  render() {
    const {
      resource,
      success, // hiddenPropertyNamesOnForm,
    } = this.state;

    return (
      <Container fluid>
        <h3 className="text-secondary font-weight-bold mt-3">
          {T.translate('webhook.form.title')}
        </h3>
        <h4 className="text-secondary font-weight-light mb-3">
          {T.translate('webhook.form.caption')}
        </h4>
        <hr />
        {success && (
          <Alert color="success">
            {T.translate('webhook.form.successMessage')}
          </Alert>
        )}
        <form onSubmit={event => this.handleSubmit(event)}>
          <FormGroup>
            <Label>{T.translate('webhook.fields.webhookUrl')}</Label>
            <Input
              type="text"
              name="webhookUrl"
              value={resource.webhookUrl || ''}
              onChange={(event) => {
                this.handleInputChange(event);
              }}
            />
          </FormGroup>
          <hr />
          <div className="clearfix text-center">
            <Link
              to="/dashboard"
              className="btn btn-rounded btn-lg btn-outline-secondary float-md-left d-block d-md-inline-block px-5"
            >
              {T.translate('webhook.form.cancelButton')}
            </Link>
            <Button
              size="lg"
              color="primary"
              className="btn-rounded float-md-right d-block d-md-inline-block m-auto px-5"
            >
              {T.translate('webhook.form.saveButton')}
            </Button>
          </div>
        </form>
      </Container>
    );
  }
}
