import T from 'i18n-react';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import {
  Button,
  Col,
  ControlLabel,
  FormControl,
  FormGroup,
  Grid,
  Row,
} from 'react-bootstrap';
import AddressSearchBox from '../../components/AddressSearchBox/AddressSearchBox';
import { Alert } from '../../components/Alert/Alert';
import API from '../../components/API/API';
import { Card } from '../../components/Card/Card';
import { LocationMap } from '../../components/LocationMap/LocationMap';

class ShipmentRecipientAddressMap extends Component {
  constructor(props) {
    super(props);

    this.state = {
      encodedOrderId: props.match.params.encodedOrderId,
      orderId: props.match.params.orderId,
      latitude: null,
      longitude: null,
      address: '',
      address2: '',
      geocoder: null,
      awaitingRequestResponse: false,
      initialLat: 24.736817,
      initialLng: 46.684993,
      currentStep: 1,
      errorMessage: null,
    };

    /**
     * Callback for when user submits the form.
     * @param event
     */
    this.onFormSubmit = async (event) => {
      event.preventDefault();

      const {
        encodedOrderId,
        initialLat,
        initialLng,
        address,
        address2,
      } = this.state;

      let { latitude, longitude } = this.state;

      if (!latitude) latitude = initialLat;

      if (!longitude) longitude = initialLng;

      const data = {
        encodedOrderId,
        latitude,
        longitude,
        address,
        address2,
      };

      this.setState({
        awaitingRequestResponse: true,
      });

      try {
        await API.post('/shipment-recipient-address', data);
        this.setState({
          awaitingRequestResponse: false,
          currentStep: 4,
        });
      } catch (error) {
        let errorMessage;
        if (error.request.status === 0) {
          errorMessage = T.translate('error.statusCode.message', {
            error: T.translate('error.statusCode.0'),
          });
        } else {
          switch (error.response.status) {
            case 422:
              errorMessage = T.translate('error.statusCode.message', {
                error: T.translate(`error.statusCode.${error.response.status}`),
              });
              break;
            default:
              errorMessage = T.translate('error.statusCode.message', {
                error: T.translate('error.statusCode.default'),
              });
              break;
          }
        }
        this.setState({
          awaitingRequestResponse: false,
          errorMessage,
        });
      }
    };

    /**
     * Initialize Google Geocoder component from the loaded
     * Google Maps library
     */
    this.initGeocoder = () => {
      if (typeof window === 'undefined') return;

      let { googleMaps } = this.props;

      if (!googleMaps) googleMaps = window.google && window.google.maps;

      if (!googleMaps) throw new Error('Google Maps API was not found in the page.');

      this.state.geocoder = new googleMaps.Geocoder();
    };

    /**
     * Process marker location change after the drag
     * @param event
     */
    this.processLocation = (event) => {
      const { address, geocoder } = this.state;

      this.setState({
        latitude: event.latLng.lat().toFixed(6),
        longitude: event.latLng.lng().toFixed(6),
        address: '',
      });

      if (!geocoder) this.initGeocoder();

      geocoder.geocode({ location: event.latLng }, (results, status) => {
        if (status === 'OK') {
          if (results[0]) {
            this.setState({
              address: results[0].formatted_address,
            });
            return;
          }
        }
        this.setState({
          address,
        });
      });
    };

    /**
     * Callback for when user input some data on form fields.
     * It saves the data in their component state.
     * @param event
     */
    this.handleInputChange = (event) => {
      const { name, value } = event.target;
      this.setState({ [name]: value });
    };
  }

  render() {
    let stepRender;
    const {
      currentStep,
      address,
      address2,
      initialLat,
      initialLng,
      errorMessage,
      awaitingRequestResponse,
      orderId,
    } = this.state;

    if (currentStep === 1) {
      stepRender = (
        <Card
          title={T.translate('locationPin.autocompleteTitle')}
          content={(
            <div>
              <AddressSearchBox
                placeholder={T.translate('locationPin.autocompletePlaceholder')}
                onLocationReceived={(places) => {
                  this.setState({
                    address: places[0].formatted_address,
                    latitude: places[0].geometry.location.lat(),
                    longitude: places[0].geometry.location.lng(),
                    initialLat: places[0].geometry.location.lat(),
                    initialLng: places[0].geometry.location.lng(),
                  });
                }}
              />
              <div className="text-center">
                <Button
                  bsStyle="primary"
                  disabled={address === ''}
                  onClick={() => {
                    this.setState({ currentStep: 2 });
                  }}
                >
                  {T.translate('locationPin.continueButton')}
                </Button>
              </div>
            </div>
)}
        />
      );
    } else if (currentStep === 2) {
      stepRender = (
        <Card
          title={T.translate('locationPin.confirmLocation')}
          content={(
            <div>
              <LocationMap
                autoLoad={false}
                mapProps={{
                  defaultZoom: 16,
                  defaultCenter: {
                    lat: initialLat,
                    lng: initialLng,
                  },
                  defaultOptions: {
                    scrollwheel: false,
                    zoomControl: true,
                  },
                }}
                markerProps={{
                  position: {
                    lat: initialLat,
                    lng: initialLng,
                  },
                  draggable: true,
                  onDragEnd: this.processLocation,
                }}
              />
              <Button
                bsStyle="default"
                className="pull-left btn-fill"
                onClick={() => {
                  this.setState({
                    currentStep: 1,
                    address: '',
                  });
                }}
              >
                {T.translate('locationPin.backButton')}
              </Button>
              <Button
                bsStyle="primary"
                className="pull-right"
                disabled={address === ''}
                onClick={() => {
                  this.setState({ currentStep: 3 });
                }}
              >
                {T.translate('locationPin.continueButton')}
              </Button>
              <div className="clearfix" />
            </div>
)}
        />
      );
    } else if (currentStep === 3) {
      stepRender = (
        <div>
          <Card
            title={T.translate('locationPin.confirmAddress')}
            content={(
              <div>
                <form
                  onSubmit={event => this.onFormSubmit(event)}
                  encType="multipart/form-data"
                >
                  <FormGroup>
                    <ControlLabel>
                      {T.translate('locationPin.address')}
                    </ControlLabel>
                    <FormControl
                      value={address}
                      name="address"
                      type="text"
                      onChange={this.handleInputChange}
                    />
                  </FormGroup>
                  <FormGroup>
                    <ControlLabel>
                      {T.translate('locationPin.address2')}
                    </ControlLabel>
                    <FormControl
                      value={address2}
                      name="address2"
                      type="text"
                      placeholder={T.translate('locationPin.optional')}
                      onChange={this.handleInputChange}
                    />
                  </FormGroup>
                  {errorMessage && <Alert type="danger" text={errorMessage} />}
                  <Button
                    bsStyle="default"
                    className="pull-left btn-fill"
                    disabled={awaitingRequestResponse || address === ''}
                    onClick={() => {
                      this.setState({
                        currentStep: 2,
                        errorMessage: null,
                      });
                    }}
                  >
                    {T.translate('locationPin.backButton')}
                  </Button>
                  <Button
                    bsStyle="primary"
                    className="pull-right"
                    type="submit"
                    disabled={awaitingRequestResponse || address === ''}
                  >
                    {awaitingRequestResponse
                      ? T.translate('locationPin.sendingButton')
                      : T.translate('locationPin.sendButton')}
                  </Button>
                  <div className="clearfix" />
                </form>
              </div>
)}
          />
        </div>
      );
    } else {
      stepRender = (
        <Card
          content={(
            <div className="text-center">
              <h1>
                <i className="pe-7s-check text-success" />
              </h1>
              <h4 className="title">
                {T.translate('locationPin.successMessageTitle')}
              </h4>
              <p className="category" style={{ marginBottom: '40px' }}>
                {T.translate('locationPin.successMessageText')}
              </p>
            </div>
)}
        />
      );
    }
    return (
      <div className="content">
        <Grid fluid>
          {currentStep === 1 && (
            <Row>
              <Col md={12} className="text-center">
                <h4 style={{ fontWeight: 300, marginTop: 0 }}>
                  {T.translate('locationPin.title', { orderId })}
                </h4>
                <p style={{ fontWeight: 400, marginBottom: '20px' }}>
                  {T.translate('locationPin.description')}
                </p>
              </Col>
            </Row>
          )}
          {currentStep < 4 && (
            <Row>
              <Col
                xs={6}
                xsOffset={3}
                sm={4}
                smOffset={4}
                md={2}
                mdOffset={5}
                style={{ marginBottom: '-15px' }}
              >
                <Card
                  content={(
                    <div style={{ textAlign: 'center' }}>
                      {T.translate('locationPin.stepTitle', {
                        currentStep,
                        totalSteps: 3,
                      })}
                    </div>
)}
                />
              </Col>
            </Row>
          )}
          <Row>
            <Col md={12}>{stepRender}</Col>
          </Row>
        </Grid>
      </div>
    );
  }
}

ShipmentRecipientAddressMap.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      encodedOrderId: PropTypes.string.isRequired,
      orderId: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
  googleMaps: PropTypes.string,
};

ShipmentRecipientAddressMap.defaultProps = {
  googleMaps: null,
};

export default ShipmentRecipientAddressMap;
