import React, { Component } from 'react';
import { withRouter, Router } from 'react-router-dom';
import {
  Grid,
  Row,
  Col,
  FormGroup,
  Button,
  ControlLabel,
  FormControl,
} from 'react-bootstrap';
import T from 'i18n-react';
import { Card } from '../../components/Card/Card';

class ShipmentRecipientAddressForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      encodedOrderId: '',
      orderId: '',
    };

    /**
     * Callback for when user submits the form.
     * @param event
     */
    this.onFormSubmit = async (event) => {
      event.preventDefault();
      const { encodedOrderId, orderId } = this.state;
      const { history } = this.props;
      if (encodedOrderId === null || orderId === null) return;
      history.push(`/pin-location-on-map/${encodedOrderId}/${orderId}`);
    };

    /**
     * Callback for when user input some data on form fields.
     * It saves the data in their component state.
     * @param event
     */
    this.handleInputChange = (event) => {
      const { target } = event;
      const { name } = target;
      const { value } = target;

      this.setState({ [name]: value });
    };
  }

  render() {
    const { orderId, encodedOrderId } = this.state;
    return (
      <div className="content">
        <Grid fluid>
          <Row>
            <Col md={12}>
              <Card
                title={T.translate('locationForm.title')}
                content={(
                  <div>
                    <p>{T.translate('locationForm.description')}</p>
                    <form onSubmit={event => this.onFormSubmit(event)}>
                      <FormGroup>
                        <ControlLabel>
                          {T.translate('locationForm.orderId')}
                        </ControlLabel>
                        <FormControl
                          value={orderId}
                          name="orderId"
                          type="text"
                          onChange={this.handleInputChange}
                        />
                      </FormGroup>
                      <FormGroup>
                        <ControlLabel>
                          {T.translate('locationForm.encodedOrderId')}
                        </ControlLabel>
                        <FormControl
                          value={encodedOrderId}
                          name="encodedOrderId"
                          type="text"
                          onChange={this.handleInputChange}
                        />
                      </FormGroup>
                      <div className="text-center">
                        <Button
                          bsStyle="primary"
                          type="submit"
                          disabled={orderId === '' || encodedOrderId === ''}
                        >
                          {T.translate('locationForm.continue')}
                        </Button>
                      </div>
                    </form>
                  </div>
)}
              />
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}

ShipmentRecipientAddressForm.propTypes = {
  history: Router.propTypes.history,
};

ShipmentRecipientAddressForm.defaultProps = {
  history: null,
};

export default withRouter(ShipmentRecipientAddressForm);
