import React, { Component } from 'react';
import PropTypes from 'prop-types';
import T from 'i18n-react';
import { Grid, Row, Col } from 'react-bootstrap';
import { Card } from '../../components/Card/Card';

export default class Error extends Component {
  constructor(props) {
    super(props);

    this.state = {
      errorCode: props.match.params.errorCode,
    };
  }

  render() {
    let errorTitle;
    let errorMessage;
    const { errorCode } = this.state;

    switch (errorCode) {
      case '404':
        errorTitle = T.translate('error.notFound.title');
        errorMessage = T.translate('error.notFound.message');
        break;

      default:
        errorTitle = T.translate('error.unknown.title');
        errorMessage = T.translate('error.unknown.message');
        break;
    }

    return (
      <div className="content">
        <Grid fluid>
          <Row>
            <Col md={12}>
              <Card
                className="text-center"
                title={errorTitle}
                content={(
                  <div>
                    <p>{errorMessage}</p>
                  </div>
)}
              />
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}

Error.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      errorCode: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};
