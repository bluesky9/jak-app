import React from 'react';
import ReactDOM from 'react-dom';
import { HashRouter, Route, Switch } from 'react-router-dom';
import T from 'i18n-react';
import DirectionProvider, {
  DIRECTIONS,
} from 'react-with-direction/dist/DirectionProvider';
import indexRoutes from './routes/index';
import i18nAr from './i18n/ar.json';
import i18nEn from './i18n/en.json';

let language = localStorage.getItem('language')
  || (navigator.languages && navigator.languages[0])
  || navigator.language
  || navigator.userLanguage;

localStorage.setItem('language', language);

if (language.length > 2) {
  language = language.substr(0, 2);
}

let textDirection;

switch (language) {
  case 'ar':
    T.setTexts(i18nAr);
    textDirection = DIRECTIONS.RTL;
    break;
  case 'en':
  default:
    T.setTexts(i18nEn);
    textDirection = DIRECTIONS.LTR;
    break;
}

const indexRoutesMapCallback = prop => (
  <Route to={prop.path} component={prop.component} key={prop.name} />
);

ReactDOM.render(
  <DirectionProvider direction={textDirection}>
    <HashRouter>
      <Switch>{indexRoutes.map(indexRoutesMapCallback)}</Switch>
    </HashRouter>
  </DirectionProvider>,
  document.getElementById('root'),
);
