import React from 'react';
import {
  FormGroup, ControlLabel, FormControl, Row,
} from 'react-bootstrap';

function FieldGroup({ label, ...props }) {
  return (
    <FormGroup>
      <ControlLabel>{label}</ControlLabel>
      <FormControl {...props} />
    </FormGroup>
  );
}

export const FormInputs = (props) => {
  const row = [];
  const { ncols, proprieties } = props;
  for (let i = 0; i < ncols.length; i += 1) {
    const fieldGroup = (
      <div key={i} className={ncols[i]}>
        <FieldGroup {...proprieties[i]} />
      </div>
    );
    row.push(fieldGroup);
  }
  return <Row>{row}</Row>;
};

export default FormInputs;
