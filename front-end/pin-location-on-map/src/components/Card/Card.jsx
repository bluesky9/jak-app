import React from 'react';
import PropTypes from 'prop-types';

export const Card = (props) => {
  const {
    plain,
    title,
    category,
    hCenter,
    ctAllIcons,
    ctTableFullWidth,
    ctTableResponsive,
    ctTableUpgrade,
    content,
    legend,
    stats,
    statsIcon,
  } = props;
  return (
    <div className={`card${plain ? ' card-plain' : ''}`}>
      {title || category ? (
        <div className={`header${hCenter ? ' text-center' : ''}`}>
          <h4 className="title">{title}</h4>
          <p className="category">{category}</p>
        </div>
      ) : (
        ''
      )}
      <div
        className={`content${ctAllIcons ? ' all-icons' : ''}${
          ctTableFullWidth ? ' table-full-width' : ''
        }${ctTableResponsive ? ' table-responsive' : ''}${
          ctTableUpgrade ? ' table-upgrade' : ''
        }`}
      >
        {content}

        <div className="footer">
          {legend}
          {stats != null ? <hr /> : ''}
          <div className="stats">
            <i className={statsIcon} />
            {' '}
            {stats}
          </div>
        </div>
      </div>
    </div>
  );
};

Card.propTypes = {
  plain: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  category: PropTypes.string.isRequired,
  hCenter: PropTypes.string.isRequired,
  ctAllIcons: PropTypes.string.isRequired,
  ctTableFullWidth: PropTypes.string.isRequired,
  ctTableResponsive: PropTypes.string.isRequired,
  ctTableUpgrade: PropTypes.string.isRequired,
  content: PropTypes.string.isRequired,
  legend: PropTypes.string.isRequired,
  stats: PropTypes.string.isRequired,
  statsIcon: PropTypes.string.isRequired,
};

export default Card;
