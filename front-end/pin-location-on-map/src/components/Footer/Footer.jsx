import React from 'react';
import { Grid, Button } from 'react-bootstrap';

const onClickLanguageButton = (event) => {
  localStorage.setItem('language', event.target.dataset.language);
  window.location.reload();
};

const languageButtons = [
  {
    label: 'ENGLISH',
    languageCode: 'en-US',
  },
  {
    label: 'ARABIC',
    languageCode: 'ar-SA',
  },
];

const Footer = () => (
  <footer className="footer">
    <Grid fluid>
      <p
        className="copyright pull-left"
        style={{ fontSize: '10px', height: '12px' }}
      >
        {languageButtons.map(button => (
          <Button
            key={button.languageCode}
            data-language={button.languageCode}
            onClick={onClickLanguageButton}
            style={{
              fontSize: '14px',
              marginTop: 0,
              marginBottom: 0,
              padding: 0,
              marginLeft: 5,
              marginRight: 5,
            }}
          >
            {button.label}
          </Button>
        ))}
      </p>
      <p className="copyright pull-right">
        &copy;
        {' '}
        {new Date().getFullYear()}
        {' '}
        <a href="https://jakapp.co">Jak Logistics</a>
      </p>
    </Grid>
  </footer>
);

export default Footer;
