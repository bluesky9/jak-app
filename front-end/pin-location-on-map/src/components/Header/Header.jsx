import React from 'react';
import { Navbar } from 'react-bootstrap';
import withDirection, {
  withDirectionPropTypes,
  DIRECTIONS,
} from 'react-with-direction';

import logo from '../../assets/img/jak-logo.png';

const Header = (props) => {
  const { direction } = props;
  const headerClass = direction === DIRECTIONS.RTL ? 'pull-right' : '';

  return (
    <Navbar fluid>
      <Navbar.Header className={headerClass}>
        <Navbar.Brand>
          <a href="https://jakapp.co">
            <img src={logo} alt="Jak Logistics" />
          </a>
        </Navbar.Brand>
      </Navbar.Header>
    </Navbar>
  );
};

Header.propTypes = {
  ...withDirectionPropTypes,
};

export default withDirection(Header);
