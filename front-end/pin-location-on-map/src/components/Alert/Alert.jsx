import React from 'react';
import PropTypes from 'prop-types';

export const Alert = (props) => {
  const { type, text } = props;
  return (
    <div className={`alert${type ? ` alert-${type}` : ''}`} role="alert">
      {text}
    </div>
  );
};

Alert.propTypes = {
  type: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
};

export default Alert;
