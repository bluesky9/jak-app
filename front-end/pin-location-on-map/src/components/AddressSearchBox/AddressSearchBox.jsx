import React from 'react';
import { compose, withProps, lifecycle } from 'recompose';
import { withScriptjs } from 'react-google-maps';
import StandaloneSearchBox from 'react-google-maps/lib/components/places/StandaloneSearchBox';

const apiKey = process.env.REACT_APP_GOOGLE_MAPS_API_KEY;

const AddressSearchBox = compose(
  withProps({
    googleMapURL: `https://maps.googleapis.com/maps/api/js${
      apiKey ? `?key=${apiKey}` : '?'
    }&v=3.exp&libraries=geometry,drawing,places`,
    loadingElement: <div />,
    containerElement: <div />,
  }),
  lifecycle({
    componentWillMount() {
      const refs = {};

      this.setState({
        places: [],
        onSearchBoxMounted: (ref) => {
          refs.searchBox = ref;
        },
        onPlacesChanged: () => {
          const places = refs.searchBox.getPlaces();
          this.setState({
            places,
          });
          this.props.onLocationReceived(places);
        },
      });
    },
  }),
  withScriptjs,
)(props => (
  <div style={{ marginBottom: '10px' }}>
    <StandaloneSearchBox
      ref={props.onSearchBoxMounted}
      bounds={props.bounds}
      onPlacesChanged={props.onPlacesChanged}
    >
      <input
        type="text"
        placeholder={props.placeholder}
        className="form-control"
      />
    </StandaloneSearchBox>
  </div>
));

export default AddressSearchBox;
