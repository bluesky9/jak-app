import axios from 'axios';
import { apiBaseUrl } from '../../variables/Variables';

export default axios.create({
  baseURL: apiBaseUrl,
});
