import ShipmentRecipientAddressForm from '../pages/Shipment/ShipmentRecipientAddressForm';
import ShipmentRecipientAddressMap from '../pages/Shipment/ShipmentRecipientAddressMap';
import Error from '../pages/Error/Error';

/**
 * Define routes and sidebar links at the same time.
 * Note that only items with 'icon' property and
 * without 'redirect' property will be rendered on sidebar.
 * @type {Object[]}
 */
const dashboardRoutes = [
  {
    path: '/error/:errorCode',
    name: 'Error Page',
    component: Error,
  },
  {
    path: '/pin-location-on-map/:encodedOrderId/:orderId',
    name: 'Shipment Recipient Address',
    component: ShipmentRecipientAddressMap,
  },
  {
    path: '/pin-location-on-map',
    name: 'Shipment Recipient Address',
    component: ShipmentRecipientAddressForm,
  },
  {
    path: '/:encodedOrderId/:orderId',
    name: 'Shipment Recipient Address',
    component: ShipmentRecipientAddressMap,
  },
  {
    redirect: true,
    path: '*',
    to: '/error/404',
    name: 'Page not Found',
  },
];

export default dashboardRoutes;
