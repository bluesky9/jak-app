import React, { Component } from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';

import Header from '../../components/Header/Header';
import Footer from '../../components/Footer/Footer';

import dashboardRoutes from '../../routes/dashboard';

class Dashboard extends Component {
  componentDidUpdate(e) {
    if (e.history.action === 'PUSH') {
      document.documentElement.scrollTop = 0;
      document.scrollingElement.scrollTop = 0;
      this.mainPanel.scrollTop = 0;
    }
  }

  render() {
    return (
      <div className="wrapper">
        <div
          id="main-panel"
          className="main-panel"
          ref={(component) => {
            this.mainPanel = component;
          }}
        >
          <Header {...this.props} />
          <Switch>
            {dashboardRoutes.map((prop) => {
              if (prop.redirect) {
                return (
                  <Redirect from={prop.path} to={prop.to} key={prop.path} />
                );
              }
              return (
                <Route
                  path={prop.path}
                  component={prop.component}
                  key={prop.path}
                />
              );
            })}
          </Switch>
          <Footer />
        </div>
      </div>
    );
  }
}

export default Dashboard;
