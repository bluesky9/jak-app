# Jak Logistics - Pin Location On Map

Welcome to the Pin Location On Map page.

This project is based on React and makes use of the [Light Bootstrap Dashboard](https://github.com/creativetimofficial/light-bootstrap-dashboard-react/) modified to look and feel like the Jak Logistics [website](https://jakapp.co).

## Quick Start

1. Run: `npm start`
2. Navigate to `http://localhost:3000/`
