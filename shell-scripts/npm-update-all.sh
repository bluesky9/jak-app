#!/bin/bash
SCRIPT_PATH=$(dirname "$(readlink -f "$0")")
cd "${SCRIPT_PATH}/../app/" && npm update && npm install
cd "${SCRIPT_PATH}/../back-end/" && npm update && npm install
cd "${SCRIPT_PATH}/../front-end/operations-team-panel/" && npm update && npm install
cd "${SCRIPT_PATH}/../front-end/partners-panel/" && npm update && npm install
cd "${SCRIPT_PATH}/../front-end/accountant-panel/" && npm update && npm install
cd "${SCRIPT_PATH}/../front-end/pin-location-on-map/" && npm update && npm install
cd "${SCRIPT_PATH}/../gitlab-webhook-server/" && npm update && npm install