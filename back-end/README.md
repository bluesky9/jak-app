# Jak Logistics API

The project is based on [LoopBack](http://loopback.io).

As database we're using PostgreSQL.

For the OS, we're using Ubuntu.

## Configuring Environments

### Development Environment

While in development, you should add a `.env` file to the root of the back-end folder with the following content:

```ini
NODE_ENV=development
DEBUG=app*,script*,loopback:connector:postgresql
GOOGLE_MAPS_API_KEY=
URL_SHORTENER_API_KEY=
```

### Staging Environment

While in staging, you should add a `.env` file to the root of the back-end folder with the following content:

```ini
NODE_ENV=staging
SSL_CERTIFICATE=/etc/letsencrypt/live/example.com/fullchain.pem
SSL_CERTIFICATE_KEY=/etc/letsencrypt/live/example.com/privkey.pem
UNIFONIC_APP_SID=
GOOGLE_MAPS_API_KEY=
URL_SHORTENER_API_KEY=
```

### Production Environment

While in production, you should add a `.env` file to the root of the back-end folder with the following content:

```ini
NODE_ENV=production
SSL_CERTIFICATE=/etc/letsencrypt/live/example.com/fullchain.pem
SSL_CERTIFICATE_KEY=/etc/letsencrypt/live/example.com/privkey.pem
UNIFONIC_APP_SID=
GOOGLE_MAPS_API_KEY=
URL_SHORTENER_API_KEY=
HASHIDS_SALT=
```

## Installing PostgreSQL on Ubuntu

Install PostgreSQL via command line:

```bash
sudo apt-get install postgresql postgresql-contrib
```

Check the location of PostgreSQL hba file with the command:

```bash
sudo -u postgres psql -c "show hba_file"
```

It should return a response like that:

```bash
hba_file
-------------------------------------
/etc/postgresql/10/main/pg_hba.conf
```

Now we know where its configs are located, lets edit it:

```bash
sudo nano /etc/postgresql/10/main/pg_hba.conf
```

Look for the line:

```bash
# "local" is for Unix domain socket connections only
local   all             all                                     peer
```

Where we have 'peer', we need to change to 'md5' so we'll be able to login with the user we created. Let's changes it then:

```bash
# "local" is for Unix domain socket connections only
local   all             all                                     md5
```

Save the file and restart PostgreSQL:

```bash
sudo service postgresql restart
```

Add the test user 'jak':

```bash
sudo -u postgres createuser jak
```

Alter user 'jak' password:

```bash
sudo -u postgres psql -c "ALTER USER jak WITH ENCRYPTED PASSWORD 'jak'"
```

Create the test DB 'jak':

```bash
sudo -u postgres createdb jak
```

Transfer the ownership of database to user 'jak':

```bash
sudo -u postgres psql -c "ALTER DATABASE jak OWNER TO jak"
```

Transfer the ownership of 'public' schema to user 'jak':

```bash
sudo -u postgres psql jak -c "ALTER SCHEMA public OWNER TO jak"
```

All done, now you've created a user 'jak' with password 'jak' allowed to access a database named 'jak'.

## Managing the database locally with PgAdmin

Install PgAdmin via command line:

```bash
sudo apt-get install pgadmin3
```

Open PgAdmin and connect to:

- Host: localhost
- Port: 5432
- MaintenanceDB: jak
- Username: jak
- Password: jak

## Useful NPM Scripts

- `npm run recreate-all-database-tables` will drop and create all tables again, based on the latest model definitions.
- `npm run insert-dummy-data-on-database` will add dummy data to database. You can use it as many times you want.

## Notes

Loopback automatically creates tables and columns names in lowercase. Also, note that PostgreSQL is case insensitive if referring to a column name without using double quotes on query (which is what Loopback does).
