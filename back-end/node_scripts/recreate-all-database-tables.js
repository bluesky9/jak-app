const path = require('path');
require('dotenv').config({ path: path.join(__dirname, '..', '.env') });

if (process.env.NODE_ENV === 'production') {
  throw new Error('This script is not supposed to run on production environment.');
}

const debug = require('debug')('script:recreate-all-database-tables');
const server = require('../server/server');
const DatabaseFiller = require('../server/classes/DatabaseFiller');

const numModels = Object.keys(server.models).length;
const dataSource = server.dataSources.db;

dataSource.setMaxListeners(numModels);

dataSource.automigrate(async (err) => {
  if (err) throw err;
  await DatabaseFiller.fillDefaultTables();
  debug('Database tables were recreated and filled with default data!');
});
