const path = require('path');
require('dotenv').config({ path: path.join(__dirname, '..', '.env') });
const debug = require('debug')('script:autoupdate-database');
const server = require('../server/server');
const DatabaseFiller = require('../server/classes/DatabaseFiller');

const dataSource = server.dataSources.db;

const modelsNamesInLowerCase = [];
const modelsLeftToCheck = [];

Object.keys(server.models).forEach((model) => {
  if (modelsNamesInLowerCase.indexOf(model.toLowerCase()) !== -1) return;
  modelsNamesInLowerCase.push(model.toLowerCase());
  modelsLeftToCheck.push(model);
});

const checkNextModel = () => {
  if (!modelsLeftToCheck.length) {
    DatabaseFiller.fillDefaultTables();
    return;
  }

  const model = modelsLeftToCheck.shift();

  dataSource.isActual(model, (err, actual) => {
    if (err) throw err;

    if (actual) {
      debug(`[✓] ${model}`);
      checkNextModel();
    } else {
      dataSource.autoupdate(model, async (error) => {
        if (error) throw error;
        debug(`[ ] ${model} - Updating... OK`);
        checkNextModel();
      });
    }
  });
};

debug('Checking if database tables are up to date...');

checkNextModel();
