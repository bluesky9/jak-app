const path = require('path');
require('dotenv').config({ path: path.join(__dirname, '..', '.env') });

const debug = require('debug')('script:assign-routes');
const RouteAssigner = require('../server/classes/RouteAssigner');

const assignRoutes = async () => {
  debug('Assigning routes...');
  await RouteAssigner.assignRoutes();
  debug('Routes assigned successfully!');
};

assignRoutes();
