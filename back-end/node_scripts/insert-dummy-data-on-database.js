const path = require('path');
require('dotenv').config({ path: path.join(__dirname, '..', '.env') });

if (process.env.NODE_ENV === 'production') {
  throw new Error('This script is not supposed to run on production environment.');
}

const debug = require('debug')('script:insert-dummy-data-on-database');
const DatabaseFiller = require('../server/classes/DatabaseFiller');

async function fillDatabase() {
  await DatabaseFiller.fillDefaultTables();
  await DatabaseFiller.createTestUsers();
  await DatabaseFiller.fillAreas(10);
  await DatabaseFiller.fillFleetOwners(10);
  await DatabaseFiller.fillAgentTags(15);
  await DatabaseFiller.fillAgents(20);
  await DatabaseFiller.fillPartners(25);
  await DatabaseFiller.fillShipments(50);
  await DatabaseFiller.fillPartnerCashInstances(200);
  await DatabaseFiller.fillPartnerDiscounts(50);
  await DatabaseFiller.fillAgentCashInstances(50);
  await DatabaseFiller.fillNotifications(50);
  debug('Dummy data successfully inserted on database.');
}

fillDatabase();
