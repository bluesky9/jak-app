const path = require('path');
require('dotenv').config({ path: path.join(__dirname, '..', '.env') });

const debug = require('debug')('script:create-routes');
const RouteCreator = require('../server/classes/RouteCreator');

const createRoutes = async () => {
  debug('Creating routes...');
  await RouteCreator.createAllRoutes();
  debug('Routes created successfully!');
};

createRoutes();
