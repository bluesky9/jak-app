module.exports = {
  /**
   * PM2 Application configuration section
   * http://pm2.keymetrics.io/docs/usage/application-declaration/
   */
  apps: [
    {
      name: 'jak-logistics-api',
      script: 'server/server.js',
      instances: 'max',
      exec_mode: 'cluster',
    },
  ],
};
