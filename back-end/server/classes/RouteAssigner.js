const loopback = require('loopback');
const get = require('get-value');
const { forEachSeries } = require('p-iteration');
const {
  AGENT_TYPE,
  AGENT_STATUS,
  ROUTE_STATUS,
} = require('common/constants/models/for-node');
const app = require('../server');

class RouteAssigner {
  /**
   * Assigns routes created to agents available.
   *
   * @returns {Array}
   */
  static async assignRoutes() {
    // Query all Routes with status "Created"
    const routesToAssign = await app.models.Route.fetchAllWithStatusCreated();

    // Loop through routes created and find the most suitable agent for each of them.
    // Update route and agent instances accordingly.
    return this.assignRoutesToAgents(routesToAssign);
  }

  /**
   * Takes an array of routes created and returns an array of routes assigned.
   *
   * @param {Array} routesToAssign
   * @returns {Array}
   */
  static async assignRoutesToAgents(routesToAssign) {
    const assignedRoutes = [];

    await forEachSeries(routesToAssign, async (routeToAssign) => {
      const route = routeToAssign;
      const agent = await this.selectAgent(route);

      if (agent) {
        // Set agent and status for the current route
        route.agent(agent);
        route.statusId = ROUTE_STATUS.ASSIGNED.id;
        await route.save(); // Save current route and push it to assignedRoutes array
        assignedRoutes.push(route);

        // Set status for the current agent
        agent.statusId = AGENT_STATUS.ON_ROUTE.id;
        await agent.save(); // Save current agent
      }
    });

    return assignedRoutes;
  }

  /**
   * Queries the list of all agents available, filters it by the current route's
   * required tags and then orders the resulting list by proximity to the route's
   * starting point.
   *
   * @param {Object} route
   * @returns {Object}
   */
  static async selectAgent(route) {
    // Query all agents with status "Available" and no route assigned to them.
    const agentsAvailable = await app.models.Agent.fetchAllWithStatusAvailable();

    // Filter agents according to the current route's requiredAgentTags
    let requiredAgentTags;
    route.requiredAgentTags((err, tags) => {
      if (err) throw new Error(err);
      requiredAgentTags = tags;
    });

    const agentsFiltered = this.filterAgentsByRequiredTags(
      agentsAvailable,
      requiredAgentTags,
    );

    // Reorder agents according to their proximity to the pickup point
    const agentsReordered = this.orderAgentsByCoordinates(
      agentsFiltered,
      route,
    );

    // Filter agents and follow approval process
    const agentsSelected = agentsReordered.filter(
      agent => agent.typeId === AGENT_TYPE.FULL_TIMER.id,
    );

    return agentsSelected && agentsSelected[0] ? agentsSelected[0] : null;
  }

  /**
   *
   * Gets array of agents available and orders/filters it by agents that best fit the required tags.
   *
   * @param {Array} agentsAvailable
   * @param {Array} requiredAgentTags
   * @returns {Array}
   *
   */
  static filterAgentsByRequiredTags(agentsAvailable, requiredAgentTags) {
    if (!requiredAgentTags || !requiredAgentTags.length) return agentsAvailable;

    const agentsWithTagCount = [];

    // Count tags that match route's required tags
    agentsAvailable.forEach((agent) => {
      let matchesCount = 0;
      let agentTags;
      agent.tags((err, tags) => {
        if (err) throw new Error(err);
        agentTags = tags;
      });

      for (let i = 0; i < agentTags.length; i += 1) {
        for (let j = 0; j < requiredAgentTags.length; j += 1) {
          if (requiredAgentTags[j].id === agentTags[i].id) {
            matchesCount += 1;
          }
        }
      }

      const newAgent = agent;
      newAgent.tagsCount = matchesCount;
      agentsWithTagCount.push(newAgent);
    });

    // Remove agents with 0 matches.
    const filteredAgents = agentsWithTagCount.filter(
      agent => agent.tagsCount > 0,
    );

    return filteredAgents;
  }

  /**
   * Gets array of agents and orders it by agents that are closest to the pickup point.
   *
   * @param {import('../models/agent').Agent[]} agents
   * @param {import('../models/route').Route} route
   * @returns {import('../models/agent').Agent[]}
   *
   */
  static orderAgentsByCoordinates(agents, route) {
    const agentsWithDistance = [];

    // default coords: 24.6037022,46.6622325 (Hotel in Riyadh)
    const defaultGeoPoint = new loopback.GeoPoint('24.6037022,46.6622325');

    // Calculate agent's distance to route's starting point
    agents.forEach((agent) => {
      const agentGeoPoint = new loopback.GeoPoint(agent.currentGeoPoint);

      let routeReferencePoint;
      let bundleType;
      route.bundle((err, bundle) => {
        if (err) throw new Error(err);
        bundleType = bundle.name;
      });

      let firstShipment;
      route.shipments((err, shipments) => {
        if (err) throw new Error(err);
        [firstShipment] = shipments;
      });

      if (bundleType === 'Bulk' || bundleType === 'Mix') {
        // Reference point will be the sender GeoPoint for the first shipment
        // If first shipment doesn't have senderGeoPoint set, use defaultGeoPoint instead
        routeReferencePoint = get(
          firstShipment,
          'senderGeoPoint',
          defaultGeoPoint,
        );
      } else if (bundleType === 'Generic') {
        // Reference point will be the recipient GeoPoint for the first shipment
        routeReferencePoint = get(
          firstShipment,
          'recipientGeoPoint',
          defaultGeoPoint,
        );
      }

      const distance = agentGeoPoint.distanceTo(routeReferencePoint, {
        type: 'kilometers',
      });
      const newAgent = agent;
      newAgent.distanceToPickUpPoint = distance;
      agentsWithDistance.push(newAgent);
    });

    // Reorder agents by proximity to route's reference point
    const agentsReordered = agentsWithDistance.sort((a, b) => {
      if (a.distanceToPickUpPoint < b.distanceToPickUpPoint) return -1;
      if (a.distanceToPickUpPoint > b.distanceToPickUpPoint) return 1;
      return 0;
    });

    return agentsReordered;
  }
}

module.exports = RouteAssigner;
