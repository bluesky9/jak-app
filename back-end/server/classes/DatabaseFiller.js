const debug = require('debug')('app:database-filler');
const faker = require('faker');
const {
  AGENT_CASH_TYPE,
  AGENT_STATUS,
  AGENT_TYPE,
  PARTNER_CASH_DIRECTION,
  PARTNER_CASH_TYPE,
  PARTNER_FEE_TYPE,
  ROLE,
  ROUTE_BUNDLE,
  ROUTE_STATUS,
  SETTING,
  SHIPMENT_STATUS,
  SHIPMENT_TYPE,
  WORKING_DAY,
  WORKING_HOUR,
} = require('common/constants/models/for-node');
const { GITLAB_USERNAMES } = require('common/constants/for-node');
const { forEachSeries } = require('p-iteration');
const app = require('../server');

class DatabaseFiller {
  /**
   * Returns a model of a specific type, defined by the model name.
   *
   * @param {string} model - Model name. Usually it's the name of the table.
   */
  static async getRandomRecordFromDatabase(model) {
    /** @type {typeof import('loopback').PersistedModel} */
    const appModel = app.models[model];
    const numberOfRecords = await appModel.count();
    const randomIndex = Math.floor(Math.random() * numberOfRecords);
    const randomRecordFromDatabase = await app.models[model].findOne({
      skip: randomIndex,
    });
    return randomRecordFromDatabase;
  }

  /**
   * Fill in an specific order the database with default data for tables, if they are empty.
   */
  static async fillDefaultTables() {
    await this.fillDefaultRoles();
    await this.fillDefaultAdminAccount();
    await this.fillDefaultWorkingDays();
    await this.fillDefaultWorkingHours();
    await this.fillDefaultAgentStatuses();
    await this.fillDefaultShipmentStatuses();
    await this.fillDefaultShipmentTypes();
    await this.fillDefaultPartnerFeeTypes();
    await this.fillDefaultPartnerCashDirections();
    await this.fillDefaultPartnerCashTypes();
    await this.fillDefaultAgentCashTypes();
    await this.fillDefaultAgentTypes();
    await this.fillDefaultRouteStatuses();
    await this.fillDefaultRouteBundles();
    await this.fillDefaultSettings();
  }

  static async createModelRecords(model, records) {
    await forEachSeries(Object.keys(records), async (key) => {
      const recordFound = await model.count({ id: records[key].id });
      if (!recordFound) await model.create(records[key]);
    });
  }

  static async fillDefaultWorkingDays() {
    return this.createModelRecords(app.models.WorkingDay, WORKING_DAY);
  }

  static async fillDefaultWorkingHours() {
    return this.createModelRecords(app.models.WorkingHour, WORKING_HOUR);
  }

  static async fillDefaultAgentStatuses() {
    return this.createModelRecords(app.models.AgentStatus, AGENT_STATUS);
  }

  static async fillDefaultShipmentStatuses() {
    return this.createModelRecords(app.models.ShipmentStatus, SHIPMENT_STATUS);
  }

  static async fillDefaultShipmentTypes() {
    return this.createModelRecords(app.models.ShipmentType, SHIPMENT_TYPE);
  }

  static async fillDefaultPartnerFeeTypes() {
    return this.createModelRecords(app.models.PartnerFeeType, PARTNER_FEE_TYPE);
  }

  static async fillDefaultPartnerCashDirections() {
    return this.createModelRecords(
      app.models.PartnerCashDirection,
      PARTNER_CASH_DIRECTION,
    );
  }

  static async fillDefaultPartnerCashTypes() {
    return this.createModelRecords(
      app.models.PartnerCashType,
      PARTNER_CASH_TYPE,
    );
  }

  static async fillDefaultAgentCashTypes() {
    return this.createModelRecords(app.models.AgentCashType, AGENT_CASH_TYPE);
  }

  static async fillDefaultAgentTypes() {
    return this.createModelRecords(app.models.AgentType, AGENT_TYPE);
  }

  static async fillDefaultRouteBundles() {
    return this.createModelRecords(app.models.RouteBundle, ROUTE_BUNDLE);
  }

  static async fillDefaultRouteStatuses() {
    return this.createModelRecords(app.models.RouteStatus, ROUTE_STATUS);
  }

  static async fillDefaultSettings() {
    return this.createModelRecords(app.models.Setting, SETTING);
  }

  static async fillDefaultRoles() {
    const model = app.models.Role;
    const records = ROLE;
    await forEachSeries(Object.keys(records), async (key) => {
      const recordFound = await model.count({ id: records[key].id });
      if (!recordFound) {
        const { name, description } = records[key];
        await model.create({ name, description });
      }
    });
  }

  static async fillDefaultAdminAccount() {
    const { User, Role, RoleMapping } = app.models;
    const hasUsersRegistered = await User.count();

    if (hasUsersRegistered) return;

    /** @type {User} */
    const adminUser = await User.create({
      username: 'admin',
      email: 'admin@jakapp.co',
      password: '123',
      emailVerified: 'true',
    });

    /** @type {Role} */
    const adminRole = await Role.findById(ROLE.ADMIN.id);

    await adminRole.principals.create({
      principalType: RoleMapping.USER,
      principalId: adminUser.id,
    });
  }

  /**
   * Adds a specific amount of fake areas to the database.
   *
   * @param {number} amount
   */
  static async fillAreas(amount = 0) {
    if (!amount) return;

    debug(`DatabaseFiller: Generating ${amount} Areas...`);

    const results = [];

    for (let index = 0; index < amount; index += 1) {
      results.push(
        app.models.Area.create({
          code: faker.random.alphaNumeric(5).toUpperCase(),
          name: faker.address.city(),
          country: faker.address.country(),
          city: faker.address.city(),
          centerGeoPoint: {
            lat: faker.random.number({
              min: 24.481452,
              max: 24.945524,
              precision: 0.000001,
            }),
            lng: faker.random.number({
              min: 46.723611,
              max: 46.811502,
              precision: 0.000001,
            }),
          },
          radius: faker.random.number({ min: 5, max: 30 }),
        }),
      );
    }

    await Promise.all(results);
  }

  /**
   * Adds a specific amount of fake agents to the database.
   *
   * @param {number} amount
   */
  static async fillAgents(amount = 0) {
    if (!amount) return;

    debug(`DatabaseFiller: Generating ${amount} Agents...`);

    const agentRole = await app.models.Role.findById(ROLE.AGENT.id);

    const createAgent = async () => {
      const randomAgentStatus = await this.getRandomRecordFromDatabase(
        'agentStatus',
      );
      const randomAgentType = await this.getRandomRecordFromDatabase(
        'agentType',
      );

      const agentData = {
        name: `${faker.name.firstName()} ${faker.name.lastName()}`,
        phone: faker.helpers.replaceSymbolWithNumber('+##-###########'),
        currentGeoPoint: {
          lat: faker.random.number({
            min: 24.481452,
            max: 24.945524,
            precision: 0.000001,
          }),
          lng: faker.random.number({
            min: 46.723611,
            max: 46.811502,
            precision: 0.000001,
          }),
        },
        agreedMinimumWorkingHours: faker.random.number({ min: 20, max: 50 }),
        agreedMinimumShipments: faker.random.number({ min: 30, max: 100 }),
        agreedMinimumPayment: faker.random.number({
          min: 750,
          max: 3000,
          precision: 0.01,
        }),
        agreedCommissionPerShipment: faker.random.number({
          min: 7,
          max: 30,
          precision: 0.01,
        }),
        statusId: randomAgentStatus.id,
        typeId: randomAgentType.id,
      };

      const shouldHaveFleetOwner = Math.random() >= 0.5; // Random chance to have fleet owner set.
      if (shouldHaveFleetOwner) {
        const randomFleetOwner = await this.getRandomRecordFromDatabase(
          'fleetOwner',
        );
        agentData.fleetOwnerId = randomFleetOwner.id;
      }

      const newAgent = await app.models.Agent.create(agentData);

      const gitlabUsernamesIndex = newAgent.id - 1; // To start from index 0.

      const newAgentUser = await newAgent.user.create({
        username: GITLAB_USERNAMES[gitlabUsernamesIndex]
          ? `${GITLAB_USERNAMES[gitlabUsernamesIndex]}.agent`
          : faker.internet.userName(),
        email: faker.internet.email(),
        password: '123',
        emailVerified: 'true',
      });

      await agentRole.principals.create({
        principalType: app.models.RoleMapping.USER,
        principalId: newAgentUser.id,
      });

      for (let i = 0; i < 3; i += 1) {
        this.getRandomRecordFromDatabase('area')
          .then(randomArea => newAgent.areas.add(randomArea))
          .catch(error => debug(error));

        this.getRandomRecordFromDatabase('workingDay')
          .then(randomWorkingDay => newAgent.workingDays.add(randomWorkingDay))
          .catch(error => debug(error));

        this.getRandomRecordFromDatabase('workingHour')
          .then(randomWorkingHour => newAgent.workingHours.add(randomWorkingHour))
          .catch(error => debug(error));

        this.getRandomRecordFromDatabase('agentTag')
          .then(randomAgentTag => newAgent.tags.add(randomAgentTag))
          .catch(error => debug(error));
      }
    };

    const results = [];

    for (let index = 0; index < amount; index += 1) {
      results.push(createAgent());
    }

    await Promise.all(results);
  }

  /**
   * Adds a specific amount of fake shipments to the database.
   *
   * @param {number} amount
   */
  static async fillShipments(amount = 0) {
    if (!amount) return;

    debug(`DatabaseFiller: Generating ${amount} Shipments...`);

    const createShipment = async () => {
      const randomShipmentType = await this.getRandomRecordFromDatabase(
        'shipmentType',
      );

      const randomSenderArea = await this.getRandomRecordFromDatabase('area');

      const randomRecipientArea = await this.getRandomRecordFromDatabase(
        'area',
      );

      let senderAddress;
      let senderGeoPoint;

      if (randomShipmentType.id === SHIPMENT_TYPE.NORMAL.id) {
        senderAddress = faker.address.streetAddress(true);
        senderGeoPoint = {
          lat: faker.random.number({
            min: 24.481452,
            max: 24.945524,
            precision: 0.000001,
          }),
          lng: faker.random.number({
            min: 46.723611,
            max: 46.811502,
            precision: 0.000001,
          }),
        };
      }

      let partnerId;
      const shouldBelongToAPartner = Math.random() >= 0.5;

      if (shouldBelongToAPartner) {
        const randomPartner = await this.getRandomRecordFromDatabase('partner');
        partnerId = randomPartner.id;
      }

      await app.models.Shipment.create({
        senderShipmentId: faker.random.alphaNumeric(7).toUpperCase(),
        senderName: `${faker.name.firstName()} ${faker.name.lastName()}`,
        senderAddress,
        senderGeoPoint,
        senderPhone: faker.helpers.replaceSymbolWithNumber('+##-###########'),
        recipientName: `${faker.name.firstName()} ${faker.name.lastName()}`,
        recipientAddress: faker.address.streetAddress(true),
        recipientGeoPoint: {
          lat: faker.random.number({
            min: 24.481452,
            max: 24.945524,
            precision: 0.000001,
          }),
          lng: faker.random.number({
            min: 46.723611,
            max: 46.811502,
            precision: 0.000001,
          }),
        },
        recipientPhone: faker.helpers.replaceSymbolWithNumber(
          '+##-###########',
        ),
        pickupDatetime: faker.date.recent(),
        deliveryDatetime: faker.date.recent(),
        numberOfPackages: faker.random.number({ min: 1, max: 10 }),
        cashToCollectOnDelivery: faker.random.number({ min: 10.5, max: 999.5 }),
        recipientAddressConfirmed: true,
        recipientAddressConfirmationDatetime: faker.date.recent(),
        description: faker.lorem.sentences(
          faker.random.number({ min: 1, max: 3 }),
        ),
        shouldNotBeAutoAssigned: faker.random.boolean(),
        statusId: SHIPMENT_STATUS.CREATED.id,
        senderAreaId: randomSenderArea.id,
        recipientAreaId: randomRecipientArea.id,
        typeId: randomShipmentType.id,
        partnerId,
      });
    };

    const results = [];

    for (let index = 0; index < amount; index += 1) {
      results.push(createShipment());
    }

    await Promise.all(results);
  }

  /**
   * Adds a specific amount of fake partners to the database.
   *
   * @param {number} amount
   */
  static async fillPartners(amount = 0) {
    if (!amount) return;

    debug(`DatabaseFiller: Generating ${amount} Partners...`);

    const partnerRole = await app.models.Role.findById(ROLE.PARTNER.id);

    const createPartner = async () => {
      const randomCodFeeType = await this.getRandomRecordFromDatabase(
        'partnerFeeType',
      );

      const newPartner = await app.models.Partner.create({
        name: faker.company.companyName(),
        website: faker.internet.url(),
        phone: faker.helpers.replaceSymbolWithNumber('+##-###########'),
        isActive: true,
        maxShipmentPartsAllowed: faker.random.number({ min: 5, max: 10 }),
        maxShipmentPartWeight: faker.random.number({
          min: 0,
          max: 20,
          precision: 0.1,
        }),
        codIsCollect: faker.random.boolean(),
        codFee: faker.random.number({ min: 1, max: 50, precision: 0.01 }),
        codFeeTypeId: randomCodFeeType.id,
        maxCodAmount: faker.random.number({ min: 1, max: 50, precision: 0.01 }),
        extraPartFee: faker.random.number({ min: 1, max: 50, precision: 0.01 }),
        extraWeightFee: faker.random.number({
          min: 1,
          max: 50,
          precision: 0.01,
        }),
        reschedulingFee: faker.random.number({
          min: 1,
          max: 50,
          precision: 0.01,
        }),
        returnsFee: faker.random.number({ min: 1, max: 50, precision: 0.01 }),
        readdressingFee: faker.random.number({
          min: 1,
          max: 50,
          precision: 0.01,
        }),
        shippingFee: faker.random.number({
          min: 1,
          max: 50,
          precision: 0.01,
        }),
      });

      const gitlabUsernamesIndex = newPartner.id - 1; // To start from index 0.

      const newPartnerUser = await newPartner.users.create({
        username: GITLAB_USERNAMES[gitlabUsernamesIndex]
          ? `${GITLAB_USERNAMES[gitlabUsernamesIndex]}.partner`
          : faker.internet.userName(),
        email: faker.internet.email(),
        password: '123',
        emailVerified: 'true',
      });

      await partnerRole.principals.create({
        principalType: app.models.RoleMapping.USER,
        principalId: newPartnerUser.id,
      });
    };

    const results = [];

    for (let index = 0; index < amount; index += 1) {
      results.push(createPartner());
    }

    await Promise.all(results);
  }

  /**
   * Adds a specific amount of fake partner cash instances to the database.
   *
   * @param {number} amount
   */
  static async fillPartnerCashInstances(amount = 0) {
    if (!amount) return;

    debug(`DatabaseFiller: Generating ${amount} Partner Cash Instances...`);

    const createPartnerCashInstance = async () => {
      const randomShipment = await this.getRandomRecordFromDatabase('agent');
      const randomPartnerCashDirection = await this.getRandomRecordFromDatabase(
        'partnerCashDirection',
      );
      const randomPartnerCashType = await this.getRandomRecordFromDatabase(
        'partnerCashType',
      );
      const randomPartner = await this.getRandomRecordFromDatabase('partner');
      const isSettled = faker.random.boolean();
      await app.models.PartnerCashInstance.create({
        amount: faker.random.number({ min: 100, max: 10000 }),
        dueDatetime: isSettled ? faker.date.past() : faker.date.recent(),
        isSettled,
        settledAt: isSettled ? faker.date.recent() : null,
        shipmentId: randomShipment.id,
        directionId: randomPartnerCashDirection.id,
        typeId: randomPartnerCashType.id,
        partnerId: randomPartner.id,
      });
    };

    const results = [];

    for (let index = 0; index < amount; index += 1) {
      results.push(createPartnerCashInstance());
    }

    await Promise.all(results);
  }

  /**
   * Adds a specific amount of fake partner discounts to the database.
   *
   * @param {number} amount
   */
  static async fillPartnerDiscounts(amount = 0) {
    if (!amount) return;

    debug(`DatabaseFiller: Generating ${amount} Partners Discounts...`);

    const createPartnerDiscount = async () => {
      const randomPartner = await this.getRandomRecordFromDatabase('partner');
      const randomFeeType = await this.getRandomRecordFromDatabase(
        'partnerFeeType',
      );
      await app.models.PartnerDiscount.create({
        code: faker.random.alphaNumeric(5).toUpperCase(),
        isActive: true,
        startDatetime: faker.date.recent(),
        endDatetime: faker.date.future(),
        count: faker.random.number(20),
        rate: faker.random.number({ min: 0, max: 1, precision: 0.01 }),
        partnerId: randomPartner.id,
        feeTypeId: randomFeeType.id,
      });
    };

    const results = [];

    for (let index = 0; index < amount; index += 1) {
      results.push(createPartnerDiscount());
    }

    await Promise.all(results);
  }

  /** Create a tests users for each all role, and all with password '123'. */
  static async createTestUsers() {
    const usersCount = await app.models.User.count();

    // We expect to have only the default admin user on the database.
    // If we have more users, it means we've already inserted the
    // test users, so we end this function here.
    if (usersCount !== 1) return;

    const { Role } = app.models;

    /** @type {Role} */
    const adminRole = await Role.findById(ROLE.ADMIN.id);

    /** @type {Role} */
    const operationsTeamMemberRole = await Role.findById(
      ROLE.OPERATIONS_TEAM_MEMBER.id,
    );

    /** @type {Role} */
    const accountantRole = await Role.findById(ROLE.ACCOUNTANT.id);

    await forEachSeries(GITLAB_USERNAMES, async (gitlabUsername) => {
      const adminUser = await app.models.User.create({
        username: `${gitlabUsername}.admin`,
        email: faker.internet.email(),
        password: '123',
        emailVerified: 'true',
      });

      await adminRole.principals.create({
        principalType: app.models.RoleMapping.USER,
        principalId: adminUser.id,
      });

      const operationsTeamMemberUser = await app.models.User.create({
        username: `${gitlabUsername}.operations`,
        email: faker.internet.email(),
        password: '123',
        emailVerified: 'true',
      });

      await operationsTeamMemberRole.principals.create({
        principalType: app.models.RoleMapping.USER,
        principalId: operationsTeamMemberUser.id,
      });

      const accountantUser = await app.models.User.create({
        username: `${gitlabUsername}.accountant`,
        email: faker.internet.email(),
        password: '123',
        emailVerified: 'true',
      });

      await accountantRole.principals.create({
        principalType: app.models.RoleMapping.USER,
        principalId: accountantUser.id,
      });
    });
  }

  /**
   * Adds a specific amount of fake agent cash instances to the database.
   *
   * @param {number} amount
   */
  static async fillAgentCashInstances(amount = 0) {
    if (!amount) return;

    debug(`DatabaseFiller: Generating ${amount} Agent Cash Instances...`);

    const createAgentCashInstance = async () => {
      const randomAgent = await this.getRandomRecordFromDatabase('agent');
      const randomAgentCashType = await this.getRandomRecordFromDatabase(
        'agentCashType',
      );
      const isSettled = faker.random.boolean();
      await app.models.AgentCashInstance.create({
        amount: faker.random.number({ min: 100, max: 10000 }),
        dueDatetime: isSettled ? faker.date.past() : faker.date.recent(),
        isSettled,
        settledAt: isSettled ? faker.date.recent() : null,
        agentId: randomAgent.id,
        typeId: randomAgentCashType.id,
      });
    };

    const results = [];

    for (let index = 0; index < amount; index += 1) {
      results.push(createAgentCashInstance());
    }

    await Promise.all(results);
  }

  /**
   * Adds a specific amount of fake fleet owners to the database.
   *
   * @param {number} amount
   */
  static async fillFleetOwners(amount = 0) {
    if (!amount) return;

    debug(`DatabaseFiller: Generating ${amount} Fleet Owners...`);

    const createFleetOwner = async () => {
      await app.models.FleetOwner.create({
        name: faker.company.companyName(),
        country: faker.address.country(),
        city: faker.address.city(),
      });
    };

    const results = [];

    for (let index = 0; index < amount; index += 1) {
      results.push(createFleetOwner());
    }

    await Promise.all(results);
  }

  /**
   * Adds a specific amount of fake agent tags to the database.
   *
   * @param {number} amount
   */
  static async fillAgentTags(amount = 0) {
    if (!amount) return;

    debug(`DatabaseFiller: Generating ${amount} Agent Tags...`);

    const createAgentTag = async () => {
      await app.models.AgentTag.create({
        name: faker.hacker.adjective(),
      });
    };

    const results = [];

    for (let index = 0; index < amount; index += 1) {
      results.push(createAgentTag());
    }

    await Promise.all(results);
  }

  /**
   * Adds a specific amount of fake notifications to the database.
   *
   * @param {number} amount
   */
  static async fillNotifications(amount) {
    if (!amount) return;

    const createNotifications = async () => {
      const randomAgent = await this.getRandomRecordFromDatabase('agent');
      await app.models.Notification.create({
        agentId: randomAgent.id,
        title: faker.lorem.words(faker.random.number({ min: 1, max: 3 })),
        message: faker.lorem.sentences(faker.random.number({ min: 1, max: 3 })),
        hasBeenRead: faker.random.boolean(),
      });
    };

    const results = [];

    for (let index = 0; index < amount; index += 1) {
      results.push(createNotifications());
    }

    await Promise.all(results);
  }
}

module.exports = DatabaseFiller;
