const axios = require('axios');
const querystring = require('querystring');

const api = axios.create({ baseURL: 'http://api.unifonic.com/rest' });

class UnifonicApi {
  /**
   * Sends a SMS to a phone number with an specific message.
   *
   * @param {string} phoneNumber - The recipient phone number. Format example: 962789309519.
   * @param {string} message - The message to be sent.
   * @returns {Object} An AxiosPromise.
   */
  static sendSms(phoneNumber, message) {
    if (
      process.env.NODE_ENV === 'development'
      || !process.env.UNIFONIC_APP_SID
    ) {
      const fakeApiResponse = Promise.resolve({
        data: `SMS that would be sent to ${phoneNumber}: ${message}`,
      });
      return fakeApiResponse;
    }

    const url = '/Messages/Send';

    const data = querystring.stringify({
      AppSid: process.env.UNIFONIC_APP_SID,
      Recipient: phoneNumber,
      Body: message,
    });

    const config = {
      headers: {
        'content-type': 'application/x-www-form-urlencoded',
      },
    };

    return api.post(url, data, config);
  }
}

module.exports = UnifonicApi;
