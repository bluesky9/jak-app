const HashIds = require('hashids');

/**
 * A salted HashIds instance. Use this throughout the
 * project to grant using the same salt everywhere.
 * @type {HashIds}
 */
const hashIds = new HashIds(process.env.HASHIDS_SALT || '');

class HashIdsWrapper {
  static encodeSingleId(id) {
    return hashIds.encode(id);
  }

  static decodeSingleHashId(hashId) {
    return hashIds.decode(hashId)[0];
  }
}

module.exports = HashIdsWrapper;
