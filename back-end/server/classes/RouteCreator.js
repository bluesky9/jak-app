const GoogleMaps = require('@google/maps');
const get = require('get-value');
const { forEach } = require('p-iteration');
const {
  ROUTE_BUNDLE,
  ROUTE_STATUS,
  SHIPMENT_TYPE,
} = require('common/constants/models/for-node');
const app = require('../server');

class RouteCreator {
  /**
   * Specialized function that will create routes for Generic, Mix and Bulk routes.
   *
   * @returns {Array} Array with the every created route id and their
   * corresponding Google API response.
   */
  static async createAllRoutes() {
    const createdRoutes = [];

    const routeBundles = await app.models.RouteBundle.find({
      where: {
        id: {
          inq: [
            ROUTE_BUNDLE.GENERIC.id,
            ROUTE_BUNDLE.BULK.id,
            ROUTE_BUNDLE.MIX.id,
          ],
        },
      },
    });

    const routeStatusCreated = await app.models.RouteStatus.findById(
      ROUTE_STATUS.CREATED.id,
    );

    // Query the shipments table getting all shipments with status 'created', with address filled
    // (even if not confirmed by the recipient)
    const [generic, normal] = await Promise.all([
      app.models.Shipment.fetchAllByTypeWithStatusCreated(
        SHIPMENT_TYPE.GENERIC.id,
      ),
      app.models.Shipment.fetchAllByTypeWithStatusCreated(
        SHIPMENT_TYPE.NORMAL.id,
      ),
    ]);

    const shipments = { generic, normal };

    // Note on bulk routes: At the time of creating the route we look at the pickup location
    // (warehouse), and bundle all those that share the pickup location.
    const warehouses = {};

    // Register the sender location for each shipment, so we can later identify warehouses
    shipments.normal.forEach((shipment) => {
      const { senderGeoPoint } = shipment.toJSON();
      const location = `${senderGeoPoint.lat}, ${senderGeoPoint.lng}`;

      if (warehouses[location]) {
        warehouses[location].push(shipment);
      } else {
        warehouses[location] = [shipment];
      }
    });

    // Step 2: Split the shipments based on their shipment type and their route type
    const shipmentsForRoute = {};
    routeBundles.forEach((bundle) => {
      shipmentsForRoute[bundle.name] = [[]];
    });

    await forEach(routeBundles, async (bundle) => {
      if (bundle.id === ROUTE_BUNDLE.GENERIC.id) {
        shipmentsForRoute.Generic = [shipments.generic];
      }

      // Treating each pickup location as a warehouse, we split the shipments for bulk/mix route
      // bundle types. Note that bulk shipments are grouped per warehouse.
      Object.keys(warehouses).forEach((location) => {
        const warehouse = warehouses[location];

        if (bundle.id === ROUTE_BUNDLE.BULK.id && warehouse.length > 1) {
          // Bulk shipments generate several different routes as we can have many warehouses.
          // Note that we add all shipments of a warehouse as a unique route
          shipmentsForRoute.Bulk.push(warehouse);
        } else if (
          bundle.id === ROUTE_BUNDLE.MIX.id
          && warehouse.length === 1
        ) {
          // Mix shipments can all be bundled together, without any ordering of shipments.
          // Note that we add each shipment of a warehouse to the shipmentsForRoute array.
          shipmentsForRoute.Mix[0].push(...warehouse);
        }
      });

      // Now that we have split the shipments into routes, lets cap the number of shipments of each
      // route due to Google Directions API limitations
      shipmentsForRoute[bundle.name] = RouteCreator.splitRouteShipments(
        shipmentsForRoute[bundle.name],
        bundle.name,
      );

      // Finally, create routes for each grouping of shipments
      await forEach(shipmentsForRoute[bundle.name], async (shipmentsArray) => {
        const route = await RouteCreator.createRouteForShipments(
          shipmentsArray,
          bundle,
          routeStatusCreated,
        );

        if (route) {
          createdRoutes.push(route);
        }
      });
    });

    return createdRoutes;
  }

  /**
   * Takes an array of shipments and returns a route for that particular group.
   *
   * @param {Array} shipments
   * @param {Object} bundle
   * @param {Object} status
   *
   * @returns {Object} Either the route or null, if it was not created.
   */
  static async createRouteForShipments(shipments, bundle, status) {
    // Fail-safe to avoid sending requests for Google Maps for empty routes.
    if (!shipments.length) return null;

    const bundleId = bundle.id;
    const statusId = status.id;

    // After shipments are bundled together the optimal route is generated using Google's Route
    // Optimization service.
    const googleRoute = await RouteCreator.buildRouteViaGoogle(
      shipments,
      bundle,
    );

    if (!googleRoute) return null;

    // If Google was able to order the shipments for us, then we can create a new route in our
    // database and assign it to each shipment

    await RouteCreator.orderShipmentsInRoute(googleRoute, shipments, bundle);

    // Create a Route, set its type to the current bundle (Generic/Mix/Bulk), set its status to
    // 'Created', and save it.
    const route = await app.models.Route.create({
      bundleId,
      statusId,
      isOffered: true,
    });

    // Then, for each shipment returned by the query, set its 'routeId' field to
    // the ID of the just created route.
    await forEach(shipments, async (shipment) => {
      const updatedShipment = shipment;
      updatedShipment.routeId = route.id;
      await updatedShipment.save();
    });

    // Returns a new object containing the googleRoute and the route object
    return { ...route, googleRoute };
  }

  static async updateOrderOfShipmentsInRoute(routeId) {
    const RouteModel = app.models.Route;

    /** @type {RouteModel} */
    const route = await RouteModel.findById(routeId, {
      include: ['shipments', 'bundle'],
    });

    const shipments = route.shipments();

    if (!shipments.length) return [];

    const bundle = route.bundle();

    const googleRoute = await RouteCreator.buildRouteViaGoogle(
      shipments,
      bundle,
    );

    if (!googleRoute) return shipments;

    return RouteCreator.orderShipmentsInRoute(googleRoute, shipments, bundle);
  }

  /**
   * Orders the shipment array based on a Google created route.
   *
   * @param {Object} googleResponse
   * @param {Array} shipments
   * @param {Object} routeBundle
   *
   * @returns {Array} Ordered shipments.
   */
  static async orderShipmentsInRoute(googleResponse, shipments, routeBundle) {
    // fail fast if the shipment array is empty
    if (!shipments.length) return [];

    // Checks if the response contains a route
    const waypointOrder = get(googleResponse, 'routes.0.waypoint_order');

    if (waypointOrder) {
      let tripCounter = 1; // always starts on 1.
      const deliveryScheduledBeforePickUp = [];

      // new index refers to the order in which this shipment is expected to be picked up
      // or delivered. Note that we must attend to the fact that Google does not know if a
      // waypoint is a pick up or a drop off location.
      await forEach(waypointOrder, async (position, newIndex) => {
        // Special case for mix routes
        if (routeBundle.id === ROUTE_BUNDLE.MIX.id) {
          const index = Math.floor(position / 2);
          const shipment = shipments[index];

          // as we always order the waypoints in ...|senderAddress|recipientAddress|..., we
          // confidently know that the sender location is in an even position and the recipient
          // location is in an odd location.
          const isDropOff = position % 2 !== 0;

          // In the case that a drop off gets scheduled ahead of a pick up for a shipment, we
          // save that position to add it to the end of current viable route.
          if (isDropOff && !shipment.routePickupOrder) {
            deliveryScheduledBeforePickUp.push(shipment);
          } else {
            const deliveryType = !shipment.routePickupOrder
              ? 'routePickupOrder'
              : 'routeDeliveryOrder';
            shipment[deliveryType] = tripCounter;
            tripCounter += 1;
          }

          await shipment.save();
        } else {
          const shipment = shipments[position];
          shipment.routeDeliveryOrder = newIndex + 1;
          await shipment.save();
        }
      });

      // Now we return to the skipped visits and add them at the end of the current viable route. We
      // traverse the array from the back. The understanding here is that the last skipped shipment
      // is probably closer to the end of the viable route.
      await forEach(deliveryScheduledBeforePickUp.reverse(), async (shipment) => {
        const updatedShipment = shipment;
        updatedShipment.routeDeliveryOrder = tripCounter;
        tripCounter += 1;
        await updatedShipment.save();
      });
    }

    return shipments;
  }

  /**
   * Create buckets of shipments with a limited number of shipments per route in order to conform
   * with Google Directions API's limitation of waypoints. The current limitation is of 25
   * waypoints, which limits our 'buckets' of shipments to 23 shipments in Generic and Bulk routes
   * and 11 shipments in Mix routes.
   *
   * @param {Array} allRouteShipments
   * @param {string} bundleName
   *
   * @returns {Array} Buckets of shipments.
   */
  static splitRouteShipments(allRouteShipments, bundleName) {
    const maxBucketsLength = {
      Generic: 23,
      Bulk: 23,
      Mix: 11,
    };

    const bucketLength = maxBucketsLength[bundleName];

    // Shield from unknown bundle
    if (!bucketLength) return null;

    const splitShipments = [];

    allRouteShipments.forEach((routeShipments) => {
      let count = 0;
      while (count < routeShipments.length) {
        splitShipments.push(routeShipments.slice(count, count + bucketLength));
        count += bucketLength;
      }
    });

    return splitShipments;
  }

  /**
   * Use Google’s Route Optimization API, to order the shipment waypoints from
   * the first to the last, according to what it suggests the agent should follow.
   *
   * @param {Object[]} shipments
   * @param {Object} bundle
   *
   * @returns {Object} The response from the Google API.
   */
  static async buildRouteViaGoogle(shipments, bundle) {
    // We will serve the google api call with waypoints.
    const waypoints = ['optimize:true'];
    let origin;
    let destination;

    // Add the pickup and the drop off locations. Generic routes do not have a pickup location.
    // This is covered by checking the senderGeoPoint variable.
    shipments.forEach((shipment) => {
      const { senderGeoPoint, recipientGeoPoint } = shipment;

      if (senderGeoPoint) {
        const geoPoint = `${senderGeoPoint.lat},${senderGeoPoint.lng}`;

        if (bundle.id === ROUTE_BUNDLE.BULK.id) {
          origin = geoPoint;
          destination = geoPoint;
        } else {
          waypoints.push(`${senderGeoPoint.lat},${senderGeoPoint.lng}`);
        }
      }

      waypoints.push(`${recipientGeoPoint.lat},${recipientGeoPoint.lng}`);
    });

    const response = await this.createGoogleRoute(
      origin,
      destination,
      waypoints.join('|'),
    );

    if (get(response, 'json.status') === 'OK') {
      return get(response, 'json', null);
    }

    return null;
  }

  /**
   * Calls the Google Directions API to create a route for our selected waypoints.
   *
   * @param {Array} waypoints
   * @param {String} origin
   * @param {String} destination
   *
   * @return {Object} Google API response object
   */
  // default coords: 24.6037022,46.6622325 (Hotel in Riyadh)
  // TODO: Remove default cords or replace them with Jak's latlong.
  static async createGoogleRoute(
    origin = '24.6037022,46.6622325',
    destination = '24.6037022,46.6622325',
    waypoints,
  ) {
    const googleMapsClient = GoogleMaps.createClient({
      key: process.env.GOOGLE_MAPS_API_KEY,
      Promise,
    });

    return googleMapsClient
      .directions({ origin, destination, waypoints })
      .asPromise();
  }
}

module.exports = RouteCreator;
