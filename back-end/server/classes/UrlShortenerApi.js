const axios = require('axios');
const querystring = require('querystring');

class UrlShortenerApi {
  /**
   * Get the short URL given an URL.
   *
   * @param {string} url - The url to be shortened.
   * @returns {import('axios').AxiosPromise} An AxiosPromise.
   */
  static getShortUrl(url) {
    if (
      process.env.NODE_ENV === 'development'
      || !process.env.URL_SHORTENER_API_KEY
    ) {
      const randomString = Math.random()
        .toString(36)
        .substr(2, 5);
      const fakeApiResponse = Promise.resolve({
        data: {
          shorturl: `http://jak.fyi/${randomString}`,
        },
      });
      return fakeApiResponse;
    }

    const data = querystring.stringify({
      signature: process.env.URL_SHORTENER_API_KEY,
      action: 'shorturl',
      format: 'json',
      url,
    });

    const config = {
      headers: {
        'content-type': 'application/x-www-form-urlencoded',
      },
    };

    return axios.post('http://jak.fyi/yourls-api.php', data, config);
  }
}

module.exports = UrlShortenerApi;
