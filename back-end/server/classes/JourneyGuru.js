const debug = require('debug')('app:journey-guru');
const {
  ROUTE_BUNDLE,
  ROUTE_STATUS,
} = require('common/constants/models/for-node');
const { JOURNEY_GURU } = require('common/constants/for-node');
const STATUS = require('http-status');
const app = require('../server');

/**
 * The journey guru is the main logic loop for agents. It is the brains behind the delivery process.
 * It handles the steps so that the agent doesn’t need to think, and it handles exceptions and
 * issues that might occur during a route.
 */
class JourneyGuru {
  /** @param {number} id - The ID of the Route. */
  static async getInstructionsForRoute(id) {
    const route = await app.models.Route.findById(id);

    if (!route) {
      const error = new Error();
      error.status = STATUS.NOT_FOUND;
      error.name = 'ROUTE_NOT_FOUND';
      error.message = 'No route found with this ID.';
      throw error;
    }

    if (!route.startDatetime) {
      debug('Setting startDateTime for the current route.');
      route.startDatetime = new Date();
      await route.save();
    }

    debug('Selecting the method to use based on the route type.');

    let instructions;

    switch (route.bundleId) {
      case ROUTE_BUNDLE.GENERIC.id:
        debug('Selected instructions for generic-route.');
        instructions = await this.getInstructionsForGenericRoute(route);
        break;

      case ROUTE_BUNDLE.BULK.id:
        debug('Selected instructions for bulk-route.');
        instructions = await this.getInstructionsForBulkRoute(route);
        break;

      case ROUTE_BUNDLE.MIX.id:
        debug('Selected instructions for mix-route.');
        instructions = await this.getInstructionsForMixRoute(route);
        break;

      case ROUTE_BUNDLE.DIRECTED_BULK.id:
        debug('Selected instructions for directed-bulk-route.');
        instructions = await this.getInstructionsForDirectedBulkRoute(route);
        break;

      default:
        throw new Error('[Journey Guru] Error: Unknown Bundle ID');
    }

    return instructions;
  }

  /**
   * In a typical journey of a generic-route, the agent is instructed to go to the first drop off
   * location (since there is no need for a pickup).
   *
   * - At the drop off location the agent is instructed to contact the recipient.
   * - Once the recipient is contacted, the agent is instructed to confirm the delivery through
   *   the OTP number that the recipient received earlier. When the number is confirmed, the
   *   delivery is complete.
   *
   * - The agent is instructed to repeat the same steps again for the next shipment on route,
   *   until all shipments are done.
   *
   * @param {import('../models/route').Route} route
   */
  static async getInstructionsForGenericRoute(route) {
    debug(`Getting instructions for generic-route #${route.id}...`);

    let id = 0;

    debug(
      `Looking for the next shipment available for delivery on route #${
        route.id
      }...`,
    );

    const shipment = await app.models.Shipment.findNextShipmentForDeliveryInRoute(
      route.id,
    );

    const routeHasBeenCompleted = route.statusId === ROUTE_STATUS.COMPLETED.id;

    debug('Preparing instructions to be sent...');

    if (!shipment || routeHasBeenCompleted) {
      debug('Found out that all shipments from route have been delivered...');
      id = JOURNEY_GURU.GENERIC_ROUTE.COMPLETED;
    } else if (!shipment.recipientWasContacted) {
      debug(
        'Found out that the recipient of current shipment has not been contacted yet...',
      );
      id = JOURNEY_GURU.GENERIC_ROUTE.CONTACT_RECIPIENT;
    } else if (!shipment.otpNumberWasConfirmed) {
      debug(
        "Found out that the current shipment didn't have the OTP Number confirmed by the recipient yet...",
      );
      id = JOURNEY_GURU.GENERIC_ROUTE.CONFIRM_OTP_NUMBER;
    }

    debug('Formating the instructions as JSON...');

    return this.formatInstructions(id, shipment);
  }

  /**
   * In a typical journey of a bulk-route, the agent is instructed to go to the warehouse location.
   *
   * - The agent is instructed to pickup and scan all shipments in the route.
   *
   * - Once all shipments are scanned, the agent is instructed to navigate to
   *   the first drop off location.
   *
   * - At the drop off location the agent is instructed to contact the recipient.
   *
   * - Once the recipient is contacted, the agent is instructed to scan the shipment for
   *   confirmation of delivery attempt.
   *
   * - Once the shipment is scanned, the agent is instructed to confirm the delivery through
   *   the OTP number that the recipient received earlier. When the number is confirmed,
   *   the delivery is complete.
   *
   * - The agent is instructed to navigate to the next drop-off location and repeat the same
   *   steps again, for the next shipment on route, until all shipments are done.
   *
   * @param {import('../models/route').Route} route
   */
  static async getInstructionsForBulkRoute(route) {
    debug(`Getting instructions for bulk-route #${route.id}...`);

    let id = 0;

    debug(
      'Checking if the agent has already scanned all shipments in the route...',
    );

    const numberOfShipmentsNotScannedInRoute = await app.models.Shipment.count({
      where: {
        routeId: route.id,
        wasScannedBeforePickup: false,
      },
    });

    debug(
      `Looking for the next shipment available for delivery on route #${
        route.id
      }...`,
    );

    const shipment = await app.models.Shipment.findNextShipmentForDeliveryInRoute(
      route.id,
    );

    const routeHasBeenCompleted = route.statusId === ROUTE_STATUS.COMPLETED.id;

    debug('Preparing instructions to be sent...');

    if (!shipment || routeHasBeenCompleted) {
      debug('Found out that all shipments in route have been delivered...');
      id = JOURNEY_GURU.BULK_ROUTE.COMPLETED;
    } else if (numberOfShipmentsNotScannedInRoute !== 0) {
      debug('Found out that not all shipments in route have been scanned...');
      id = JOURNEY_GURU.BULK_ROUTE.SCAN_ALL_SHIPMENTS_TO_CONFIRM_PICKUP;
    } else if (!shipment.recipientWasContacted) {
      debug(
        'Found out that the recipient of current shipment has not been contacted yet...',
      );
      id = JOURNEY_GURU.BULK_ROUTE.CONTACT_RECIPIENT;
    } else if (!shipment.wasScannedAfterContactingRecipient) {
      debug(
        'Found out that the shipment has not been scanned after contacting recipient...',
      );
      id = JOURNEY_GURU.BULK_ROUTE.SCAN_SHIPMENT_TO_CONFIRM_DELIVERY;
    } else if (!shipment.otpNumberWasConfirmed) {
      debug(
        "Found out that the current shipment didn't have the OTP Number confirmed by the recipient yet...",
      );
      id = JOURNEY_GURU.BULK_ROUTE.CONFIRM_OTP_NUMBER;
    }

    debug('Formating the instructions as JSON...');

    return this.formatInstructions(id, shipment);
  }

  /**
   * In a typical journey of a directed-bulk-route, the agent is instructed to go to the warehouse location.
   *
   * - The agent is instructed to pickup and scan as many shipments he wants to add to their route.
   *
   * - Once all shipments are scanned, the agent is instructed to navigate to the first drop off location.
   *
   * - At the drop off location the agent is instructed to contact the recipient.
   *
   * - Once the recipient is contacted, the agent is instructed to scan the shipment for
   *   confirmation of delivery attempt.
   *
   * - Once the shipment is scanned, the agent is instructed to confirm the delivery through
   *   the OTP number that the recipient received earlier. When the number is confirmed,
   *   the delivery is complete.
   *
   * - The agent is instructed to navigate to the next drop-off location and repeat the same
   *   steps again, for the next shipment on route, until all shipments are done.
   *
   * @param {import('../models/route').Route} route
   */
  static async getInstructionsForDirectedBulkRoute(route) {
    debug(`Getting instructions for directed-bulk-route #${route.id}...`);

    let id = 0;

    debug('Checking if the agent has started the route...');

    const routeHasStarted = route.statusId === ROUTE_STATUS.STARTED.id;

    debug(
      `Looking for the next shipment available for delivery on route #${
        route.id
      }...`,
    );

    const shipment = await app.models.Shipment.findNextShipmentForDeliveryInRoute(
      route.id,
    );

    const routeHasBeenCompleted = route.statusId === ROUTE_STATUS.COMPLETED.id;

    debug('Preparing instructions to be sent...');

    if (routeHasBeenCompleted) {
      debug('Found out that all shipments in route have been delivered...');
      id = JOURNEY_GURU.DIRECTED_BULK_ROUTE.COMPLETED;
    } else if (!routeHasStarted) {
      debug('Found out that route has not started yet...');
      id = JOURNEY_GURU.DIRECTED_BULK_ROUTE.ADD_SHIPMENT_TO_ROUTE;
    } else if (!shipment) {
      debug('Found out that all shipments in route have been delivered...');
      id = JOURNEY_GURU.DIRECTED_BULK_ROUTE.COMPLETED;
    } else if (!shipment.recipientWasContacted) {
      debug(
        'Found out that the recipient of current shipment has not been contacted yet...',
      );
      id = JOURNEY_GURU.DIRECTED_BULK_ROUTE.CONTACT_RECIPIENT;
    } else if (!shipment.wasScannedAfterContactingRecipient) {
      debug(
        'Found out that the shipment has not been scanned after contacting recipient...',
      );
      id = JOURNEY_GURU.DIRECTED_BULK_ROUTE.SCAN_SHIPMENT_TO_CONFIRM_DELIVERY;
    } else if (!shipment.otpNumberWasConfirmed) {
      debug(
        "Found out that the current shipment didn't have the OTP Number confirmed by the recipient yet...",
      );
      id = JOURNEY_GURU.DIRECTED_BULK_ROUTE.CONFIRM_OTP_NUMBER;
    }

    debug('Formating the instructions as JSON...');

    return this.formatInstructions(id, shipment);
  }

  /**
   * In a typical journey of a mix-route, the agent is instructed to go to the first
   * pickup location.
   *
   * - At the pickup location the agent is instructed to contact the sender.
   *
   * - Once contacted the agent is instructed to scan the shipment to confirm pick.
   *
   * - Based on the route, the next step can either be another pickup location,
   *   or a drop off location.
   *
   *   - If it’s a drop off location: The agent is instructed to do the steps for drop-offs,
   *     and if it’s pick up then the agent is instructed to do the steps for pickups. And repeat.
   *
   * @param {import('../models/route').Route} route
   */
  static async getInstructionsForMixRoute(route) {
    debug(`Getting instructions for mix-route #${route.id}...`);

    let id = 0;

    debug(
      `Looking for the next shipment available for delivery on route #${
        route.id
      }...`,
    );

    const shipment = await app.models.Shipment.findNextShipmentForDeliveryInRoute(
      route.id,
    );

    const routeHasBeenCompleted = route.statusId === ROUTE_STATUS.COMPLETED.id;

    debug('Preparing instructions to be sent...');

    if (!shipment || routeHasBeenCompleted) {
      debug('Found out that all shipments in route have been delivered...');
      id = JOURNEY_GURU.MIX_ROUTE.COMPLETED;
    } else if (!shipment.senderWasContacted) {
      debug(
        'Found out that the sender of the current shipment has not been contacted yet...',
      );
      id = JOURNEY_GURU.MIX_ROUTE.CONTACT_SENDER;
    } else if (!shipment.wasScannedBeforePickup) {
      debug(
        'Found out that the shipment was picked up but has not been scanned yet...',
      );
      id = JOURNEY_GURU.MIX_ROUTE.SCAN_SHIPMENT_TO_CONFIRM_PICKUP;
    } else if (!shipment.recipientWasContacted) {
      debug(
        'Found out that the recipient of current shipment has not been contacted yet...',
      );
      id = JOURNEY_GURU.MIX_ROUTE.CONTACT_RECIPIENT;
    } else if (!shipment.wasScannedAfterContactingRecipient) {
      debug(
        'Found out that the shipment has not been scanned after contacting recipient...',
      );
      id = JOURNEY_GURU.MIX_ROUTE.SCAN_SHIPMENT_TO_CONFIRM_DELIVERY;
    } else if (!shipment.otpNumberWasConfirmed) {
      debug(
        "Found out that the current shipment didn't have the OTP Number confirmed by the recipient yet...",
      );
      id = JOURNEY_GURU.MIX_ROUTE.CONFIRM_OTP_NUMBER;
    }

    debug('Formating the instructions as JSON...');

    return this.formatInstructions(id, shipment);
  }

  /**
   * Exceptions can happen in the following cases, which would cause the journey guru to remove
   * the exception shipment from route list:
   * - Cancellations
   * - Returns
   * - Time Passing without response
   * - Delivery errors
   * - Others.
   *
   * @param {number} shipmentId
   */
  static reportShipmentException(shipmentId) {
    throw new Error(
      `Method not implemented yet. But received shipmentId=${shipmentId}.`,
    );
  }

  /**
   * Returns the instruction in JSON format.
   *
   * @param {number} id - The instruction ID.
   * @param {string} shipment - The shipment object related to this instruction.
   * @returns {{id: number, shipment: string}}
   */
  static formatInstructions(id, shipment) {
    return { id, shipment };
  }
}

module.exports = JourneyGuru;
