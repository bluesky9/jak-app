const path = require('path');
const fs = require('fs');
const https = require('https');
const dotenvResult = require('dotenv').config({
  path: path.join(__dirname, '..', '.env'),
});

if (dotenvResult.error) {
  throw new Error(
    "There's no .env file on the root. Please, refer to README.md to get instructions about creating the environment configuration file.",
  );
}

const loopback = require('loopback');
const boot = require('loopback-boot');
const debug = require('debug')('app');

const app = loopback();

module.exports = app;

if (process.env.SSL_CERTIFICATE && process.env.SSL_CERTIFICATE_KEY) {
  const serverOptions = {
    key: fs.readFileSync(process.env.SSL_CERTIFICATE_KEY),
    cert: fs.readFileSync(process.env.SSL_CERTIFICATE),
  };

  app.start = () => {
    https.createServer(serverOptions, app).listen(app.get('port'), () => {
      app.emit('started');
      const baseUrl = `https://${app.get('host')}:${app.get('port')}`;
      debug('Web server listening at: %s', baseUrl);
      if (app.get('loopback-component-explorer')) {
        const explorerPath = app.get('loopback-component-explorer').mountPath;
        debug('Browse your REST API at %s%s', baseUrl, explorerPath);
      }
    });
  };
} else {
  app.start = () => app.listen(() => {
    app.emit('started');
    const baseUrl = app.get('url').replace(/\/$/, '');
    debug('Web server listening at: %s', baseUrl);
    if (app.get('loopback-component-explorer')) {
      const explorerPath = app.get('loopback-component-explorer').mountPath;
      debug('Browse your REST API at %s%s', baseUrl, explorerPath);
    }
  });
}

boot(app, __dirname, (err) => {
  if (err) throw err;
  if (require.main === module) {
    app.start();
  }
});
