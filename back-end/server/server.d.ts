import { LoopBackApplication, Model, AccessToken, ACL, RoleMapping, Role } from 'loopback';
import { Area } from './models/area';
import { Agent } from './models/agent';
import { AgentStatus } from './models/agent-status';
import { WorkingDay } from './models/working-day';
import { WorkingHour } from './models/working-hour';
import { Shipment } from './models/shipment';
import { ShipmentStatus } from './models/shipment-status';
import { ShipmentEvent } from './models/shipment-event';
import { Route } from './models/route';
import { PartnerCashType } from './models/partner-cash-type';
import { PartnerCashDirection } from './models/partner-cash-direction';
import { PartnerFeeType } from './models/partner-fee-type';
import { Partner } from './models/partner';
import { PartnerDiscount } from './models/partner-discount';
import { PartnerCashInstance } from './models/partner-cash-instance';
import { AgentCashType } from './models/agent-cash-type';
import { AgentCashInstance } from './models/agent-cash-instance';
import { User } from './models/user';
import { ShipmentType } from './models/shipment-type';
import { FleetOwner } from './models/fleet-owner';
import { AgentType } from './models/agent-type';
import { AgentTag } from './models/agent-tag';
import { RouteBundle } from './models/route-bundle';
import { RouteStatus } from './models/route-status';
import { Notification } from './models/notification';
import { Setting } from './models/setting';
import { UserDevice } from './models/user-device';

// @ts-ignore
declare interface JakApplication extends LoopBackApplication {
  models: {
    AccessToken: typeof AccessToken;
    ACL: typeof ACL;
    RoleMapping: typeof RoleMapping;
    Role: typeof Role;
    Area: typeof Area;
    Agent: typeof Agent;
    AgentStatus: typeof AgentStatus;
    WorkingDay: typeof WorkingDay;
    WorkingHour: typeof WorkingHour;
    Shipment: typeof Shipment;
    ShipmentStatus: typeof ShipmentStatus;
    ShipmentEvent: typeof ShipmentEvent;
    Route: typeof Route;
    PartnerCashType: typeof PartnerCashType;
    PartnerCashDirection: typeof PartnerCashDirection;
    PartnerFeeType: typeof PartnerFeeType;
    Partner: typeof Partner;
    PartnerDiscount: typeof PartnerDiscount;
    PartnerCashInstance: typeof PartnerCashInstance;
    AgentCashType: typeof AgentCashType;
    AgentCashInstance: typeof AgentCashInstance;
    User: typeof User;
    UserDevice: typeof UserDevice;
    ShipmentType: typeof ShipmentType;
    FleetOwner: typeof FleetOwner;
    AgentType: typeof AgentType;
    AgentTag: typeof AgentTag;
    RouteBundle: typeof RouteBundle;
    RouteStatus: typeof RouteStatus;
    Notification: typeof Notification;
    Setting: typeof Setting;
  };
}

declare var app: JakApplication

export = app
