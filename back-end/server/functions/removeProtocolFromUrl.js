function removeProtocolFromUrl(url) {
  return url.replace(/(^\w+:|^)\/\//, '');
}

module.exports = removeProtocolFromUrl;
