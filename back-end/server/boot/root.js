/**
 * Install a `/` route that returns server status.
 *
 * @param {typeof import('../server')} server
 */
module.exports = (server) => {
  const router = server.loopback.Router();
  router.get('/', server.loopback.status());
  server.use(router);
};
