const registerCorporateShippingsCreate = require('../legacy-api-routes/corporate/shippings/create');
const registerCorporateShippingsFind = require('../legacy-api-routes/corporate/shippings/find');
const registerCorporateShippingsFindById = require('../legacy-api-routes/corporate/shippings/find-by-id');
const registerCorporateShippingsFindStatuses = require('../legacy-api-routes/corporate/shippings/find-statuses');

/**
 * Register custom routes compatible with the old Jak Logistics API.
 *
 * @param {import('../server')} app
 */
module.exports = (app) => {
  registerCorporateShippingsCreate(app);
  registerCorporateShippingsFind(app);
  registerCorporateShippingsFindById(app);
  registerCorporateShippingsFindStatuses(app);
};
