/**
 * A flat list of developers usernames on GitLab.
 * @type {string[]}
 */
const gitlabUsernames = [
  'aliz',
  'apalmer',
  'dmitryg',
  'gfranciscani',
  'kirylk',
  'matheusa',
  'saad',
  'santiagof',
  'thiagop',
  'victorn',
];

module.exports = gitlabUsernames;
