const axios = require('axios');
const app = require('../server');

/** @param {import('express').Request} req */
const extractPartnerIdFromRequestData = async (req) => {
  const accessTokenFromRequest = req.get('Authorization');

  if (!accessTokenFromRequest) return null;

  const accessTokenFromOurDatabase = await app.models.AccessToken.findById(
    accessTokenFromRequest,
    {
      include: 'user',
    },
  );

  if (!accessTokenFromOurDatabase) return null;

  const user = await accessTokenFromOurDatabase.user();

  if (!user || !user.partnerId) return null;

  return user.partnerId;
};

/**
 * Function to POST a JSON to the Partner Webhook URL regarding a shipment status update.
 *
 * @param {number} shipmentId
 * @param {number} shipmentStatusId
 */
const postUpdateToPartnerWebhookUrl = async (shipmentId, shipmentStatusId) => {
  /** @type {app.models.Shipment} */
  const shipment = await app.models.Shipment.findById(shipmentId, {
    include: ['partner', 'route'],
  });

  /** @type {app.models.ShipmentStatus} */
  const shipmentStatus = await app.models.ShipmentStatus.findById(
    shipmentStatusId,
  );

  if (!shipment || !shipmentStatus) return;

  const partner = await shipment.partner();

  const route = await shipment.route();

  if (!partner || !route) return;

  let rider = null;

  if (route.agentId) {
    /** @type {app.models.Agent} */
    const agent = await app.models.Agent.findById(route.agentId);

    if (agent) {
      rider = {
        name: agent.name,
        phone: agent.phone,
        location: {
          latitude: agent.currentGeoPoint ? agent.currentGeoPoint.lat : '',
          longitude: agent.currentGeoPoint ? agent.currentGeoPoint.lng : '',
        },
      };
    }
  }

  if (partner.webhookUrl) {
    const payload = {
      id: shipment.id,
      status: shipmentStatus.name.toLowerCase().replace(' ', '_'),
      sub_status: '',
      timestamp: Math.round(new Date().getTime() / 1000),
      status_label: shipmentStatus.name,
      rider,
    };

    axios.post(partner.webhookUrl, payload);
  }
};

module.exports = {
  extractPartnerIdFromRequestData,
  postUpdateToPartnerWebhookUrl,
};
