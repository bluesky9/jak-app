const STATUS = require('http-status');
const { extractPartnerIdFromRequestData } = require('../../helpers');

/** @param {import('../../../server')} app */
module.exports = (app) => {
  app.post('/legacy-api/corporate/shippings', async (req, res) => {
    const partnerId = await extractPartnerIdFromRequestData(req);

    if (!partnerId) {
      res.sendStatus(STATUS.UNAUTHORIZED);
      return;
    }

    const shipmentData = req.body || {};

    shipmentData.partnerId = partnerId;

    const createdShipment = await app.models.Shipment.create(shipmentData);

    res.json(createdShipment);
  });
};
