const STATUS = require('http-status');
const { extractPartnerIdFromRequestData } = require('../../helpers');

/** @param {import('../../../server')} app */
module.exports = (app) => {
  app.get('/legacy-api/corporate/shippings', async (req, res) => {
    const partnerId = await extractPartnerIdFromRequestData(req);

    if (!partnerId) {
      res.sendStatus(STATUS.UNAUTHORIZED);
      return;
    }

    const filter = req.query.filter || {};

    filter.where = { partnerId };

    const shipments = await app.models.Shipment.find(filter);

    res.json(shipments);
  });
};
