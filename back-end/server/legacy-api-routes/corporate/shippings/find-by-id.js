const STATUS = require('http-status');
const { extractPartnerIdFromRequestData } = require('../../helpers');

/** @param {import('../../../server')} app */
module.exports = (app) => {
  app.get('/legacy-api/corporate/shippings/:id', async (req, res) => {
    const partnerId = await extractPartnerIdFromRequestData(req);

    if (!partnerId) {
      res.sendStatus(STATUS.UNAUTHORIZED);
      return;
    }

    const filter = req.query.filter || {};

    filter.where = {
      id: req.param.id,
      partnerId,
    };

    const shipment = await app.models.Shipment.findOne(filter);

    res.json(shipment);
  });
};
