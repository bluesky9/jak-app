import l from 'loopback';

declare class User extends l.User {
  id: number;
  realm: any;
  username: string;
  password: any;
  email: any;
  emailVerified: any;
  verificationToken: any;
  partnerId: number;
  agentId: number;
  createdAt: Date;
  updatedAt: Date;
}
