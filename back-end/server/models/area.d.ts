import { PersistedModel } from 'loopback';

declare class Area extends PersistedModel {
  id: number;
  code: any;
  name: string;
  country: any;
  city: any;
  centerGeoPoint: any;
  radius: any;
  createdAt: Date;
  updatedAt: Date;

  agents(): import('./agent').Agent[];
}
