import { PersistedModel } from 'loopback';

declare class PartnerCashDirection extends PersistedModel {
  id: number;
  name: string;

  cashInstances(): import('./partner-cash-instance').PartnerCashInstance[];
}
