import { PersistedModel } from 'loopback';

declare class AgentType extends PersistedModel {
  id: number;
  name: string;
  description: string;

  agents(): import('./agent').Agent[];
}
