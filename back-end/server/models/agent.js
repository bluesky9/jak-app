const STATUS = require('http-status');
const {
  AGENT_TYPE,
  ROUTE_STATUS,
  AGENT_STATUS,
  AGENT_CASH_TYPE,
  SHIPMENT_STATUS,
  ROUTE_BUNDLE,
} = require('common/constants/models/for-node');
const app = require('../server');

/**
 * Represents a person who delivers shipments to their respective recipients.
 *
 * @param {typeof app.models.Agent} Model
 */
module.exports = (Model) => {
  const AgentModel = Model;

  AgentModel.observe('before save', async (ctx) => {
    const contextModel = ctx.instance || ctx.data;

    // If Friendly ID is not set yet, generate one.
    if (!contextModel.friendlyId) {
      contextModel.friendlyId = AgentModel.generateFriendlyId(
        contextModel.name,
      );
    }
  });

  AgentModel.fetchAllWithStatusAvailable = async () => {
    const agentsAvailable = await AgentModel.find({
      where: {
        statusId: AGENT_STATUS.AVAILABLE.id,
        routeId: null,
      },
      include: ['type', 'tags'],
    });

    return agentsAvailable;
  };

  AgentModel.totalEarnings = async (id) => {
    const agent = await AgentModel.findOne({
      fields: ['typeId'],
      where: {
        id,
      },
    });

    let earningType;

    if (agent.typeId === AGENT_TYPE.FULL_TIMER.id) {
      earningType = 'Salary';
    } else if (agent.typeId === AGENT_TYPE.FREELANCER.id) {
      earningType = 'Commission';
    }

    const AgentCashType = await app.models.AgentCashType.findOne({
      fields: ['id'],
      where: {
        name: earningType,
      },
    });

    const earnings = await app.models.AgentCashInstance.find({
      where: {
        agentId: id,
        typeId: AgentCashType.id,
      },
    });

    return earnings
      .map(earning => earning.amount)
      .reduce((acc, cur) => acc + parseFloat(cur), 0);
  };

  AgentModel.cashAtHand = async (id) => {
    const cashAtHand = await app.models.AgentCashInstance.find({
      where: {
        agentId: id,
        typeId: AGENT_CASH_TYPE.CASH_COLLECTED.id,
        isSettled: false,
      },
    });

    return cashAtHand
      .map(earning => earning.amount)
      .reduce((acc, cur) => acc + parseFloat(cur), 0);
  };

  AgentModel.totalTransactions = async (id) => {
    // Get agent's routes.
    const routes = await app.models.Route.find({
      where: { agentId: id },
    });

    // Get cash to collect.
    const allShipments = await app.models.Shipment.find({
      fields: ['cashToCollectOnDelivery'],
      where: {
        statusId: SHIPMENT_STATUS.DELIVERED.id,
        routeId: { inq: routes.map(route => route.id) },
      },
    });

    // Sum cashToCollectOnDelivery of all shipments.
    return allShipments
      .map(shipment => shipment.cashToCollectOnDelivery)
      .reduce((acc, cur) => acc + parseFloat(cur), 0);
  };

  AgentModel.acceptRouteOffer = async (agentId, routeId) => {
    /** @type {AgentModel} */
    const agent = await AgentModel.findById(agentId);

    if (!agent) {
      const error = new Error();
      error.status = STATUS.NOT_FOUND;
      error.name = 'AGENT_NOT_FOUND';
      error.message = 'No agent was found with this ID.';
      throw error;
    }

    const agentAssignedRoutes = await agent.routes({
      where: {
        or: [
          {
            statusId: ROUTE_STATUS.ASSIGNED.id,
          },
          {
            statusId: ROUTE_STATUS.STARTED.id,
          },
        ],
      },
    });

    if (agentAssignedRoutes && agentAssignedRoutes.length > 0) {
      const error = new Error();
      error.status = STATUS.CONFLICT;
      error.name = 'AGENT_HAS_AN_ASSIGNED_ROUTE';
      error.message = 'You need to finish your current route before accepting this offer.';
      throw error;
    }

    const { Route } = app.models;

    /** @type {Route} */
    const route = await Route.findById(routeId);

    if (!route) {
      const error = new Error();
      error.status = STATUS.NOT_FOUND;
      error.name = 'ROUTE_NOT_FOUND';
      error.message = 'No route was found with this ID.';
      throw error;
    }

    const routeIsNotAvailableAnymore = [
      ROUTE_STATUS.ASSIGNED.id,
      ROUTE_STATUS.STARTED.id,
    ].includes(route.statusId);

    if (route.agentId || routeIsNotAvailableAnymore) {
      const error = new Error();
      error.status = STATUS.NOT_ACCEPTABLE;
      error.name = 'OFFER_EXPIRED';
      error.message = 'This Offer has already been taken by another agent.';
      throw error;
    }

    route.startDatetime = new Date();
    route.agent(agent);
    route.statusId = ROUTE_STATUS.STARTED.id;
    await route.save();

    return route;
  };

  /**
   * Generates 6-characters uppercase alphanumeric strings to identify the agent, where:
   * - The first 3 characters are from the agent name
   * - The last 3 characters are from a random integer >= 100 and <= 999.
   *
   * @param {string} agentName
   */
  AgentModel.generateFriendlyId = (agentName) => {
    const threeFirstCharsFromName = agentName.substr(0, 3);
    const threeDigitsRandomInteger = Math.floor(100 + Math.random() * 899);
    const friendlyId = `${threeFirstCharsFromName}${threeDigitsRandomInteger}`.toUpperCase();
    return friendlyId;
  };

  AgentModel.assignedRoute = async (id) => {
    /** @type {AgentModel} */
    const agent = await AgentModel.findById(id);

    if (!agent) {
      const error = new Error();
      error.status = STATUS.NOT_FOUND;
      error.name = 'AGENT_NOT_FOUND';
      error.message = 'No agent was found with this ID.';
      throw error;
    }

    const agentAssignedRoutes = await agent.routes({
      include: ['agent', 'shipments', 'bundle', 'status', 'requiredAgentTags'],
      where: {
        or: [
          {
            statusId: ROUTE_STATUS.ASSIGNED.id,
          },
          {
            statusId: ROUTE_STATUS.STARTED.id,
          },
        ],
      },
    });

    if (!agentAssignedRoutes.length) {
      const error = new Error();
      error.status = STATUS.NOT_FOUND;
      error.name = 'NO_ASSIGNED_ROUTE_FOUND';
      error.message = 'Agent has no assigned route at the moment.';
      throw error;
    }

    const [assignedRoute] = agentAssignedRoutes;

    return assignedRoute;
  };

  /**
   * Returns personalized route offers to the agent.
   *
   * @param {number} id - The Agent ID.
   */
  AgentModel.routeOffers = async (id) => {
    /** @type {AgentModel} */
    const agent = await app.models.Agent.findById(id, {
      include: ['tags'],
    });

    if (!agent) {
      const error = new Error();
      error.status = STATUS.NOT_FOUND;
      error.name = 'AGENT_NOT_FOUND';
      error.message = 'No agent was found with this ID.';
      throw error;
    }

    const { Route } = app.models;

    /** @type {Route} */
    const routeWhereFilter = {
      agentId: null,
      isOffered: true,
      statusId: ROUTE_STATUS.CREATED.id,
      bundleId: { neq: ROUTE_BUNDLE.DIRECTED_BULK.id },
    };

    /** @type {Route[]} */
    const routes = await Route.find({
      include: ['shipments', 'bundle', 'requiredAgentTags'],
      where: routeWhereFilter,
    });

    /** @type {Route[]} */
    const routeOffers = [];

    routes.forEach((route) => {
      const routeRequiredAgentTags = route.requiredAgentTags();

      if (routeRequiredAgentTags.length) {
        const agentTagIds = agent.tags().map(tag => tag.id);

        const routeRequiredAgentTagIds = routeRequiredAgentTags.map(
          routeRequiredAgentTag => routeRequiredAgentTag.id,
        );

        for (let i = 0, l = agentTagIds.length; i < l; i += 1) {
          const agentTagId = agentTagIds[i];
          if (routeRequiredAgentTagIds.includes(agentTagId)) {
            routeOffers.push(route);
            break;
          }
        }
      } else {
        routeOffers.push(route);
      }
    });

    routeOffers.forEach((element) => {
      const route = element;

      if (!route.value) {
        if (agent.typeId === AGENT_TYPE.FREELANCER.id) {
          route.value = (
            route.totalShipments * agent.agreedCommissionPerShipment
          ).toFixed(2);
        } else if (agent.typeId === AGENT_TYPE.FULL_TIMER.id) {
          route.value = route.totalCoD.toFixed(2);
        }
      }
    });

    return routeOffers;
  };
};
