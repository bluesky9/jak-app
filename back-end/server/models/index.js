
const app = require('../server');

class Models {
  static get AccessToken() { return app.models.AccessToken; }

  static get ACL() { return app.models.ACL; }

  static get RoleMapping() { return app.models.RoleMapping; }

  static get Role() { return app.models.Role; }

  static get Area() { return app.models.area; }

  static get Agent() { return app.models.agent; }

  static get AgentStatus() { return app.models.agentStatus; }

  static get WorkingDay() { return app.models.workingDay; }

  static get WorkingHour() { return app.models.workingHour; }

  static get Shipment() { return app.models.shipment; }

  static get ShipmentStatus() { return app.models.shipmentStatus; }

  static get ShipmentEvent() { return app.models.shipmentEvent; }

  static get Route() { return app.models.route; }

  static get PartnerCashType() { return app.models.partnerCashType; }

  static get PartnerCashDirection() { return app.models.partnerCashDirection; }

  static get PartnerFeeType() { return app.models.partnerFeeType; }

  static get Partner() { return app.models.partner; }

  static get PartnerDiscount() { return app.models.partnerDiscount; }

  static get PartnerCashInstance() { return app.models.partnerCashInstance; }

  static get AgentCashType() { return app.models.agentCashType; }

  static get AgentCashInstance() { return app.models.agentCashInstance; }

  static get User() { return app.models.user; }

  static get ShipmentType() { return app.models.shipmentType; }

  static get FleetOwner() { return app.models.fleetOwner; }

  static get AgentType() { return app.models.agentType; }

  static get AgentTag() { return app.models.agentTag; }

  static get RouteBundle() { return app.models.routeBundle; }

  static get RouteStatus() { return app.models.routeStatus; }

  static get Notification() { return app.models.notification; }

  static get Setting() { return app.models.setting; }
}

module.exports = Models;
