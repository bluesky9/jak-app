/**
 * Represents the direction of the cash.
 *
 * Inbound: Belongs to Jak to be collected from a partner.
 *
 * Outbound: Belongs to a partner to be collected from Jak.
 */
module.exports = (/* PartnerCashDirection */) => {};
