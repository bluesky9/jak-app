import { PersistedModel } from 'loopback';

declare class ShipmentEvent extends PersistedModel {
  notes: any;
  id: number;
  shipmentId: number;
  statusId: number;
  createdAt: Date;
  updatedAt: Date;
}
