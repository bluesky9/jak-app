import { PersistedModel } from 'loopback';

declare class Setting extends PersistedModel {
  id: number;
  name: string;
  value: string;
  createdAt: Date;
  updatedAt: Date;
}
