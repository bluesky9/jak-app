const { PARTNER_CASH_DIRECTION } = require('common/constants/models/for-node');

/**
 * Represents cash instances related to partners, be it fees they have to pay
 * or collections we made on their behalf and owe to them.
 *
 * @param {typeof import('./partner-cash-instance').PartnerCashInstance} Model
 */
module.exports = (Model) => {
  const PartnerCashInstanceModel = Model;

  /**
   * Creates a report with total amount for settled and not settled
   * cash instances with a given due date, by type.
   *
   * @param {number} typeId
   * @param {Date} from
   * @param {Date} to
   */
  PartnerCashInstanceModel.report = async (typeId, from, to) => {
    const notSettled = await PartnerCashInstanceModel.find({
      where: {
        isSettled: false,
        typeId,
        and: [{ dueDatetime: { gt: from } }, { dueDatetime: { lt: to } }],
      },
    });
    const settled = await PartnerCashInstanceModel.find({
      where: {
        isSettled: true,
        typeId,
        and: [{ dueDatetime: { gt: from } }, { dueDatetime: { lt: to } }],
      },
    });

    const data = {
      settled: {
        count: settled.length,
        amount: {
          total: settled.reduce((prev, value) => prev + value.amount, 0),
          inbound: settled.reduce(
            (prev, value) => prev
              + (value.directionId === PARTNER_CASH_DIRECTION.INBOUND.id
                ? value.amount
                : 0),
            0,
          ),
          outbound: settled.reduce(
            (prev, value) => prev
              + (value.directionId === PARTNER_CASH_DIRECTION.OUTBOUND.id
                ? value.amount
                : 0),
            0,
          ),
        },
      },
      notSettled: {
        count: notSettled.length,
        amount: {
          total: notSettled.reduce((prev, value) => prev + value.amount, 0),
          inbound: notSettled.reduce(
            (prev, value) => prev
              + (value.directionId === PARTNER_CASH_DIRECTION.INBOUND.id
                ? value.amount
                : 0),
            0,
          ),
          outbound: notSettled.reduce(
            (prev, value) => prev
              + (value.directionId === PARTNER_CASH_DIRECTION.OUTBOUND.id
                ? value.amount
                : 0),
            0,
          ),
        },
      },
    };

    return data;
  };
};
