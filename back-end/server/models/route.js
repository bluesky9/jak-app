const STATUS = require('http-status');
const {
  AGENT_CASH_TYPE,
  ROUTE_STATUS,
  AGENT_TYPE,
  ROUTE_BUNDLE,
  SHIPMENT_STATUS,
} = require('common/constants/models/for-node');
const RouteAssigner = require('../classes/RouteAssigner');
const RouteCreator = require('../classes/RouteCreator');
const JourneyGuru = require('../classes/JourneyGuru');
const app = require('../server');

/**
 * Represents a sequence of waypoints that an agent should follow when delivering shipments.
 *
 * @param {typeof import('./route').Route} Model
 */
module.exports = (Model) => {
  const RouteModel = Model;

  RouteModel.observe('before save', async (ctx) => {
    /** @type {RouteModel} */
    const contextModel = ctx.instance || ctx.data;

    if (!contextModel.value) await RouteModel.autoSetValueIfNeeded(contextModel);
  });

  RouteModel.observe('after save', async (ctx) => {
    /** @type {RouteModel} */
    const contextModel = ctx.instance || ctx.data;

    if (contextModel.agentId && ctx.isNewInstance) {
      const routeBundleIsDirectedBulk = contextModel.bundleId === ROUTE_BUNDLE.DIRECTED_BULK.id;
      const routeHasStatusAssigned = contextModel.statusId === ROUTE_STATUS.ASSIGNED.id;

      if (routeBundleIsDirectedBulk && routeHasStatusAssigned) {
        await app.models.Notification.notifyAgentOfDirectedRouteAssigned(
          contextModel.agentId,
        );
      }
    }
  });

  RouteModel.autoSetValueIfNeeded = async (routeBeingSaved) => {
    const route = routeBeingSaved;

    const shipmentsInThisRoute = await app.models.Shipment.count({
      routeId: route.id,
    });

    const routeHasStatusCreated = route.statusId === ROUTE_STATUS.CREATED.id;
    const routeBundleIsBulk = route.bundleId === ROUTE_BUNDLE.BULK.id;
    const routeHasLessThan25Shipments = shipmentsInThisRoute < 25;

    if (
      routeHasStatusCreated
      && routeBundleIsBulk
      && routeHasLessThan25Shipments
    ) {
      route.value = 100;
    }
  };

  RouteModel.generate = async () => RouteCreator.createAllRoutes();

  RouteModel.autoAssign = async () => RouteAssigner.assignRoutes();

  /**
   * Returns instructions for route `id`.
   *
   * @param {number} id
   */
  RouteModel.journeyInstructions = async id => JourneyGuru.getInstructionsForRoute(id);

  /**
   * Given 'lat,long' coordinates, returns the raw result from Google Directions API.
   *
   * @param {string} origin
   * @param {string} destination
   * @param {Array} waypoints
   */
  RouteModel.directionsForGoogleMaps = async (origin, destination, waypoints) => RouteCreator.createGoogleRoute(origin, destination, waypoints);

  /**
   * Returns all routes with status 'Created'.
   */
  RouteModel.fetchAllWithStatusCreated = async () => {
    // All routes with status created
    const routesWithStatusCreated = await RouteModel.find({
      where: {
        statusId: ROUTE_STATUS.CREATED.id,
      },
      include: ['shipments', 'bundle', 'requiredAgentTags'],
    });

    return routesWithStatusCreated;
  };

  RouteModel.orderedWaypoints = async (id) => {
    /** @type {RouteModel} */
    const route = await RouteModel.findById(id, {
      include: ['bundle', 'shipments'],
    });

    if (!route) {
      const error = new Error();
      error.status = STATUS.NOT_FOUND;
      error.name = 'ROUTE_NOT_FOUND';
      error.message = 'No routes were found with this ID.';
      throw error;
    }

    const routeBundle = await route.bundle();

    const orderedWaypoints = [];

    // If it's a mix-route, we need to consider the
    // 'routePickupOrder' and 'routeDeliveryOrder' fields.
    if (routeBundle.id === ROUTE_BUNDLE.MIX.id) {
      const mixedPickupAdnDeliveryAddresses = [];
      const shipments = await route.shipments();

      shipments.forEach((shipment) => {
        const {
          id: shipmentId,
          trackingId,
          routePickupOrder,
          routeDeliveryOrder,
          senderAddress,
          senderGeoPoint,
          recipientAddress,
          recipientGeoPoint,
        } = shipment;

        mixedPickupAdnDeliveryAddresses[routePickupOrder] = {
          shipmentId,
          trackingId,
          type: 'Pickup',
          routePickupOrder,
          address: senderAddress,
          ...senderGeoPoint,
        };

        mixedPickupAdnDeliveryAddresses[routeDeliveryOrder] = {
          shipmentId,
          trackingId,
          type: 'Delivery',
          routeDeliveryOrder,
          address: recipientAddress,
          ...recipientGeoPoint,
        };
      });

      mixedPickupAdnDeliveryAddresses.forEach((addressAndGeoPoint) => {
        orderedWaypoints.push(addressAndGeoPoint);
      });
    } else {
      const shipments = await route.shipments({
        order: 'routeDeliveryOrder ASC',
      });

      // If it's a bulk-route we set the first waypoint as the first shipment senderAddress.
      if (routeBundle.id === ROUTE_BUNDLE.BULK.id) {
        if (shipments && shipments[0]) {
          const [firstShipment] = shipments;
          const {
            id: shipmentId,
            trackingId,
            senderAddress,
            senderGeoPoint,
          } = firstShipment;

          orderedWaypoints.push({
            shipmentId,
            trackingId,
            type: 'Pickup',
            routePickupOrder: 0,
            address: senderAddress,
            ...senderGeoPoint,
          });
        }
      }

      shipments.forEach((shipment) => {
        const {
          id: shipmentId,
          trackingId,
          routeDeliveryOrder,
          recipientAddress,
          recipientGeoPoint,
        } = shipment;

        orderedWaypoints.push({
          shipmentId,
          trackingId,
          type: 'Delivery',
          routeDeliveryOrder,
          address: recipientAddress,
          ...recipientGeoPoint,
        });
      });
    }

    return orderedWaypoints;
  };

  RouteModel.computeTotalCoD = async (route) => {
    const shipments = await app.models.Shipment.find({
      fields: ['cashToCollectOnDelivery'],
      where: {
        routeId: route.id,
      },
    });

    return shipments.reduce((accumulator, shipment) => {
      const sum = accumulator.cashToCollectOnDelivery || accumulator;
      return sum + shipment.cashToCollectOnDelivery;
    }, 0);
  };

  RouteModel.computeTotalShipments = async route => app.models.Shipment.count({ routeId: route.id });

  RouteModel.observe('after save', async (ctx) => {
    /** @type {RouteModel} */
    const contextModel = ctx.instance || ctx.data;
    const routeHasAnAgentAssigned = !!contextModel.agentId;
    const routeIsCompleted = contextModel.statusId === ROUTE_STATUS.COMPLETED.id;
    const routeHasNoAgentCashInstanceGeneratedYet = !contextModel.agentCashInstanceId;

    if (
      routeHasAnAgentAssigned
      && routeIsCompleted
      && routeHasNoAgentCashInstanceGeneratedYet
    ) {
      const { Agent } = app.models;
      /** @type {Agent} */
      const agent = await Agent.findById(contextModel.agentId);

      const agentIsAFreelancer = agent.typeId === AGENT_TYPE.FREELANCER.id;

      if (agentIsAFreelancer) {
        const numberOfShipments = await RouteModel.computeTotalShipments(
          contextModel,
        );

        const { AgentCashInstance } = app.models;

        /** @type {AgentCashInstance} */
        const agentCashInstanceToCreate = {
          amount:
            contextModel.value
            || numberOfShipments * agent.agreedCommissionPerShipment,
          dueDatetime: new Date(),
          agentId: contextModel.agentId,
          typeId: AGENT_CASH_TYPE.COMMISSION.id,
        };

        /** @type {AgentCashInstance} */
        const agentCashInstanceCreated = await AgentCashInstance.create(
          agentCashInstanceToCreate,
        );

        contextModel.agentCashInstanceId = agentCashInstanceCreated.id;
        contextModel.save();
      }
    }
  });

  RouteModel.markAsCompletedIfAllShipmentsHaveBeenDelivered = async (id) => {
    /** @type {RouteModel} */
    const route = await RouteModel.findById(id);

    if (!route) {
      const error = new Error();
      error.status = STATUS.NOT_FOUND;
      error.name = 'ROUTE_NOT_FOUND';
      error.message = 'No routes were found with this ID.';
      throw error;
    }

    const routeHasNotBeenMarkedAsCompleted = route.statusId !== ROUTE_STATUS.COMPLETED.id;

    if (routeHasNotBeenMarkedAsCompleted) {
      const shipmentsDeliveredInThisRoute = await app.models.Shipment.count({
        routeId: id,
        statusId: SHIPMENT_STATUS.DELIVERED.id,
      });

      const allShipmentsHaveBeenDelivered = shipmentsDeliveredInThisRoute === route.totalShipments;

      if (allShipmentsHaveBeenDelivered) {
        route.endDatetime = new Date();
        route.statusId = ROUTE_STATUS.COMPLETED.id;
        return route.save();
      }
    }

    return route;
  };

  RouteModel.markAsStarted = async (id) => {
    /** @type {RouteModel} */
    const route = await RouteModel.findById(id);

    if (!route) {
      const error = new Error();
      error.status = STATUS.NOT_FOUND;
      error.name = 'ROUTE_NOT_FOUND';
      error.message = 'No routes were found with this ID.';
      throw error;
    }

    route.statusId = ROUTE_STATUS.STARTED.id;

    return route.save();
  };
};
