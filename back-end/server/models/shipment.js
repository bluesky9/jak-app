const STATUS = require('http-status');
const debug = require('debug')('app:shipment');
const get = require('get-value');
const {
  SHIPMENT_STATUS,
  ROUTE_BUNDLE,
  AGENT_STATUS,
  SHIPMENT_TYPE,
  PARTNER_CASH_TYPE,
  PARTNER_CASH_DIRECTION,
} = require('common/constants/models/for-node');
const { removeProtocolFromUrl } = require('common/functions/for-node');
const UnifonicApi = require('../classes/UnifonicApi');
const UrlShortenerApi = require('../classes/UrlShortenerApi');
const HashIdsWrapper = require('../classes/HashIdsWrapper');
const RouteCreator = require('../classes/RouteCreator');
const {
  postUpdateToPartnerWebhookUrl,
} = require('../legacy-api-routes/helpers');
const app = require('../server');

/**
 * The validator function must be declared as local function, not as a variable arrow function.
 *
 * @param {Function} err
 * @this {import('./shipment').Shipment}
 */
function checkRequiredFieldsForNormalShipments(err) {
  const shipmentTypeIsNormal = this.typeId === SHIPMENT_TYPE.NORMAL.id;
  const thereAreRequiredFieldsMissing = !this.senderAddress || !this.senderGeoPoint;
  if (shipmentTypeIsNormal && thereAreRequiredFieldsMissing) err();
}

/**
 * Checks if the shipment type is compatible with the route bundle it's being added to.
 * Rule: A Warehouse-Shipment can only be added to a Directed-Bulk-Route.
 * The validator function must be declared as local function, not as a variable arrow function.
 *
 * @param {Function} err
 * @param {Function} done
 * @this {import('./shipment').Shipment}
 */
async function checkIfTypeIsCompatibleWithRouteBundle(err, done) {
  if (this.routeId) {
    const shipmentTypeIsWarehouse = this.typeId === SHIPMENT_TYPE.WAREHOUSE.id;

    const routeBundleIsDirectedBulk = await app.models.Route.count({
      id: this.routeId,
      bundleId: ROUTE_BUNDLE.DIRECTED_BULK.id,
    });

    const meetsErrorCondition1 = shipmentTypeIsWarehouse && !routeBundleIsDirectedBulk;

    const meetsErrorCondition2 = !shipmentTypeIsWarehouse && routeBundleIsDirectedBulk;

    if (meetsErrorCondition1 || meetsErrorCondition2) err();
  }

  done();
}

/**
 * Represents a package to be delivered.
 *
 * @param {typeof app.models.Shipment} Model
 */
module.exports = (Model) => {
  const ShipmentModel = Model;

  ShipmentModel.validate(
    'senderAddress',
    checkRequiredFieldsForNormalShipments,
    {
      message: 'is required for Normal Shipments',
    },
  );
  ShipmentModel.validate(
    'senderGeoPoint',
    checkRequiredFieldsForNormalShipments,
    {
      message: 'is required for Normal Shipments',
    },
  );
  ShipmentModel.validateAsync(
    'routeId',
    checkIfTypeIsCompatibleWithRouteBundle,
    {
      message:
        'is not the ID of Directed-Bulk-Route, which is required to include Warehouse-Shipments',
    },
  );

  /**
   * Generates a TackingID by joining four parts (ABCD):
   * A: The letter J.
   * B: The area ID.
   * C: 1111000+Total shipments count.
   * D: Two random digits.
   *
   * Example:
   * A: J.
   * B: RY02.
   * C: 1111001.
   * D: 23.
   *
   * Results in:
   * JRY02111100123.
   *
   * @param {string} recipientAreaCode
   */
  ShipmentModel.generateTrackingId = async (recipientAreaCode) => {
    const parts = [];

    parts.push('J');

    parts.push(recipientAreaCode);

    const totalShipmentsCount = await ShipmentModel.count();

    parts.push(1111000 + totalShipmentsCount);

    parts.push(Math.floor(Math.random() * 99));

    return parts.join('');
  };

  /**
   * Updates the recipientGeopoint of an specific shipment
   * and returns the updated shipment.
   *
   * @param {number} id
   * @param {string} address
   * @param {string} latitude
   * @param {string} longitude
   */
  ShipmentModel.confirmGeolocation = async (
    id,
    address,
    latitude,
    longitude,
  ) => {
    /** @type {ShipmentModel} */
    const shipment = await ShipmentModel.findById(id);

    if (!shipment) {
      const error = new Error();
      error.status = STATUS.NOT_FOUND;
      error.name = 'SHIPMENT_NOT_FOUND';
      error.message = 'No shipments were found with this ID.';
      throw error;
    }

    shipment.recipientGeoPoint = {
      lat: latitude,
      lng: longitude,
    };

    shipment.recipientAddress = address;
    shipment.recipientAddressConfirmed = true;
    shipment.recipientAddressConfirmationDatetime = new Date();

    return shipment.save();
  };

  /**
   * Sets the field "wasScannedBeforePickup" of an specific shipment to "true"
   * and returns the updated shipment.
   *
   * @param {number} id
   */
  ShipmentModel.markAsScannedBeforePickup = async (id) => {
    const shipment = await ShipmentModel.findById(id);

    if (!shipment) {
      const error = new Error();
      error.status = STATUS.NOT_FOUND;
      error.name = 'SHIPMENT_NOT_FOUND';
      error.message = 'No shipments were found with this ID.';
      throw error;
    }

    shipment.wasScannedBeforePickup = true;
    shipment.statusId = SHIPMENT_STATUS.PICKED_UP.id;

    return shipment.save();
  };

  /**
   * Sets the field "senderWasContacted" of an specific shipment to "true"
   * and returns the updated shipment.
   *
   * @param {number} id
   */
  ShipmentModel.markSenderAsContacted = async (id) => {
    const shipment = await ShipmentModel.findById(id);

    if (!shipment) {
      const error = new Error();
      error.status = STATUS.NOT_FOUND;
      error.name = 'SHIPMENT_NOT_FOUND';
      error.message = 'No shipments were found with this ID.';
      throw error;
    }

    shipment.senderWasContacted = true;

    return shipment.save();
  };

  /**
   * Sets the field "recipientWasContacted" of an specific shipment to "true"
   * and returns the updated shipment.
   *
   * @param {number} id
   */
  ShipmentModel.markRecipientAsContacted = async (id) => {
    const shipment = await ShipmentModel.findById(id);

    if (!shipment) {
      const error = new Error();
      error.status = STATUS.NOT_FOUND;
      error.name = 'SHIPMENT_NOT_FOUND';
      error.message = 'No shipments were found with this ID.';
      throw error;
    }

    shipment.recipientWasContacted = true;

    return shipment.save();
  };

  /**
   * Sets the field "wasScannedAfterContactingRecipient" of an specific shipment to "true"
   * and returns the updated shipment.
   *
   * @param {number} id
   */
  ShipmentModel.markAsScannedAfterContactingRecipient = async (id) => {
    /** @type {ShipmentModel} */
    const shipment = await ShipmentModel.findById(id);

    if (!shipment) {
      const error = new Error();
      error.status = STATUS.NOT_FOUND;
      error.name = 'SHIPMENT_NOT_FOUND';
      error.message = 'No shipments were found with this ID.';
      throw error;
    }

    shipment.wasScannedAfterContactingRecipient = true;
    shipment.statusId = SHIPMENT_STATUS.ON_ROUTE.id;

    return shipment.save();
  };

  /**
   * Compares otpNumber provided with shipment's otpNumber, if otpNumber is mandatory.
   * If they are equal, sets the field "otpNumberWasConfirmed" of the shipment to "true".
   * If they are not equal, sets the field "otpNumberWasConfirmed" of the shipment to "false".
   * Then returns the updated shipment.
   *
   * @param {number} id
   * @param {number} otpNumber
   */
  ShipmentModel.confirmOtpNumber = async (id, otpNumber) => {
    /** @type {ShipmentModel} */
    const shipment = await ShipmentModel.findById(id);

    if (!shipment) {
      const error = new Error();
      error.status = STATUS.NOT_FOUND;
      error.name = 'SHIPMENT_NOT_FOUND';
      error.message = 'No shipments were found with this ID.';
      throw error;
    }

    const otpNumberProvidedIsCorrect = otpNumber === shipment.otpNumber;

    if (shipment.otpNumberIsNotMandatory || otpNumberProvidedIsCorrect) {
      shipment.otpNumberWasConfirmed = true;
      shipment.statusId = SHIPMENT_STATUS.DELIVERED.id;
    } else {
      const error = new Error();
      error.status = STATUS.NOT_ACCEPTABLE;
      error.name = 'WRONG_OTP_NUMBER';
      error.message = "The OTP Number provided doesn't match this shipment's OTP Number.";
      throw error;
    }

    return shipment.save();
  };

  /**
   * Returns all shipments with status 'Created' and with address filled
   * (even if not confirmed by the recipient), with sub-objects recipientArea,
   * recipientArea.agents filled.
   * Also, 'recipientArea.agents' are only those with status 'Available'.
   */
  ShipmentModel.fetchAllWithStatusCreated = async () => ShipmentModel.find({
    where: {
      statusId: SHIPMENT_STATUS.CREATED.id,
      routeId: null,
    },
    include: {
      relation: 'recipientArea',
      scope: {
        include: {
          relation: 'agents',
          scope: {
            where: {
              statusId: AGENT_STATUS.AVAILABLE.id,
            },
          },
        },
      },
    },
  });

  /**
   * Returns all shipments with status 'Created' and with address filled
   * (even if not confirmed by the recipient) and of a specific type.
   *
   * @param {number} typeId
   */
  ShipmentModel.fetchAllByTypeWithStatusCreated = async (typeId) => {
    if (!typeId) throw new Error('You must specify a shipment type ID.');

    // All shipments with status created, of a certain type and no route set.
    return ShipmentModel.find({
      where: {
        typeId,
        statusId: SHIPMENT_STATUS.CREATED.id,
        routeId: null,
        shouldNotBeAutoAssigned: false,
      },
    });
  };

  /**
   * Returns the next shipment from the route that is
   * not marked as 'Delivered' nor 'Canceled'.
   *
   * @param {number} routeId
   */
  ShipmentModel.findNextShipmentForDeliveryInRoute = async (routeId) => {
    const { Route } = app.models;

    /** @type {Route} */
    const route = await Route.findById(routeId);

    if (route.bundleId === ROUTE_BUNDLE.MIX.id) {
      /** @type {ShipmentModel} */
      const pickedShipment = await ShipmentModel.findOne({
        where: {
          routeId,
          statusId: {
            inq: [SHIPMENT_STATUS.PICKED_UP.id, SHIPMENT_STATUS.ON_ROUTE.id],
          },
        },
        order: 'routeDeliveryOrder ASC',
      });

      /** @type {ShipmentModel} */
      const createdShipment = await ShipmentModel.findOne({
        where: {
          routeId,
          statusId: SHIPMENT_STATUS.CREATED.id,
        },
        order: 'routePickupOrder ASC',
      });

      if (!pickedShipment && !createdShipment) return null;

      if (!createdShipment) return pickedShipment;

      if (!pickedShipment) return createdShipment;

      const pickedShipmentHasPriority = pickedShipment.routeDeliveryOrder < createdShipment.routePickupOrder;

      return pickedShipmentHasPriority ? pickedShipment : createdShipment;
    }

    return ShipmentModel.findOne({
      where: {
        routeId,
        statusId: {
          nin: [SHIPMENT_STATUS.DELIVERED.id, SHIPMENT_STATUS.CANCELED.id],
        },
      },
      order: 'routeDeliveryOrder ASC',
    });
  };

  ShipmentModel.observe('before save', async (ctx) => {
    /** @type {ShipmentModel} */
    const contextModel = ctx.instance || ctx.data;

    // If Tracking ID is not set yet, generate one.
    if (!contextModel.trackingId) {
      let recipientArea;
      let trackingId;

      if (contextModel.recipientAreaId) {
        recipientArea = await app.models.Area.findById(
          contextModel.recipientAreaId,
        );
      }

      if (get(recipientArea, 'code')) {
        trackingId = await ShipmentModel.generateTrackingId(recipientArea.code);
        contextModel.trackingId = trackingId;
      }
    }

    // If OTP Number is not set yet, set it to a random integer of 6 digits.
    if (!contextModel.otpNumber) {
      contextModel.otpNumber = Math.floor(100000 + Math.random() * 899999);
    }

    // Grant Warehouse Shipments are not auto-assignable.
    if (contextModel.typeId === SHIPMENT_TYPE.WAREHOUSE.id) {
      contextModel.shouldNotBeAutoAssigned = true;
    }
  });

  ShipmentModel.observe('after save', async (ctx) => {
    /** @type {ShipmentModel} */
    const contextModel = ctx.instance || ctx.data;

    // Check if its status has changed. If so, we create a
    // Shipment Event, to record the status change.
    if (contextModel.statusId) {
      const lastShipmentChange = await app.models.ShipmentEvent.findLastChangeOfShipment(
        contextModel.id,
      );

      const shipmentHasChangedToANewStatus = !lastShipmentChange
        || lastShipmentChange.statusId !== contextModel.statusId;

      if (shipmentHasChangedToANewStatus) {
        /** @type {app.models.ShipmentEvent} */
        const shipmentEvent = await app.models.ShipmentEvent.create({
          shipmentId: contextModel.id,
          statusId: contextModel.statusId,
        });

        if (shipmentEvent) {
          postUpdateToPartnerWebhookUrl(
            shipmentEvent.shipmentId,
            shipmentEvent.statusId,
          );
        }
      }
    }

    // When creating the shipment, we send the recipient a SMS to inform him.
    if (ctx.isNewInstance && contextModel.recipientPhone) {
      const orderId = contextModel.id;

      const encodedOrderId = HashIdsWrapper.encodeSingleId(orderId);

      const url = `https://location.jakapp.co/#/${encodedOrderId}/${orderId}`;

      const onGetShortUrlSuccessOrFailure = (urlForSmsMessage) => {
        const urlWithoutProtocol = removeProtocolFromUrl(urlForSmsMessage);
        const message = `لديك شحنة من جاك! الرجاء تحديد موقعك في أسرع وقت: ${urlWithoutProtocol}`;

        UnifonicApi.sendSms(contextModel.recipientPhone, message)
          .then(unifonicApiResponse => debug(unifonicApiResponse.data))
          .catch((unifonicApiError) => {
            debug(
              unifonicApiError.response
                ? unifonicApiError.response.data
                : `Unifonic API Error: ${unifonicApiError.message}`,
            );
          });
      };

      UrlShortenerApi.getShortUrl(url)
        .then(urlShortenerResponse => onGetShortUrlSuccessOrFailure(urlShortenerResponse.data.shorturl))
        .catch(() => {
          onGetShortUrlSuccessOrFailure(url);
        });
    }

    // If shipment has a routeId but doesn't have 'routeDeliveryOrder' property filled yet...
    if (contextModel.routeId && !contextModel.routeDeliveryOrder) {
      RouteCreator.updateOrderOfShipmentsInRoute(contextModel.routeId);
    }

    const shipmentHasBeenDelivered = contextModel.statusId === SHIPMENT_STATUS.DELIVERED.id;

    // Check if this shipment has been delivered and if is in a route.
    if (shipmentHasBeenDelivered && contextModel.routeId) {
      // If so, check if all other shipments from this route have status Delivered.
      await app.models.Route.markAsCompletedIfAllShipmentsHaveBeenDelivered(
        contextModel.routeId,
      );
    }

    // When the shipment is marked as Delivered, we create a new cash instance for the ShippingFee
    // that is owed to Jak by the partner. Also, if there is any CoD for this shipment, we create
    // a new CoD Partner Cash Instance (jak owns the partner).
    if (shipmentHasBeenDelivered) {
      const {
        id: shipmentId,
        partnerId,
        cashToCollectOnDelivery,
      } = contextModel;

      const feeTypeId = PARTNER_CASH_TYPE.FEE.id;
      const codTypeId = PARTNER_CASH_TYPE.COD.id;
      const inboundDirectionId = PARTNER_CASH_DIRECTION.INBOUND.id;
      const outboundDirectionId = PARTNER_CASH_DIRECTION.OUTBOUND.id;

      if (partnerId) {
        /** @type {import('./partner').Partner} */
        const partner = await app.models.Partner.findById(partnerId);

        let { shippingFee } = partner;

        if (shippingFee) shippingFee = parseFloat(shippingFee);

        const foundFeesForThisShipment = await app.models.PartnerCashInstance.count(
          {
            shipmentId,
            typeId: feeTypeId,
            directionId: inboundDirectionId,
          },
        );

        if (!foundFeesForThisShipment && shippingFee) {
          await app.models.PartnerCashInstance.create({
            shipmentId,
            typeId: feeTypeId,
            amount: shippingFee,
            dueDatetime: new Date(),
            directionId: inboundDirectionId,
            partnerId,
          });
        }

        if (cashToCollectOnDelivery) {
          const foundCodForThisShipment = await app.models.PartnerCashInstance.count(
            {
              shipmentId,
              typeId: codTypeId,
              directionId: outboundDirectionId,
            },
          );

          if (!foundCodForThisShipment) {
            await app.models.PartnerCashInstance.create({
              shipmentId,
              typeId: codTypeId,
              amount: cashToCollectOnDelivery,
              dueDatetime: new Date(),
              directionId: outboundDirectionId,
              partnerId,
            });
          }
        }
      }
    }

    const numberOfShipmentsWithTheSameTrackingId = await ShipmentModel.count({
      trackingId: contextModel.trackingId,
    });

    // If there are more the one shipment with the same trackingId, we simply set the trackingId
    // to 'null', so on shipment 'before save' hook a new trackingId will be generated.
    if (numberOfShipmentsWithTheSameTrackingId > 1) {
      contextModel.trackingId = null;
      contextModel.save();
    }
  });

  /**
   * Mark the shipment as scanned before pickup and add the shipment
   * to the route from (identified by the routeId parameter).
   *
   * @param {number} id
   * @param {number} routeId
   *
   * @returns {Promise<import('./shipment').Shipment>}
   */
  ShipmentModel.addToDirectedBulkRoute = async (id, routeId) => {
    /** @type {ShipmentModel} */
    const shipment = await ShipmentModel.findById(id);

    if (!shipment) {
      const error = new Error();
      error.status = STATUS.NOT_FOUND;
      error.name = 'SHIPMENT_NOT_FOUND';
      error.message = 'No shipments were found with this ID.';
      throw error;
    }

    const routeExists = await app.models.Route.exists(routeId);

    if (!routeExists) return shipment;

    shipment.wasScannedBeforePickup = true;
    shipment.routeId = routeId;
    return shipment.save();
  };

  /**
   * Mark the shipment as not scanned before pickup and remove the
   * shipment from the route (identified by the routeId parameter).
   *
   * @param {number} id
   * @param {number} routeId
   *
   * @returns {Promise<import('./shipment').Shipment>}
   */
  ShipmentModel.removeFromDirectedBulkRoute = async (id, routeId) => {
    /** @type {ShipmentModel} */
    const shipment = await ShipmentModel.findById(id);

    if (!shipment) {
      const error = new Error();
      error.status = STATUS.NOT_FOUND;
      error.name = 'SHIPMENT_NOT_FOUND';
      error.message = 'No shipments were found with this ID.';
      throw error;
    }

    if (shipment.routeId !== routeId) return shipment;

    shipment.wasScannedBeforePickup = false;
    shipment.routeId = null;

    return shipment.save();
  };
};
