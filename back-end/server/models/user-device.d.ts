import { PersistedModel } from 'loopback';

declare class UserDevice extends PersistedModel {
  id: number;
  registrationToken: string;
  hasNotificationsEnabled: boolean;
  createdAt: Date;
  updatedAt: Date;

  userId: number;
  user: { get(): Promise<import('./user').User>; }

  /**
   * Sends a push message.
   * @param userId - The User ID.
   * @param body - The message body.
   * @param data - Optional data linked to this push message.
   */
  static sendPushNotificationToUser(userId: number, body: string, data?: object): void;

  /**
   * Sends a push message.
   * @param agentId - The Agent ID.
   * @param body - The message body.
   * @param data - Optional data linked to this push message.
   */
  static sendPushNotificationToAgent(agentId: number, body: string, data?: object): void;

  /**
   * Sends a push message.
   * @param registrationToken - The Device Registration Token.
   * @param body - The message body.
   * @param data - Optional data linked to this push message.
   */
  static sendPushNotificationToDevice(registrationToken: string, body: string, data?: object): void;
}
