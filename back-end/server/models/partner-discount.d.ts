import { PersistedModel } from 'loopback';

declare class PartnerDiscount extends PersistedModel {
  id: number;
  code: string;
  isActive: boolean;
  startDatetime: Date;
  endDatetime: Date;
  count: number;
  rate: number;
  partnerId: number;
  feeTypeId: number;
  createdAt: Date;
  updatedAt: Date;

  partner(): import('./partner').Partner;
  feeType(): import('./partner-fee-type').PartnerFeeType;
}
