import { PersistedModel } from 'loopback';

declare class PartnerFeeType extends PersistedModel {
  id: number;
  name: string;

  discounts(): import('./partner-discount').PartnerDiscount[];
  partner(): import('./partner').Partner;
}
