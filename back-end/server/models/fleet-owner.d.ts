import { PersistedModel } from 'loopback';

declare class FleetOwner extends PersistedModel {
  id: number;
  name: string;
  city: any;
  country: any;
  createdAt: Date;
  updatedAt: Date;

  agents(): import('./agent').Agent[];
}
