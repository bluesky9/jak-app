import { PersistedModel } from 'loopback';
import { Agent } from './agent';

/**
 * Represents a message sent to an agent.
 */
declare class Notification extends PersistedModel {
  id: number;
  title: string;
  message: string;
  hasBeenRead: boolean;
  createdAt: Date;
  updatedAt: Date;

  agentId: number;
  agent(): Agent;

  static markAsRead(id: number): Promise<Notification>;
  static notifyAgentOfDirectedRouteAssigned(agentId: number): Promise<Notification>;
}
