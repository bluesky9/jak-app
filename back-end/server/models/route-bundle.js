/**
 * Represents a route bundle. Currently, route bundles can be either 'Generic', 'Mix' or 'Bulk'.
 *
 * Generic: A route with no pickup location, but with one or more drop off locations.
 *
 * Mix: A route with multiple pickup and and multiple drop-off locations. A single pickup and a
 * single drop-off is considered a mix-route as well.
 *
 * Bulk: A route with one pickup location and multiple drop off locations.
 */
module.exports = (/* RouteBundle */) => {};
