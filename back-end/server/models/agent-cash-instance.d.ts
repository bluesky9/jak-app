import { PersistedModel } from 'loopback';
import { Agent } from './agent'
import { AgentType } from './agent-type'

declare class AgentCashInstance extends PersistedModel {
  id: number;
  amount: number;
  dueDatetime: Date;
  isSettled: boolean;
  settledAt: Date;
  agentId: number;
  typeId: number;
  createdAt: Date;
  updatedAt: Date;

  agent(): Agent;
  type(): AgentType;
}
