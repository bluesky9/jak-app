import { PersistedModel } from 'loopback';

declare class PartnerCashInstance extends PersistedModel {
  id: number;
  amount: number;
  dueDatetime: Date;
  isSettled: boolean;
  settledAt: Date;

  shipment(): import('./shipment').Shipment;
  shipmentId: number;

  direction(): import('./partner-cash-direction').PartnerCashDirection;
  directionId: number;

  type(): import('./partner-cash-type').PartnerCashType;
  typeId: number;

  partner(): import('./partner').Partner;
  partnerId: number;

  static report(): Promise<Object>;
}
