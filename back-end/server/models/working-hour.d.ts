import { PersistedModel } from 'loopback';

declare class WorkingHour extends PersistedModel {
  id: number;
  name: string;

  agents(): import('./agent').Agent[];
}
