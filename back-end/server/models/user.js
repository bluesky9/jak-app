const STATUS = require('http-status');
const app = require('../server');

/**
 * Represents a user of the application.
 *
 * @param {typeof app.models.User} Model - The User Model.
 */
module.exports = (Model) => {
  const UserModel = Model;

  // After saving an user, verify changes in user role to update mapping.
  UserModel.observe('after save', async (ctx) => {
    const contextModel = ctx.instance || ctx.data;

    if (
      typeof contextModel.role !== 'undefined'
      && ctx.options.authorizedRoles.admin
    ) {
      const { RoleMapping } = app.models;
      if (contextModel.role === '') {
        RoleMapping.destroyAll({
          principalType: RoleMapping.USER,
          principalId: contextModel.id,
        });
      } else {
        const { Role } = app.models;
        const role = await Role.findOne({
          where: {
            name: contextModel.role,
          },
        });

        if (!role) {
          const error = new Error();
          error.status = STATUS.NOT_FOUND;
          error.name = 'ROLE_NOT_FOUND';
          error.message = 'No role were found with this name.';
          throw error;
        }

        RoleMapping.findOrCreate({
          principalType: RoleMapping.USER,
          principalId: contextModel.id,
          roleId: role.id,
        });
        RoleMapping.destroyAll({
          principalType: RoleMapping.USER,
          principalId: contextModel.id,
          roleId: { neq: role.id },
        });
      }
    }
  });
};
