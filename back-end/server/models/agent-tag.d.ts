import { PersistedModel } from 'loopback';

declare class AgentTag extends PersistedModel {
  id: number;
  name: string;

  agents(): import('./agent').Agent[];
}
