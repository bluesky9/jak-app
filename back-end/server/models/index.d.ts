declare class Models {
  static AccessToken: typeof import('loopback').AccessToken;
  static ACL: typeof import('loopback').ACL;
  static RoleMapping: typeof import('loopback').RoleMapping;
  static Role: typeof import('loopback').Role;
  static Area: import('./area').Model;
  static Agent: import('./agent').Model;
  static AgentStatus: import('./agent-status').Model;
  static WorkingDay: import('./working-day').Model;
  static WorkingHour: import('./working-hour').Model;
  static Shipment: import('./shipment').Model;
  static ShipmentStatus: import('./shipment-status').Model;
  static ShipmentEvent: import('./shipment-event').Model;
  static Route: import('./route').Model;
  static PartnerCashType: import('./partner-cash-type').Model;
  static PartnerCashDirection: import('./partner-cash-direction').Model;
  static PartnerFeeType: import('./partner-fee-type').Model;
  static Partner: import('./partner').Model;
  static PartnerDiscount: import('./partner-discount').Model;
  static PartnerCashInstance: import('./partner-cash-instance').Model;
  static AgentCashType: import('./agent-cash-type').Model;
  static AgentCashInstance: import('./agent-cash-instance').Model;
  static User: import('./user').Model;
  static ShipmentType: import('./shipment-type').Model;
  static FleetOwner: import('./fleet-owner').Model;
  static AgentType: import('./agent-type').Model;
  static AgentTag: import('./agent-tag').Model;
  static RouteBundle: import('./route-bundle').Model;
  static RouteStatus: import('./route-status').Model;
  static Notification: import('./notification').Model;
  static Setting: import('./setting').Model;
}

export = Models;
