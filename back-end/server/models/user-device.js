const { Expo } = require('expo-server-sdk');
const debug = require('debug')('app:user-device');
const { forEach } = require('p-iteration');
const app = require('../server');

/** @param {typeof import('./user-device').UserDevice} Model */
module.exports = (Model) => {
  const UserDeviceModel = Model;

  UserDeviceModel.chunkAndPushNotifications = async (notifications) => {
    if (!notifications || !notifications.length) return;

    const expo = new Expo();

    // The Expo push notification service accepts batches of notifications so
    // that you don't need to send 1000 requests to send 1000 notifications. We
    // recommend you batch your notifications to reduce the number of requests
    // and to compress them (notifications with similar content will get
    // compressed).
    const chunks = expo.chunkPushNotifications(notifications);
    const tickets = [];

    // Send the chunks to the Expo push notification service. There are
    // different strategies you could use. A simple one is to send one
    //  chunk at a time, which nicely spreads the load out over time:
    forEach(chunks, async (chunk) => {
      try {
        const ticketChunk = await expo.sendPushNotificationsAsync(chunk);
        debug(ticketChunk);
        tickets.push(...ticketChunk);
      } catch (error) {
        // NOTE: If a ticket contains an error code in ticket.details.error, you
        // must handle it appropriately. The error codes are listed in the Expo
        // documentation:
        // https://docs.expo.io/versions/latest/guides/push-notifications#response-format
        debug(error);
      }
    });
  };

  UserDeviceModel.sendPushNotificationToUser = async (
    userId,
    body,
    data = null,
  ) => {
    /** @type {import('./user-device').UserDevice[]}  */
    const userDevices = await UserDeviceModel.find({
      where: {
        userId,
        hasNotificationsEnabled: true,
      },
    });

    if (!userDevices.length) return;

    const notifications = [];

    userDevices.forEach((userDevice) => {
      if (Expo.isExpoPushToken(userDevice.registrationToken)) {
        notifications.push({
          to: userDevice.registrationToken,
          body,
          data,
        });
      }
    });

    await UserDeviceModel.chunkAndPushNotifications(notifications);
  };

  UserDeviceModel.sendPushNotificationToAgent = async (
    agentId,
    body,
    data = null,
  ) => {
    /** @type {import('./user').User} */
    const user = await app.models.User.findOne({
      where: {
        agentId,
      },
    });

    if (!user) return;

    await UserDeviceModel.sendPushNotificationToUser(user.id, body, data);
  };

  UserDeviceModel.sendPushNotificationToDevice = async (
    registrationToken,
    body,
    data = null,
  ) => {
    const notifications = [];

    if (Expo.isExpoPushToken(registrationToken)) {
      notifications.push({
        to: registrationToken,
        body,
        data,
      });
    }

    await UserDeviceModel.chunkAndPushNotifications(notifications);
  };

  UserDeviceModel.register = async (registrationToken, userId) => {
    /** @type {UserDeviceModel} */
    const registeredDevice = await UserDeviceModel.findOne({
      where: {
        registrationToken,
        userId,
      },
    });

    if (registeredDevice) {
      registeredDevice.updatedAt = new Date();
      return registeredDevice.save();
    }

    return UserDeviceModel.create({
      registrationToken,
      userId,
    });
  };
};
