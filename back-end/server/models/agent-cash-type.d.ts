import { PersistedModel } from 'loopback';
import { AgentCashInstance } from './agent-cash-instance'

declare class AgentCashType extends PersistedModel {
  id: number;
  name: string;
  isProfitForCompany: boolean;

  cashInstances(): AgentCashInstance[];
}
