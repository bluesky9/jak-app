import { PersistedModel } from 'loopback';

declare class Agent extends PersistedModel {
  id: number;
  name: string;
  phone: string;
  currentGeoPoint: { lat: number, lng: number };
  friendlyId: string;
  agreedMinimumWorkingHours: number;
  agreedMinimumShipments: number;
  agreedMinimumPayment: number;
  agreedCommissionPerShipment: number;
  createdAt: Date;
  updatedAt: Date;
  statusId: number;
  fleetOwnerId: number;
  typeId: number;

  areas(): import('./area').Area[];
  status(): import('./agent-status').AgentStatus[];
  workingDays(): import('./working-day').WorkingDay[];
  workingHours(): import('./working-hour').WorkingHour[];
  routes(): import('./route').Route[];
  cashInstances(): import('./agent-cash-instance').AgentCashInstance[];
  fleetOwner(): import('./fleet-owner').FleetOwner;
  type(): import('./agent-type').AgentType;
  tags(): import('./agent-tag').AgentTag[];
  user(): import('./user').User;
  notifications(): import('./notification').Notification[];

  static fetchAllWithStatusAvailable(): Promise<Agent[]>;
  static totalEarnings(id: number): Promise<number>;
  static cashAtHand(id: number): Promise<number>;
  static totalTransactions(id: number): Promise<number>;
  static acceptRouteOffer(agentId: number, routeId: number): Promise<import('./route').Route>;
  static generateFriendlyId(agentName: string): string;
  static assignedRoute(id: number): Promise<import('./route').Route>;
}
