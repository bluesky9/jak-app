import { PersistedModel } from 'loopback';

declare class RouteStatus extends PersistedModel {
  id: number;
  name: string;
}
