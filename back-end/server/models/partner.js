const STATUS = require('http-status');
const app = require('../server');

const ETERNAL_ACCESS_TOKEN_TTL = -1;

/**
 * Represents a company or person that makes use of our services.
 *
 * @param {typeof app.models.Partner} Model
 */
module.exports = (Model) => {
  const PartnerModel = Model;

  /**
   * @param {number} partnerId
   * @returns {typeof app.models.User}
   */
  const findUserFromPartnerId = async (partnerId) => {
    const { User } = app.models;

    /** @type {User} */
    const user = await User.findOne({ where: { partnerId } });

    if (!user) {
      const error = new Error();
      error.status = STATUS.NOT_FOUND;
      error.name = 'PARTNER_USER_NOT_FOUND';
      error.message = 'No user found with this partner ID.';
      throw error;
    }

    return user;
  };

  PartnerModel.getEternalAccessToken = async (id) => {
    const { AccessToken } = app.models;

    /** @type {User} */
    let user;

    try {
      user = await findUserFromPartnerId(id);
    } catch (error) {
      throw error;
    }

    /** @type {AccessToken} */
    let accessToken = await AccessToken.findOne({
      where: {
        userId: user.id,
        ttl: ETERNAL_ACCESS_TOKEN_TTL,
      },
    });

    if (!accessToken) accessToken = await user.createAccessToken(ETERNAL_ACCESS_TOKEN_TTL);

    return accessToken.id;
  };

  PartnerModel.renewEternalAccessToken = async (id) => {
    const { AccessToken } = app.models;

    /** @type {User} */
    let user;

    try {
      user = await findUserFromPartnerId(id);
    } catch (error) {
      throw error;
    }

    await AccessToken.destroyAll({
      userId: user.id,
      ttl: ETERNAL_ACCESS_TOKEN_TTL,
    });

    /** @type {AccessToken} */
    const accessToken = await user.createAccessToken(ETERNAL_ACCESS_TOKEN_TTL);

    return accessToken.id;
  };
};
