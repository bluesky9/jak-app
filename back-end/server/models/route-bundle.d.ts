import { PersistedModel } from 'loopback';

declare class RouteBundle extends PersistedModel {
  id: number;
  name: string;
}
