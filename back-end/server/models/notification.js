const STATUS = require('http-status');
const app = require('../server');

/**
 * Represents a message sent to an agent.
 *
 * @param {typeof import('./notification').Notification} Model
 */
module.exports = (Model) => {
  const NotificationModel = Model;

  NotificationModel.markAsRead = async (id) => {
    /** @type {NotificationModel} */
    const notification = await NotificationModel.findById(id);

    if (!notification) {
      const error = new Error();
      error.status = STATUS.NOT_FOUND;
      error.name = 'NOTIFICATION_NOT_FOUND';
      error.message = 'No notification was found with this ID.';
      throw error;
    }

    notification.hasBeenRead = true;

    return notification.save();
  };

  NotificationModel.notifyAgentOfDirectedRouteAssigned = async (agentId) => {
    const message = "You've been assigned to a new Directed Bulk Route! Get more info about where to pick up the shipments with Jak's Operations Manager!";

    /** @type {NotificationModel} */
    const notificationToCreate = {
      title: 'New Directed Bulk Route',
      message,
      agentId,
    };

    /** @type {NotificationModel} */
    const notification = await NotificationModel.create(notificationToCreate);

    if (notification) {
      app.models.UserDevice.sendPushNotificationToAgent(agentId, message, {
        id: 'DIRECTED_BULK_ROUTE_ASSIGNED',
      });
    }

    return notification;
  };
};
