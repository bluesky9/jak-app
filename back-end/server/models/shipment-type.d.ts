import { PersistedModel } from 'loopback';

declare class ShipmentType extends PersistedModel {
  id: number;
  name: string;
}
