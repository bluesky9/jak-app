import { PersistedModel } from 'loopback';

declare class ShipmentStatus extends PersistedModel {
  id: number;
  name: string;
}
