import { PersistedModel } from 'loopback';

declare class WorkingDay extends PersistedModel {
  id: number;
  name: string;

  agents(): import('./agent').Agent[];
}
