import { PersistedModel } from 'loopback';

declare class Route extends PersistedModel {
  id: number;
  startDatetime: Date;
  endDatetime: Date;
  value: number;
  agentId: number;
  bundleId: number;
  statusId: number;
  agentCashInstanceId: number;
  totalCoD: number;
  totalShipments: number;
  isOffered: boolean;
  createdAt: Date;
  updatedAt: Date;

  agent(): import('./agent').Agent;
  shipments(): import('./shipment').Shipment[];
  status(): import('./route-status').RouteStatus;
  bundle(): import('./route-bundle').RouteBundle;
  requiredAgentTags(): import('./agent-tag').AgentTag[];
  agentCashInstance(): import('./agent-cash-instance').AgentCashInstance;

  static autoSetValueIfNeeded(): void;
  static autoAssign(): void;
  static journeyInstructions(): void;
  static directionsForGoogleMaps(): void;
  static fetchAllWithStatusCreated(): Promise<Route[]>;
  static orderedWaypoints(): Promise<Object[]>;
  static computeTotalCoD(route: Route): Promise<number>;
  static computeTotalShipments(route: Route): Promise<number>;
  static markAsCompletedIfAllShipmentsHaveBeenDelivered(id: number): Promise<Route>;
  static markAsStarted(id: number): Promise<Route>;
}
