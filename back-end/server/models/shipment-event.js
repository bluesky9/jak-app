/**
 * Represents a change in a shipment status and may contain notes explaining the reason.
 *
 * @param {typeof import('./shipment-event').ShipmentEvent} Model
 */
module.exports = (Model) => {
  const ShipmentEventModel = Model;

  ShipmentEventModel.findLastChangeOfShipment = async (shipmentId) => {
    const lastChangeOfShipment = await ShipmentEventModel.findOne({
      where: {
        shipmentId,
      },
      order: 'createdAt DESC',
    });

    return lastChangeOfShipment;
  };
};
