import { PersistedModel } from 'loopback';

declare class AgentStatus extends PersistedModel {
  id: number;
  name: string;

  agents(): import('./agent').Agent[];
}
