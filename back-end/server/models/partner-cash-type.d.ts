import { PersistedModel } from 'loopback';

declare class PartnerCashType extends PersistedModel {
  id: number;
  name: string;

  cashInstances(): import('./partner-cash-instance').PartnerCashInstance[];
}
