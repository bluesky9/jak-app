import { PersistedModel } from 'loopback';

declare class Partner extends PersistedModel {
  id: number;
  name: string;
  website: string;
  phone: string;
  isActive: boolean;
  maxShipmentPartsAllowed: number;
  maxShipmentPartWeight: number;
  codIsCollect: boolean;
  codFee: number;
  maxCodAmount: number;
  extraPartFee: number;
  extraWeightFee: number;
  reschedulingFee: number;
  returnsFee: number;
  readdressingFee: number;
  shippingFee: number;
  webhookUrl: string;
  codFeeTypeId: number;
  createdAt: Date;
  updatedAt: Date;

  cashInstances(): import('./partner-cash-instance').PartnerCashInstance[];
  discounts(): import('./partner-discount').PartnerDiscount[];
  codFeeType(): import('./partner-fee-type').PartnerFeeType;
  shipments(): import('./shipment').Shipment[];
  users(): import('./user').User[];


  static getEternalAccessToken(id: number): Promise<string>;
  static renewEternalAccessToken(id: number): Promise<string>;
}
