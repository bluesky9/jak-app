module.exports = {
  /**
   * PM2 Application configuration section
   * http://pm2.keymetrics.io/docs/usage/application-declaration/
   */
  apps: [
    {
      name: 'gitlab-webhook-server',
      script: 'server.js',
    },
  ],
};
