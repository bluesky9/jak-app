This is a git webhook callback server to fetch new code.

To start the webhook server, simply run:

```
npm start
```

Please, check the documentation at <https://www.npmjs.com/package/git-webhook-ci>.
